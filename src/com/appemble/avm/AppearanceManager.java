/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.TextUtils;
import android.util.StateSet;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.appemble.avm.Utils.ReflectionBitmapProcessor;
import com.appemble.avm.Utils.RoundedBitmapProcessor;
import com.appemble.avm.Utils.RoundedBorderBitmapProcessor;
import com.appemble.avm.models.AppearanceModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.dao.AppearanceDao;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.ImageDownloader.Scheme;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;

public class AppearanceManager {
	private static AppearanceManager _appearanceManager;
	private HashMap<String, HashMap<Integer, AppearanceModel>> mDbMap; // This map stores the dbname-> appearancemap and within it AppeaenceId to a Appearance Object
	private HashMap<String, Typeface> mTypefaceMap;
	private float fCurrentScreenHeight, fCurrentScreenWidth;
	final int[] iStates = new int[] { 
			-1, 
			-android.R.attr.state_enabled, android.R.attr.state_enabled,
			-android.R.attr.state_focused, android.R.attr.state_focused, 
			-android.R.attr.state_pressed, android.R.attr.state_pressed, 
			-android.R.attr.state_checked, -android.R.attr.state_checked, 
			0};
	
	class BitmapState {
		public Bitmap bitmap;
		public int state;
		public BitmapState(Bitmap b, int state) {
			this.bitmap = b; this.state = state;
		}
	}
	
	List<int[]> lStates;
	
	public static synchronized AppearanceManager getInstance() {
		if (_appearanceManager == null) {
			_appearanceManager = new AppearanceManager();
			// TODO Implement persistence using a flat file as AppearanceManager works across DB's
		}
		return _appearanceManager;
	}

	private AppearanceManager() {
		mDbMap = new HashMap<String, HashMap<Integer, AppearanceModel>>();
		mTypefaceMap = new HashMap<String, Typeface>();
		lStates = Arrays.asList(iStates);
	}
	
	
	public boolean updateTextAppearance(TextView textView, ControlModel controlModel, int appearanceId) {
		AppearanceModel appearanceModel =null;
		if (0.0f == Cache.fScaleDPToPixels)
			Cache.fScaleDPToPixels = textView.getContext().getResources().getDisplayMetrics().density;
		
		// Use appearanceId from the controlModel.
		appearanceModel = appearanceId == 0 ? getAppearance(controlModel.sSystemDbName, controlModel.iAppearanceId) : 
			getAppearance(controlModel.sSystemDbName, appearanceId);
		
		if (appearanceModel == null)
			return false;

		// Font family
		Typeface typeface = getTypeface(textView.getContext(), appearanceModel);

		// Bold & Italic
		int bold_italic = Typeface.NORMAL;
		if (appearanceModel.bFontStyleBold && appearanceModel.bFontStyleItalic)
			bold_italic = Typeface.BOLD_ITALIC;

		if (!appearanceModel.bFontStyleBold && appearanceModel.bFontStyleItalic)
			bold_italic = Typeface.ITALIC;

		if (appearanceModel.bFontStyleBold && !appearanceModel.bFontStyleItalic)
			bold_italic = Typeface.BOLD;

		if (!appearanceModel.bFontStyleBold && !appearanceModel.bFontStyleItalic)
			bold_italic = Typeface.NORMAL;

		textView.setTypeface(typeface, bold_italic);

		// Font Size
		textView.setTextSize(Utilities.getDimension(appearanceModel.sFontSize, fCurrentScreenHeight/Cache.fScaleDPToPixels));

		// Font Color (Foreground)
		int color = appearanceModel.iFontColor;
		textView.setTextColor(color);
		
		// Justify
		if (appearanceModel.iJustify != Gravity.NO_GRAVITY)
			textView.setGravity(appearanceModel.iJustify);

		// Shadow
		if (appearanceModel.bFontStyleShadow) // for backward compatibility. TODO remove this.
			textView.setShadowLayer(1, 1, 1, Color.parseColor("Black"));
		else if (appearanceModel.fShadowRadius > 0)
			textView.setShadowLayer(appearanceModel.fShadowRadius, appearanceModel.fShadowHorizontalOffset, 
					appearanceModel.fShadowVerticalOffset, appearanceModel.iShadowColor);
		// Make it single line or 
		if (controlModel.bWordWrap == false)
			textView.setSingleLine(true);
		if (controlModel.bEllipsized == true) {
			textView.setEllipsize(TextUtils.TruncateAt.END);
			String sEllipsizedType = controlModel.mExtendedProperties.get("ellipsized_type");
			if (null == sEllipsizedType);
			else if (sEllipsizedType.equals("MARQUEE")) {
				textView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
				textView.setHorizontallyScrolling(true);
				textView.setSelected(true);
			}
			else if (sEllipsizedType.equals("START"))
				textView.setEllipsize(TextUtils.TruncateAt.START);
			else if (sEllipsizedType.equals("MIDDLE"))
				textView.setEllipsize(TextUtils.TruncateAt.MIDDLE);
		}
		// Set the padding		
		if (appearanceModel.sPaddingLeft != null || appearanceModel.sPaddingTop  != null || 
				appearanceModel.sPaddingRight  != null || appearanceModel.sPaddingBottom  != null) {
			float fPaddingLeft = Utilities.getDimension(appearanceModel.sPaddingLeft, fCurrentScreenHeight/Cache.fScaleDPToPixels);
			float fPaddingTop = Utilities.getDimension(appearanceModel.sPaddingTop, fCurrentScreenHeight/Cache.fScaleDPToPixels);
			float fPaddingRight = Utilities.getDimension(appearanceModel.sPaddingRight, fCurrentScreenHeight/Cache.fScaleDPToPixels);
			float fPaddingBottom = Utilities.getDimension(appearanceModel.sPaddingBottom, fCurrentScreenHeight/Cache.fScaleDPToPixels);
			textView.setPadding((int)fPaddingLeft, (int)fPaddingTop, (int)fPaddingRight, (int)fPaddingBottom);
		}
		return true;
	}

	public Typeface getTypeface(Context context, AppearanceModel appearanceModel) {
		Typeface typeface = Typeface.SANS_SERIF;
		// FontName
		String sFontName = appearanceModel.sFontName;
		Typeface fontTypeface = null;
		if (sFontName != null) {
			fontTypeface = getTypeface(context, sFontName);
		}
		if (fontTypeface != null)
			typeface = fontTypeface;
		else {
			//LogManger.LogError("Font", "Font file: " + fontname
			//		+ " not found!");
			if (appearanceModel.sFontFamily != null) {
				if (appearanceModel.sFontFamily.equalsIgnoreCase("Serif"))
					typeface = Typeface.SERIF;
				else if (appearanceModel.sFontFamily.equalsIgnoreCase("Monospace"))
					typeface = Typeface.MONOSPACE;
				else
					typeface = Typeface.SANS_SERIF;
			}
		}
		if (appearanceModel.bFontStyleItalic)
			return Typeface.create(typeface,Typeface.ITALIC);
		return typeface;
	}
	
	/*	
	public boolean updateTextBackgroundAppearance(View view, ControlModel controlModel) {
		AppearanceModel appearanceModel = getAppearance(controlModel.sSystemDbName, controlModel.iAppearanceId);
		if (appearanceModel == null)
			return false;

		// if a border is present or rounded corners, then it can be achieved via GradientDrawable
		if (appearanceModel.iBorderWidth > 0 || (appearanceModel.fBorderRadii != null 
				&& appearanceModel.fBorderRadii.length > 0)) { 
			view.setBackgroundDrawable(getGradientDrawable(appearanceModel));
			return true;
		} else { // just use the view's background		
			// set transparency
			Drawable background = view.getBackground();
			if (background != null)
				background.setAlpha(appearanceModel.iAlpha);
	
			// Background Color
			int iBackgroundColor = 0;
			if (null != appearanceModel.iBackgroundColors && appearanceModel.iBackgroundColors.length > 0)
				iBackgroundColor = appearanceModel.iBackgroundColors[0];
			int iARGB = convert_RGB_to_ARGB(iBackgroundColor, appearanceModel.iAlpha);
			if (view instanceof Button)
				background.setColorFilter(iBackgroundColor, android.graphics.PorterDuff.Mode.MULTIPLY); // Background Color
			else
				view.setBackgroundColor(iARGB);
		}
		return true;
	}
*/	
	public Drawable updateBackgroundAppearance(View view, ControlModel controlModel, Drawable drawable) {
		return updateBackgroundAppearance(view, controlModel, drawable, null);
	}
	
	public Drawable updateBackground(final View view, String imageUri) {
		// Load image, decode it to Bitmap and display Bitmap in ImageView
		if (imageUri.startsWith(Constants.URL_IMAGE_PREFIX))
			imageUri = "assets://"+imageUri.substring(Constants.URL_IMAGE_PREFIX_LENGTH + 1);
		else if (Scheme.UNKNOWN == Scheme.ofUri(imageUri))
			imageUri = "assets://"+imageUri;
		ImageLoader.getInstance().loadImage(imageUri,
				getDisplayImageOptions(view, null, null), new SimpleImageLoadingListener() {
		    @Override
		    public void onLoadingComplete(String imageUri, View v, Bitmap loadedImage) {
		    	view.setBackgroundDrawable(new BitmapDrawable(loadedImage));
		    }
		});			
		return null;
	}
	
	public Drawable updateBackgroundAppearance(View view, ControlModel controlModel, 
		Drawable drawable, String imageUri) {
		if (null == view || null == controlModel)
			return null; // nothing to update.
		// If the controlModel has its own drawable, just use that.
		if (null != drawable) {
				view.setBackgroundDrawable(drawable);
			return drawable;
		}
		
		AppearanceModel defaultAppearanceModel = getAppearance(controlModel.sSystemDbName, controlModel.iAppearanceId);
		AppearanceModel[] appearanceModels = getAppearances(controlModel.sSystemDbName, controlModel.mExtendedProperties.get("appearances"));
		return updateViewWithGradientBackground(view, defaultAppearanceModel, appearanceModels);
	}
	
	public void updateImage(ImageView view, ControlModel controlModel, Drawable drawable, String imageUri) {
		if (null == view || null == controlModel)
			return; // nothing to update.
		// If the controlModel has its own drawable, just use that.
		if (null != drawable) {
			view.setImageDrawable(drawable);
			return;
		}
		
		AppearanceModel defaultAppearanceModel = getAppearance(controlModel.sSystemDbName, controlModel.iAppearanceId);
		if (controlModel.mExtendedProperties.containsKey("on_enabled_image") || // if there is a image for the state enabled
			controlModel.mExtendedProperties.containsKey("on_pressed_image") || // if there is a image for the state pressed
			controlModel.mExtendedProperties.containsKey("on_focused_image") || // if there is a image for the state focused.
			controlModel.mExtendedProperties.containsKey("on_disabled_image") || // if there is a image for the state disabled.
			controlModel.mExtendedProperties.containsKey("on_checked_image")) { // if there is a image for the state checked.
			Vector<BitmapState> bitmapStates = new Vector<BitmapState>();
			updateViewWithImages(view, controlModel, imageUri, defaultAppearanceModel, iStates[0], bitmapStates);
		} else {
			updateImageView(view, controlModel, imageUri, defaultAppearanceModel);
		}
	}
	
//	public Drawable updateViewWithGradientBackground(final View view, final ControlModel controlModel) {
	public Drawable updateViewWithGradientBackground(final View view, 
			final AppearanceModel defaultAppearanceModel, AppearanceModel[] appearanceModels) {
		if (null == view || null == defaultAppearanceModel)
			return null;
		if (defaultAppearanceModel.sPaddingLeft != null || defaultAppearanceModel.sPaddingTop  != null || 
				defaultAppearanceModel.sPaddingRight  != null || defaultAppearanceModel.sPaddingBottom  != null) {
			float fPaddingLeft = Utilities.getDimension(defaultAppearanceModel.sPaddingLeft, fCurrentScreenHeight/Cache.fScaleDPToPixels);
			float fPaddingTop = Utilities.getDimension(defaultAppearanceModel.sPaddingTop, fCurrentScreenHeight/Cache.fScaleDPToPixels);
			float fPaddingRight = Utilities.getDimension(defaultAppearanceModel.sPaddingRight, fCurrentScreenHeight/Cache.fScaleDPToPixels);
			float fPaddingBottom = Utilities.getDimension(defaultAppearanceModel.sPaddingBottom, fCurrentScreenHeight/Cache.fScaleDPToPixels);
			view.setPadding((int)fPaddingLeft, (int)fPaddingTop, (int)fPaddingRight, (int)fPaddingBottom);
		}
		
/*
		String sEnabledAppearance = controlModel.mExtendedProperties.get("on_enabled_appearance_name");
		String sPressedAppearance = controlModel.mExtendedProperties.get("on_pressed_appearance_name");
		String sFocusedAppearance = controlModel.mExtendedProperties.get("on_focused_appearance_name");
		String sCheckedAppearance = controlModel.mExtendedProperties.get("on_checked_appearance_name");
		String sDisabledAppearance = controlModel.mExtendedProperties.get("on_disabled_appearance_name");
*/		
		if ((null != defaultAppearanceModel && null != defaultAppearanceModel.iBackgroundColors && defaultAppearanceModel.iBackgroundColors.length > 1) || // if there are two or more backgorund colors for a gradient. 
			(null != defaultAppearanceModel && defaultAppearanceModel.iBorderWidth > 0) || // if there is a border
			(null != defaultAppearanceModel && defaultAppearanceModel.fBorderRadii != null && defaultAppearanceModel.fBorderRadii.length > 0) || // if there are rounded corners 
//			(null != sEnabledAppearance && sEnabledAppearance.length() > 0) || // if there is a appearance for the state enabled.
//			(null != sPressedAppearance && sPressedAppearance.length() > 0) || // if there is a appearance for the state pressed.
//			(null != sFocusedAppearance && sFocusedAppearance.length() > 0) || // if there is a appearance for the state focused.
//			(null != sCheckedAppearance && sCheckedAppearance.length() > 0) || // if there is a appearance for the state checked.
//			(null != sDisabledAppearance && sDisabledAppearance.length() > 0)) { // if there is a appearance for the state disabled.
			(defaultAppearanceModel.states != null && (defaultAppearanceModel.states.length > 1 || 
			(defaultAppearanceModel.states.length == 1 && defaultAppearanceModel.states[0].length > 1))) ||
			(appearanceModels != null && appearanceModels.length > 0)) { // if there are more than 1 states
			// than create a StateListDrawable
			StateListDrawable states = new StateListDrawable();
			Drawable d = null;
			if (appearanceModels != null && appearanceModels.length > 0)
				for (AppearanceModel appearanceModel : appearanceModels) {
					d = getDrawable(appearanceModel);
					for (int i = 0; null != d && null != appearanceModel.states && i < appearanceModel.states.length; i++)
						states.addState(appearanceModel.states[i], d);
				}

			d = getDrawable(defaultAppearanceModel);
			for (int i = 0; null != d && null != defaultAppearanceModel.states && 
				i < defaultAppearanceModel.states.length; i++)
				states.addState(defaultAppearanceModel.states[i], d);
//			if (null != sDisabledAppearance) {
//				d = getGradientDrawable(getAppearance(controlModel.sSystemDbName, sDisabledAppearance));
//				if (null != d)
//					states.addState(new int[] { -android.R.attr.state_enabled }, d);
//			}
//			
//			if (null != sEnabledAppearance) {
//				d = getGradientDrawable(getAppearance(controlModel.sSystemDbName, sEnabledAppearance));
//				if (null != d)
//					states.addState(new int[] { android.R.attr.state_enabled }, d);
//			}
//			
//			if (null != sFocusedAppearance) {
//				d = getGradientDrawable(getAppearance(controlModel.sSystemDbName, sFocusedAppearance));
//				if (null != d)
//					states.addState(new int[] { android.R.attr.state_focused }, d);			
//			}
//			
//			if (null != sPressedAppearance) {
//				d = getGradientDrawable(getAppearance(controlModel.sSystemDbName, sPressedAppearance));
//				if (null != d)
//					states.addState(new int[] { android.R.attr.state_pressed }, d);
//			}
//			
//			if (null != sCheckedAppearance) {
//				d = getGradientDrawable(getAppearance(controlModel.sSystemDbName, sCheckedAppearance));
//				if (null != d)
//					states.addState(new int[] { android.R.attr.state_checked }, d);
//			}
//			d = getGradientDrawable(getAppearance(controlModel.sSystemDbName, sUncheckedAppearance)); 			
//			if (null != d)
//				states.addState(new int[] { -android.R.attr.state_checked }, d);

//			d = getGradientDrawable(defaultAppearanceModel); // add the normal state
//			if (null != d) 
//				states.addState(StateSet.WILD_CARD, d);
			if (view instanceof ImageView) 
				((ImageView)view).setImageDrawable(states);
			else
				view.setBackgroundDrawable(states);
			return states;
		} else if (null != defaultAppearanceModel) { // just use the view's background	// set transparency // Background Color
			int iBackgroundColor = 0;
			if (null != defaultAppearanceModel.iBackgroundColors && defaultAppearanceModel.iBackgroundColors.length > 0)
				iBackgroundColor = defaultAppearanceModel.iBackgroundColors[0];
			if (view instanceof Button || view instanceof EditText || view instanceof ImageView) {
				// set transparency
				Drawable background = view.getBackground();
				if (background != null) {
					background.setAlpha(defaultAppearanceModel.iAlpha);		
					background.setColorFilter(iBackgroundColor, android.graphics.PorterDuff.Mode.MULTIPLY); // Background Color
				}
			}
			else {
				int iARGB = convert_RGB_to_ARGB(iBackgroundColor, defaultAppearanceModel.iAlpha);
				view.setBackgroundColor(iARGB);
			}
		}
		return null;
	}

	public Drawable updateViewWithImages(final View view, final ControlModel controlModel, 
			String imageUri, final AppearanceModel am, final int iState, 
			final Vector<BitmapState> bitmapStates) {
		if (null == view || null == controlModel)
			return null; // nothing to update.
		if (iState == 0) {
			if (bitmapStates.size() == 0) {
				return setBackground(view, controlModel, am, null);
			} else if (bitmapStates.size() == 1)
				return setBackground(view, controlModel, am, bitmapStates.get(0).bitmap);
			else if (bitmapStates.size() > 1) {
				StateListDrawable d = new StateListDrawable();
				for (BitmapState bitmapState : bitmapStates) {
					// TODO All states that have the same image name / appearance must be combined 
					// into one bitmap
					((StateListDrawable)d).addState(bitmapState.state == -1 ? StateSet.WILD_CARD : new int[] { bitmapState.state }, 
						new BitmapDrawable(processBitmap(view, bitmapState.bitmap, controlModel, am)));
				}
				view.setBackgroundDrawable(d);
				return d;
			}
			return null;
		}
		// Load image, decode it to Bitmap and display Bitmap in ImageView
		if (null != imageUri) {
			if (imageUri.startsWith(Constants.URL_IMAGE_PREFIX))
				imageUri = "assets://"+imageUri.substring(Constants.URL_IMAGE_PREFIX_LENGTH + 1);
			else if (Scheme.UNKNOWN == Scheme.ofUri(imageUri))
				imageUri = "assets://"+imageUri;
		}
		ImageLoader.getInstance().loadImage(imageUri,
				getDisplayImageOptions(view, controlModel, am), new SimpleImageLoadingListener() {
		    @Override
		    public void onLoadingComplete(String uri, View v, Bitmap loadedImage) {
		    	bitmapStates.add(new BitmapState(loadedImage, iState));
		    	int iNextState = getNextState(iState);
		    	String sImageUri = getImageUri(controlModel, iNextState);
		    	while (sImageUri == null && iNextState != 0) { // traverse until end of states
		    		sImageUri = getImageUri(controlModel, iNextState = getNextState(iNextState));
		    	}
		    	updateViewWithImages(view, controlModel, sImageUri, am, iNextState, 
		    			bitmapStates);
		    }
		});			
		return null;
	}

	public void updateImageView(final ImageView view, final ControlModel controlModel, 
			String imageUri, final AppearanceModel am) {
		if (null == view || null == controlModel)
			return; // nothing to update.
		
		// Load image, decode it to Bitmap and display Bitmap in ImageView
		if (null != imageUri) {
			if (imageUri.startsWith(Constants.URL_IMAGE_PREFIX))
				imageUri = "assets://"+imageUri.substring(Constants.URL_IMAGE_PREFIX_LENGTH + 1);
			else if (Scheme.UNKNOWN == Scheme.ofUri(imageUri))
				imageUri = "assets://"+imageUri;
		}
		ImageLoader.getInstance().displayImage(imageUri, view,
				getDisplayImageOptions(view, controlModel, am), new SimpleImageLoadingListener() {
		    @Override
		    public void onLoadingComplete(String uri, View v, Bitmap loadedImage) {
		    }
		});
		return;
	}

	public static Bitmap imagePixelstoDP(View view, Bitmap in, ControlModel controlModel) {
		if (false == Utilities.parseBoolean(controlModel.mExtendedProperties.get("scale_up_by_density")) || null == in || 
			null == view || null == controlModel)
			return in;
		return Bitmap.createScaledBitmap(in, (int)(in.getWidth()*Cache.fScaleDPToPixels), 
				(int)(in.getHeight()*Cache.fScaleDPToPixels), false);
	}
	
	public static Bitmap processBitmap(View view, Bitmap bitmap, ControlModel cm, AppearanceModel am) {
		if (null == bitmap)
			return null;
		if (null == am || null == cm)
			return bitmap;
		BitmapProcessor bp = null;
		ScaleType scaleType = Utilities.getScaleType(cm.mExtendedProperties.get("scale_type"));
		if (null != am) {
			float fBorderRadius = null != am.fBorderRadii ? am.fBorderRadii[0] : 0;
			if (am.iBorderWidth > 0)
				bp = new RoundedBorderBitmapProcessor(view.getWidth(), view.getHeight(), 
						scaleType, am.iBorderWidth, fBorderRadius, am.iBorderColor);
			else if (null != am.fBorderRadii && fBorderRadius > 0)
				bp = new RoundedBitmapProcessor(view.getWidth(), view.getHeight(),
						scaleType, am.fBorderRadii[0]);
			bitmap = bp.process(bitmap);
		}
		if (cm.mExtendedProperties.containsKey("reflection_ratio")) {
			try {
				String sReflectionRatio = cm.mExtendedProperties.get("reflection_ratio");
				float fReflectionRatio = 0;
				if (null != sReflectionRatio && sReflectionRatio.length() > 0)
					fReflectionRatio = Float.parseFloat(sReflectionRatio);
				float fReflectionGap = 0;
				String sReflectionGap = cm.mExtendedProperties.get("reflection_gap");
				if (null != sReflectionGap && sReflectionGap.length() > 0)
					fReflectionGap = Float.parseFloat(sReflectionGap);
				bp = new ReflectionBitmapProcessor(fReflectionRatio, fReflectionGap);
				bitmap = bp.process(bitmap);
			} catch (NumberFormatException nfe) {				
			} catch (Exception e) {
			}
		}
		return bitmap;		
	}
	
	private DisplayImageOptions getDisplayImageOptions(final View view, final ControlModel controlModel, 
			AppearanceModel am) {
		BitmapDisplayer bd = null;
//		ArrayList<BitmapDisplayer> al = new ArrayList<BitmapDisplayer>();
		if (null != am) {
			float fBorderRadius = null != am.fBorderRadii ? am.fBorderRadii[0] : 0;
//			if (am.iBorderWidth > 0)
//				bd = new RoundedBorderBitmapDisplayer(am.iBorderWidth, fBorderRadius, am.iBorderColor);
//			else 
			if (fBorderRadius > 0)
				bd = new RoundedBitmapDisplayer((int)fBorderRadius);
//			if (null != bd)
//				al.add(bd);
		} 
//		if (null != controlModel && null != controlModel.mExtendedProperties && 
//				controlModel.mExtendedProperties.containsKey("reflection_ratio")) {
//			try {
//				String sReflectionRatio = controlModel.mExtendedProperties.get("reflection_ratio");
//				float fReflectionRatio = 0;
//				if (null != sReflectionRatio && sReflectionRatio.length() > 0)
//					fReflectionRatio = Float.parseFloat(sReflectionRatio);
//				float fReflectionGap = 0;
//				String sReflectionGap = controlModel.mExtendedProperties.get("reflection_gap");
//				if (null != sReflectionGap && sReflectionGap.length() > 0)
//					fReflectionGap = Float.parseFloat(sReflectionGap);
//				
//				bd = new ReflectionBitmapDisplayer(fReflectionRatio, fReflectionGap);
////				al.add(bd);
//			} catch (NumberFormatException nfe) {				
//			} catch (Exception e) {				
//			}
//		}
		DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder()
//	        .showImageOnLoading(R.drawable.ic_stub) // resource or drawable
	        .showImageForEmptyUri(R.drawable.ic_empty) // resource or drawable
	        .showImageOnFail(R.drawable.ic_error); // resource or drawable
//        .delayBeforeLoading(1000)
//		if (al.size() > 0)
//		builder.displayers(al.toArray(new BitmapDisplayer[0]));
		if (null != view && null != controlModel)
			builder.postProcessor(new BitmapProcessor() {
			    @Override
			    public Bitmap process(Bitmap bmp) {
			        return imagePixelstoDP(view, bmp, controlModel);
			    }
			});
		if (null != bd)
			builder.displayer(bd);
		DisplayImageOptions options = builder.build();
		return options;
	}
	
//	private DisplayImageOptions getLoadImageOptions(View view, ControlModel controlModel, 
//			AppearanceModel am) {
//		DisplayImageOptions.Builder builder = new DisplayImageOptions.Builder()
//	        .showImageOnLoading(R.drawable.ic_stub) // resource or drawable
//	        .showImageForEmptyUri(R.drawable.ic_empty) // resource or drawable
//	        .showImageOnFail(R.drawable.ic_error); // resource or drawable
////        .delayBeforeLoading(1000)
//		DisplayImageOptions options = builder.build();
//		return options;
//	}

	private int getNextState(int iState) {
		int i = 0;
		for (i = 0; i < iStates.length-1; i++)
			if (iStates[i] == iState)
				break;
		if (i == iStates.length-1)
			i--;
		return iStates[++i];
	}
	
	private String getImageUri(ControlModel controlModel, int iState) {
		switch(iState) {
			case -android.R.attr.state_enabled: 
				return controlModel.mExtendedProperties.get("on_disabled_image");
			case android.R.attr.state_enabled: 
				return controlModel.mExtendedProperties.get("on_enabled_image");
			case android.R.attr.state_focused: 
				return controlModel.mExtendedProperties.get("on_focused_image");
			case android.R.attr.state_pressed: 
				return controlModel.mExtendedProperties.get("on_pressed_image");
			case android.R.attr.state_checked: 
				return controlModel.mExtendedProperties.get("on_checked_image");
			case -android.R.attr.state_checked: 
				return controlModel.mExtendedProperties.get("on_unchecked_image");
			default:
				return null;
		}
	}
	
	private Drawable setBackground(View view, ControlModel controlModel, AppearanceModel am, 
			Bitmap bitmap) {
		if (null == view)
			return null;
		bitmap = processBitmap(view, bitmap, controlModel, am);
		if (view instanceof ImageView)
			((ImageView) view).setImageBitmap(bitmap);
		else
			view.setBackgroundDrawable(new BitmapDrawable(bitmap));
		return null; // if the return value is needed then bitmap should be converted to the drawable before returning it. 
	}

	AppearanceModel[] getAppearances(String sSystemDbName, String sAppearanceNames) {
		if (null == sAppearanceNames || null == sSystemDbName)
			return null;
		String[] sAppearances = Utilities.splitCommaSeparatedString(sAppearanceNames);
		if (null == sAppearances || sAppearances.length == 0)
			return null;
		AppearanceModel[] appearances = new AppearanceModel[sAppearances.length];
		for (int i = 0; i < sAppearances.length; i++)
			appearances[i] = getAppearance(sSystemDbName, sAppearances[i]);
		return appearances;
	}
	
	@SuppressLint("UseSparseArrays")
	public AppearanceModel getAppearance(String sDBName, int appearanceId) {
		if (null == sDBName || 0 == appearanceId)
			return null;
		HashMap<Integer, AppearanceModel> appearanceMap = null;
		appearanceMap = mDbMap.get(sDBName);
		if (null == appearanceMap) {
			appearanceMap = new HashMap<Integer, AppearanceModel>();
			mDbMap.put(sDBName, appearanceMap);
		}
		
		Integer iAppearanceId = Integer.valueOf(appearanceId);
		AppearanceModel appearanceModel = appearanceMap.get(iAppearanceId);
		if (null != appearanceModel)
			return appearanceModel;
		
		appearanceModel = AppearanceDao.getAppearance(sDBName,appearanceId);
		if (null != appearanceModel)
			appearanceMap.put(appearanceModel.id, appearanceModel);
		else
			LogManager.logError("Unable to get appearance id: " + appearanceId);
		return appearanceModel;
	}

	@SuppressLint("UseSparseArrays")
	public AppearanceModel getAppearance(String sDBName, String sappearanceName) {
		if (null == sDBName || null == sDBName || null == sappearanceName || sappearanceName.length() == 0)
			return null;
		HashMap<Integer, AppearanceModel> appearanceMap = null;
		appearanceMap = mDbMap.get(sDBName);
		if (null == appearanceMap) {
			appearanceMap = new HashMap<Integer, AppearanceModel>();
			mDbMap.put(sDBName, appearanceMap);
		}
		AppearanceModel appearanceModel = null;
		for (Integer iAppearanceId : appearanceMap.keySet()) {
			appearanceModel = appearanceMap.get(iAppearanceId);
			if (sappearanceName.equals(appearanceModel.sName))
				return appearanceModel;			
		}
		
		appearanceModel = AppearanceDao.getAppearance(sDBName, sappearanceName);
		if (null != appearanceModel)
			appearanceMap.put(appearanceModel.id, appearanceModel);
		else
			LogManager.logError("Unable to get appearance name: " + sappearanceName);
		return appearanceModel;
	}

	public Drawable getDrawable(AppearanceModel appearanceModel) {
		if (null == appearanceModel)
			return null;
		String sImageSource = appearanceModel.mExtendedProperties.get("image");
		return null != sImageSource ? ImageManager.getInstance().getDrawable(null, sImageSource, false, 0) : 
			getGradientDrawable(appearanceModel);			
	}
	
	private GradientDrawable getGradientDrawable(AppearanceModel appearanceModel) {
		if (null == appearanceModel)
			return null;
		GradientDrawable gd = null;
		
		if (null != appearanceModel.iBackgroundColors && appearanceModel.iBackgroundColors.length >= 2)
			gd = new GradientDrawable(
				appearanceModel.mGradientOrientation, appearanceModel.iBackgroundColors);
		else {
			gd = new GradientDrawable();
			int iBackgroundColor = 0;
			if (null != appearanceModel.iBackgroundColors && appearanceModel.iBackgroundColors.length > 0)
				iBackgroundColor = appearanceModel.iBackgroundColors[0];
			int iARGB = AppearanceManager.convert_RGB_to_ARGB(iBackgroundColor, appearanceModel.iAlpha);
			gd.setColor(iARGB);
		}
		gd.setAlpha(appearanceModel.iAlpha);
	    if (appearanceModel.iBorderWidth > 0)
	    	gd.setStroke(appearanceModel.iBorderWidth, appearanceModel.iBorderColor);
    	if (null != appearanceModel.fBorderRadii && appearanceModel.fBorderRadii.length > 0) {
	    	if (appearanceModel.fBorderRadii.length == 8)
	    		gd.setCornerRadii(appearanceModel.fBorderRadii);
	    	else 
	    		gd.setCornerRadius(appearanceModel.fBorderRadii[0]);
    	}
    	gd.setShape(appearanceModel.iGradientShape);
    	gd.setGradientType(appearanceModel.iGradientType);
	    return gd;
	}

	public Typeface getTypeface(Context context, String sFontName) {
		Typeface fontTypeface = mTypefaceMap.get(sFontName);
		if (null != fontTypeface || mTypefaceMap.containsKey(sFontName))
			return fontTypeface;
		try {
			// add all other fonts here
			// TODO: find a way to specify fonts in strings.xml and add them here.
//			if (sFontName.equalsIgnoreCase("arial")) {
//				fontTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/arial.ttf");
//			}
//			else if (sFontName.equalsIgnoreCase("Helvetica")) {
//				fontTypeface = Typeface.createFromAsset(context.getAssets(), "fonts/helvetica.ttf");
//			}
		} catch (Exception ioe) {
			LogManager.logWTF(ioe, "Unable to get font" + sFontName );
		} finally {
			mTypefaceMap.put(sFontName, fontTypeface);
		}
		return fontTypeface;
	}
	
	public void setInitialWindowHeight(int iAllowedLayout, float iWidth, float iHeight) {		
		fCurrentScreenHeight = iHeight;
		fCurrentScreenWidth = iWidth;
		return;
	}

	public float getScreenHeight() {		
		return fCurrentScreenHeight;
	}

	public static String toRgbText(int rgb) {
		// clip input value.
		if (rgb > 0xFFFFFF)
			rgb = 0xFFFFFF;
		if (rgb < 0)
			rgb = 0;

		String str = "000000" + Integer.toHexString(rgb); //$NON-NLS-1$ 
		return "#" + str.substring(str.length() - 6); //$NON-NLS-1$ 
	}
	
	public static int convert_RGB_to_ARGB(int rgb, int alpha) {
		int r = (rgb >> 16) & 0xFF;
		int g = (rgb >> 8) & 0xFF;
		int b = (rgb >> 0) & 0xFF;
		return (alpha << 24) | (r << 16) | (g << 8) | b;
	}
}

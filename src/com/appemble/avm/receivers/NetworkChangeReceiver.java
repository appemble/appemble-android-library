/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ActionModel;

public class NetworkChangeReceiver extends BroadcastReceiver {
	static public final int ON_NETWORK_AVAILABLE = "ON_NETWORK_AVAILABLE".hashCode();

	@Override
    public void onReceive(final Context context, final Intent intent) {
 
        boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
        if (noConnectivity)
        	return; // no connectivity available.
        
    	ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork && activeNetwork.isAvailable() && activeNetwork.isConnected()) { // call the event ON_NETWORK_AVAILABLE
    		if (null == Cache.context && false == Cache.initiate(context.getApplicationContext(), false))
    			return;
    		String sSystemDbName = Cache.bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME);
    		String sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);
    		if (null == sSystemDbName || null == sContentDbName)
    			return;
			ActionModel[] actions = ActionModel.getActions(sSystemDbName, sContentDbName, ON_NETWORK_AVAILABLE);
			if (null == actions || actions.length == 0)
				return;
            Bundle targetParameterValueList = new Bundle();
            Utilities.putData(targetParameterValueList, Constants.INTEGER, "type", activeNetwork.getType());
            for (int i = 0; null != actions && i < actions.length; i++) {
	    		Action a = new Action();
	    		a.execute(context.getApplicationContext(), null, null, actions[i], false, targetParameterValueList);
            }
        } // else // call the event ON_NETWORK_NOT_AVAILABLE 
    }
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.os.Bundle;
import android.util.SparseArray;

import com.appemble.avm.models.dao.ActionDao;

public class ConfigManager {
	private static ConfigManager _configManager;
	public HashMap<String, Validation> mScreenDeckValidation; 
	private HashMap<String, ActivityMetaData> mActivitiesMap; 
	private HashMap<String, ControlMetaData> mControlsMap; 
	private SparseArray<ControlMetaData> mControlsHashCodeMap; 
	private HashMap<String, ActionMetaData> mActionsMap; 
	private SparseArray<EventMetaData> mEventsMap; 
	private SparseArray<MetaData> mDataTypesMap; 
    private static final String ns = null;
    private static int iActivityId = 1;
    private static int iControlId = 1;
    private static int iActionId = 1;
    private static int iEventId = 1;
    private static int iDataTypeId = 1;
	static public SparseArray<String> mMethodsMap; 

    public class Validation {
    	public String sName;
    	public String sType;
    	public boolean bMandatory;
    	public String[] aValues;
    	public String sDefault;
    	public String sFormat;
    }
    
    public class MetaData {
    	public int id;
    	public String sName;
    	public String sClass;
    	public HashMap<String, Validation> mValidationMap;
    }
    
    public class ActivityMetaData extends MetaData {
    	
    }
    
    public class ControlMetaData extends MetaData {
    	public boolean isClickable = false;
    	public boolean isGroup = false;
    	public int iCursorMgmt;
    	public int iChildControlDataMgmt;
    	public boolean bProcessRemoteDS=true;
//    	public String[] aInputs;
    	public int[] aMethods;
    	public boolean bHasMethodGetControl;
    	public boolean bHasMethodGetView;
    	public boolean bCanHaveDefaultValue;
    }

    public class ActionMetaData extends MetaData {
    	public boolean bCanModifyData = false;
    }

    public class EventMetaData extends MetaData {
    	public String sActionName;
    	public String sTarget;
    	public Vector<String> vInputParameter;
    	public Vector<String> vTargetParameter;
    }

	public static synchronized ConfigManager getInstance() {
		if (_configManager == null) {
			_configManager = new ConfigManager();
			_configManager.initialize();
		}
		return _configManager;
	}

	private ConfigManager() {
		mScreenDeckValidation = new HashMap<String, ConfigManager.Validation>();
		mActivitiesMap = new HashMap<String, ActivityMetaData>();
		mControlsMap = new HashMap<String, ControlMetaData>();
		mControlsHashCodeMap = new SparseArray<ControlMetaData>();		
		mActionsMap = new HashMap<String, ActionMetaData>();
		mEventsMap = new SparseArray<EventMetaData>();
		mDataTypesMap = new SparseArray<MetaData>();
	}

	private void initialize() {
		// read the appemble-config.xml and initialize the Controls and Actions Map
		mMethodsMap = new SparseArray<String>();
//		mMethodsMap.put(Constants.ON_START_SCREEN, "onStart");
		mMethodsMap.put(Constants.ON_RESUME_SCREEN, "onResume");
		mMethodsMap.put(Constants.ON_UPDATE, "onUpdate");
		mMethodsMap.put(Constants.ON_PAUSE_SCREEN, "onPause");
//		mMethodsMap.put(Constants.ON_STOP_SCREEN, "onStop");
		mMethodsMap.put(Constants.ON_DESTROY_SCREEN, "onDestroy");
	}
	
	public ActivityMetaData getActivityMetaData(String sActivity) {
		return mActivitiesMap.get(sActivity);
	}

	public String getActivityClass(String sActivityName) {
		ActivityMetaData metaData = mActivitiesMap.get(sActivityName);
		if (null == metaData)
			return null;
		return metaData.sClass;
	}

	public int getActivityId(String sActivityName) {
		ActivityMetaData metaData = mActivitiesMap.get(sActivityName);
		if (null == metaData)
			return -1;
		return metaData.id;
	}

	public ControlMetaData getControlMetaData(String sControlName) {
		return mControlsMap.get(sControlName);
	}

	public ControlMetaData getControlMetaData(int iHashCode) {
		return mControlsHashCodeMap.get(iHashCode);
	}

	public String getControlClass(String sControlName) {
		ControlMetaData metaData = mControlsMap.get(sControlName);
		if (null == metaData)
			return null;
		return metaData.sClass;
	}

	public int getControlId(String sControlName) {
		ControlMetaData metaData = mControlsMap.get(sControlName);
		if (null == metaData)
			return -1;
		return metaData.id;
	}

	public int getControlId(int iHashcode) {
		ControlMetaData metaData = mControlsHashCodeMap.get(iHashcode);
		if (null == metaData)
			return -1;
		return metaData.id;
	}

	public ActionMetaData getActionMetaData(String sActionName) {
		return mActionsMap.get(sActionName);
	}

	public String getActionClass(String sActionName) {
		ActionMetaData metaData = mActionsMap.get(sActionName);
		if (null == metaData)
			return null;
		return metaData.sClass;
	}

	public int getActionId(String sActionName) {
		ActionMetaData metaData = mActionsMap.get(sActionName);
		if (null == metaData)
			return -1;
		return metaData.id;
	}

	public int getEventId(int iEventHashCode) {
		MetaData metaData = mEventsMap.get(iEventHashCode);
		if (null == metaData)
			return -1;
		return metaData.id;
	}

	public String getEventString(int iEventHashCode) {
		MetaData metaData = mEventsMap.get(iEventHashCode);
		if (null == metaData)
			return null;
		return metaData.sName;
	}
	
	public EventMetaData getEventMetaData(int iEventHashCode) {
		return mEventsMap.get(iEventHashCode);
	}

	public String getDataTypesString(int iDataTypeHashCode) {
		MetaData metaData = mDataTypesMap.get(iDataTypeHashCode);
		if (null == metaData)
			return null;
		return metaData.sName;
	}
	
	public boolean readConfigFile(XmlPullParser xpp) {
		if (null == xpp)
			return false;
		String sText = xpp.getText();
	    try {	        
	        while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
	            if (xpp.getEventType() == XmlPullParser.START_TAG) {
	            	String tagName = xpp.getName(); 
	            	if (tagName.equals("config"))
	            		readAppembleConfig(xpp);
	            }	          
	            xpp.next();
	        }
	    } catch (Throwable t) {
	    	System.out.println("Error in initiating your app. Please check the syntax of the configuration file. It should have a tag " + sText);
	    	t.printStackTrace();
	    	return false;
	    	  // Log error here
	    }
//	    updateMethodsForControls();
		return true;
	}

	// Processes appemble-config
	private void readAppembleConfig(XmlPullParser parser) throws IOException, XmlPullParserException {
	    parser.require(XmlPullParser.START_TAG, ns, "config");
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        String tag = parser.getName();
	        String sItemTagName = null;
        	if (tag.equals("screendeck"))
        		sItemTagName = "screendeck";
        	else if (tag.equals("activities"))
        		sItemTagName = "activity";
        	else if (tag.equals("appearances")) 
        		sItemTagName = "appearance";
        	else if (tag.equals("controls")) 
        		sItemTagName = "control";
        	else if (tag.equals("actions"))
        		sItemTagName = "action";
        	else if (tag.equals("events"))
        		sItemTagName = "event";
        	else if (tag.equals("datatypes"))
        		sItemTagName = "datatype";
        	else
        		continue;
    		readItems(parser, tag, sItemTagName);
		}
	    parser.require(XmlPullParser.END_TAG, ns, "config");
	    return;
	}

	// Processes activities, controls or actions
	private void readItems(XmlPullParser parser, String tag, String sItemTagName) throws IOException, XmlPullParserException {
	    parser.require(XmlPullParser.START_TAG, ns, tag);
	    while (parser.next() != XmlPullParser.END_TAG) {
        	String tagName = parser.getName();
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        if (tagName.startsWith("validation_") && parser.getEventType() != XmlPullParser.END_TAG) {
    	    	readCommonValidation(parser, tagName); // reached start of validation tag.
//    	    	parser.next(); // read the end tag for validation_
    	    }
	        if (parser.getEventType() == XmlPullParser.START_TAG) // start of <activity>, or <control> or <action> or <event>
	        	readItem(parser, sItemTagName);
		}
	    parser.require(XmlPullParser.END_TAG, ns, tag);
	    return;
	}

	// Processes an activity, control or an action
	private void readItem(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
	    parser.require(XmlPullParser.START_TAG, ns, tag);
	    MetaData metaData = null;
	    String sName = parser.getAttributeValue(null, "name");  
	    if (tag.equals("activity")) {
	    	ActivityMetaData activity = new ActivityMetaData();
	    	metaData = activity;
	    	activity.id = iActivityId++;
	    	mActivitiesMap.put(sName, activity);
	    } else if (tag.equals("control")) {
	    	ControlMetaData control = new ControlMetaData();
	    	control.id = iControlId++;
	    	metaData = control;
		    String sTag;
		    sTag = parser.getAttributeValue(null, "isClickable");
		    if (null != sTag && sTag.length() > 0 && (sTag.equals(Constants.YES) || sTag.equals(Constants.TRUE)))
		    	control.isClickable = true;
		    sTag = parser.getAttributeValue(null, "isGroup");
		    if (null != sTag && sTag.length() > 0 && (sTag.equalsIgnoreCase(Constants.YES) || sTag.equalsIgnoreCase(Constants.TRUE)))
		    	control.isGroup = true;
//		    sTag = parser.getAttributeValue(null, "input");
//		    if (null != sTag && sTag.length() > 0)
//		    	control.aInputs = sTag.split(",");
		    sTag = parser.getAttributeValue(null, "cursorMgmt");
		    if (null != sTag && sTag.length() > 0)
		    	control.iCursorMgmt = sTag.equalsIgnoreCase("self") ? 
	    			Constants.CURSOR_MGMT_SELF : Constants.CURSOR_MGMT_SCREEN;
		    sTag = parser.getAttributeValue(null, "childControlDataMgmt");
		    if (null != sTag && sTag.length() > 0)
		    	control.iChildControlDataMgmt = sTag.equalsIgnoreCase("self") ? 
	    			Constants.CHILD_CONTROL_DATA_MGMT_SELF : Constants.CHILD_CONTROL_DATA_MGMT_SCREEN;
		    sTag = parser.getAttributeValue(null, "process_remote_ds");
		    if (null != sTag && sTag.length() > 0 && (sTag.equals(Constants.NO) || sTag.equals(Constants.FALSE)))
		    	control.bProcessRemoteDS = false;
	    	control.bHasMethodGetControl = Utilities.parseBoolean(parser.getAttributeValue(null, "hasMethodGetControl"));
	    	control.bHasMethodGetView = Utilities.parseBoolean(parser.getAttributeValue(null, "hasMethodGetView"));
	    	control.bCanHaveDefaultValue = true; // default is true
	    	if (parser.getAttributeValue(null, "canHaveDefaultValue") != null) // if the config file defines this param then parse it.
	    		control.bCanHaveDefaultValue = Utilities.parseBoolean(parser.getAttributeValue(null, "canHaveDefaultValue"));
	    	mControlsMap.put(sName, control);
	    	mControlsHashCodeMap.put(sName.hashCode(), control);
	    }  else if (tag.equals("action")) {
	    	ActionMetaData action = new ActionMetaData();
	    	action.id = iActionId++;
	    	metaData = action;
		    String sTag = parser.getAttributeValue(null, "can_modify_data");
		    if (null != sTag && sTag.length() > 0 && (sTag.equalsIgnoreCase(Constants.YES) || sTag.equalsIgnoreCase(Constants.TRUE)))
		    	action.bCanModifyData = true;
	    	mActionsMap.put(sName, action);
	    } else if (tag.equals("appearance")) {
	    	// do nothing for now...
	    } else if (tag.equals("event"))  {
	    	EventMetaData event = new EventMetaData();
	    	event.sName = sName.trim();
	    	event.id = iEventId++;
		    event.sActionName = parser.getAttributeValue(null, "action_name");
		    event.sTarget = parser.getAttributeValue(null, "target");
		    event.vInputParameter = ActionDao.parseParameterList(parser.getAttributeValue(null, "input_parameter_list"));
		    event.vTargetParameter = ActionDao.parseParameterList(parser.getAttributeValue(null, "target_parameter_list"));
		    
		    String sTag = parser.getAttributeValue(null, "code");
		    int iCode = 0;
		    try {
		    	if (null != sTag)
		    		iCode = Integer.decode(sTag);
		    } catch (NumberFormatException nfe) {	
		    	LogManager.logWTF("Unable to parse event code: " + sTag);
		    }
	    	mEventsMap.put(iCode == 0 ? sName.hashCode() : iCode, event);
	    } else if (tag.equals("datatype"))  {
	    	metaData = new MetaData();
	    	metaData.sName = sName.trim();
	    	metaData.id = iDataTypeId++;
	    	mDataTypesMap.put(sName.hashCode(), metaData);
	    } else if (tag.equals("screendeck")) {
	    	// do nothing for now...
	    }
	    
	    if (null != metaData) {
		    metaData.sName = sName; 
		    metaData.sClass = parser.getAttributeValue(null, "class");
	    }
	    int eventType = parser.next();
	    if (eventType != XmlPullParser.END_TAG) {
	    	readValidation(parser, metaData, parser.getName()); // reached start of validation tag.
	    	parser.next(); // read the end tag for control
	    }
	    parser.require(XmlPullParser.END_TAG, ns, tag);	    
	    return;
	}

	private void readCommonValidation(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
	    parser.require(XmlPullParser.START_TAG, ns, tag); // validation_
	    Validation validation = null;
	    while (parser.next() != XmlPullParser.END_TAG) {
	    	validation = readField(parser, parser.getName());
	    	if (tag.endsWith("screendeck") && null != validation)
	    		mScreenDeckValidation.put(validation.sName, validation);
//	    	Validation validation = readField(parser, parser.getName());
//	    	if (null == metaData.mValidationMap)
//	    		metaData.mValidationMap = new HashMap<String, Validation>();
//	    	metaData.mValidationMap.put(validation.sName, validation);	    	
	    }
	    parser.require(XmlPullParser.END_TAG, ns, tag); // reached end of validation_ tag.
	}

	private void readValidation(XmlPullParser parser, MetaData metaData, String tag) throws IOException, XmlPullParserException {
	    parser.require(XmlPullParser.START_TAG, ns, tag);
	    while (parser.next() != XmlPullParser.END_TAG) {
	    	Validation validation = readField(parser, parser.getName());
	    	if (null == metaData.mValidationMap)
	    		metaData.mValidationMap = new HashMap<String, Validation>();
	    	metaData.mValidationMap.put(validation.sName, validation);	    	
	    }
	    parser.require(XmlPullParser.END_TAG, ns, tag); // reached end of validation tag.
	}
	
	private Validation readField(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
	    parser.require(XmlPullParser.START_TAG, ns, tag);
	    Validation validation = new Validation();
	    String string = parser.getAttributeValue(null, "name");
	    if (null != string && string.length() > 0)
	    	validation.sName = string;
	    else 
	    	return null; // name must be present.
	    string = parser.getAttributeValue(null, "type");
	    if (null != string && string.length() > 0)
	    	validation.sType = string;
	    string = parser.getAttributeValue(null, "mandatory");
	    if (null != string && string.length() > 0 && (string.equalsIgnoreCase(Constants.YES) || string.equalsIgnoreCase(Constants.TRUE)))
	    	validation.bMandatory = true;
	    string = parser.getAttributeValue(null, "default");
	    if (null != string && string.length() > 0)
	    	validation.sDefault = string;
	    string = parser.getAttributeValue(null, "values");
	    if (null != string && string.length() > 0)
	    	validation.aValues = Utilities.splitCommaSeparatedString(string);
	    string = parser.getAttributeValue(null, "format");
	    if (null != string && string.length() > 0)
	    	validation.sFormat = string;
	    parser.next();
	    parser.require(XmlPullParser.END_TAG, ns, tag); // end tag for field
	    return validation;
	}

	@SuppressWarnings("unused")
	private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
	    if (parser.getEventType() != XmlPullParser.START_TAG) {
	        throw new IllegalStateException();
	    }
	    int depth = 1;
	    while (depth != 0) {
	        switch (parser.next()) {
	        case XmlPullParser.END_TAG:
	            depth--;
	            break;
	        case XmlPullParser.START_TAG:
	            depth++;
	            break;
	        }
	    }
	}
	String sMethodNames[] = { "onStart", "onResume", "onUpdate", "onPause", "onStop", "onDestroy" };
	
	public void updateMethodsForControls() {
		for (String sName : mControlsMap.keySet()) {
			ControlMetaData cmd = mControlsMap.get(sName);
			try {
				Class<?> cmClass = Class.forName(cmd.sClass);
				int i = 0;
				for (String sMethodName : sMethodNames) {
					try {
						if (null != cmClass.getMethod(sMethodName, Bundle.class)) {
							if (null == cmd.aMethods)
								cmd.aMethods = new int[sMethodNames.length];
							cmd.aMethods[i++] = sMethodName.hashCode();
						}
					} catch (NoSuchMethodException nsme) {
						continue; // no issues. The control writer chose not to implement this method.
					} 
				}			
			} catch (ClassNotFoundException e) {
				continue; // is an error. But not logging it.
			} 
		}
	}
	
	public static boolean methodExists(String sControlClassName, String sMethodName) {
		if (null == sControlClassName || null == sMethodName)
			return false;
		int iMethodName = sMethodName.hashCode();
		ControlMetaData cmd = _configManager.mControlsMap.get(sControlClassName);
		if (null == cmd || cmd.aMethods == null)
			return false;
		for (int iMethod : cmd.aMethods) {
			if (iMethod == iMethodName)
				return true;
		}
		return false;
	}
}

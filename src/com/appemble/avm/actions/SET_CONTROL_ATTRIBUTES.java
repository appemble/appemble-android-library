/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.util.Set;
import java.util.Vector;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.AppearanceModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class SET_CONTROL_ATTRIBUTES extends ActionBase {
	public SET_CONTROL_ATTRIBUTES () {}

	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		if (null == parentView || action.sTarget == null || null == targetParameterValueList || 
				0 == targetParameterValueList.size())
			return Boolean.valueOf(false);
		String[] controlNames = action.sTarget.split(",");
		if (null == controlNames || controlNames.length == 0)
			return Boolean.valueOf(false);

		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity)
			return Boolean.valueOf(false);
		ScreenModel screenModel = appembleActivity.getScreenModel();
		Set<String> keySet = targetParameterValueList.keySet();
		for(int i = 0; null != controlNames && i < controlNames.length; i++) {
			ControlModel controlModel = Action.getControl(appembleActivity.getRootLayout(), 
				appembleActivity.getAllControls(), controlNames[i], Constants.FROM_NAME);
			int iControlId = controlModel != null ? controlModel.id : 0; 
				
			if (iControlId <= 0)
				continue;
			View controlView = Action.getViewForControl(parentView, iControlId);
			if (null == controlView)
				continue;
			boolean bLocationProcessed = false, bAppearanceProcessed = false, bDataSourcesProcessed = false;
			for (String key : keySet) {
				if (null == key || key.length() == 0 || key.equals("data_types"))
					continue;
				if (key.equals("x") || key.equals("y") || key.equals("width") || key.equals("height")) {
					if (bLocationProcessed)
						continue;
					setLocation(controlModel, controlView, screenModel, key);
					bLocationProcessed = true;
				} else if (key.equals("appearance_id") || key.equals("word_wrap") || key.equals("ellipsized")) {
					if (bAppearanceProcessed)
						continue;
					setAppearance(controlModel, controlView, screenModel, key);
					bAppearanceProcessed = true;
				} else if (key.equals("remote_data_source") || key.equals("remote_request_type") || key.equals("local_data_source")
						|| key.equals("remote_data_format")) {
					if (bDataSourcesProcessed)
						continue;
					setDataSources(controlModel, controlView, screenModel, key);
					bDataSourcesProcessed = true;
				} else
					setAttributes(controlModel, controlView, screenModel, key);
			}
		}
		return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
	
	private boolean setAttributes(ControlModel cm, View cv, ScreenModel screenModel, String sKey) {
		if (null == sKey || null == cm || null == cv)
			return false;
		String sValue = null;
		sValue = targetParameterValueList.getString(sKey);
		if (sKey.equals("is_visible"))
			cv.setVisibility(Boolean.parseBoolean(sValue) ? View.VISIBLE : View.GONE);
		else if (sKey.equals("permission"))
			cm.setPermission(sValue);
		else if (sKey.equals("data_type"))
			cm.setDataType(sValue);
		else if (sKey.equals("format_type"))
			cm.sFormat = sValue;
		else if (sKey.equals("field_name"))
			cm.sFieldName = sValue;
		else if (sKey.equals("size"))
			cm.iSize = Integer.parseInt(sValue);
//		Not needed. cm.mExtendedProperties.put(sKey, sValue) should do the job.		
//		else if (sKey.equals(Constants.STORAGE_FORMAT))
//			cm.mExtendedProperties.put(Constants.STORAGE_FORMAT, sValue);
		else if (sKey.equals("on_resume_update"))
			cm.bOnResumeUpdate = Boolean.parseBoolean(sValue);
		else if (cm.mExtendedProperties.containsKey(sKey)) // update_screen etc...
			cm.mExtendedProperties.put(sKey, sValue);
		else if (sKey.equals("default_value")) {
			cm.sDefaultValue = sValue;
			((ControlInterface)cv).setValue(sValue);
		} else if (sKey.equals("value"))
			((ControlInterface)cv).setValue(sValue);
		else
			cm.mExtendedProperties.put(sKey, sValue);
		return true;
	}

	private boolean setDataSources(ControlModel cm, View cv, ScreenModel screenModel, String sKey) {
		if (null == sKey || null == cm || null == cv)
			return false;
		String sValue = null;
		sValue = targetParameterValueList.getString("remote_data_source");
		if (null != sValue && sValue.length() > 0) {
			cm.sRemoteDataSource = sValue;
			Vector<ControlModel> vControls = new Vector<ControlModel>();
			vControls.add(cm);
			AppembleActivityImpl.deparameterizeDataSources(screenModel, vControls, targetParameterValueList, 
					action.sContentDbName); // substitute <parameter> with value in targetParameterValueList
		}
		sValue = targetParameterValueList.getString("local_data_source");
		if (null != sValue && sValue.length() > 0)
			cm.sLocalDataSource = sValue;
		sValue = targetParameterValueList.getString("remote_request_type");
		if (null != sValue && sValue.length() > 0)
			cm.setRemoteRequestType(sValue);
		sValue = targetParameterValueList.getString("remote_data_format");
		if (null != sValue && sValue.length() > 0)
			cm.iRemoteResponseType = Constants.mRemoteDataFormat.get(sValue);
		return true;
	}

	private boolean setAppearance(ControlModel cm, View cv, ScreenModel screenModel, String sKey) {
		if (null == sKey || null == cm || null == cv)
			return false;
		if (sKey.equals("appearance_id") || sKey.equals("word_wrap") || sKey.equals("ellipsized")) {
			if (sKey.equals("appearance_id")) {
				String sAppearanceName = targetParameterValueList.getString("appearance_id");
				AppearanceModel am = null;
				if (null != sAppearanceName)
					am = AppearanceManager.getInstance().getAppearance(action.sSystemDbName, sAppearanceName);
				if (null != am)
					cm.iAppearanceId = am.id;
			} else if (sKey.equals("word_wrap"))
				cm.bWordWrap = targetParameterValueList.getBoolean("word_wrap");
			else if (sKey.equals("ellipsized"))
				cm.bEllipsized = targetParameterValueList.getBoolean("ellipsized");
			if (cm.iAppearanceId > 0) {
				if (cv instanceof ControlInterface)
					AppearanceManager.getInstance().updateBackgroundAppearance(cv, cm, null);
				if (cv instanceof TextView)
					AppearanceManager.getInstance().updateTextAppearance((TextView)cv, cm, Constants.APPEARANCE_ID_NOT_SET);
			}
		}
		return true;
	}
	
	private boolean setLocation(ControlModel cm, View cv, ScreenModel screenModel, String sKey) {
		if (null == sKey || null == cm || null == cv)
			return false;
		if (sKey.equals("x") || sKey.equals("y") || sKey.equals("width") || sKey.equals("height")) {
			if (targetParameterValueList.containsKey("x"))
				cm.sX = targetParameterValueList.getString("x");
			if (targetParameterValueList.containsKey("y"))
				cm.sY = targetParameterValueList.getString("y");
			if (targetParameterValueList.containsKey("width"))
				cm.sWidth = targetParameterValueList.getString("width");
			if (targetParameterValueList.containsKey("height"))
				cm.sHeight = targetParameterValueList.getString("height");
			float x = Utilities.getDimension(cm.sX, 
				Cache.bDimenRelativeToParent ? ((View)cv.getParent()).getWidth(): screenModel.fWidthInPixels);
			float y = Utilities.getDimension(cm.sY, 
				Cache.bDimenRelativeToParent ? ((View)cv.getParent()).getHeight(): screenModel.fHeightInPixels);
			float width = Utilities.getDimension(cm.sWidth, 
				Cache.bDimenRelativeToParent ? ((View)cv.getParent()).getWidth(): screenModel.fWidthInPixels);
			float height = Utilities.getDimension(cm.sHeight, 
				Cache.bDimenRelativeToParent ? ((View)cv.getParent()).getHeight(): screenModel.fHeightInPixels);
			LayoutParams lParams = new RelativeLayout.LayoutParams((int)width, (int)height);
			lParams.leftMargin = (int)x;
			lParams.topMargin = (int)y;
			cv.setLayoutParams(lParams);
		}
		return true;
	}
}

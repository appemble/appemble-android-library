/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.ActivityData;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleDialog;
import com.appemble.avm.models.ActionModel;

public class SHOW_DIALOG extends ActionBase {
	public SHOW_DIALOG() {} 

	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		if (null == context)
			return Boolean.valueOf(false);
		
//		Dialog dialog = new Dialog(context);
		//dialog.
//		DynamicLayout dynamicLayout = new DynamicLayout(context);
//		
//		Vector<ControlModel> vChildControls = ControlDao.getControlsinScreen(action.sSystemDbName, action.sContentDbName, action.sTarget);
//		//if (false == dynamicLayout.generateLayout(Constants.DIALOG, Constants.SCREEN_PARENT_ID, action.getDBName(), vChildControls))
//		ScreenModel screenModel = ScreenDao.getScreenByName(action.sSystemDbName, action.sContentDbName, action.sTarget);
//		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
//		screenModel.iWidth = (screenModel.iDimensionType == Constants.PIXELS ? 
//				screenModel.iWidth : (screenModel.iWidth * display.getWidth()) / 100); 
//		screenModel.iHeight = (screenModel.iDimensionType == Constants.PIXELS ? 
//				screenModel.iHeight : (screenModel.iHeight * display.getHeight()) / 100);
//		// if (false == dynamicLayout.generateLayout(Constants.DIALOG, Constants.SCREEN_ID, screenModel, vChildControls))
//		if (false == dynamicLayout.generateLayout(Constants.SCREEN_ID, screenModel, vChildControls))
//			return Boolean.valueOf(false);
//		String sBackground = screenModel.sBackground != null ? screenModel.sBackground :
//			TableDao.getColumnValue(action.sSystemDbName, "_screen_deck", "background", 1);
//		DynamicActivity.setBackground(dynamicLayout, sBackground);
//		dynamicLayout.setTag(R.integer.dialog, dialog);
//		dynamicLayout.setTag(R.integer.targetParameterValueList, targetParameterValueList);
//		dynamicLayout.setTag(R.integer.screenModel, screenModel);
//		// call function to fetch remote data, local data
//		// update the controls.
//		dialog.setContentView(dynamicLayout);
//		dialog.show();
		AppembleDialog ad = new AppembleDialog(context);
		if (false == ad.createLayout(action.sSystemDbName, action.sContentDbName, action.sTarget, targetParameterValueList))
			return Boolean.valueOf(false);
		// Disallow re-orientation if it is allowed after opening the AppembleDialog.
		AppembleActivity appembleActivity = Utilities.getActivity(ad.getOwnerActivity(), null, null);
		if (null != appembleActivity && appembleActivity instanceof Activity) {
			Activity activity = (Activity) appembleActivity;
			ActivityData activityData = appembleActivity.getActivityData();
			if (null != activityData && null != activityData.screenModel && 
					activityData.screenModel.iAllowedLayouts == Configuration.ORIENTATION_UNDEFINED &&
					activityData.screenModel.bAllowReorientation) {
				if (activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
				    activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				else 
					activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}
		}
		ad.show();
//		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//		lp.copyFrom(ad.getWindow().getAttributes());
//		lp.width = (int)ad.getScreenModel().iWidth;
//		lp.height = (int)ad.getScreenModel().iHeight;
//		ad.getWindow().setAttributes(lp);
		ad.onResume();
		return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}

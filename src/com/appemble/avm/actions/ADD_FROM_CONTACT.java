/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;

import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.models.ActionModel;

public class ADD_FROM_CONTACT extends ActionBase {	
	public ADD_FROM_CONTACT() {}
	public final static int iOnPickContact = "ON_PICK_CONTACT".hashCode();
	
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
			
		super.initialize(ctx, v, pView, a, tplv);
		if (view == null)
			return Boolean.valueOf(false);
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity)
			return Boolean.valueOf(false);

		Intent intentContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		appembleActivity.startActivityForResult(intentContact, iOnPickContact);
		return Boolean.valueOf(true);
	}

	public Object executeCascadingActions(boolean bReturn, Bundle targetParameterValueList) {
		return Boolean.valueOf(true);
	} 
}

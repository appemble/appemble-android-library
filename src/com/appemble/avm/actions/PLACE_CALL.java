/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.LogManager;
import com.appemble.avm.models.ActionModel;

public class PLACE_CALL extends ActionBase {
	public PLACE_CALL() {}
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);

		Intent intent = new Intent(Intent.ACTION_CALL);
		
		String sTelephoneNumber = (null != action.sTarget) ? action.sTarget : 
			targetParameterValueList.getString("phone_number");
		String encodedHash = Uri.encode("#");
		sTelephoneNumber = sTelephoneNumber.replaceAll("#", encodedHash);
		try {
			intent.setData(Uri.parse("tel:"+sTelephoneNumber));
		} catch (Exception e) {
			LogManager.logWTF(e, "Unable to parse the telephone number.");
			return Boolean.valueOf(false);
		}
		context.startActivity(intent);
		return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}

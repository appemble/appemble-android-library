/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Vector;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.R;
import com.appemble.avm.Utilities;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.AppembleDialog;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.TableDao;

public class Action extends Object {
	// eclipse gives warning for the if(actionModel == null) statement
	public Action() {}

	public Object execute(final Context context, View view, View parentView, 
			ActionModel actionModel, boolean bGetDataFromScreen) {
		return execute(context, view, parentView, actionModel, bGetDataFromScreen, new Bundle());
	}

	public Object execute(final Context context, View view, View parentView, 
			ActionModel actionModel) {
		return execute(context, view, parentView, actionModel, true, new Bundle());
	}
	
	// can be called from a screen or a service.
	// Within a screen this can be called before creating the screen or while destroying the screen.
	public Object execute(final Context context, View view, View parentView, ActionModel actionModel,
			boolean bGetDataFromScreen, Bundle targetParameterValueList) {
		getData(context, view, parentView, actionModel, bGetDataFromScreen, targetParameterValueList);
		// now deparameterize target using targetParameterValueList
		int iTargetType = Constants.NONE;
		if (Utilities.isUrl(actionModel.sTarget))
			iTargetType = Constants.URL;
		else if (Utilities.isQuery(actionModel.sTarget))
			iTargetType = Constants.SQL;
		actionModel.sTarget = Utilities.replaceParameterisedStrings(actionModel.sTarget, targetParameterValueList, actionModel.sContentDbName, iTargetType);
		return callAction(context, view, parentView, actionModel, targetParameterValueList);
	}

	public static void setViewState(View view, ActionModel actionModel, boolean bEnabled) {
		if (null == view || (!(view instanceof ControlInterface)))
			return;
//		if (view says that I should be disabled on this event,
//		then increase the disable counter.
//	If counter == 1, disable the view)
		ControlModel controlModelRecevingAction = ((ControlInterface) view).getControlModel();
		if (null == controlModelRecevingAction || null == controlModelRecevingAction.iDisableOnEventList ||
				0 == controlModelRecevingAction.iDisableOnEventList.length)
			return;
		for (int iActionEvent : actionModel.iEventList) {
			for (int iControlEvent : controlModelRecevingAction.iDisableOnEventList) {
				if (iControlEvent == iActionEvent) {
					Object oViewStateCounter = view.getTag(R.integer.view_state_counter);
					int iViewStateCounter = 0;
					if (null != oViewStateCounter)
						iViewStateCounter = ((Integer)oViewStateCounter).intValue();
					if (bEnabled) {
						iViewStateCounter--;
						if (0 == iViewStateCounter) { // if view is to be enabled again
							view.setClickable(true);view.setEnabled(true);
						}
					} else {
						iViewStateCounter++;
						if (1 == iViewStateCounter) { // if view is to be disabled. Called 1st time.
							view.setClickable(false);view.setEnabled(false);
						}
					}
					view.setTag(R.integer.view_state_counter, Integer.valueOf(iViewStateCounter));
					return;
				}
			}
		}
	}

	private Bundle getData(final Context context, View view, View parentView, 
			ActionModel actionModel, boolean bGetDataFromScreen, Bundle targetParameterValueList) {
//		Bundle targetParameterValueList = new Bundle();
		if (null == targetParameterValueList)
			targetParameterValueList = new Bundle();
		if (actionModel == null) {
			LogManager.logError("Unable to execute action. The action model is a MUST to process an action");
			return targetParameterValueList;
		}
		ControlModel controlModelRecevingAction = null;
		try {
			if (null != view)
				controlModelRecevingAction = ((ControlInterface) view).getControlModel();
		} catch (ClassCastException cce) {
			LogManager.logWTF(cce, "Unable to execute action: " + actionModel.getActionName() + 
				". Make sure that the view passed in one of the approved controls by Appemble Framework. The view must be implement ControlInterface.");
			return targetParameterValueList;			
		}
		AppembleActivity appembleActivity = null;
		if (bGetDataFromScreen)
			appembleActivity = Utilities.getActivity(context, view, parentView);

		String sContentDbName = actionModel.sContentDbName;
		Bundle inputParameterValueList = new Bundle();

		Object field_value;
		Vector<ControlModel> vGroupControls = null;
		Vector<ControlModel> vControlsInScreen = null;
		AppembleDialog appembleDialog = null;
		Bundle orignatorScreenTargetParameterValueList = null;
		if (parentView instanceof DynamicLayout) { // treat the dialog separately.
			appembleDialog = (AppembleDialog)parentView.getTag(R.integer.dialog);
			if (bGetDataFromScreen && null != appembleDialog)
				orignatorScreenTargetParameterValueList = (Bundle)parentView.getTag(R.integer.targetParameterValueList);
		}
		if (bGetDataFromScreen && null != appembleActivity && null == orignatorScreenTargetParameterValueList)
			orignatorScreenTargetParameterValueList = appembleActivity.getTargetParameterValueList();
		// for each field in the input_parameter_list of the action,
		//	if the key starts with GLOBAL: then get the value from _properties table and continue to the next input_parameter
		//	if the ket starts with CONSTANT: then evaluate the constant and continue to the next input_parameter
		//  get the view of the control based on the field name
		//     To get the view, first see if the controlModelRecevingAction has the same field name. 
		//     if not found then search in the group of controls if the current view is a group view. 
		//     if not found then search in the control list for the screen. 
		//     if view is found get the value of that control by converting the control id to view. Continue to the next input_parameter
		//  if not found then use targetParameterValueList of the screen from where the action is originated. if found get the value and continue to the next parameter

		Vector<String> vInputParameterList = actionModel.vInputParameters;
		if (null != vInputParameterList)
		for (String sInputParameter : vInputParameterList) {
			// If vInputParameterList.elementAt(i) has GLOBAL: then get the
			// parameter value from the _properties table. continue;
			if (sInputParameter.startsWith(Constants.GLOBAL_PARAMETER_PREFIX)) {
				int iIndex = sInputParameter.indexOf(Constants.GLOBAL_PARAMETER_PREFIX);
				if (iIndex > -1) {
					sInputParameter = sInputParameter.substring(Constants.GLOBAL_PARAMETER_PREFIX_LENGTH + 1);
					HashMap<String, String> row = TableDao.get1Row(sContentDbName,
							"_properties", "key = \'" + sInputParameter + "\'");
					String sValue = row.get("value");
					if (null != sValue)
						Utilities.putData(inputParameterValueList, Constants.VARCHAR, 
								Constants.GLOBAL_PARAMETER_PREFIX + Constants.FIELD_SEPARATER + sInputParameter, 
								sValue);
					else
						LogManager.logDebug("While executing action: " + actionModel.getActionName() + 
								", unable to find Global Property: " + sInputParameter); 
				}
				continue; // go to next parameter in inputParameterList
			}
			if (sInputParameter.startsWith(Constants.CONSTANT_PREFIX)) {
				String sValue = sInputParameter.substring(Constants.CONSTANT_PREFIX_LENGTH + 1);
				String[] sValues = Utilities.parseStringArray(sValue);
				if (null != sValues)
					Utilities.putData(inputParameterValueList, Constants.STRING_ARRAY, sInputParameter, sValues);
				else if (null != sValue)
					Utilities.putData(inputParameterValueList, Constants.VARCHAR, sInputParameter, sValue);
				else
					; // TODO Log an error.
				continue; // go to next input parameter in inputParameterList
			}
			if (Utilities.isQuery(sInputParameter) // skip all queries and go to next parameter in inputParameterList in this 1st pass
					|| false == bGetDataFromScreen) { // no need to get data from screen if bGetDataFromScreen == false
				continue; 
			}
			// if the control that is receiving the action has its field
			// name as the input parameter
			if (null != controlModelRecevingAction && null != controlModelRecevingAction.sFieldName && 
					controlModelRecevingAction.sFieldName.equals(sInputParameter)) {
				field_value = getValueFromView(view);
				field_value = validateFormat(controlModelRecevingAction, field_value);
				Utilities.putData(inputParameterValueList, controlModelRecevingAction.iDataType, 
						sInputParameter, field_value);
				continue; // go to next parameter in inputParameterList
			}
			if (null == vGroupControls && null != appembleActivity && 
					null != controlModelRecevingAction && controlModelRecevingAction.isGroup()) {
				vGroupControls = new Vector<ControlModel>(); 
				AppembleActivityImpl.getChildControls(appembleActivity.getAllControls(), 
						controlModelRecevingAction.id, vGroupControls);
			}
			if (null != vGroupControls && null != parentView) {
				ControlModel controlModel = getControl(parentView, vGroupControls, sInputParameter, Constants.FROM_FIELD_NAME);
				if (null != controlModel) {
					int iControlId = controlModel.id;
					if (iControlId > 0) {
						View v = getViewForControl(parentView, iControlId);
						if (null == v) { // TODO log error here. View not found
							continue; // go to next parameter in inputParameterList
						}
						field_value = getValueFromView(v);
						field_value = validateFormat(controlModel, field_value);
						Utilities.putData(inputParameterValueList, controlModel.iDataType, 
								sInputParameter, field_value);
						continue; // go to next parameter in inputParameterList
					}
				}
			}

			if (null == vControlsInScreen && null != appembleDialog)
				vControlsInScreen = appembleDialog.getAllControls();
			if (null == vControlsInScreen && null != appembleActivity)
				vControlsInScreen = appembleActivity.getAllControls();
			if (null != vControlsInScreen) {
				ControlModel controlModel = getControl(appembleActivity.getRootLayout(), vControlsInScreen, sInputParameter, Constants.FROM_FIELD_NAME);
				// TODO the children of list control should be excluded from here.
				// Do not know which listItem should the value be picked from
				if (null != controlModel) {
					int iControlId = controlModel.id;
					if (iControlId > 0 && null != parentView) { // find the view in the parent View first.
						View v = parentView.findViewById(controlModel.id);
						if (null == v) // now find the view in the screen.
							v = AppembleActivityImpl.findViewInScreen(appembleActivity, controlModel);
						if (null != v) {
							field_value = getValueFromView(v);
							field_value = validateFormat(controlModel,field_value);
							Utilities.putData(inputParameterValueList, controlModel.iDataType, 
									sInputParameter, field_value);
							continue;
						}
					}
				}
			}
			// still not found? use targetParameterValueList of the appembleActivity. This is the last try
			if (null != orignatorScreenTargetParameterValueList && 
					orignatorScreenTargetParameterValueList.containsKey(sInputParameter)) {
				Object object = orignatorScreenTargetParameterValueList.get(sInputParameter);
				Bundle dataTypes = orignatorScreenTargetParameterValueList.getBundle(Constants.STR_DATA_TYPES);
				int iDataType = dataTypes != null ? dataTypes.getInt(sInputParameter) : Constants.VARCHAR;
				if (0 == iDataType)
					iDataType = Constants.VARCHAR;
				Utilities.putData(inputParameterValueList, iDataType, sInputParameter, object);
				continue;
			}
			// Here log the error message as the value of the parameter could not be found.
			LogManager.logError("While executing action: " + actionModel.getActionName() + actionModel.sTarget + 
					", unable to find value for the parameter: " + sInputParameter); 			
		}
		// now process all queries
		if (null != vInputParameterList)
			for (String sQuery : vInputParameterList) {
				if (Utilities.isQuery(sQuery)) {
					// replace parameters in this query from the input values of
					// other parameters in the inputParameterList
					String sDeparameterizedQuery = Utilities.replaceParameterisedStrings(sQuery,
									inputParameterValueList, sContentDbName, Constants.SQL);
					if (false == _getInputParametersFromDb(sQuery, sDeparameterizedQuery, sContentDbName,
							inputParameterValueList))
						; // log error here
				}
			}
		// now convert the parameters into targetParameterValue List
		// for performance reason, we just convert the keys in the
		// inputParameterValueList so that
		// the inputParameterValueList is transformed to the
		// targetParameterValueList
		Vector<String> vTargetParameterList = actionModel.vTargetParameters;
		int i = 0;
		String sTargetParameter = null;
		if (null != vInputParameterList)
			for (String sInputParameter : vInputParameterList) {
				Object value = inputParameterValueList.get(sInputParameter);
				if (null != vTargetParameterList && vTargetParameterList.size() > i)
					sTargetParameter = vTargetParameterList.get(i);
				else
					sTargetParameter = sInputParameter;
				Bundle dataTypes = inputParameterValueList.getBundle(Constants.STR_DATA_TYPES);
				Utilities.putData(targetParameterValueList, 
						dataTypes != null ? dataTypes.getInt(sInputParameter) : Constants.VARCHAR, 
						sTargetParameter, value);
				i++;
			}
		return targetParameterValueList;
	}

	public static ControlModel getControl(View parentView, Vector<ControlModel> vControls,
			String toMatch, int iSource) {
		if (null == toMatch || null == vControls)
			return null;
		for (ControlModel cm : vControls) {
			String sName = iSource == Constants.FROM_FIELD_NAME ? cm.sFieldName : cm.sName;
			if (null != sName && sName.equalsIgnoreCase(toMatch))
				return cm;
			if (false == cm.metaData.bHasMethodGetControl)
				continue;
			try {
				View view = getViewForControl(parentView, cm.id);
				if (null == view)
					continue;
				Class<?> controlClass = Class.forName(cm.metaData.sClass);
				Method mMethodName = controlClass.getMethod("getControl", String.class, Integer.class);
				ControlModel cmChild = (ControlModel) mMethodName.invoke(view, toMatch, iSource);
				if (null != cmChild)
					return cmChild;
			} catch (ClassNotFoundException e) {
	    		LogManager.logError("Cannot initiate screen state change. Implementation class not found: " + 
	    				cm.getClassName());
			} catch (NoSuchMethodException nsme) {
				// no issues. The control writer chose not to implement this method.
			} catch (InvocationTargetException ite) {
				
			} catch (IllegalAccessException iae) {
				LogManager.logError("Unable to access method for contorl Type: " + cm.getClassName());
			}
//				 invokeMethod getControl(String). Try to find the controlModel
		}
		return null;
	}

	public static Object getValueFromView(View controlView) {
		Object value = null;
		try {
			ControlInterface c = (ControlInterface) controlView;
			value = c.getValue();
		} catch (ClassCastException cce) {
			LogManager.logWTF(cce, "Control class must implement ControlInterface");
		}
		return value;
	}

	public static View getViewForControl(View parentView, int control_id) {
		View v = null;
//		try {
			v = parentView.findViewById(control_id);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		return v;
	}

	public static Object callScreenActions(AppembleActivity appembleActivity, int iAction, View view, 
			View parentView, Bundle targetValueParameterList) {
		return callScreenActions(appembleActivity, iAction, view, parentView, true, targetValueParameterList);
	}
	
	public static Object callScreenActions(AppembleActivity appembleActivity, int iAction, View view, 
			View parentView, boolean bScreenOnly, Bundle targetParameterValueList) {
		ScreenModel screenModel = null;
		if (null == appembleActivity || (screenModel = appembleActivity.getScreenModel()) == null)
			return Boolean.FALSE;
		if (bScreenOnly && screenModel.bActionYN == false)
			return Boolean.valueOf(true);
	    ActionModel[] actions = appembleActivity.getScreenModel().getActions(iAction, bScreenOnly);
	    Boolean bReturn = true;
	    Object bObject = null;
	    if (null != actions)
		    for (int i = 0; bReturn && i < actions.length; i++) {
		    	Action a = new Action();
		    	actions[i].mScreenObject = appembleActivity; // send the activity as well. In case from Fragment, 
		    	bObject = a.execute(appembleActivity.getContext(), view, parentView, 
		    		actions[i], true, targetParameterValueList);
		    	if (bObject instanceof Boolean)
		    		bReturn = (Boolean) bObject;
		    }
	    return bObject;
	}

	public static Object callAction(final Context context, final View view, final View parentView,
			final ActionModel actionModel, final Bundle targetParameterValueList) {
		if (null == actionModel)
			return Boolean.FALSE;
		try {
			if (Constants.isVerboseMode) {
				String sViewName = null;
				if (null != view && view instanceof ControlInterface) {
					ControlModel cm = ((ControlInterface) view).getControlModel();
					if (null != cm)
						sViewName = cm.sName;
				}
				if (null != sViewName)
					LogManager.logVerbose("Calling action '" + actionModel.getActionName() + "'. On View: '" + sViewName +  
							"' with target: '" + actionModel.sTarget + "'. Params: " + targetParameterValueList.toString());
				else
					LogManager.logVerbose("Calling action '" + actionModel.getActionName() + "' with target: '" + 
							actionModel.sTarget + "'. Params: " + targetParameterValueList.toString());
			}
			// Make dynamic function call based on ACTION name
			ActionBase ai = (ActionBase) Class
					.forName(actionModel.getClassName())
					.getDeclaredConstructor().newInstance();
			if (null == ai)
				return Boolean.FALSE;
			setViewState(view, actionModel, Constants.DISABLE);
			Object oReturn = ai.execute(context, view, parentView, actionModel, targetParameterValueList);
			setViewState(view, actionModel, Constants.ENABLE);
			return oReturn;
		} catch (IllegalAccessException iae) {
			LogManager.logWTF(iae, "Error executing an action: " + actionModel.getActionName() + 
				". ParameterList: " + targetParameterValueList.toString());
			return Boolean.FALSE;
		} catch (InvocationTargetException ite) {
			LogManager.logWTF(ite, "Error executing an action: " + actionModel.getActionName() + 
				". ParameterList: " + targetParameterValueList.toString());
			return Boolean.FALSE;
		} catch (InstantiationException ie) {
			LogManager.logWTF(ie, "Error executing an action: " + actionModel.getActionName() + 
				". ParameterList: " + targetParameterValueList.toString());
			return Boolean.FALSE;
		} catch (ClassNotFoundException cnfe) {
			LogManager.logWTF(cnfe, "Error executing an action: " + actionModel.getActionName() + "Unable to find class: " + 
				actionModel.getClassName() + ". ParameterList: " + targetParameterValueList.toString());
			return Boolean.FALSE;
		} catch (NoSuchMethodError nsme) {
			LogManager.logWTF(nsme, "Error executing an action: " + actionModel.getActionName() + "Unable to find method execute in class: " + 
				actionModel.getClassName() + ". ParameterList: " + targetParameterValueList.toString());
			return Boolean.FALSE;
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			LogManager.logWTF(e, "Error executing an action: " + actionModel.getActionName() + 
					". Unable to find method execute in class: " + actionModel.getClassName() + 
					". ParameterList: " + targetParameterValueList == null ? "" : 
						targetParameterValueList.toString());
			Looper mainLooper = Looper.getMainLooper();
			if (null != mainLooper) {		
	            Handler refresh = new Handler(mainLooper);
	            if (null != refresh) {
		            refresh.post(new Runnable() {
		                public void run() {
		        			ALERT alert = new ALERT();
		        			String sError = e.getMessage();
		        			if (null != sError && sError.length() > 0) {
		        				actionModel.sTarget = "Alert";
		        				if (null != targetParameterValueList) {
			        				targetParameterValueList.putString("title", "Alert");
			        				targetParameterValueList.putString("message", sError);
		        				}
		        			}
		        			alert.execute(context, view, parentView, actionModel, targetParameterValueList);
		               	}
		            });
	            }
            }
			return Boolean.FALSE;
		}
	}

	private boolean _getInputParametersFromDb(String sQuery, String sDeparameterizedQuery, String sDbName,
			Bundle inputParameterValueList) {
		String[] sValues = TableDao.getValuesFromDb(sQuery, sDbName);
		if (null == sValues)
			LogManager.logDebug("While creating target parameter value list, the query " + sQuery + " returned no data");
		else if (sValues.length > 1)
			LogManager.logDebug("While creating target parameter value list, the query " + sQuery + " returned multiple rows. Only the first column of the first row will be used");
		Utilities.putData(inputParameterValueList, Constants.VARCHAR, sQuery, sValues[0]);
		return true;
	}

	public static Object validateFormat(ControlModel controlModel, Object sValue) {
		if (null == controlModel || null == sValue)
			return sValue;
		int iDataType = controlModel.iDataType;
		// pass the date in standard internal format (yyyy-MM-dd, yyyy-MM-dd
		// hh:mm:ss, hh:mm:ss)
		if (iDataType == Constants.DATE || iDataType == Constants.DATETEXT
				|| iDataType == Constants.DATETIME
				|| iDataType == Constants.TIME
				|| iDataType == Constants.TIMESTAMP) {
			String sStorageFormat = controlModel.mExtendedProperties != null ? 
					controlModel.mExtendedProperties.get(Constants.STORAGE_FORMAT) : null;
			if (sValue instanceof String) 
				return Utilities.formatDate((String)sValue, iDataType, controlModel.sFormat,
						((sStorageFormat != null) ? sStorageFormat : controlModel.sFormat));
			else {
				LogManager.logError("Error in validating data for control: " + controlModel.sName);
				return null;
			}
		}
		if (controlModel.iPermission != Constants.READWRITE)
			return sValue;
		if (iDataType == Constants.TEXT || iDataType == Constants.CLOB || iDataType == Constants.STRING 
				|| iDataType == Constants.MEMO || iDataType == Constants.NTEXT) {
			if (sValue instanceof String)
				return sValue;
			else {
				LogManager.logError("Error in validating data for control: " + controlModel.sName);
				return null;
			}
		} else if (iDataType == Constants.VARCHAR
				|| iDataType == Constants.VARCHAR2
				|| iDataType == Constants.GUID || iDataType == Constants.NCHAR
				|| iDataType == Constants.NVARCHAR
				|| iDataType == Constants.NVARCHAR2) {
			if (sValue instanceof String) {
				String sText = (String) sValue;
				if (sText.length() > controlModel.iSize) {
					LogManager.logDebug("While processing an action, the text in control: " + 
							controlModel.sName + " with field_name = " + controlModel.sFieldName + 
							" exceeds its size. Truncating it...");
					sText = sText.substring(0, controlModel.iSize);
				}
				return sText;
			} else {
					LogManager.logError("Error in validating data for control: " + controlModel.sName);
					return null;
			}
		} else if (iDataType == Constants.TINYINT || iDataType == Constants.INT64 || 
				iDataType == Constants.INTEGER || iDataType == Constants.LARGEINT || 
				iDataType == Constants.INT || iDataType == Constants.NUMBER || 
				iDataType == Constants.AUTOINC || iDataType == Constants.BIGINT || 
				iDataType == Constants.NUMERIC || iDataType == Constants.SMALLINT) {
				if (sValue instanceof String) {
					try {
						if (null != sValue && ((String)sValue).length() > 0)
							Integer.parseInt((String) sValue); // try to convert to integer only if sValue has some length.
						return sValue; // return the empty sValue or sValue must be parsed to an integer.
					} catch (NumberFormatException nfei) {
						LogManager.logDebug("While processing an action, the value in control: " + 
								controlModel.sName + " with field_name = " + controlModel.sFieldName + 
								"is not an integer. Please set the keyboard type right or add validation rules.");
						return null;
					}
				} else if (sValue instanceof Integer)
					return sValue;
				else {
					LogManager.logError("Error in validating data for control: " + controlModel.sName);
					return null;
				}					
		} else if (iDataType == Constants.DECIMAL || iDataType == Constants.DEC
				|| iDataType == Constants.FLOAT) {
			if (sValue instanceof String) {
				try {
					Float.parseFloat((String) sValue);
					return sValue;
				} catch (NumberFormatException nfef) {
					LogManager.logDebug("While processing an action, the value in control: " + 
							controlModel.sName + " with field_name = " + controlModel.sFieldName + 
							"is not a float. Please set the keyboard type right or add validation rules.");
					return null;
				} 
			} else if (sValue instanceof Float) 
				return sValue;
			else {
				LogManager.logError("Error in validating data for control: " + controlModel.sName);
				return null;
			}
		} else if (iDataType == Constants.DOUBLE || iDataType == Constants.DOUBLE_PRECISION
				|| iDataType == Constants.REAL) {
			if (sValue instanceof String) {
				try {
					Double.parseDouble((String) sValue);
					return sValue;
				} catch (NumberFormatException nfed) {
					LogManager.logDebug("While processing an action, the value in control: " + 
							controlModel.sName + " with field_name = " + controlModel.sFieldName + 
							"is not a double. Please set the keyboard type right or add validation rules.");
					return null;
				}
			} else if (sValue instanceof Double)
				return sValue;
			else {
				LogManager.logError("Error in validating data for control: " + controlModel.sName);
				return null;
			}
		} else if (iDataType == Constants.CHAR) {
			if (sValue instanceof String) {
				String sText = (String) sValue;				
				if (sText.length() > 1) {
					LogManager.logDebug("While processing an action, the value in control: " + 
							controlModel.sName + " with field_name = " + controlModel.sFieldName + 
							"is more than one character. Truncating it...");
					return sText.substring(0, 1);
				}
				return sText;
			} else {
				LogManager.logError("Error in validating data for control: " + controlModel.sName);
				return null;
			}
		} else if (iDataType == Constants.MONEY || iDataType == Constants.CURRENCY
				|| iDataType == Constants.SMALLMONEY) {
			if (sValue instanceof String) {
				try {
					Float.parseFloat((String) sValue);
					return sValue;
				} catch (NumberFormatException nfef) {
					LogManager.logDebug("While processing an action, the value in control: " + 
							controlModel.sName + " with field_name = " + controlModel.sFieldName + 
							"is not a float. Please set the keyboard type right or add validation rules.");
					return null;
				} 
			} else if (sValue instanceof Float)
				return sValue;
			else
				return null;
		} else if (iDataType == Constants.STRING_ARRAY) {
			if (sValue instanceof String[])
				return sValue;
			else {
				LogManager.logDebug("While processing an action, the value in control: " + 
						controlModel.sName + " with field_name = " + controlModel.sFieldName + 
						"is not an array of Strings.");
				return null;
			}
		} else if (iDataType == Constants.BITMAP) {
			if (sValue instanceof byte[])
				return sValue;
			else {
				LogManager.logDebug("While processing an action, the value in control: " + 
						controlModel.sName + " with field_name = " + controlModel.sFieldName + 
						"is not an array of bytes.");
				return null;
			}
		}
		return sValue;
	}
    
//	executeCascadingActions() methodology.
//	1. Preserve the values that are being passed from the parent and the child needs them.
//	2. If the child action has a input parameter that is not part of the target parameter of the parent, 
//		then child action should call getData only with those input parameters that are not part of the parent's
//		targetParameterValueList
//  3. Call Action.callAction to execute the child action.	
	public static Object executeCascadingActions(Context context, View view, View parentView, 
		ActionModel action, Bundle targetParameterValueList, boolean bExecuteAll, Object oReturnObject) {
    	if (null == action)
    		return oReturnObject;
    	Boolean bReturn = Boolean.valueOf(true);
	    ActionModel[] actions = action.getNextActions();
	    if (null == actions || actions.length == 0)
	    	return oReturnObject;
	    for (int i = 0; bReturn.booleanValue() && i < actions.length; i++) {
	    	Bundle newTargetParameterValueList = 
	    		actions[i].transformTargetParameterList(targetParameterValueList);		    	
	    	// If the size of the vInputParametes > 0, then call action.getData() (with just reduced size of input parameters list)
	    	// combine the newTargetParameterValueList with the actions[i].targetParameterValueList
	    	// call Action.callAction();
	    	if (actions[i].vInputParameters != null && actions[i].vInputParameters.size() > 0) {
	    		Action a = new Action(); 
	    		Bundle subsetTargetParameterValueList = new Bundle(); 
	    		subsetTargetParameterValueList = a.getData(context, view, parentView, actions[i], true, subsetTargetParameterValueList);
	    		if (null != subsetTargetParameterValueList && subsetTargetParameterValueList.size() > 0)
	    			newTargetParameterValueList.putAll(subsetTargetParameterValueList);
	    	}
    		// Now deparameterize the actions[i].target with the newTargetParameterValueList
    		int iTargetType = Constants.NONE;
    		if (Utilities.isUrl(actions[i].sTarget))
    			iTargetType = Constants.URL;
    		else if (Utilities.isQuery(actions[i].sTarget))
    			iTargetType = Constants.SQL;
    		actions[i].sTarget = Utilities.replaceParameterisedStrings(actions[i].sTarget, 
    				newTargetParameterValueList, actions[i].sContentDbName, iTargetType);
    		oReturnObject = (Boolean)Action.callAction(context, view, parentView, actions[i], newTargetParameterValueList);
    		if (oReturnObject instanceof Boolean && false == bExecuteAll)
    			bReturn = (Boolean) oReturnObject;		    	
    	}
	    return oReturnObject; // pass on the last action's return object.
    }
	
	public static boolean callAction(Context context, View view, View parentView, ControlModel controlModel, 
			int iEventType) {
		if (controlModel.bActionYN == false)
			return true;
		ActionModel[] actions = controlModel.getActions(iEventType);
		if (null == actions)
			return true;
		boolean bReturn = true;
		for (int i = 0; bReturn && i < actions.length; i++) {
			if (actions[i].iPrecedingActionId == 0) {
				AppembleDialog dialog = (AppembleDialog)parentView.getTag(R.integer.dialog);
				actions[i].mScreenObject = dialog;
				Action a = new Action();
				Object o = a.execute(context, view, parentView, actions[i]);
				if (o instanceof Boolean)
					bReturn = (Boolean)o;
			}
		}
		return bReturn;
	}
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.util.Vector;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.TableDao;
import com.appemble.avm.models.dao.db.DbConnectionManager;

public class NEXT_SCREEN_DECK extends ActionBase {
	String token_id;
	public String sSystemDbName, sContentDbName;
	String[] sTargets;
	
	public NEXT_SCREEN_DECK() {}
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		if (null == action.sTarget || action.sTarget.length() == 0)
			return Boolean.valueOf(false);
		String[] target = action.sTarget.split(",");
		if (null == target || null == target[0])
			return Boolean.valueOf(false);
		if (target.length == 2) {
			sTargets = target;
			sTargets[0] = sTargets[0].trim();
			sTargets[1] = sTargets[1].trim();
		}
		else if (target.length == 1) {
			sTargets = new String[2];
			target[0] = target[0].trim();
			sTargets[1] = sTargets[0] = target[0];
		}
		String sSysDbName = Utilities.replaceParameterisedStrings(sTargets[0], targetParameterValueList, action.sContentDbName, 
				Utilities.isUrl(sTargets[0]) ? Constants.URL : Constants.NONE);
		if (sTargets[0].equalsIgnoreCase(sTargets[1]))
			sTargets[1] = sSysDbName;
		else
			sTargets[1] = Utilities.replaceParameterisedStrings(sTargets[1], targetParameterValueList, action.sContentDbName, 
				Utilities.isUrl(sTargets[1]) ? Constants.URL : Constants.NONE);
		sTargets[0] = sSysDbName;
		// Now download the new screen deck in a separate task.
		ProgressDialog progress = new ProgressDialog(context);
		progress.setMessage("Connecting...");
		MyTask myTask = new MyTask(progress);
		myTask.execute();
		return Boolean.TRUE;
	}

	public class MyTask extends AsyncTask<Void, Void, Integer> {
		private ProgressDialog progress;
		public MyTask(ProgressDialog progress) {
			this.progress = progress;
		}

		public void onPreExecute() {
			if (null != progress)
				progress.show();
		}

		public synchronized Integer doInBackground(Void... unused) {
			sSystemDbName = DbConnectionManager.getInstance().copyDb(sTargets[0], true, Constants.isDebugMode);
			if (false == sTargets[0].equalsIgnoreCase(sTargets[1]))
				sContentDbName = DbConnectionManager.getInstance().copyDb(sTargets[1], true, Constants.isDebugMode);
			else
				sContentDbName = sSystemDbName;
			return 1;
		}

		public void onPostExecute(Integer unused) {
			if (null != progress)
				progress.dismiss();

			if (null == sSystemDbName || null == sContentDbName)
				return; //TODO Log error here that the Db could not be downloaded.
			Intent intent = ScreenModel.getIntent(sSystemDbName, sContentDbName, null, targetParameterValueList);
			if (null != intent)
				context.startActivity(intent);		
			Vector<String> sTargetParameterList = action.vTargetParameters;
			long lRowId = 0;
			// Now extract the GLOBAL parameters and put them in the database.
			if (sTargetParameterList != null)
			for (String sGlobalParameter : sTargetParameterList) {
				int iIndex = sGlobalParameter.indexOf(Constants.GLOBAL_PARAMETER_PREFIX);
				if (iIndex > -1) {
					String sParameter = sGlobalParameter.substring(Constants.GLOBAL_PARAMETER_PREFIX_LENGTH+1);
					ContentValues cv = new ContentValues();
					cv.put("key", sParameter);
					cv.put("value", targetParameterValueList.getString(sGlobalParameter));
					lRowId = TableDao.replace1Row(sContentDbName, Constants.STR_PROPERTIES_TABLE, cv);
					if (lRowId < 0)
						; // TODO Log an error here
				}
			}
			// call any actions whose preceding action is this CALL_URL
			executeCascadingActions(true, targetParameterValueList);
		}
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.dao.TableDao;
import com.appemble.avm.services.AlarmService;

public class SET_ALARM extends ActionBase {
	// Set Alarm does following
		// checks if the required parameters are there in the incoming request
		// checks if the format is valid
		// checks repeat rules and parameters required for each alarm type.
		// Persists the alarms in the database
		// calls the service to set the alarm
	public SET_ALARM() {} 
	
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		if((null == targetParameterValueList || targetParameterValueList.size() == 0) && 
			(null == a.mExtendedProperties || 0 == a.mExtendedProperties.size()))
			return Boolean.FALSE;

		
		String sAlarmType = targetParameterValueList.getString("type"); // class, email
		if (null == sAlarmType) {
			sAlarmType = a.mExtendedProperties.get("type");	
			if (null != sAlarmType)
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "type", sAlarmType); // set default alarm type as audio
		}
		// Check alarm type
//		if (null == sAlarmType) {
//			sAlarmType = "audio";
//			targetParameterValueList.putString("type", sAlarmType); // set default alarm type as audio
//			Utilities.putData(targetParameterValueList, Constants.VARCHAR, "type", sAlarmType); // set default alarm type as audio
//		}
		// Check repeat condition
		String sRepeat = targetParameterValueList.getString(AlarmService.STR_REPEAT);
		if (null == sRepeat) {
			sRepeat = a.mExtendedProperties.get(AlarmService.STR_REPEAT);
			if (null != sRepeat)
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, AlarmService.STR_REPEAT, sRepeat);
		}
		if (null == sRepeat)
			sRepeat = "none"; // set default repeat as none

		String sDateFormat = targetParameterValueList.getString(AlarmService.STR_FORMAT);
		if (null == sDateFormat) {
			sDateFormat = a.mExtendedProperties.get(AlarmService.STR_FORMAT);
			if (null != sDateFormat)
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, AlarmService.STR_FORMAT, sDateFormat);
		}
		String sDate = targetParameterValueList.getString(AlarmService.STR_DATE);
		if (null == sDate) {
			sDate = a.mExtendedProperties.get(AlarmService.STR_DATE);
			if (null != sDate)
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, AlarmService.STR_DATE, sDate);
		}
		Date date = null;
		try {
			if (null != sDateFormat && null != sDate) {
				SimpleDateFormat sdf = new SimpleDateFormat(sDateFormat, Cache.context.getResources().getConfiguration().locale);
				date = sdf.parse(sDate);
				sDate = sdf.format(date);
			}
		} catch (ParseException pe) {
			String sErrorString = "While setting the alarm, there is an error in parsing date: " + sDate + " using format: " + sDateFormat;
			Utilities.putData(targetParameterValueList, Constants.VARCHAR, "error", sErrorString);
        	LogManager.logWTF(pe, sErrorString);
			return Boolean.valueOf(false);
		}
		String sRepeatRulesErrorMessage = null;
		if (false == sRepeat.equalsIgnoreCase("none"))
			sRepeatRulesErrorMessage = checkRepeatRules(date, sDateFormat, sRepeat); // Check repeat rules only if sRepeat is NOT none
		
		String sInterval = targetParameterValueList.getString(AlarmService.STR_INTERVAL);
		if (null == sInterval) {
			sInterval = a.mExtendedProperties.get(AlarmService.STR_INTERVAL);
			if (null != sInterval)
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, AlarmService.STR_INTERVAL, sInterval);
		}
		// if not repeat, no interval  from current timestamp, and date is present and is in past then return an error.
		if (sRepeat.equalsIgnoreCase("none") && null == sInterval && null != date && date.getTime() < new Date().getTime())	
			sRepeatRulesErrorMessage = "Cannot set alarm non-repeating alarm whose date and time is in the past";
		String sIntervalErrorMessage = null;
		if (sInterval != null) {
			Calendar c = Calendar.getInstance();
			if (null != date)
				c.setTime(date); // the interval is calculated from the date given by the caller. If there is no date, it is from current time. 
			sIntervalErrorMessage = AlarmService.checkAndSetInterval(c, sInterval);
			sDateFormat = "yyyy-MM-dd HH:mm:ss";
			Utilities.putData(targetParameterValueList, Constants.VARCHAR, AlarmService.STR_FORMAT, sDateFormat);
			SimpleDateFormat sdf = new SimpleDateFormat(sDateFormat, Cache.context.getResources().getConfiguration().locale);
			Utilities.putData(targetParameterValueList, Constants.VARCHAR, 
				AlarmService.STR_DATE, sdf.format(new Date(c.getTimeInMillis())));
		}
		String sAlarmTypeErrorMessage = null != sAlarmType ? checkAndAddAlarmType(sAlarmType) : null;
		if (null != sRepeatRulesErrorMessage || null != sIntervalErrorMessage || null != sAlarmTypeErrorMessage) {
			StringBuffer sErrorMessage = new StringBuffer();
			if (null != sRepeatRulesErrorMessage)
				sErrorMessage.append(sRepeatRulesErrorMessage);
			if (null != sIntervalErrorMessage) {
				sErrorMessage.append("\r\n"); sErrorMessage.append(sIntervalErrorMessage);
			}
			if (null != sAlarmTypeErrorMessage) {
				sErrorMessage.append("\r\n"); sErrorMessage.append(sAlarmTypeErrorMessage);
			}
			Utilities.putData(targetParameterValueList, Constants.VARCHAR, "error", sRepeatRulesErrorMessage);
        	LogManager.logError("Unable to set alarm with format " + sDateFormat + " and date " + 
        			sDate + ". Error Message:" + sRepeatRulesErrorMessage);
			return sRepeatRulesErrorMessage;
		}
		
		Utilities.putData(targetParameterValueList, Constants.VARCHAR, Constants.STR_CONTENT_DATABASE_NAME, action.sContentDbName);
		Utilities.putData(targetParameterValueList, Constants.VARCHAR, Constants.STR_SYSTEM_DATABASE_NAME, action.sSystemDbName);
		String sId = targetParameterValueList.getString("_id");
		int iAlarmId = 0;
		if (null != sId)
			iAlarmId = Integer.parseInt(sId);

		// Persist the alarm to the database and return the id.
		ContentValues valueList = new ContentValues();
		if (iAlarmId > 0)
			Utilities.putData(targetParameterValueList, Constants.VARCHAR, "_id", Integer.toString(iAlarmId));
		
		Bundle dataTypes = targetParameterValueList.getBundle("data_types");
		if (null != dataTypes)
			targetParameterValueList.remove("data_types");
		int iDataType = 0; String value = null;
		for(String key : targetParameterValueList.keySet()) {
			value = null;
			if (null != dataTypes) {
				iDataType = dataTypes.getInt(key); 
				if (iDataType == Constants.VARCHAR || iDataType == Constants.VARCHAR2 ||
					iDataType == Constants.TEXT || iDataType == Constants.CHAR ||  
					iDataType == Constants.STRING || iDataType == Constants.CLOB) 
					value = targetParameterValueList.getString(key);
				else
					continue;
				
			} else // if data types is not present just assume string
				value = targetParameterValueList.getString(key);
            if (key.equalsIgnoreCase("type"))
            	valueList.put(key, value);
// The _alarm_manager does not have the fields below.            
//            else if (key.equalsIgnoreCase(AlarmService.STR_INTERVAL))
//            	valueList.put(key, value);
            else if (key.equalsIgnoreCase(AlarmService.STR_DATE))
            	valueList.put(key, value);
            else if (key.equalsIgnoreCase(AlarmService.STR_FORMAT))
            	valueList.put(key, value);
            else if (key.equalsIgnoreCase(AlarmService.STR_REPEAT))
            	valueList.put(key, value);
// The _alarm_manager does not have the fields below.            
//            else if (key.equalsIgnoreCase("display_activity_class"))
//            	valueList.put(key, value);
//            else if (key.equalsIgnoreCase(Constants.STR_SCREEN_NAME))
//            	valueList.put(key, value);
            else if (key.equalsIgnoreCase(Constants.STR_SYSTEM_DATABASE_NAME))
            	valueList.put(key, value);
            else if (key.equalsIgnoreCase(Constants.STR_CONTENT_DATABASE_NAME))
            	valueList.put(key, value);
        }
		// Put all the targetParameterValueList into the _alarm_manager.data as data by serializing it
		HashMap<String, String> hashmap = Utilities.convertBundleToHashMap(targetParameterValueList);
		byte[] data = Utilities.serializeObject(hashmap);
    	valueList.put("data", data);
		int iNewAlarmId = (int)TableDao.replace1Row(action.sContentDbName, "_alarm_manager", valueList);
    	if (iAlarmId == 0 && iNewAlarmId != iAlarmId) { // add the new _id in the targetParameterValueList
    		hashmap.put("_id", Long.toString(iNewAlarmId));
			data = Utilities.serializeObject(hashmap);
	    	valueList.put(AlarmService.STR_DATA, data);
	    	valueList.put("_id", iNewAlarmId);
	    	valueList.put("type", targetParameterValueList.getString("type"));
	    	valueList.put(AlarmService.STR_DATE, targetParameterValueList.getString(AlarmService.STR_DATE));
	    	valueList.put(AlarmService.STR_FORMAT, targetParameterValueList.getString(AlarmService.STR_FORMAT));
	    	valueList.put(AlarmService.STR_REPEAT, targetParameterValueList.getString(AlarmService.STR_REPEAT));
			TableDao.replace1Row(action.sContentDbName, "_alarm_manager", valueList);
			Utilities.putData(targetParameterValueList, Constants.VARCHAR, "_id", Long.toString(iNewAlarmId));
    	}
		if (iNewAlarmId == 0) {
			LogManager.logError("Unable to save alarm in the database: " + action.sContentDbName);
			Utilities.putData(targetParameterValueList, Constants.VARCHAR, "error", "Unable to save alarm");
			Boolean.valueOf(false); // We have a BIG problem.
		}
		if (null != dataTypes)
			targetParameterValueList.putBundle("data_types", dataTypes);
		Intent intent = new Intent(Cache.context, AlarmService.class);
		intent.setData(Uri.parse("custom://" + action.sContentDbName.hashCode() + "/" + Integer.toString(iNewAlarmId)));
		intent.setAction(ConfigManager.getInstance().getActionClass("SET_ALARM"));
		intent.putExtra(AlarmService.STR_DATA, data);
		// do not need this. This is part of action. intent.putExtra("_id", lAlarmId);
		intent.putExtra(Constants.STR_SYSTEM_DATABASE_NAME, action.sSystemDbName);
		intent.putExtra(Constants.STR_CONTENT_DATABASE_NAME, action.sContentDbName);
	    context.startService(intent);
	    return executeCascadingActions(Integer.valueOf(iNewAlarmId), targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 

	// Now put the rules for repeat types none, daily, weekly, monthly, yearly
	private String checkRepeatRules(Date date, String sDateFormat, String sRepeat) {
		if (sRepeat == null)
			return "Repeat type not present";
		// repeat == "none" must contain a valid datetime or date and time
		if (sRepeat.equalsIgnoreCase("none") && (date == null || !sDateFormat.contains("M") ||
				!sDateFormat.contains("d") || !sDateFormat.contains("y") || 
				
				!(sDateFormat.contains("H") || 
				sDateFormat.contains("h") ||		
				sDateFormat.contains("k") ||		
				sDateFormat.contains("K")) || 
				
				!sDateFormat.contains("m")))// || 
//				!sDateFormat.contains("s")))
			return "Both Day and Time must be specified in order to set this alarm";
		else if (sRepeat.equalsIgnoreCase(AlarmService.STR_HOURLY) && (date == null ||
				!sDateFormat.contains("m")))
			return "Minute (of an hour) must be specified in order to set this alarm";
		else if (sRepeat.equalsIgnoreCase(AlarmService.STR_DAILY) && (date == null ||
				
				!(sDateFormat.contains("H") || 
				sDateFormat.contains("h") ||		
				sDateFormat.contains("k") ||		
				sDateFormat.contains("K")) || 
				
				!sDateFormat.contains("m")))
			return "Time of day must be specified in order to set this alarm";
		else if (sRepeat.equalsIgnoreCase(AlarmService.STR_WEEKLY) && (date == null || 
				!sDateFormat.contains("E") || 
				
				!(sDateFormat.contains("H")|| 
				sDateFormat.contains("h") || 		
				sDateFormat.contains("k") ||		
				sDateFormat.contains("K")) || 
				
				!sDateFormat.contains("m")))
			return "Weekday and Time must be specified in order to set this alarm";
		else if (sRepeat.equalsIgnoreCase(AlarmService.STR_MONTHLY) && (date == null || 
				!(sDateFormat.contains("d") || sDateFormat.contains("F")) ||
				
				!(sDateFormat.contains("H") || 
				sDateFormat.contains("h") ||		
				sDateFormat.contains("k") ||		
				sDateFormat.contains("K")) || 
				
				!sDateFormat.contains("m")))
			return "Day of month and Time must be specified in order to set this alarm";
		else if (sRepeat.equalsIgnoreCase(AlarmService.STR_YEARLY) && (date == null || !sDateFormat.contains("M") ||
				!sDateFormat.contains("d") ||
				
				!(sDateFormat.contains("H") || 
				sDateFormat.contains("h") ||		
				sDateFormat.contains("k") ||		
				sDateFormat.contains("K")) || 
				
				!sDateFormat.contains("m")))
			return "Day, Month and Time must be specified in order to set this alarm";
		return null;
	}
	
	private String checkAndAddAlarmType(String sAlarmType) {
		if (null == sAlarmType)
			return null;
		if (sAlarmType.equals("video") || sAlarmType.equals("audio")) {
			// No need to check for file as it is optional anyway
			// No need to check for class name as it is optional anyway.
		} else if (sAlarmType.equals("email")) {
			String to = targetParameterValueList.getString("to");
			String subject = targetParameterValueList.getString("subject");
			String body = targetParameterValueList.getString("body");
			if (null == to)
				return "To must be present";
			if (null == subject && null == body)
				return "Subject or Body must be present";
		} else if (sAlarmType.equals("phone")) {
			String phone_number = targetParameterValueList.getString("phone_number");
			if (null == phone_number)
				return "Phone Number must be present";
		}
		String sClassName = targetParameterValueList.getString("display_activity_class");
		if (null == sClassName) {
			sClassName = action.mExtendedProperties.get("display_activity_class");
			if (null != sClassName)
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "display_activity_class", sClassName);
		}
			
		if (sClassName != null && sClassName.length() > 0 && false == sClassName.equalsIgnoreCase("alert") &&
				false == sClassName.equalsIgnoreCase("dialog")) {
			try {
				Class.forName (sClassName);
				return null;
			} catch (ClassNotFoundException exception) {
				String sErrorString = "The following class does not exists" + sClassName;
				LogManager.logWTF(exception, sErrorString);
				return sErrorString;
			}
		} 
		return null;
	}	
}
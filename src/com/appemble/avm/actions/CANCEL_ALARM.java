/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.LogManager;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.dao.TableDao;
import com.appemble.avm.services.AlarmService;

public class CANCEL_ALARM extends ActionBase {	
	public CANCEL_ALARM() {}
	
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
		super.initialize(ctx, v, pView, a, tplv);
		boolean bReturn = true;		
		if(null == targetParameterValueList || targetParameterValueList.size() == 0)
			return Boolean.FALSE;
		String sId = targetParameterValueList.getString("_id");
		if (null == sId || (sId = sId.trim()).length() == 0) {
			LogManager.logError("Failed to execute CANCEL_ALARM. The parameter _id is missing in the target parameter list.");
			return Boolean.FALSE;
		}
		
		// now call the service to cancel the alarm.
		Intent intent = new Intent(Cache.context, AlarmService.class);
		// remove from the db
		if ((sId.equalsIgnoreCase("all") && 
			TableDao.delete(action.sContentDbName, "_alarm_manager", null, null) <= 0)) {
			bReturn = false;
			LogManager.logError("Database does not contain any alarms.");
		}
		else {
			try {
				int iId = Integer.parseInt(sId);
				if (iId > 0) {
					String[] params = new String[] { String.valueOf(sId) };
					if (TableDao.delete(action.sContentDbName, "_alarm_manager", "_id = ?", params) <= 0) {
						bReturn = false;
						LogManager.logError("Unable to delete an alarm from the database. Id: " + sId);
					}
				}
				intent.setData(Uri.parse("custom://" + action.sContentDbName.hashCode() + "/" + sId));
			} catch(NumberFormatException nfe) {
				LogManager.logError("Unable to parse id" + sId);
				bReturn = false;
			}
		}
			
		intent.setAction(ConfigManager.getInstance().getActionClass("CANCEL_ALARM"));
		intent.putExtra("db_name", action.sContentDbName);
	    context.startService(intent);
	    // call cascading actions.
		return executeCascadingActions(bReturn ? Boolean.TRUE : Boolean.FALSE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.R;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.ActivityData;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleDialog;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ActionModel;

public class CLOSE_DIALOG extends ActionBase {
	public CLOSE_DIALOG() {}
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
		super.initialize(ctx, v, pView, a, tplv);
		
		if (parentView instanceof DynamicLayout) {
			AppembleDialog dialog = (AppembleDialog)parentView.getTag(R.integer.dialog);
			targetParameterValueList = (Bundle)parentView.getTag(R.integer.targetParameterValueList);
			// ScreenModel screenModel = (ScreenModel)parentView.getTag(R.integer.screenModel);
			if (null != dialog) {
				// allow re-orientation if it was allowed after the AppembleDialog closes.
				Context context = dialog.getOwnerActivity();
				AppembleActivity appembleActivity = Utilities.getActivity(context, null, null);
				if (null != appembleActivity && appembleActivity instanceof Activity) {
					ActivityData activityData = appembleActivity.getActivityData();
					if (null != activityData && null != activityData.screenModel &&
							activityData.screenModel.iAllowedLayouts == Configuration.ORIENTATION_UNDEFINED &&
							activityData.screenModel.bAllowReorientation) {
						((Activity)appembleActivity).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
					}
				    ActionModel[] actions = dialog.mActivityData.screenModel.getActions(Constants.ON_DESTROY_SCREEN);
				    for (int i = 0; null != actions && i < actions.length; i++) {
				    	actions[i].mScreenObject = dialog;
				    	Action.callAction(dialog.getOwnerActivity(), null, parentView, actions[i], dialog.mActivityData.targetParameterValueList);
				    }
				}
				((Activity)context).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
				dialog.dismiss();
			}
		}
		if (null == action)
			return Boolean.valueOf(false);
	    return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, 
//				targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}

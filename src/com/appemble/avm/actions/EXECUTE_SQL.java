/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.dao.TableDao;

public class EXECUTE_SQL extends ActionBase {	
	public EXECUTE_SQL() {}
	
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
		super.initialize(ctx, v, pView, a, tplv);
		if (action.sTarget == null) {
			LogManager.logError("Target must be present for this action EXECUTE_SQL");
			return Boolean.valueOf(false);
		}

		boolean bReturn = true;
		String sQuery = Utilities.replaceParameterisedStrings(action.sTarget, targetParameterValueList, 
				action.sContentDbName, Constants.SQL);
		Object object = TableDao.executeSQL(action.sContentDbName, sQuery, null);
		if (object instanceof Cursor) {
			((Cursor)object).close(); 
		} else if (object instanceof Integer)
			targetParameterValueList.putInt("result", (Integer)object);
		else if (object instanceof String) {
			LogManager.logError("The query " + sQuery + " resulted in error: " + (String)object);
			bReturn = false;
		}
		// call any actions whose preceding action is this EXECUTE_SQL. Future actions will not be executed if 
		// an action return FALSE.
		
		return executeCascadingActions(bReturn, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bReturn, Bundle targetParameterValueList) {
//		if (false == bReturn)
//			return Boolean.valueOf(bReturn);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.AppembleDialog;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;

public class REFRESH extends ActionBase {
	public REFRESH () {}
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity) {
			LogManager.logError(Constants.MISSING_ACTIVITY);
			return Boolean.valueOf(false);
		}
		if (appembleActivity instanceof AppembleDialog && null != a.sTarget && "parent".equals(a.sTarget)) {
			Activity activity = ((AppembleDialog)appembleActivity).getOwnerActivity();
			if (activity instanceof AppembleActivity) {
				 appembleActivity = (AppembleActivity)activity;
				 a.sTarget = null;
			}
		}
		return refresh(appembleActivity);
		
	}
	
	public Object execute(AppembleActivity appembleActivity, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(appembleActivity.getContext(), v, pView, a, tpvl);
		return refresh(appembleActivity);
	}
	
	private Object refresh (AppembleActivity appembleActivity) {
		boolean bRemoteDataAlso = false;
		initializeAction();
		if (action.sTarget == null) { // Refresh all controls with local data and remote data
			boolean bIsRemoteDataPresent = AppembleActivityImpl.isRemoteDataPresent(appembleActivity.getScreenModel(), 
					appembleActivity.getAllControls());
			// Fetch remote data in the background thread and then use UI thread to update the UI
		    AppembleActivityImpl.deparameterizeDataSources(appembleActivity.getScreenModel(), 
		    		appembleActivity.getAllControls(), appembleActivity.getTargetParameterValueList(),
		    		action.sContentDbName); // substitute <parameter> with value in targetParameterValueList
			if (bIsRemoteDataPresent && bRemoteDataAlso) {
				AppembleActivityImpl.updateRemoteData(appembleActivity, null, true, this, 
						appembleActivity.getNetworkAvailableFlag(), appembleActivity.getActivityCreateStatus());
				return Boolean.valueOf(false); // do not execute cascading actions. they will be execute 
			}
			else {
				AppembleActivityImpl.fetchLocalData(appembleActivity, null, true); 
				// paint the screen with local data. It is part of lazy loading...
				if (false == AppembleActivityImpl.update(appembleActivity, null, null))
					return Boolean.valueOf(false); // if the function return 0 means there is an error while executing the function.
			}
		} else {				
			String[] controlNames = action.sTarget.split(",");
			for(int i = 0; null != controlNames && i < controlNames.length && controlNames[i] != null; i++) {
				ControlModel controlModel = Action.getControl(appembleActivity.getRootLayout(), 
					appembleActivity.getAllControls(), controlNames[i].trim(), Constants.FROM_NAME);
				if (null == controlModel) {
					LogManager.logError("Cannot refresh control with name= " + controlNames[i].trim());
					continue;
				}
				if (null != controlModel.sLocalDataSource)
					controlModel.sDeparameterizedLocalDataSource = (Utilities.replaceParameterisedStrings(
							controlModel.sLocalDataSource, appembleActivity.getTargetParameterValueList(), 
							action.sContentDbName, Constants.SQL));
				if (controlModel.sRemoteDataSource != null && bRemoteDataAlso) {
					if (null != controlModel.sRemoteDataSource)
						controlModel.sDeparameterizedRemoteDataSource = (Utilities.replaceParameterisedStrings(
								controlModel.sRemoteDataSource, appembleActivity.getTargetParameterValueList(), 
								action.sContentDbName, Constants.URL));
					AppembleActivityImpl.updateRemoteData(appembleActivity, controlModel, true, this,
						appembleActivity.getNetworkAvailableFlag(), appembleActivity.getActivityCreateStatus());
				}
				else {
					AppembleActivityImpl.fetchLocalData(appembleActivity, controlModel, true); 
					// paint the screen with local data. It is part of lazy loading...
					if (false == AppembleActivityImpl.update(appembleActivity, controlModel, null))
						return Boolean.valueOf(false); // if the function return 0 means there is an error while executing the function.
				}
			}
		}
// commented because data might be deleted. In this case, the update control should happen
//				if (appembleActivity.isLocalDataAvailable(controlModel, true))
//			AppembleActivityImpl.fetchLocalData(appembleActivity, controlModel, true);
//			AppembleActivityImpl.update(appembleActivity, controlModel, appembleActivity.getTargetParameterValueList());
	    return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
	}
	
//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
	
	private void initializeAction() {
		if (null == action)
			return;
					
		if (action.mExtendedProperties == null)
			action.mExtendedProperties = new HashMap<String, String>();
		if (false == action.mExtendedProperties.containsKey("remote_data_also"))
			action.mExtendedProperties.put("remote_data_also", "true");
	}
}

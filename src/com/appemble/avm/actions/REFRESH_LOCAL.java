/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;

public class REFRESH_LOCAL extends ActionBase {
	public REFRESH_LOCAL () {}
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
			
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity) {
			LogManager.logError(Constants.MISSING_ACTIVITY);
			return Boolean.valueOf(false);
		}
		
		if (action.sTarget == null) {
			// Refresh all controls with local data
//			if (appembleActivity.isLocalDataAvailable(null, true)) // Data might be made null as a result of a delete. Still refresh it. 
			AppembleActivityImpl.fetchLocalData(appembleActivity, null, true); 
			AppembleActivityImpl.update(appembleActivity, null, null);
			return executeCascadingActions(true, targetParameterValueList);
		}
		
		String[] controlNames = action.sTarget.split(",");
		if (null == controlNames || controlNames.length == 0)
			return Boolean.valueOf(false);
		// Deparameterize the local data sources as they might have changes. That is why the refresh_local is being called.
		AppembleActivityImpl.deparameterizeDataSources(appembleActivity.getScreenModel(), appembleActivity.getAllControls(), 
				appembleActivity.getTargetParameterValueList(), action.sContentDbName); // substitute <parameter> with value in targetParameterValueList
		for(int i = 0; null != controlNames && i < controlNames.length; i++) {
			ControlModel controlModel = Action.getControl(appembleActivity.getRootLayout(), 
				appembleActivity.getAllControls(), controlNames[i], Constants.FROM_NAME);
// commented because data might be deleted. In this case, the update control should happen
//			if (appembleActivity.isLocalDataAvailable(controlModel, true))
			AppembleActivityImpl.fetchLocalData(appembleActivity, controlModel, true);
			AppembleActivityImpl.update(appembleActivity, controlModel, null);
		}
		// trigger the next action.
		return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}

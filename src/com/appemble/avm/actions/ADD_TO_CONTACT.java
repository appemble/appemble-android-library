/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.RawContacts;
import android.view.View;
import android.widget.Toast;

import com.appemble.avm.LogManager;
import com.appemble.avm.models.ActionModel;

public class ADD_TO_CONTACT  extends ActionBase {
	public ADD_TO_CONTACT() {}
			
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
		super.initialize(ctx, v, pView, a, tplv);
		
    	ContentResolver cr = context.getContentResolver();
		if (null == cr)
			return Boolean.valueOf(false);
		ArrayList < ContentProviderOperation > ops = new ArrayList < ContentProviderOperation > ();
		Cursor contactListCursor = null;
		String sDisplayName = targetParameterValueList.getString("display_name");
        String where = null;
	    String[] params = null;
		if (null != sDisplayName && sDisplayName.length() > 0) {
	        where = ContactsContract.Data.DISPLAY_NAME + " = ?";
	        params = new String[] { sDisplayName };
		    contactListCursor = cr.query(ContactsContract.Data.CONTENT_URI, 
		    		new String[]{RawContacts._ID}, where, params, null);
		}

	    if ((null == contactListCursor) || contactListCursor.getCount() == 0) {
			ops.add(ContentProviderOperation.newInsert(
					ContactsContract.RawContacts.CONTENT_URI)
				.withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
				.withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
				.build());
		    addFields(ops);
	    } else if (null != where && null != params) {
//		    ContentProviderOperation.Builder builder = 
//		    		ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI);
//		    updateFields(builder);
//        	builder.withSelection(where, params);
//	    	ops.add(builder.build());
	    	if (contactListCursor.moveToNext()) {
		    	int iRawId = contactListCursor.getInt(0);
//		        where = ContactsContract.Data.CONTACT_ID + " = ?";
//		        params = new String[] { ""+iRawId };
//	        	ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
//	        	    .withSelection(where, params)
//	        	    .withValue(ContactsContract.Data.RAW_CONTACT_ID, iRawId)
//	        	    .build());
		    	updateFields(ops, iRawId);
	    	}
	    }
	    
	    if (null != contactListCursor)
	    	contactListCursor.close();
    
	    
		 // Asking the Contact provider to create a new contact                 
        try {
//        	ContentProviderResult[] crr = 
        			cr.applyBatch(ContactsContract.AUTHORITY, ops);
        	
		    Toast.makeText(context, "Added the contact", Toast.LENGTH_SHORT).show();
		} catch (RemoteException e) {
			LogManager.logWTF(e, "Unable to add contacts.");
		} catch (OperationApplicationException e) {
			LogManager.logWTF(e, "Please add user permission WRITE_CONTACTS in your AnrdoidManifest.xml");
		}
		
		return executeCascadingActions(true, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 

	void addFields(ArrayList <ContentProviderOperation> ops) {
		// Structured Names
		addField(ops, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredName.PREFIX, "prefix_name");
		addField(ops, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, "given_name");
		addField(ops, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME, "family_name");
		addField(ops, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "display_name");
		addField(ops, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME, "middle_name");
		addField(ops, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredName.SUFFIX, "suffix_name");
		// Phones
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "home_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "mobile_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "work_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "work_fax_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_FAX_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "home_fax_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_FAX_HOME);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "pager_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_PAGER);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "other_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "callback_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CALLBACK);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "car_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_CAR);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "company_main_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_COMPANY_MAIN);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "isdn_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_ISDN);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "main_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MAIN);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "other_fax_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_OTHER_FAX);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "radio_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_RADIO);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "telex_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_TELEX);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "tty_tdd_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_TTY_TDD);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "work_mobile_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK_MOBILE);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "work_pager_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK_PAGER);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "assistant_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_ASSISTANT);
		addField(ops, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "mms_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MMS);
		// Emails
		addField(ops, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Email.DATA, "custom_email",
				ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Email.DATA, "home_email",
				ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_HOME);
		addField(ops, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Email.DATA, "work_email",
				ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Email.DATA, "other_email",
				ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_OTHER);
		// Addresses
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.POBOX, "po_box_home_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_HOME);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.STREET, "street_home_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_HOME);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.CITY, "city_home_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_HOME);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.REGION, "state_home_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_HOME);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY, "country_home_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_HOME);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.POBOX, "po_box_work_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.STREET, "street_work_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.CITY, "city_work_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.REGION, "state_work_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY, "country_work_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE,
				ContactsContract.CommonDataKinds.StructuredPostal.POBOX, "po_box_other_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.STREET, "street_other_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.CITY, "city_other_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.REGION, "state_other_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY, "country_other_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE,
				ContactsContract.CommonDataKinds.StructuredPostal.POBOX, "po_box_custom_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.STREET, "street_custom_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.CITY, "city_custom_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.REGION, "state_custom_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY, "country_custom_address",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		// Organization
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.COMPANY, "company_name_work",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.TITLE, "title_at_work",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.DEPARTMENT, "department_work",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION, "job_description_work",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION, "office_location_work",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.COMPANY, "company_name_other",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.TITLE, "title_at_other",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.DEPARTMENT, "department_other",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION, "job_description_other",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION, "office_location_other",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_OTHER);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.COMPANY, "company_name_custom",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.TITLE, "title_at_custom",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.DEPARTMENT, "department_custom",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION, "job_description_custom",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION, "office_location_custom",
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE, 
				ContactsContract.CommonDataKinds.StructuredPostal.TYPE_CUSTOM);
		// Dates
		addField(ops, ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Event.START_DATE, "anniversary_date",
				ContactsContract.CommonDataKinds.Event.TYPE, 
				ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY);
		addField(ops, ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Event.START_DATE, "birthday_date",
				ContactsContract.CommonDataKinds.Event.TYPE, 
				ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY);
		addField(ops, ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Event.START_DATE, "custom_date",
				ContactsContract.CommonDataKinds.Event.TYPE, 
				ContactsContract.CommonDataKinds.Event.TYPE_CUSTOM);
		addField(ops, ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Event.START_DATE, "other_date",
				ContactsContract.CommonDataKinds.Event.TYPE, 
				ContactsContract.CommonDataKinds.Event.TYPE_OTHER);
		// URL's
		addField(ops, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Website.DATA, "homepage_url",
				ContactsContract.CommonDataKinds.Website.TYPE, 
				ContactsContract.CommonDataKinds.Website.TYPE_HOMEPAGE);
		addField(ops, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Website.DATA, "blog_url",
				ContactsContract.CommonDataKinds.Website.TYPE, 
				ContactsContract.CommonDataKinds.Website.TYPE_BLOG);
		addField(ops, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Website.DATA, "profile_url",
				ContactsContract.CommonDataKinds.Website.TYPE, 
				ContactsContract.CommonDataKinds.Website.TYPE_PROFILE);
		addField(ops, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Website.DATA, "home_url",
				ContactsContract.CommonDataKinds.Website.TYPE, 
				ContactsContract.CommonDataKinds.Website.TYPE_HOME);
		addField(ops, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Website.DATA, "work_url",
				ContactsContract.CommonDataKinds.Website.TYPE, 
				ContactsContract.CommonDataKinds.Website.TYPE_WORK);
		addField(ops, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Website.DATA, "ftp_url",
				ContactsContract.CommonDataKinds.Website.TYPE, 
				ContactsContract.CommonDataKinds.Website.TYPE_FTP);
		addField(ops, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Website.DATA, "other_url",
				ContactsContract.CommonDataKinds.Website.TYPE, 
				ContactsContract.CommonDataKinds.Website.TYPE_OTHER);		
	}
	
	void updateFields(ArrayList <ContentProviderOperation> ops, int iRawId) {
		// Structured Names
		updateField(ops, iRawId, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, "display_name");
		updateField(ops, iRawId, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE, 
				ContactsContract.CommonDataKinds.Phone.NUMBER, "home_phone",
				ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME);
	}
	
	void addField(ArrayList <ContentProviderOperation> ops, String sItemType,
			String sFieldName, String sInputFieldName) {
		addField(ops, sItemType, sFieldName, sInputFieldName, null, 0);
	}
	
	void updateField(ArrayList <ContentProviderOperation> ops, int iRawId, String sItemType,
			String sFieldName, String sInputFieldName) {
		updateField(ops, iRawId, sItemType, sFieldName, sInputFieldName, null, 0);
	}
	
	void addField(ArrayList <ContentProviderOperation> ops, String sMimeType,
			String sContactsContractFieldName, String sAttribute, String sDataType, int iType) {
		if (targetParameterValueList.containsKey(sAttribute)) {
		    ContentProviderOperation.Builder builder = 
	    		ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
		        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
		        .withValue(ContactsContract.Data.MIMETYPE, sMimeType);

		    if (null != sDataType)
		    	builder.withValue(sDataType, iType);
		    
	        builder.withValue(sContactsContractFieldName, targetParameterValueList.getString(sAttribute));
	        ops.add(builder.build());
		}
	}

	void updateField(ArrayList <ContentProviderOperation> ops, int iRawId, String sMimeType,
			String sContactsContractFieldName, String sAttribute, String sDataType, int iType) {
		if (targetParameterValueList.containsKey(sAttribute)) {
			String sWhere = ContactsContract.Data.RAW_CONTACT_ID + "=? AND "
				+ ContactsContract.Data.MIMETYPE + "=? ";
			if (null != sDataType)
				sWhere += " AND " + sDataType + " = ?";  
			String[] params = null;
			if (null != sDataType)
				params = new String[] { ""+iRawId, sMimeType, ""+ iType };
			else
				params = new String[] { ""+iRawId, sMimeType }; 
//			String sWhere = ContactsContract.Data.RAW_CONTACT_ID + "=? ";
//			String[] params = null;
//			params = new String[] { ""+iRawId }; 
		    ContentProviderOperation.Builder builder = 
//		    		ContentProviderOperation.newDelete(ContactsContract.Data.CONTENT_URI)
//			        .withSelection(sWhere, params);
//		        ops.add(builder.build());
//		    builder = 
//		    		ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
//			        .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
//			        .withValue(ContactsContract.Data.MIMETYPE, sMimeType)
//			        .withValue(sContactsContractFieldName, targetParameterValueList.getString(sAttribute));
		    		ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
			        .withSelection(sWhere, params)
//			        .withValue(ContactsContract.Data.MIMETYPE, sMimeType)
			        .withValue(sContactsContractFieldName, targetParameterValueList.getString(sAttribute));
//			    if (null != sDataType)
//			    	builder.withValue(sDataType, iType);		    
		        ops.add(builder.build());
		}
	}

	/*
	public Object executeCascadingActions(boolean bExecuteResults) {
		if (false == bExecuteResults)
			return Boolean.valueOf(bExecuteResults);
		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
    	if (object instanceof Boolean)
    		return (Boolean)object;
    	else
    		return Boolean.valueOf(false);
	} 

    private void createContact() {
    	ContentResolver cr = context.getContentResolver();
    	
    	Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
    	String name = targetParameterValueList.get("display_name");
    	
    	if (cur.getCount() > 0) {
        	while (cur.moveToNext()) {
        		String existName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        		if (existName.contains(name)) {
                	updateContact();
                	return;        			
        		}
        	}
    	}
    	
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ops.add(ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, "accountname@gmail.com")
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, "com.google")
                .build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                .build());
        ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, targetParameterValueList.get("home_phone"))
                .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                .build());

        
        try {
			cr.applyBatch(ContactsContract.AUTHORITY, ops);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			LogManager.logWTF(e);
		} catch (OperationApplicationException e) {
			// TODO Auto-generated catch block
			LogManager.logWTF(e);
		}

//    	Toast.makeText(NativeContentProvider.this, "Created a new contact with name: " + name + " and Phone No: " + phone, Toast.LENGTH_SHORT).show();
    	
    }
    
    private void updateContact() {
    	ContentResolver cr = context.getContentResolver();
 
        String where = ContactsContract.Data.DISPLAY_NAME + " = ? AND " + 
        			ContactsContract.Data.MIMETYPE + " = ? AND " +
        			String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE) + " = ? ";
        String[] params = new String[] {targetParameterValueList.get("display_name"),
        		ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
        		String.valueOf(ContactsContract.CommonDataKinds.Phone.TYPE_HOME)};

        Cursor phoneCur = cr.query(ContactsContract.Data.CONTENT_URI, null, where, params, null);
        
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        
        if ( (null == phoneCur)  ) {
        	createContact();
        } else {
        	ops.add(ContentProviderOperation.newUpdate(ContactsContract.Data.CONTENT_URI)
        	        .withSelection(where, params)
        	        .withValue(ContactsContract.CommonDataKinds.Phone.DATA, targetParameterValueList.get("home_phone"))
        	        .build());
        }
        
        phoneCur.close();
        
        try {
			cr.applyBatch(ContactsContract.AUTHORITY, ops);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			LogManager.logWTF(e);
		} catch (OperationApplicationException e) {
			// TODO Auto-generated catch block
			LogManager.logWTF(e);
		}

//		Toast.makeText(NativeContentProvider.this, "Updated the phone number of 'Sample Name' to: " + phone, Toast.LENGTH_SHORT).show();
    }
    
    private void deleteContact(String name) {

    	ContentResolver cr = context.getContentResolver();
    	String where = ContactsContract.Data.DISPLAY_NAME + " = ? ";
    	String[] params = new String[] {name};
    
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
        ops.add(ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI)
    	        .withSelection(where, params)
    	        .build());
        try {
			cr.applyBatch(ContactsContract.AUTHORITY, ops);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			LogManager.logWTF(e);
		} catch (OperationApplicationException e) {
			// TODO Auto-generated catch block
			LogManager.logWTF(e);
		}

//		Toast.makeText(NativeContentProvider.this, "Deleted the contact with name '" + name +"'", Toast.LENGTH_SHORT).show();
  
    }
*/
}

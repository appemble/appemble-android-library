/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;

import com.appemble.avm.Cache;
import com.appemble.avm.LogManager;
import com.appemble.avm.models.ActionModel;

public class FUNCTION extends ActionBase {
	public FUNCTION() {}
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		boolean bReturn = true;
		if (action.sTarget == null)
			return Boolean.valueOf(false);
		
		// Make dynamic function call based on file
		String sFunctionPackage = null;
		Object oReturnObject = null;
		try {
			if (action.sTarget.startsWith("com"))
				sFunctionPackage = action.sTarget;
			else {
				sFunctionPackage = Cache.bootstrap.getValue("function_package_name");
				if (null == sFunctionPackage) {
					LogManager.logError("Error in executing custom function. Please define function_package_name in res/values/strings.xml");
					return Boolean.valueOf(false);
				}
				sFunctionPackage += "." + action.sTarget;
			}
			Class<?> cls = Class.forName(sFunctionPackage);
			if (null == cls) {
				LogManager.logError("Unable to find the FUNCTION: " + sFunctionPackage);
				return Boolean.valueOf(false);
			}
			CustomFunctionInterface ai = (CustomFunctionInterface) cls.getDeclaredConstructor().newInstance();
			if (null != ai) {
				oReturnObject = ai.execute(context, view, parentView, action, targetParameterValueList);
				if (oReturnObject instanceof Boolean)
					bReturn = ((Boolean)oReturnObject).booleanValue();
			}
			// call the cascading action
			// call any actions whose preceding action is this CALL_URL
			return executeCascadingActions(oReturnObject, targetParameterValueList);
		} catch (ClassNotFoundException cnfe) {
			LogManager.logWTF(cnfe, "Error in executing custom function. Cannot find class: " + sFunctionPackage);
		} catch (ClassCastException cce) {
			LogManager.logWTF(cce, "Error in executing custom function. Your custom function's class definition must implement CustomInterface: " + sFunctionPackage);
		} catch (Exception e) {
			LogManager.logWTF(e, "Error in executing custom function.");
			Looper mainLooper = Looper.getMainLooper();
			if ((null != context || null != view || null != parentView) && null != mainLooper) {
				action.sTarget = "Alert";
				action.setActionName("ALERT");
				Action.callAction(context, view, parentView, action, targetParameterValueList);
			}
			return Boolean.FALSE;
		}
		return Boolean.valueOf(bReturn);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}

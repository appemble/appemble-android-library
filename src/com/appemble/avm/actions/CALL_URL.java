/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.util.HashMap;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.models.ActionModel;

public class CALL_URL extends ActionBase {
	public CALL_URL() {}

	// same as CALL_GET_URL
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
		super.initialize(ctx, v, pView, a, tplv);
		boolean bReturn = true;
		if (null == action)
			return Boolean.valueOf(false);
		initializeAction();
		
		if (action.sTarget == null) {
			LogManager.logError("While executing action: " + action.getActionName() + 
					", the target MUST be present.");
			return Boolean.valueOf(false);
		}
		AppembleActivity dynamicActivity = Utilities.getActivity(context, view, parentView);
		if (null == dynamicActivity) {
			LogManager.logError("While executing action: " + action.getActionName() + 
				", unable to find parent activity. Please check the control on which this action is attached to.");
			return Boolean.valueOf(false);
		}
		String sDeparameterizedRemoteDataSource;
		sDeparameterizedRemoteDataSource = Utilities.replaceParameterisedStrings(
				action.sTarget, targetParameterValueList, action.sContentDbName, Constants.URL);
		Bundle results = new Bundle();
		StringBuffer sErrorMsg = new StringBuffer();
		
		boolean bRemoteDataSaveLocally = Utilities.parseBoolean(
				action.getExtendedProperty("remote_data_save_locally"));
		boolean bRunInBackground = Utilities.parseBoolean(
				action.getExtendedProperty("run_in_background"));
		int iRequestType = Constants.getRequestType(action.getExtendedProperty("remote_request_type"));
		boolean bUpdateScreen = Utilities.parseBoolean(action.getExtendedProperty("update_screen"));
		int iRemoteDataFormat = Constants.getRemoteDataFormat(
				action.getExtendedProperty("remote_data_format"));
		if (bRunInBackground) {
			Action.setViewState(view, action, Constants.DISABLE); // Disable the view
			AppembleActivityImpl.getRemoteData(dynamicActivity, action.sTarget, sDeparameterizedRemoteDataSource, 
					iRequestType, results, sErrorMsg, null, true, bUpdateScreen, this, bRemoteDataSaveLocally,
					iRemoteDataFormat);
	
			// call any actions whose preceding action is this CALL_URL
		} else {
			// show the progress bar
			//dynamicActivity.progressBar(Constants.SHOW);
			// execute the URL
			Bundle mRemoteData = new Bundle();
			String sErrorMessage = AppembleActivityImpl.getData(dynamicActivity, action.sTarget, 
					sDeparameterizedRemoteDataSource, iRequestType, mRemoteData, action.sContentDbName,
					dynamicActivity.getActivityCreateStatus(), bRemoteDataSaveLocally, iRemoteDataFormat); 
			// hide the progress bar
			//dynamicActivity.progressBar(Constants.HIDE);
			// post processing
			if (null == sErrorMessage || sErrorMessage.length() == 0) {
				AppembleActivityImpl.fetchLocalData(dynamicActivity, null, true); // Have to be in post execute so that if multiple threads are created for fetchinig the data, they are serailized by the UI thread to update the screen data.
				// even if the local data is not available, we may have to update the screen with the remote data directly.
				AppembleActivityImpl.update(dynamicActivity, null, mRemoteData);
				// call the cascading actions after merging the mRemoteData to targetParameterValueList TODO
				Utilities.merge(targetParameterValueList, mRemoteData);
				return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
			} else if (dynamicActivity.isDestroyed() == false) {
				if (targetParameterValueList != null)
					targetParameterValueList.putString("error", sErrorMessage);
				bReturn = Boolean.valueOf(false);
			}
		}
		return Boolean.valueOf(bReturn);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
	
	private void initializeAction() {
		if (null == action)
			return;
		if (null == action.mExtendedProperties)
			action.mExtendedProperties = new HashMap<String, String>();
		if (false == action.mExtendedProperties.containsKey("remote_request_type"))
			action.mExtendedProperties.put("remote_request_type", "GET");
		if (false == action.mExtendedProperties.containsKey("remote_data_save_locally"))
			action.mExtendedProperties.put("remote_data_save_locally", "true");
		if (false == action.mExtendedProperties.containsKey("remote_data_format"))
			action.mExtendedProperties.put("remote_data_format", "JSON");
		if (false == action.mExtendedProperties.containsKey("run_in_background"))
			action.mExtendedProperties.put("run_in_background", "true");
		if (false == action.mExtendedProperties.containsKey("update_screen"))
			action.mExtendedProperties.put("update_screen", "true");
	}
}
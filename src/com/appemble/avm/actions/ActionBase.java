/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.models.ActionModel;

abstract public class ActionBase {
	public Context context;
	public ActionModel action;
	public View parentView;
	public View view;
	public Bundle targetParameterValueList;
	
	public void initialize(Context context, View view, View parentView, 
			ActionModel action, Bundle targetParameterValueList) {
		this.context = context;
		this.action = action;
		this.parentView = parentView;
		this.view = view;		
		this.targetParameterValueList = targetParameterValueList;
	}
	
	public abstract Object execute(final Context ctx, View view, View parentView, final ActionModel action, 
			Bundle targetParameterValueList);
	
	public Object executeCascadingActions(Object oReturnObject, Bundle targetParameterValueList) {
		if (oReturnObject instanceof Boolean && false == ((Boolean)oReturnObject))
			return oReturnObject;
		return Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false, oReturnObject);
	} 
//	public Object executeCascadingActions(Object oReturnObject, Bundle targetParameterValueList); 
}
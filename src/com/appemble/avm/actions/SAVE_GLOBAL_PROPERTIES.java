/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.dao.TableDao;

public class SAVE_GLOBAL_PROPERTIES extends ActionBase {
	public SAVE_GLOBAL_PROPERTIES() {}
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		if (null == targetParameterValueList || targetParameterValueList.size() == 0)
			Boolean.valueOf(false);
		
		
		boolean bReturn = true;
		long lRowId = -1;
		for (String key : targetParameterValueList.keySet()) {
			if (null == key)
				continue;
			ContentValues cv = new ContentValues();
			if (key.equals("data_types")) // skip the data_Types in the bundle.
				continue;
			cv.put("key", key);
			cv.put("value", targetParameterValueList.getString(key));
			lRowId = TableDao.replace1Row(action.sContentDbName, Constants.STR_PROPERTIES_TABLE, cv);
			if (lRowId < 0) {
				LogManager.logError("In action SAVE_GLOBAL_PROPERTIES, unable to save the parameter: " + key);
				bReturn = false;
			}
		}
		return executeCascadingActions(bReturn?Boolean.TRUE:Boolean.FALSE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}
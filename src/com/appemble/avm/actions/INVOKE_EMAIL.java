/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;

import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ActionModel;

public class INVOKE_EMAIL extends ActionBase {	
	public INVOKE_EMAIL() {}
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		
		String mailToAddress = null;
		
		if(null == targetParameterValueList || targetParameterValueList.size() == 0)
			return Boolean.valueOf(false);
		
//		mailToAddress = targetParameterValueList.getString("to");
//		if (null == mailToAddress) {
//			LogManager.logError("INVOKE_MAIL action needs a value for \"to\""); // TODO Put an error log here. This is a error.
//			return Boolean.valueOf(false);
//		}
		
		// see if there is a file attachment.
		ArrayList<Uri> uris = new ArrayList<Uri>();
	    //convert from paths to Android friendly Parcelable Uri's
	    Set<String> keys = targetParameterValueList.keySet();
	    for(String key: keys) {
            if (key.startsWith("file")) {
            	String sFileName = targetParameterValueList.getString(key);
            	if (null == sFileName)
            		continue;
    	        File fileIn = new File(targetParameterValueList.getString(key));
    	        if (false == fileIn.exists()) {
    	        	LogManager.logError("Cannnot read file " + targetParameterValueList.getString(key) + 
    	        			". Please check the path and file name.");
    	        	continue;
    	        }
    	        Uri u = Uri.fromFile(fileIn);
    	        uris.add(u);
            }
	    }

	    Intent intent = new Intent(uris.size() > 0 ? Intent.ACTION_SEND_MULTIPLE : Intent.ACTION_SEND);
	
		String sIntentType = targetParameterValueList.getString("type");
		intent.setType(sIntentType != null ? sIntentType : "text/plain");
		 
		mailToAddress = targetParameterValueList.getString("to");
		if(mailToAddress!=null){
			String[] to = mailToAddress.split(",");
			intent.putExtra(android.content.Intent.EXTRA_EMAIL, to);
		}
		
		String cc = targetParameterValueList.getString("cc");
		if (null != cc) {
			String[] ccs = cc.split(",");
			intent.putExtra(android.content.Intent.EXTRA_CC, ccs);
		}
		
		String bcc = targetParameterValueList.getString("bcc");
		if (null != bcc) {
			String[] bccs = bcc.split(",");
			intent.putExtra(android.content.Intent.EXTRA_BCC, bccs);
		}

		String mailSubject = targetParameterValueList.getString("subject");
		if(mailSubject!=null)
			intent.putExtra(android.content.Intent.EXTRA_SUBJECT, mailSubject);
		 
		String mailBodyText = targetParameterValueList.getString("body");
		if(mailBodyText!=null) {
//			String sLowerCaseMailBodyText = mailBodyText.toLowerCase();
			if (Utilities.parseBoolean(targetParameterValueList.getString("fromHtml")))
				intent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(mailBodyText));
			else
				intent.putExtra(android.content.Intent.EXTRA_TEXT, mailBodyText);
		}
		if (uris.size() > 0)
			intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
		context.startActivity(Intent.createChooser(intent, "Select an email client:"));
		return Boolean.TRUE;
	}
}
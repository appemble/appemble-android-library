/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.app.Activity;
import android.app.TabActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TabHost;

import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ScreenDao;

public class SHOW_TAB extends ActionBase {	
	public SHOW_TAB() {}
	
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		if (action.sTarget == null)
			return Boolean.valueOf(false);
		
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity || null == action.sTarget || 0 == action.sTarget.length())
			return Boolean.valueOf(false);
		boolean bReturn = true;
		Activity activity = appembleActivity.getMyParent();
		try {
			TabActivity tabs = (TabActivity) activity;
			ScreenModel screen = ScreenDao.getScreenByName(action.sSystemDbName, action.sContentDbName, action.sTarget);
			TabHost tabHost = tabs.getTabHost(); // The activity TabHost
			tabHost.setCurrentTab(screen.iMenuOrder);
		} catch (ClassCastException cce) {
			LogManager.logWTF(cce, "Check action SHOW_TAB as the target screen does not have a tab_group_name: " + action.sTarget);
			bReturn = false;
		}
		// call any actions whose preceding action is this CALL_URL
	    return executeCascadingActions(bReturn, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return object;
//    	else
//    		return Boolean.valueOf(true);
//	} 
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
// Unused and not present in iOS
public class HIDE_KEYBOARD extends ActionBase {
	public HIDE_KEYBOARD () {}
	
	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		if (action.sTarget == null || null == parentView)
			return Boolean.valueOf(false);
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity)
			return Boolean.valueOf(false);
		
		String[] controlNames = action.sTarget.split(",");
		if (null == controlNames || controlNames.length == 0)
			return Boolean.valueOf(false);
		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		for(int i = 0; null != controlNames && i < controlNames.length; i++) {
			ControlModel controlModel = Action.getControl(appembleActivity.getRootLayout(), 
				appembleActivity.getAllControls(), controlNames[i], Constants.FROM_NAME);
			int iControlId = controlModel != null ? controlModel.id : 0;
			int iType = 0;
			if (null != controlModel)
				iType = controlModel.getType();
			if (iControlId <= 0 || (iType != ConfigManager.getInstance().getControlId("EDIT") && 
					iType != ConfigManager.getInstance().getControlId("EDITNOWORDWRAP") &&
					iType != ConfigManager.getInstance().getControlId("PASSWORD")))
				continue;
			View view = Action.getViewForControl(parentView, iControlId);
			if (null != view)
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0); // TODO check if the softinput is open or not. Break the loop otherwise.
		}
	    return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.models.ActionModel;

public class GET_MEDIA extends ActionBase {	
	public GET_MEDIA() {}
	public final static int iGetMedia = "ON_GET_MEDIA".hashCode();
	private Uri fileUri;
	public static final int MEDIA_TYPE_IMAGE = 1;
	public static final int MEDIA_TYPE_AUDIO = 2;
	public static final int MEDIA_TYPE_VIDEO = 3;
	
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
			
		super.initialize(ctx, v, pView, a, tplv);
		if (view == null)
			return Boolean.valueOf(false);
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity)
			return Boolean.valueOf(false);

		Intent intent = null;
		String sType = a.mExtendedProperties.get("type");
		String sFileName = tplv.getString("file");
		if (null != sFileName) {
			if (sFileName.charAt(0) == File.pathSeparatorChar)
				fileUri = Uri.parse("file://" + sFileName);
			else
				fileUri = Uri.parse("assets://" + sFileName);
		} else
			fileUri = getOutputMediaFileUri(sType);
		
		intent = getIntent(sType);
		if (null == intent || null == fileUri)
			return Boolean.valueOf(false);
	    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name		
	    appembleActivity.startActivityForResult(intent, iGetMedia);
		Utilities.putData(targetParameterValueList, Constants.VARCHAR, "media_file", fileUri.getEncodedPath());
		return Boolean.valueOf(true);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 

	private Uri getOutputMediaFileUri(String sType) {
		Uri uri = null;
		if (null != sType && sType.equals("video"))
			uri = getOutputMediaFileUri(MEDIA_TYPE_VIDEO);
		else if (null != sType && sType.equals("audio"))
			uri = getOutputMediaFileUri(MEDIA_TYPE_AUDIO);
		else
			uri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
		return uri;
	}

	private Intent getIntent(String sType) {
		Intent intent;
		if (null != sType && sType.equals("video"))
			intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
		else if (null != sType && sType.equals("audio"))
			intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
		else
			intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		return intent;
	}

	/** Create a file Uri for saving an image or video */
	private static Uri getOutputMediaFileUri(int type){
	      return Uri.fromFile(getOutputMediaFile(type));
	}
	
	/** Create a File for saving an image or video */
	private static File getOutputMediaFile(int type){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.

	    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	              Environment.DIRECTORY_PICTURES), "MyCameraApp");
	    // This location works best if you want the created images to be shared
	    // between applications and persist after your app has been uninstalled.

	    // Create the storage directory if it does not exist
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            LogManager.logDebug("Failed to create directory");
	            return null;
	        }
	    }

	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile;
	    if (type == MEDIA_TYPE_IMAGE){
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "IMAGE_"+ timeStamp + ".jpg");
	    } else if(type == MEDIA_TYPE_VIDEO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "VIDEO_"+ timeStamp + ".mp4");
	    } else if(type == MEDIA_TYPE_AUDIO) {
	        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "AUDIO_"+ timeStamp + ".mp3");
	    } else {
	        return null;
	    }

	    return mediaFile;
	}
}

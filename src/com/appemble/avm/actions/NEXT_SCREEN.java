/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ScreenModel;

public class NEXT_SCREEN extends ActionBase {
	// ProgressDialog progress;
	public NEXT_SCREEN() {
	}

	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		if (a.mExtendedProperties.containsKey("is_foreground") && 
			Utilities.parseBoolean(a.mExtendedProperties.get("is_foreground"))) {
			Action.setViewState(view, action, Constants.DISABLE);
			startActivity();
			Action.setViewState(view, action, Constants.ENABLE);
		}
		else
			new MyTask(targetParameterValueList).execute();
		return Boolean.valueOf(true);
	}

	private void startActivity() {
		String[] sTargetList = action.sTarget.split(",");
		String sScreenName = sTargetList[0];
		String sSystemDbName = action.sSystemDbName;
		String sContentDbName = action.sContentDbName;
		if (sTargetList.length == 3) {
			sSystemDbName = sTargetList[1];
			sContentDbName = sTargetList[2];
		}

		Intent intent = ScreenModel.getIntent(sSystemDbName, sContentDbName,
				sScreenName, targetParameterValueList);
		if (targetParameterValueList.containsKey("new_task")) {
			boolean bNewTask = Utilities.parseBoolean(targetParameterValueList.getString("new_task"));
			if (bNewTask)
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		}
		if (null != intent)
			context.startActivity(intent);
	}

	public class MyTask extends AsyncTask<Void, Void, Integer> {

		public MyTask(Bundle flv) {
		}

		public void onPreExecute() {
			Action.setViewState(view, action, Constants.DISABLE);
		}

		public Integer doInBackground(Void... unused) {
			startActivity();
			return 1;
		}

		public void onPostExecute(Integer unused) {
			Action.setViewState(view, action, Constants.ENABLE); // enable 
			executeCascadingActions(Boolean.TRUE, targetParameterValueList);
		}
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Utilities;
import com.appemble.avm.models.ActionModel;

public class ALERT extends ActionBase {
	public ALERT() {}
	
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
		super.initialize(ctx, v, pView, a, tplv);
		
		AlertDialog.Builder alt_bld = new AlertDialog.Builder(context);
		if (null != action && null != action.sTarget && !action.sTarget.matches("Alert")) {
			alt_bld.setTitle("We got it under control...");
			alt_bld.setMessage("this function: " + action.sTarget + " is not yet implemented. But working hard for it!");
		} else {
			alt_bld.setTitle(targetParameterValueList.getString("title"));
			alt_bld.setMessage(targetParameterValueList.getString("message"));
		}
		alt_bld.setCancelable(false);
		alt_bld.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			//  Action for 'NO' Button
			dialog.cancel();
			boolean bDoNotCallCascadingActions = false;
			if (action.mExtendedProperties.containsKey("do_not_call_cascading_actions"))
				bDoNotCallCascadingActions = Utilities.parseBoolean(action.mExtendedProperties.get("do_not_call_cascading_actions"));
			if (targetParameterValueList.containsKey("do_not_call_cascading_actions"))
				bDoNotCallCascadingActions = Utilities.parseBoolean(targetParameterValueList.getString("do_not_call_cascading_actions"));
		    executeCascadingActions(!bDoNotCallCascadingActions ? Boolean.TRUE : Boolean.FALSE, targetParameterValueList);
			}
		});
		
		AlertDialog alert = alt_bld.create();
	
		// Icon for AlertDialog
		alert.setIcon(R.drawable.btn_star);
		alert.show();
		return Boolean.valueOf(false);
	}
	
//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
//
	public static void showAlert(Context context, String sTitle, String sMessage) {
		if (null == sTitle && null == sMessage)
			return;
		AlertDialog.Builder alt_bld = new AlertDialog.Builder(context);
		alt_bld.setTitle(sTitle);
		alt_bld.setMessage(sMessage);
		alt_bld.setCancelable(false);
		alt_bld.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			//  Action for 'NO' Button
			dialog.cancel();
			}
		});
		
		AlertDialog alert = alt_bld.create();
	
		// Icon for AlertDialog
		alert.setIcon(R.drawable.btn_star);
		alert.show();
	}
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.actions;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.Cache;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleDialog;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ScreenModel;

public class CLOSE_SCREEN extends ActionBase {
	public CLOSE_SCREEN () {} 
	public Object execute(final Context ctx, View v, View pView, final ActionModel a, 
			Bundle tplv) {
		super.initialize(ctx, v, pView, a, tplv);
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity)
			return Boolean.valueOf(false);
		appembleActivity.finish();
		if (null == action)
			return Boolean.valueOf(false);
		
		if (action.sTarget != null && action.sTarget.length() > 0) {
			try {
				Cache.iNumScreensToClose = Integer.parseInt(action.sTarget);
			} catch (NumberFormatException nfe) { // this is not an error. It is a way to check if a number is being passed
				if (appembleActivity instanceof AppembleDialog && null != a.sTarget && "parent".equals(a.sTarget)) {
					Activity activity = ((AppembleDialog)appembleActivity).getOwnerActivity();
					if (null != activity)
						activity.finish();
				} else {			
					ScreenModel screenModel = appembleActivity.getScreenModel();
					if (null != screenModel && screenModel.sName != null && 
							!screenModel.sName.equalsIgnoreCase(action.sTarget)) {
						Cache.sScreenToClose = action.sTarget;
						Cache.bScreenCloseUpto = Utilities.parseBoolean(screenModel.mExtendedProperties.get("close_target"));
					}
				}
			}
		}
		
	    return executeCascadingActions(Boolean.TRUE, targetParameterValueList);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
}

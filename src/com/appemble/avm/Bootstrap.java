/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

//import net.sqlcipher.database.SQLiteDatabase;

abstract public class Bootstrap {
	abstract public boolean execute();
	abstract public String getValue(String sKey);
	public String getContentDbEncryptionKeys() { return null; }
	abstract public int getIntValue(String sKey);
	abstract public String getSValue(String sKey);
    public void onSystemDbUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
    public void onContentDbUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
    public Uri getRawUri(String sName) { return null; }
}

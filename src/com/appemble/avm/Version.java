package com.appemble.avm;

public class Version implements Comparable<Version> {

    private String version;

    public final String get() {
        return this.version;
    }

    public Version(String version) {
        if(version == null)
            throw new IllegalArgumentException("Version can not be null");
        if(!version.matches("[0-9]+(\\.[0-9]+)*"))
            throw new IllegalArgumentException("Invalid version format");
        this.version = version.trim();
    }

    @Override public int compareTo(Version that) {
        if(that == null)
            return 1;
        if (this.version.equals(that.version))
        	return 0;
        String[] thisParts = this.get().split("\\.");
        String[] thatParts = that.get().split("\\.");
        int length = Math.max(thisParts.length, thatParts.length);
        for(int i = 0; i < length; i++) {
            int thisPart = i < thisParts.length ?
                Integer.parseInt(thisParts[i]) : 0;
            int thatPart = i < thatParts.length ?
                Integer.parseInt(thatParts[i]) : 0;
            if(thisPart < thatPart)
                return -1;
            if(thisPart > thatPart)
                return 1;
        }
        return 0;
    }

    @Override public boolean equals(Object that) {
        if(this == that)
            return true;
        if(that == null)
            return false;
        if(this.getClass() != that.getClass())
            return false;
        return this.compareTo((Version) that) == 0;
    }
    
    static public int compare(String v1, String v2) {
    	if (null == v1 && null != v2)
    		return -1;
		Version version1 = null;
		Version version2 = null;
    	try {
    		version1 = new Version(v1);
    		version2 = new Version(v2);
    		
    	} catch (IllegalArgumentException iae) {
    		return -2; // error
    	}
    	return version1.compareTo(version2);    	
    }
}
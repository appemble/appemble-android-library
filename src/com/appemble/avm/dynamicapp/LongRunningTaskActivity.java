/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dynamicapp;

import android.os.AsyncTask;
import android.os.Bundle;

import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;

// Following article helped in shaping this
// http://www.techrepublic.com/blog/app-builder/using-androids-asynctask-to-handle-long-running-io/670
//How to run LongRunningTask in the background of an activity.
//
//Create a screen of type "LongRunningTaskActivity" (Misc1 to contain LongRunningTaskActivity)
//When you want to execute a long running task and show this activity, call the action NEXT_SCREEN("screen name")
//This new screen will display itself and start a background thread.
//This background thread will have a function called doInBackground() which in turn calls an event "ON_DO_IN_BACKGROUND_TASK"
//Attach an action on this event to call a FUNCTION that runs the long running task.
//Once this long running function finishes, the activity will stop automatically.

public class LongRunningTaskActivity extends DynamicActivity {
	private LongRunningIO mLongRunningIO = null;
	static final int ON_DO_IN_BACKGROUND = "ON_DO_IN_BACKGROUND".hashCode();
	static final int ON_DO_IN_BACKGROUND_ERROR = "ON_DO_IN_BACKGROUND_ERROR".hashCode();
		
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//The parent class DynamicActivity initializes the Cache
//      if (null == Cache.context && false == Cache.initiate(this.getApplicationContext(), true))
//			return;
        if (bBeingCreated == false)
        	return;
		boolean bNetworkConnectionRequired = Utilities.parseBoolean(
				mActivityData.screenModel.mExtendedProperties.get(Constants.STR_REQUIRES_NETWORK_CONNECTION));
		if (bNetworkConnectionRequired && false == AppembleActivityImpl.isNetworkAvailable(this)) {
			AppembleActivityImpl.showAlertAndCloseScreen(this, "Alert...", Constants.STR_NETWORK_CONNECTION_REQUIRED);
			return;
		}
		
        mLongRunningIO = (LongRunningIO)getLastNonConfigurationInstance();
        if (mLongRunningIO != null) {
        	if (mLongRunningIO.isDone())
        		finish();
        	mLongRunningIO.attach(this);
        } else {
			mLongRunningIO = new LongRunningIO();
		    mLongRunningIO.attach(this);
		    mLongRunningIO.execute();        	
        }
    }
    
	 @Override
	  public Object onRetainNonConfigurationInstance() {
	    if (mLongRunningIO != null) {
	    	mLongRunningIO.detach();
	    }
	    return(mLongRunningIO);
	  }
	
	 static class LongRunningIO extends AsyncTask <Void, Integer, Integer> {
		private DynamicActivity mActivity = null;
		private boolean mDone = false, bReturn = true;
		
		boolean isDone() { return mDone; }
		
		@Override
		protected Integer doInBackground(Void... params) {
			if (null == mActivity || null == mActivity.mActivityData.screenModel)
				return 0;
			Object bObject = Action.callScreenActions(mActivity, ON_DO_IN_BACKGROUND, null, 
				null, mActivity.mActivityData.targetParameterValueList);
			if (bObject instanceof Boolean)
				bReturn = (Boolean) bObject;
			mDone = true;
			return 1;
		}
		
		protected void onPostExecute(Integer unused) {
			// if there is an error, invoke the event ON_DO_IN_BACKGROUND_ERROR. The user should not call any UI like dialog box
			// as this activity will be finished right after raising the event.
			if (false == bReturn || (null != mActivity && null != mActivity.mActivityData.targetParameterValueList &&
					mActivity.mActivityData.targetParameterValueList.containsKey(Constants.STR_SERVER_ERROR)))
				Action.callScreenActions(mActivity, ON_DO_IN_BACKGROUND_ERROR,
						null, null, mActivity.mActivityData.targetParameterValueList);
			 if (mActivity != null)
				 mActivity.finish();
	    }	

		@Override
		protected void onProgressUpdate(Integer... progress) {
			 if (mActivity != null) {
			 }
	    }	
		
		void attach(DynamicActivity a) {
			mActivity = a;
		}
		
		void detach() {
			mActivity = null;
		}	
	}
}

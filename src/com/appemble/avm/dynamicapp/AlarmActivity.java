/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dynamicapp;

import java.util.HashMap;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class AlarmActivity extends Activity implements AppembleActivity {
	Object mPlayer = null;
	AppembleDialog mAppembleDialog = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (null == Cache.context && false == Cache.initiate(this.getApplicationContext(), true))
			return;
	    super.onCreate(savedInstanceState);

		@SuppressWarnings("unchecked")
		HashMap<String, String> parametersMap = (HashMap<String, String>) Utilities.deserializeObject(
				getIntent().getByteArrayExtra(Constants.STR_TARGET_PARAMETER_VALUE_LIST));
		if (null == parametersMap)
			parametersMap = new HashMap<String, String>();
		String sDisplayClassActivity = parametersMap.get("display_activity_class");
		String sSystemDbName = parametersMap.get(Constants.STR_SYSTEM_DATABASE_NAME);
		String sContentDbName = parametersMap.get(Constants.STR_CONTENT_DATABASE_NAME); 
		String sTarget = parametersMap.get(Constants.STR_SCREEN_NAME);
		Bundle targetParameterValueList = Utilities.convertHashMapToBundle(parametersMap);
		if (null != sSystemDbName && null != sContentDbName && null != sTarget &&  
				null != sDisplayClassActivity && "dialog".equalsIgnoreCase(sDisplayClassActivity)) {
			mAppembleDialog = new AppembleDialog(this);
			if (false == mAppembleDialog.createLayout(sSystemDbName, sContentDbName, sTarget, targetParameterValueList))
				return;
			mAppembleDialog.show();
			mAppembleDialog.onResume();
			// TODO how to finish() the activity... The dialog should call getContext(), typecase it to Activity and call Activtiy.finish()
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
		    builder
		    	.setMessage(parametersMap.get("message"))
		    	.setTitle(parametersMap.get("title"))
		    	.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int which) { //Stop the activity
		                finish();    
		            }	
		        })
				.create()
		    	.show();
		}
		String sFileName = null;
		if (null != parametersMap)
			sFileName = parametersMap.get("file");
		mPlayer = Utilities.playSoundEffects(Cache.context, sFileName);			
	}

	@Override
	public Context getContext() {
		return null;
	}

	@Override
	public HashMap<Integer, Cursor> getCursors() {
		return null;
	}

	@Override
	public View getRootLayout() {
		return null;
	}

	@Override
	public void setActivityCreateStatus(boolean bBoolean) {
	}

	@Override
	public boolean getActivityCreateStatus() {
		return false;
	}

	@Override
	public boolean getActivityRecreateStatus() {
		return false;
	}

	@Override
	public boolean getNetworkAvailableFlag() {
		return false;
	}

	@Override
	public boolean isDestroyed() {
		return false;
	}

	@Override
	public Vector<ControlModel> getAllControls() {
		return mAppembleDialog != null ? mAppembleDialog.mActivityData.vControls : null;
	}

	@Override
	public ScreenModel getScreenModel() {
		return mAppembleDialog != null ? mAppembleDialog.mActivityData.screenModel : null;
	}

	@Override
	public Bundle getTargetParameterValueList() {
		return mAppembleDialog != null ? mAppembleDialog.mActivityData.targetParameterValueList : null;
	}

	public ActivityData getActivityData() {
		return mAppembleDialog != null ? mAppembleDialog.mActivityData : null;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		stopMediaPlayer();
	}

	@Override
	public Activity getMyParent() {
		return null;
	}
	
	public void stopMediaPlayer() {
		if (null != mPlayer) {
			if (mPlayer instanceof Vibrator)
	    		((Vibrator)mPlayer).cancel();
			else if (mPlayer instanceof MediaPlayer )
	    		((MediaPlayer)mPlayer).stop();
			mPlayer = null;
		}		
	}
}

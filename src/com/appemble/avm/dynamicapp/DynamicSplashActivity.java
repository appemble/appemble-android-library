/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dynamicapp;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MotionEvent;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.R;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ScreenDao;

public class DynamicSplashActivity extends DynamicActivity {
	public static final int ON_CLOSE_SCREEN = "ON_CLOSE_SCREEN".hashCode();

	/**
	 * The thread to process splash screen events
	 */
	boolean _active = true;
	int _splashTime = 0;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (null == Cache.context && false == Cache.initiate(this.getApplicationContext(), true))
				return;
		String sSystemDbName = null, sContentDbName = null;
		if (getIntent().hasExtra(Constants.STR_SYSTEM_DATABASE_NAME))
			sSystemDbName = getIntent().getExtras().getString(Constants.STR_SYSTEM_DATABASE_NAME);
		else
			sSystemDbName = Cache.bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME);
		if (getIntent().hasExtra(Constants.STR_CONTENT_DATABASE_NAME))
			sContentDbName = getIntent().getExtras().getString(Constants.STR_CONTENT_DATABASE_NAME);
		else
			sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);
		if (sSystemDbName == null || null == sContentDbName) {
			super.onCreate(savedInstanceState);
			LogManager.logError("Error in finding system database. Please check res/values/strings.xml for system_dbname and content_dbname. Make sure they are present in the assets folder. Aborting...");
			finish();
			return;
		}
	    mActivityData = (ActivityData) getLastNonConfigurationInstance();
	    if (null == mActivityData)
	    	mActivityData = new ActivityData();
	    if (null == mActivityData.screenModel)
	    	mActivityData.screenModel = ScreenDao.getSplashScreenModel(sSystemDbName, sContentDbName);
		if (null != mActivityData.screenModel) {
			mActivityData.sScreenName = mActivityData.screenModel.sName;
			String sSplashTime = mActivityData.screenModel.mExtendedProperties.get("splash_time");
			if (null != sSplashTime) {
				int iSplashTime = Integer.parseInt(sSplashTime);
				_splashTime = Math.abs(iSplashTime); // For a splash screen, the initial focus keeps the splash time.
			}
			if (_splashTime == 0) {
				 Resources res = Cache.context.getResources();
				 if (res != null)
					_splashTime = res.getInteger(R.integer.splash_time);
			}
			LogManager.logVerbose("Creating SPLASH: " + mActivityData.sScreenName + ". SystemDbName=" + 
					mActivityData.screenModel.sSystemDbName + " sContentDbName = " + mActivityData.screenModel.sContentDbName);
			super.onCreate(savedInstanceState);
			boolean bNetworkConnectionRequired = Utilities.parseBoolean(
					mActivityData.screenModel.mExtendedProperties.get(Constants.STR_REQUIRES_NETWORK_CONNECTION));
			if (bNetworkConnectionRequired && false == AppembleActivityImpl.isNetworkAvailable(this)) {
				AppembleActivityImpl.showAlertAndCloseScreen(this, "Alert...", Constants.STR_NETWORK_CONNECTION_REQUIRED);
				return;
			}
			mSplashThread.start();
		} else {
			super.onCreate(savedInstanceState);
			finishActivity(sSystemDbName, sContentDbName);
		}
	}

	// The thread to wait for splash screen events
	private Thread mSplashThread = new Thread() {
		@Override
		public void run() {
			try {
				synchronized (this) {
	                int waited = 0;
	                while(_active && (waited < _splashTime)) {
	                    sleep(100);
	                    if(_active) {
	                        waited += 100;
	                    }
	                }					}
			} catch (InterruptedException ex) {
				LogManager.logWTF(ex, "Internal error oon Splash activity.");
			} finally {
				finishActivity(null, null);
			}
		}
	};

	private void finishActivity(String sSystemDbName, String sContentDbName) {
		String sScreenName = null;
		Object oReturn = Action.callScreenActions(this, ON_CLOSE_SCREEN, null, 
			activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
		if (oReturn instanceof String)
			sScreenName = (String) oReturn;
		Intent intent = ScreenModel.getIntent(mActivityData.screenModel != null ? 
				mActivityData.screenModel.sSystemDbName : sSystemDbName, 
				mActivityData.screenModel != null ? mActivityData.screenModel.sContentDbName : 
					sContentDbName, sScreenName, mActivityData.targetParameterValueList);
		if (null != intent)
			startActivity(intent);
		this.finish();
	}
	
	/**
	 * Processes splash screen touch events
	 */
	@Override
	public boolean onTouchEvent(MotionEvent evt) {
		if (evt.getAction() == MotionEvent.ACTION_DOWN) {
			if (null == mSplashThread)
				return true;
			synchronized (mSplashThread) {
				// mSplashThread.notifyAll();
				_active = false;
			}			
		}
		return true;
	}
}
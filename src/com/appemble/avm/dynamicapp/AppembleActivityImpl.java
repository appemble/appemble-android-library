/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dynamicapp;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.HttpUtils;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.ALERT;
import com.appemble.avm.actions.Action;
import com.appemble.avm.actions.ActionBase;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.dataconnector.JSONProcessor;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ControlDao;
import com.appemble.avm.models.dao.TableDao;
import com.loopj.android.http.AppembleJsonHttpResponseHandler;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class AppembleActivityImpl {
	public static final int ON_POST_CALL_URL = "ON_POST_CALL_URL".hashCode();
	
	public static void deparameterizeDataSources(ScreenModel screenModel, Vector<ControlModel> vControls, 
			Bundle targetParameterValueList, String sDbName) {
		if (null == vControls)
			return;
		String sDataSource;
		sDataSource = screenModel.sRemoteDataSource;
		if (null != sDataSource)
			screenModel.sDeparameterizedRemoteDataSource = (Utilities.replaceParameterisedStrings(sDataSource, targetParameterValueList, sDbName, Constants.URL));
		sDataSource = screenModel.sLocalDataSource;
		if (null != sDataSource)
			screenModel.sDeparameterizedLocalDataSource = (Utilities.replaceParameterisedStrings(sDataSource, targetParameterValueList, sDbName, Constants.SQL));
		for (ControlModel controlModel: vControls) {
			sDataSource = controlModel.sRemoteDataSource;
			if (null != sDataSource)
				controlModel.sDeparameterizedRemoteDataSource = (Utilities.replaceParameterisedStrings(sDataSource, targetParameterValueList, sDbName, Constants.URL));
			sDataSource = controlModel.sLocalDataSource;
			if (null != sDataSource)
				controlModel.sDeparameterizedLocalDataSource = (Utilities.replaceParameterisedStrings(sDataSource, targetParameterValueList, sDbName, Constants.SQL));
		}
		
		return;
	}
	
	// creates the cursors for any query ( in local data source ) associated with any control in a screen.
	// The function first closes the cursors if already present and then empties the hashmap of cursors
	// then for the screen it creates the cursor
	// then for all the controls (irrespective of the parentid) it creates the cursors...
	public static boolean fetchLocalData(AppembleActivity appembleActivity, 
			ControlModel controlWithLocalDataToUpdate, boolean bRecursive) {
		// local data is not available if this function is being called in the function OnResume, 
		// which is called as a result of onResume and not being created and bOnResumeUpdate == false
		ScreenModel screenModel = appembleActivity.getScreenModel();
		HashMap<Integer, Cursor> cursors = appembleActivity.getCursors();
		Vector<ControlModel> vControls = appembleActivity.getAllControls();
		boolean bBeingCreated = appembleActivity.getActivityCreateStatus();
		if (null == cursors || 
				(false == bBeingCreated && 
				((null == controlWithLocalDataToUpdate && screenModel.bOnResumeUpdate == false) || // if screen should not get updated on resume.
				(null != controlWithLocalDataToUpdate && false == controlWithLocalDataToUpdate.bOnResumeUpdate)))) // if control should not get updated on resume
			return false; 
		String query; 
		Cursor cursor;
		String sDbName = (null != controlWithLocalDataToUpdate) ? 
				controlWithLocalDataToUpdate.sContentDbName : screenModel.sContentDbName;

		int controlWithLocalDataToUpdateId = Constants.SCREEN_ID;
		if (null == controlWithLocalDataToUpdate)
			query = screenModel.sDeparameterizedLocalDataSource;
		else {
			query = controlWithLocalDataToUpdate.sDeparameterizedLocalDataSource;
			controlWithLocalDataToUpdateId = controlWithLocalDataToUpdate.id;
		}
		
		if (null != query && Utilities.isQuery(query)) {
			cursor = TableDao.createCursor(sDbName, query, null);
			if (null != cursor) {
				if (null == controlWithLocalDataToUpdate && cursor.getCount() == 0) // if this cursor is for screen.
					cursor.close();
				else if (null != controlWithLocalDataToUpdate && // if this cursor is for controls that do not manage cursor and rows retrieved is zero
						controlWithLocalDataToUpdate.metaData.iCursorMgmt != Constants.CURSOR_MGMT_SELF &&
						cursor.getCount() == 0)
					cursor.close();
				else // if row count is > 0 || controls that manage cursor themselves will always get the cursor even if the rows retrieved is zero.
					cursors.put(Integer.valueOf(controlWithLocalDataToUpdateId), cursor);
			}
		}
		
		// now create cursors for queries present with the child controls 
		Vector <ControlModel> vControlsToUpdate = null;
		if (null == controlWithLocalDataToUpdate)
			vControlsToUpdate = vControls; // get local data source for all controls irrespective how deep they are in the control hierarchy
		else if (controlWithLocalDataToUpdate.isGroup()) {	
			vControlsToUpdate = new Vector<ControlModel> ();
			AppembleActivityImpl.getChildControls(vControls, controlWithLocalDataToUpdate.id, vControlsToUpdate);
		}

		// Get the local data source for the child controls.
		if (null != vControlsToUpdate)
		for (ControlModel controlModel: vControlsToUpdate) {
			// if the child control is a group and function is called to traverse deep and it not called for screen then
			// call this function again for the child group control
			if (controlModel.isGroup() && bRecursive && controlWithLocalDataToUpdate != null) {
				return AppembleActivityImpl.fetchLocalData(appembleActivity, controlModel, bRecursive);
//				for (Cursor cursor1 : cursors.values()) {
//				    if (cursor1.getCount() > 0)
//				    	return bReturn;
//				}
//				return false;
			}
			
			query = controlModel.sDeparameterizedLocalDataSource;
			if (null == query || !Utilities.isQuery(query))
				continue;
			cursor = TableDao.createCursor(sDbName, query, null);
			if (null != cursor) {
				if (null != controlModel && // if this cursor is for controls that do not manage cursor and rows retrieved is zero
						controlModel.metaData.iCursorMgmt != Constants.CURSOR_MGMT_SELF &&
						cursor.getCount() == 0)
					cursor.close();
				else // if row count is > 0 || controls that manage cursor themselves will always get the cursor even if the rows retrieved is zero.
					cursors.put(Integer.valueOf(controlModel.id), cursor);
			}
		}
//		for (Cursor cursor1 : cursors.values()) {
//		    if (cursor1.getCount() > 0)
//		    	return true;
//		}
		// note: 2013 02 24. The return value should be true when the size of the cursors > 0. 
		// See if above for loop can be commented and the following line could be added
		return cursors.size() > 0 ? true : false;

//		return false;
	}

	public static boolean isRemoteDataPresent(ScreenModel screenModel, Vector<ControlModel> vControls) {
		boolean bGetRemoteData = false;
		bGetRemoteData = screenModel.sDeparameterizedRemoteDataSource != null;
		if (false == bGetRemoteData)
		for (ControlModel controlModel: vControls) {
			String sRemoteData = controlModel.sDeparameterizedRemoteDataSource;
			bGetRemoteData = sRemoteData != null && !sRemoteData.startsWith(Constants.URL_IMAGE_PREFIX); // image is not considered as remote data
			if (bGetRemoteData)
				return bGetRemoteData;
		}
		return bGetRemoteData;
	}
	
	// This function calls the fetchRemoteData. The FetchRemoteData function can be called recursively to traverse the hierarchy of controls
	// and fetching each child controls remote data.
	public static boolean updateRemoteData(AppembleActivity appembleActivity, ControlModel controlToUpdate, 
			boolean bRecursive, ActionBase ai, boolean isNetworkAvailable, boolean bBeingCreated) {
		boolean bNetworkStatus = AppembleActivityImpl.isNetworkAvailable(appembleActivity.getContext()); 
		if (!bNetworkStatus) {
			if (isNetworkAvailable) {
				isNetworkAvailable = false;
				ALERT.showAlert(appembleActivity.getContext(), "Alert...", 
						Constants.STR_NETWORK_CONNECTION_REQUIRED);
			}
			return isNetworkAvailable;
		} else if (!isNetworkAvailable)
			isNetworkAvailable = true;
		Bundle remoteData = new Bundle();
		StringBuffer sErrorMsg = new StringBuffer();
		AppembleActivityImpl.fetchRemoteData(appembleActivity, controlToUpdate, bRecursive, remoteData, sErrorMsg, ai,
				bBeingCreated);			
		return isNetworkAvailable;
	}
// Update the controlToUpdate. If null == controlToUpdate then update screen with the remote data.
		// If there is a remote data to be fetched, start a AsyncTask .
		// The Async task will used background thread to fetch the remote data
		// The Async task will then update the controls using the ***UI thread*** in the function PostExecute...
	// Update the child controls with Remote data by calling this function recursively
		// If there is a remote data to be fetched, start a AsyncTask .
		// The Async task will used background thread to fetch the remote data
		// The Async task will then update the controls using the ***UI thread*** in the function PostExecute...
	public static void fetchRemoteData(AppembleActivity appembleActivity, ControlModel controlToUpdate, boolean bRecursive, 
			Bundle remoteData, StringBuffer sErrorMsg, ActionBase ai, boolean bBeingCreated) {
		// if request sRemoteDataSource present at screen level, execute that
		// loop through all the controls. If sRemoteDataSource present, execute that.
		// execute the remote url for the controlToUpdate
			// for screen
		ScreenModel screenModel = appembleActivity.getScreenModel();
		if (null == controlToUpdate) {
			int iRemoteDataFormat = Constants.getRemoteDataFormat(screenModel.mExtendedProperties.get("remote_data_format"));
			if (screenModel.sDeparameterizedRemoteDataSource != null) {
				AppembleActivityImpl.getRemoteData(appembleActivity, screenModel.sRemoteDataSource, 
						screenModel.sDeparameterizedRemoteDataSource, screenModel.iRemoteRequestType, 
						remoteData, sErrorMsg, null, bRecursive, true, ai, screenModel.bRemoteDataSaveLocally,
						iRemoteDataFormat);
			} else { // 2014-02-14 update the screen with the local data. It is quite possible that one of the controls in the screen has remote data source and that is why fetch remote data is bding called.
				AppembleActivityImpl.fetchLocalData(appembleActivity, null, true); 
				AppembleActivityImpl.update(appembleActivity, null, null);
			}
		} else {
			int iRemoteDataFormat = Constants.getRemoteDataFormat(controlToUpdate.mExtendedProperties.get("remote_data_format"));
			if (controlToUpdate.sDeparameterizedRemoteDataSource != null) { // if controlToUpdate is not null
				AppembleActivityImpl.getRemoteData(appembleActivity, controlToUpdate.sRemoteDataSource, 
						controlToUpdate.sDeparameterizedRemoteDataSource, controlToUpdate.iRemoteRequestType, 
						remoteData, sErrorMsg, controlToUpdate, bRecursive, true, ai, 
						controlToUpdate.bRemoteDataSaveLocally, iRemoteDataFormat);
			}
		}
		
		// if the function is NOT called to update the child controls with remote data then return 
		if (false == bRecursive)
			return;
		
		// update child controls with remote data if bRecursive
		Vector <ControlModel> vControlsToUpdate = null;
		if (null == controlToUpdate)
//			vControlsToUpdate = vControlsInScreenAndTitle;
			vControlsToUpdate = appembleActivity.getAllControls();
		else if (controlToUpdate.isGroup()) {	
			vControlsToUpdate = new Vector<ControlModel> ();
			AppembleActivityImpl.getChildControls(appembleActivity.getAllControls(), controlToUpdate.id, vControlsToUpdate);
		}
		
		if (null != vControlsToUpdate)
			for (ControlModel controlModel: vControlsToUpdate) {
				String sRemoteDataSource = controlModel.sDeparameterizedRemoteDataSource;
				if (sRemoteDataSource != null && !sRemoteDataSource.startsWith(Constants.URL_IMAGE_PREFIX))
					fetchRemoteData(appembleActivity, controlModel, bRecursive, remoteData, sErrorMsg, 
							ai, bBeingCreated);
		}
		return;
	}

	public static void getRemoteData(final AppembleActivity appembleActivity, final String sRemoteDataSource, 
			final String sDeparameterizedRemoteDataSource, int iRequestType, 
			final Bundle results, final StringBuffer sErrorMsg, 
			final ControlModel controlToUpdate, final boolean bRecursive, final boolean bUpdateScreen, 
			final ActionBase ai, boolean bSaveLocally, int iResponseDataFormat) {
		
		final ScreenModel screenModel = appembleActivity.getScreenModel();
		final boolean bBeingCreated = appembleActivity.getActivityCreateStatus();
		if (null != controlToUpdate && controlToUpdate.metaData.bProcessRemoteDS == false) {
			AppembleActivityImpl.fetchLocalData(appembleActivity, controlToUpdate, bRecursive);
			Bundle tplv = appembleActivity.getTargetParameterValueList();
			tplv.putAll(results);
			update(appembleActivity, controlToUpdate, tplv);
			return; // Do not process remote data source if the control exclusively does not want it.
		}
		AsyncHttpClient client = HttpUtils.getAsyncHttpClient();
		client.setCookieStore(Cache.cookieStore);
		AsyncHttpResponseHandler responseHandler = getResponseHandler(appembleActivity, sRemoteDataSource, 
			sDeparameterizedRemoteDataSource, bBeingCreated, results, sErrorMsg, screenModel, 
			controlToUpdate, bRecursive, bUpdateScreen, ai, bSaveLocally, iResponseDataFormat);

		switch (iRequestType) {
		case Constants.GET: {
			client.get(appembleActivity.getContext(), sDeparameterizedRemoteDataSource, responseHandler);
		}
		break;
		case Constants.POST: {
			RequestParams requestParams = new RequestParams(Utilities.BundleToHashMap(
					appembleActivity.getTargetParameterValueList()));
			client.post(appembleActivity.getContext(), sDeparameterizedRemoteDataSource, requestParams, 
					responseHandler);
		}
		break;
		case Constants.PUT: {
			RequestParams requestParams = new RequestParams(Utilities.BundleToHashMap(
					appembleActivity.getTargetParameterValueList()));
			client.put(appembleActivity.getContext(), sDeparameterizedRemoteDataSource, requestParams, 
					responseHandler);
		}
		break;
		case Constants.DELETE: {
			client.delete(appembleActivity.getContext(), sDeparameterizedRemoteDataSource, null, 
					responseHandler);
		}
		break;
		}		
	}

	public static AsyncHttpResponseHandler getResponseHandler(
			final AppembleActivity appembleActivity, final String sRemoteDataSource, 
			final String sDeparameterizedRemoteDataSource, final boolean bBeingCreated,
			final Bundle results, final StringBuffer sErrorMsg, final ScreenModel screenModel, 
			final ControlModel controlToUpdate, final boolean bRecursive, final boolean bUpdateScreen, 
			final ActionBase ai, final boolean bSaveLocally, int iResponseDataFormat) {
		AsyncHttpResponseHandler responseHandler = null;
		if (iResponseDataFormat == Constants.JSON) {
			responseHandler = new AppembleJsonHttpResponseHandler(
				controlToUpdate != null ? controlToUpdate.sContentDbName : screenModel.sContentDbName, 
				null, results, bSaveLocally) {
				@Override
		        public void onStart() {
					AppembleActivityImpl.progressBar(appembleActivity, Constants.SHOW, results);
				}
				
				@Override
		        public void onSuccess(JSONObject response) {
					if (bUpdateScreen) {
						// Have to be in postexecute so that if multiple threads are created for fetching the data, they are serailized by the UI thread to update the screen data.						
						// even if the local data is not available, we may have to update the screen with the remote data directly.
						AppembleActivityImpl.fetchLocalData(appembleActivity, controlToUpdate, bRecursive); 
						update(appembleActivity, controlToUpdate, results);
					}
					ActionModel[] actions = screenModel.getActions(ON_POST_CALL_URL);
					// Now go through each remote data source (for screen and child controls). If there is a ON_POST_CALL_URL action, execute it.
				    for (int i = 0; null != actions && i < actions.length; i++) {
						if (null != actions[i] && null != actions[i].sTarget && actions[i].sTarget.equals(sRemoteDataSource))
							Action.callAction(appembleActivity.getContext(), null, null, actions[i], results);
					}
					if (null != ai) {
						Bundle tplv = appembleActivity.getTargetParameterValueList();
						Utilities.merge(tplv, results);
						ai.executeCascadingActions(true, tplv);
					}
				}
		
				@Override
			    public void onFailure(Throwable error, String content) {
					sErrorMsg.append(error != null ? error.getMessage() : "" + ": Unable to save JSON object into database returned from url: " + sDeparameterizedRemoteDataSource);// TODO replace with logManager
					ALERT.showAlert(appembleActivity.getContext(), "Alert...", sErrorMsg.toString());
				}
		
				@Override
		        public void onFinish() {
					progressBar(appembleActivity, Constants.HIDE, results);
					if (bBeingCreated)
						appembleActivity.setActivityCreateStatus(false);
					if (ai instanceof ActionBase)
						Action.setViewState(ai.view, ai.action, Constants.ENABLE);
				}		
			};
		}			
		return responseHandler;
	}
	
	// This function is called by the AsyncTask Background thread... See file UpdateWithRemoteDataInBackground for details
	public static String getData(AppembleActivity appembleActivity, String sRemoteDataSource, 
			String sDeparameterizedRemoteDataSource, int requestType, Bundle results, String sContentDbName,
			boolean bBeingCreated, boolean bRemoteDataSaveLocally, int iRemoteDataFormat) {
		String sErrorMsg = null;
		if (null == sDeparameterizedRemoteDataSource || sDeparameterizedRemoteDataSource.length() == 0)
			return sErrorMsg;
		// prepare the sDeparameterizedRemoteDataSource by substituting tokens with actual values in input_parameter_list
		// execute the sDeparameterizedRemoteDataSource. 
		// If the sDeparameterizedRemoteDataSource is fetching the image, use Image Manager to save 
		// 	the file locally and then the image after readnig from local file system
		// if the sDeparameterizedRemoteDataSource is a JSON response parse the response using the JSONProcessor.saveJSONArraysIntoDb
		// update the fields for the caller. The caller can be screen list group control or anything...
		
		// Here the sDeparameterizedRemoteDataSource could be a image file or a sDeparameterizedRemoteDataSource with json response. 
		// The sDeparameterizedRemoteDataSource may have a http-public prefix that needs to be resolved.
		String sRemoteDataType = null;
		sDeparameterizedRemoteDataSource = sDeparameterizedRemoteDataSource.trim();
		if (sDeparameterizedRemoteDataSource.startsWith(Constants.URL_IMAGE_PREFIX)) {
			sRemoteDataType = Constants.URL_IMAGE_PREFIX;
		} else if (sDeparameterizedRemoteDataSource.startsWith(Constants.URL_JSON_PREFIX)) {
			sRemoteDataType = Constants.URL_JSON_PREFIX;
		} else if (sDeparameterizedRemoteDataSource.startsWith(Constants.URL_XML_PREFIX)) {
			sRemoteDataType = Constants.URL_XML_PREFIX;
		} else if (sDeparameterizedRemoteDataSource.startsWith(Constants.URL_GPROTO_PREFIX)) {
			sRemoteDataType = Constants.URL_GPROTO_PREFIX;
		}
		if (null != sRemoteDataType) {
			sDeparameterizedRemoteDataSource = sDeparameterizedRemoteDataSource.substring(sRemoteDataType.length()); 		
			sDeparameterizedRemoteDataSource = sDeparameterizedRemoteDataSource.substring(sDeparameterizedRemoteDataSource.indexOf(Constants.FIELD_SEPARATER)+1);
		} else
			sRemoteDataType = Constants.URL_JSON_PREFIX; // default type

		sDeparameterizedRemoteDataSource.trim();
		sDeparameterizedRemoteDataSource = (Utilities.replaceParameterisedStrings(sDeparameterizedRemoteDataSource, 
				appembleActivity.getTargetParameterValueList(), sContentDbName, Constants.URL));
		
		// Download IMAGE
		if (sRemoteDataType.equalsIgnoreCase(Constants.URL_IMAGE_PREFIX) || iRemoteDataFormat == Constants.IMAGE) { 
			// see if the image is locally present... do not download if already present... 
			// download and save the image. In the results, add key = sDeparameterizedRemoteDataSource, location = imagepath.
			return sErrorMsg;
		} else if (sRemoteDataType.equalsIgnoreCase(Constants.URL_JSON_PREFIX) || iRemoteDataFormat == Constants.JSON ) {
			boolean bResult = true;
			progressBar(appembleActivity, Constants.SHOW, results);
			JSONObject jsonObject = HttpUtils.getJSONResponse(sDeparameterizedRemoteDataSource, requestType, 
					appembleActivity.getTargetParameterValueList());
			try {
				if (null == jsonObject || jsonObject.has(Constants.STR_SERVER_ERROR))
					sErrorMsg = jsonObject.getString(Constants.STR_SERVER_ERROR);
			} catch (JSONException e) {
				LogManager.logError(e, "Unable to parse JSON response for url: " + sDeparameterizedRemoteDataSource);
			}
			if (bRemoteDataSaveLocally)
				bResult = JSONProcessor.saveJSONObjectIntoDb(jsonObject, sContentDbName, null, results, 0);
			progressBar(appembleActivity, Constants.HIDE, results);
			if (bResult == false)
				sErrorMsg = "Unable to save JSON object into database returned from url: " + sDeparameterizedRemoteDataSource;// TODO replace with logManager
		    ActionModel[] actions = appembleActivity.getScreenModel().getActions(ON_POST_CALL_URL);
		    for (int i = 0; null != actions && i < actions.length; i++) {
		    	if (null != actions[i] && null != actions[i].sTarget && actions[i].sTarget.equals(sRemoteDataSource))
		    		Action.callAction(appembleActivity.getContext(), null, null, actions[i], results);
		    }			
			
			return sErrorMsg;
		} else if (sRemoteDataType.equalsIgnoreCase(Constants.URL_XML_PREFIX) ) {
			return sErrorMsg;
		} 
		// Future Enhancement: Currently the saveJSONArraysIntoDb does not remove those json arrays that get saved into db.
		// it should return the array that did not get saved into db
		// it should also return the name/value pairs that did not belong to a table.
		return sErrorMsg;
	}

	// This function is called by AsynTask's PostExecute
	// It is called both at the screen level (controlToUpdate == null) and at the control group level. 
	// A control group has type = CONTROL_GROUP, LIST, ARRANGEDLIST, RADIOGROUP etc
	
	// Algorithm
	// Get the list of controls (in the screen or in the control group)
	// get the cursor that is associated with the screen or the control group
	// Now update the control (or controls which are children of controlToUpdate) with the values in the cursor.
	// if the controlToUpdate == list or arranged list, do not close the cursor.

	// This function can be called with params null, null. What does this mean?
	// Step 1: update all controls at the screen level.
	// Step 2: For each control group in the screen repeat update all child controls. 
	// 		   If a query is present at the group level
	//         execute it and set the values for all controls that have control group's id = parent_id.
	
	// Step 3: one of the deficiency of the algorithm is that if a query is preset at the individual control level
	// it would not be executed. TODO To be TESTED
	// NOTE: When a query is run for the screen or CONTROL_GROUP, it should return only 1 row. 
	// if there are multiple rows, then the query needs to be redesigned...
	
	public static boolean update(AppembleActivity appembleActivity, ControlModel controlToUpdate, 
			Bundle remoteData) {
		// Do not update the control if this function is being called in the function OnResume, 
		// which is called as a result of onResume and not being created and bOnResumeUpdate == false
		boolean bBeingCreated = appembleActivity.getActivityCreateStatus();
		if (false == bBeingCreated && 
				((null == controlToUpdate && appembleActivity.getScreenModel().bOnResumeUpdate == false) || 
				(null != controlToUpdate && false == controlToUpdate.bOnResumeUpdate)))
			return true; 
		Vector <ControlModel> vControlsToUpdate;
			
		boolean bResult = true;
		
		// update the controlToUpdate.
		Integer iControlToUpdateId = Integer.valueOf((null == controlToUpdate? Constants.SCREEN_ID : controlToUpdate.id));		
		Cursor cursor = appembleActivity.getCursors().get(iControlToUpdateId);
		if (null != controlToUpdate)
			bResult = updateControl(appembleActivity, controlToUpdate, cursor, remoteData, bBeingCreated) && bResult;

		if (null == controlToUpdate)
			vControlsToUpdate = appembleActivity.getAllControls();
		else {	
			vControlsToUpdate = new Vector<ControlModel> ();
			AppembleActivityImpl.getChildControls(appembleActivity.getAllControls(), controlToUpdate.id, vControlsToUpdate);
		}
		// Set the data values for all the child controls except group controls. 
		// Call this function recursively for group controls 
		if (null != vControlsToUpdate)
			for (ControlModel childControlModel: vControlsToUpdate) {
			// update a control if its parent id = controlToUpdateId or
			// if parent id is screen and control belongs to the title.
			if (childControlModel.iParentId == iControlToUpdateId || (iControlToUpdateId == Constants.SCREEN_ID && 
					childControlModel.iParentId == Constants.TITLE_ID)) 
			{
				// if the controlToUpdate is not TITLE or BODY and this child's parent has the ChildControlDataMgmt is == SELF then call this function recursively
				if (null != controlToUpdate && (childControlModel.iParentId == iControlToUpdateId && 
						controlToUpdate.metaData.iChildControlDataMgmt == Constants.CHILD_CONTROL_DATA_MGMT_SELF))
					continue;
				// if a group control is found then call this function recursively
				if (childControlModel.isGroup() 
					&& null == childControlModel.sFieldName) // added null == sFieldName. Some group controls may want to get updated with Parent's cursor. In that case they will have a field name
					bResult = update(appembleActivity, childControlModel, remoteData) && bResult;
				// if controlModel has a local data source then call this function recursively.
				else if (childControlModel.sDeparameterizedLocalDataSource != null)
					bResult = update(appembleActivity, childControlModel, remoteData) && bResult;
				else // just update this child control with the parent's cursor
					bResult = updateControl(appembleActivity, childControlModel, cursor, remoteData, bBeingCreated) && bResult;
			}
		}
		if (null != cursor) {
			// if the cursor is not self managed and is not closed, close it.
			if (null != controlToUpdate && controlToUpdate.metaData.iCursorMgmt == Constants.CURSOR_MGMT_SELF) {
				appembleActivity.getCursors().remove(iControlToUpdateId);
			} else if (false == cursor.isClosed()) { 
				cursor.close();
				appembleActivity.getCursors().remove(iControlToUpdateId);
			}
		}
		return bResult;
	}
	
	private static boolean updateControl(AppembleActivity appembleActivity, ControlModel controlModel, Cursor cursor, 
			Bundle remoteData, boolean bBeingCreated) {
		boolean bResult = true;
		if (null == controlModel)
			return false;
		// Do not update the control if this function is being called in the function OnResume, 
		// which is called as a result of onResume and not being created and bOnResumeUpdate == false
		if (false == bBeingCreated && false == controlModel.bOnResumeUpdate)
			return true; 
		View controlView = findViewInScreen(appembleActivity, controlModel);
		if (null == controlView)
			return false; // TODO Log an error. The control was not created
		Bundle targetParameterValueList = appembleActivity.getTargetParameterValueList();
		if (AppembleActivityImpl.invokeMethod(controlView, controlModel, targetParameterValueList, Constants.ON_UPDATE))
			return true;

		// Now find the value of the control. 
		// If the control's data type is a cursor or data_source_type is a cursor then just set the cursor and return
		// else {
		// 		Now find the value of the control. Start with Default, 
		// 		Override default if value in localDataSource
		// 		Override localDataSource if value in remoteDataSource
		// 		Override remoteDataSource if value in targetParameterValueList
		// 		Override targetParameterValueList if value in cursor
		// 		Override cursor if value in remote data present
		// }
		if (controlModel.iDataType == Constants.CURSOR || controlModel.iDataSourceType == Constants.CURSOR) {
			((ControlInterface)controlView).setValue(cursor);
			return bResult;
		}
		// If the screen is being recreated && the mScreenData has a value for this field, set it and return
		if (appembleActivity.getActivityRecreateStatus()) { // If activity is being recreated.
			ActivityData activityData = appembleActivity.getActivityData();
			if (null != activityData && null != controlModel.sFieldName && 
					activityData.mScreenData.containsKey(controlModel.sFieldName)) {
				((ControlInterface)controlView).setValue(activityData.mScreenData.get(controlModel.sFieldName));
				return bResult;
			}
		}
// 		If remote data contains a value for this control, then set this value
// 		if controlValue is null, and if cursor contains a value for this control, then set this value
// 		if controlValue is null and targetParameterValueList contains a value for this control, then set this value
// 		if the remote data source field contains IMAGE:<URL for an image> then set this control value 
// 		If the local data source field contains IMAGE:<file name> then set this control value
// Assign the control value as its default_value,// 		Cannot have this algo because the remote data source or cursor can set the value of the control as null.		
//		Object controlValue = null; 
//		if (null != remoteData && remoteData.get(controlModel.sFieldName) != null)
//			controlValue = remoteData.get(controlModel.sFieldName);
//		if (null == controlValue && null != cursor && controlModel.sFieldName != null) {
//			int iIndex = cursor.getColumnIndex(controlModel.sFieldName);
//			if (iIndex > -1 && cursor.getCount() > 0)
//				controlValue = Utilities.getCursorData(cursor, iIndex, controlModel.iDataType); // .getString(iIndex);
//		}
//		if (null == controlValue && null != controlModel.sFieldName) {
//			Bundle targetParameterValueList = appembleActivity.getTargetParameterValueList();
//			if (null != targetParameterValueList && targetParameterValueList.containsKey(controlModel.sFieldName))
//				controlValue = targetParameterValueList.get(controlModel.sFieldName);
//		}
//		if (null == controlValue && null != controlModel.sDeparameterizedRemoteDataSource && 
//				(controlModel.sDeparameterizedRemoteDataSource.startsWith(Constants.URL_IMAGE_PREFIX) ||
//				false == controlModel.metaData.bProcessRemoteDS))
//			controlValue = controlModel.sDeparameterizedRemoteDataSource;
//		if (null == controlValue && null != controlModel.sDeparameterizedLocalDataSource && 
//				(controlModel.sDeparameterizedLocalDataSource.startsWith(Constants.URL_IMAGE_PREFIX)))
//			controlValue = controlModel.sDeparameterizedLocalDataSource;
//		if (null == controlValue)
//			controlValue = controlModel.sDefaultValue; // inputs.get(controlModel.getFieldName());
// 		Keeping this algorithm because a remote data or a cursor may return a null value for that control.
//		In that case, we should not set it to default value
		
		Object controlValue = null;//controlModel.sDefaultValue; // inputs.get(controlModel.getFieldName());
		boolean bControlValueShouldBeSet = false;
		if (null != controlModel.sDeparameterizedLocalDataSource && 
				(controlModel.sDeparameterizedLocalDataSource.startsWith(Constants.URL_IMAGE_PREFIX))) {
			controlValue = controlModel.sDeparameterizedLocalDataSource;
			bControlValueShouldBeSet = true;
		}
		if (null != controlModel.sDeparameterizedRemoteDataSource && 
				(controlModel.sDeparameterizedRemoteDataSource.startsWith(Constants.URL_IMAGE_PREFIX) ||
				false == controlModel.metaData.bProcessRemoteDS)) {
			controlValue = controlModel.sDeparameterizedRemoteDataSource;
			bControlValueShouldBeSet = true;
		}
		String sFieldName = controlModel.sFieldName;
		if (null != sFieldName && null != targetParameterValueList && 
				targetParameterValueList.containsKey(sFieldName)) {
			controlValue = targetParameterValueList.get(controlModel.sFieldName);
			bControlValueShouldBeSet = true;
		}
		if (null != cursor && sFieldName != null) {
			int iIndex = cursor.getColumnIndex(sFieldName);
			if (iIndex > -1 && cursor.getCount() > 0) {
				controlValue = Utilities.getCursorData(cursor, iIndex, 0 != controlModel.iDataSourceType ? 
						controlModel.iDataSourceType : controlModel.iDataType); // .getString(iIndex);
				bControlValueShouldBeSet = true;
			}
		}
		if (null != remoteData && remoteData.get(sFieldName) != null) {
			controlValue = remoteData.get(sFieldName);
			bControlValueShouldBeSet = true;
		}
		// Now set the value to the view.
		// set the value of the control even if cursor is null
		if (bControlValueShouldBeSet)
			((ControlInterface)controlView).setValue(controlValue);
		
		return bResult;
	}
	
	public static View findViewInScreen(AppembleActivity appembleActivity, 
			ControlModel controlModel) {
		View controlView = null;
		if (null == appembleActivity || null == controlModel)
			return controlView;
		if (controlModel.iParentId == Constants.TITLE_ID) {
			Vector<ControlModel> vTitleControls = new Vector<ControlModel>();
			// get top level controls for title
			AppembleActivityImpl.getChildControls(appembleActivity.getAllControls(), Constants.TITLE_ID, vTitleControls);
			controlView = findViewById(controlModel.id, appembleActivity.getRootLayout(), null, vTitleControls);
		}
		else {
			Vector<ControlModel> vBodyControls = new Vector<ControlModel>();
			// get top level controls for screen
			AppembleActivityImpl.getChildControls(appembleActivity.getAllControls(), Constants.SCREEN_ID, vBodyControls);
			controlView = findViewById(controlModel.id, appembleActivity.getRootLayout(), null, vBodyControls);
		}
		return controlView;
	}

	public static View findViewById(int iControlId, View parentView, 
			ControlModel parentControlModel, Vector <ControlModel> vControlsToSearch) {
		View view = null;
		if (null == parentView)
			return null; // parentView is a must to find the view.
		view = parentView.findViewById(iControlId);
		if (null != view)
			return view;
		if (null == vControlsToSearch)
			return null;
		// Call this function recursively for group controls 
		for (ControlModel pControlModel: vControlsToSearch) {
		// if a group control is found then call this function recursively
			if (pControlModel.isGroup()) {
				View pView = parentView.findViewById(pControlModel.id);
				Vector<ControlModel> vChildrenToSearch = new Vector<ControlModel> ();
				AppembleActivityImpl.getChildControls(vControlsToSearch, pControlModel.id, vChildrenToSearch);
				view = findViewById(iControlId, pView, pControlModel, vChildrenToSearch);
				if (null != view)
					break;
				else { // the control may not be found if the child controls are groups that manage their own control list like FRAGMENTS.
					// for each control
						// if it has OnFindView invoke it
					if (false == pControlModel.metaData.bHasMethodGetView)
						continue;
					try {
						Class<?> controlClass = Class.forName(pControlModel.metaData.sClass);
						Method mMethodName = controlClass.getMethod("getView", Integer.class, View.class, ControlModel.class, Vector.class);
						view = (View) mMethodName.invoke(view, iControlId, pView, pControlModel, vChildrenToSearch);
						if (null != view)
							break;
					} catch (ClassNotFoundException e) {
			    		LogManager.logError("Cannot initiate screen state change. Implementation class not found: " + 
			    			pControlModel.getClassName());
					} catch (NoSuchMethodException nsme) {
						// no issues. The control writer chose not to implement this method.
					} catch (InvocationTargetException ite) {
						
					} catch (IllegalAccessException iae) {
						LogManager.logError("Unable to access method for contorl Type: " + pControlModel.getClassName());
					}
				}
			}
		}		
		return view;
	}

	public static ControlModel findControlModelByName(AppembleActivity appembleActivity, 
			String sName) {
		if (null == appembleActivity || null == sName)
			return null;
		Vector<ControlModel> vControls = appembleActivity.getAllControls();
		for (ControlModel controlModel: vControls) {
			if (sName.equals(controlModel.sName))
					return controlModel;
		}
		return null; 
	}

	public static void progressBar(AppembleActivity appembleActivity, int iShow, Bundle remoteData) {
		ControlModel progressBarModel = AppembleActivityImpl.getProgressBar(appembleActivity.getAllControls());
		if (null != progressBarModel) {
			View controlView = findViewInScreen(appembleActivity, progressBarModel);
			if (controlView != null) {
				if (iShow == Constants.SHOW) {
					controlView.setVisibility(View.VISIBLE);
					update(appembleActivity, progressBarModel, remoteData);
				} else
					controlView.setVisibility(View.GONE);
			}
		}
	}
	
	// Currently the routine below only returns children of the controlId. However, if grand children are present,
	// they would not be included. If need be this routine needs to be modified to include the grand children.
	public static void getChildControls(Vector<ControlModel> vControls, int controlId, Vector<ControlModel> vChildControls) {
		if (null == vControls || null == vChildControls)
			return;
		
		for (ControlModel controlModel: vControls) {
			if (controlModel.iParentId == controlId) {
				vChildControls.add(controlModel);
				if (controlId != Constants.SCREEN_ID && controlModel.isGroup()) // for all group controls other than screen, include grand children also
					getChildControls(vControls, controlModel.id, vChildControls);
			}
		}
		return; 
	}

	public static ControlModel getProgressBar(Vector<ControlModel> vControls) {
		if (null == vControls)
			return null;
		ControlModel screendeckProgressBar = null;
		int progressBarControlType = ConfigManager.getInstance().getControlId("PROGRESSBAR");
		for (ControlModel controlModel: vControls) {
			// if (controlModel.iType == Constants.PROGRESSBAR) {
			if (controlModel.getType() == progressBarControlType) {
				if (controlModel.iScreenId > 0)
					return controlModel;
				else
					screendeckProgressBar = controlModel;
			}
		}
		return screendeckProgressBar; 
	}

	public static void setBackground(DynamicLayout dynamicLayout, String sBackground) {
		if (null == sBackground) 
			return;
		if (sBackground.startsWith("#"))
			dynamicLayout.setBackgroundColor(Color.parseColor(sBackground));
		else
//			ImageManager.getInstance().UpdateView(dynamicLayout, sBackground, true);
			AppearanceManager.getInstance().updateBackground(dynamicLayout, sBackground);
	}
	
	public static boolean isNetworkAvailable(Context context) {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected();
	}

	public static int whichNetworkConnection(Context context) {
	    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	            if (ni.isConnected())
	                return Constants.NETWORK_CONNECTIVITY_WIFI;
	        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	            if (ni.isConnected())
	                return Constants.NETWORK_CONNECTIVITY_MOBILE;
	    }
	    return 0;
	}	

//	public static void onResume(int iParentId, View parentView, Vector<ControlModel> vControls, Bundle targetParameterValueList) {
//		if (parentView == null)
//			return;
//		for (ControlModel controlModel: vControls) {
//			if (controlModel.iParentId != iParentId)
//				continue;			
//			View view = parentView.findViewById(controlModel.id);
//			if (null != view)
//				((ControlInterface)view).onResume(targetParameterValueList);
//			// if a group control is found then call this function recursively
//			if (controlModel.isGroup())
//				onResume(controlModel.id, view, vControls, targetParameterValueList); // call onResume for its children
//		}
//		return;
//	}

	public static void onScreenStateChange(int iParentId, View parentView, 
			Vector<ControlModel> vControls, Bundle targetParameterValueList, int iEventType) {
		if (parentView == null || 0 == iEventType || null == vControls || vControls.size() == 0)
			return;
		View view = null;
		for (ControlModel controlModel: vControls) {
			if (controlModel.iParentId != iParentId)
				continue;
			view = parentView.findViewById(controlModel.id);
			invokeMethod(view, controlModel, targetParameterValueList, iEventType);
//			try {
//				String sMethodName = ConfigManager.mMethodsMap.get(iEventType);
//				if (controlModel.metaData != null && ConfigManager.methodExists(controlModel.metaData.sName, sMethodName)) { // if this method exists? 
//					Class<?> cmClass = Class.forName(controlModel.getClassName());
//					Method mMethodName = cmClass.getMethod(sMethodName, Bundle.class);
//					view = parentView.findViewById(controlModel.id);
//					if (null != view)
//						mMethodName.invoke(view, targetParameterValueList);
//				}
//			} catch (ClassNotFoundException e) {
//	    		LogManager.logError("Cannot initiate screen state change. Implementation class not found: " + 
//	    				controlModel.getClassName());
//			} catch (NoSuchMethodException nsme) {
//				continue; // no issues. The control writer chose not to implement this method.
//			} catch (InvocationTargetException ite) {
//				
//			} catch (IllegalAccessException iae) {
//				LogManager.logError("Unable to access method for event Type: " + iEventType);
//			}
			if (controlModel.isGroup()) {
//				if (null == view) // if a group control is found then call this function recursively
//				view = parentView.findViewById(controlModel.id);
				onScreenStateChange(controlModel.id, view, vControls, targetParameterValueList, 
						iEventType); // call onScreenStateChange for its children
			}
		}
		return;
	}

	public static boolean invokeMethod(View view, 
			ControlModel controlModel, Bundle targetParameterValueList, int iEventType) {
		if (view == null || 0 == iEventType || null == controlModel)
			return false;
		try {
			String sMethodName = ConfigManager.mMethodsMap.get(iEventType);
			if (controlModel.metaData != null && ConfigManager.methodExists(controlModel.metaData.sName, sMethodName)) { // if this method exists? 
				Class<?> cmClass = Class.forName(controlModel.getClassName());
				Method mMethodName = cmClass.getMethod(sMethodName, Bundle.class);
				mMethodName.invoke(view, targetParameterValueList);
				return true;
			}
		} catch (ClassNotFoundException e) {
    		LogManager.logError("Cannot initiate screen state change. Implementation class not found: " + 
    				controlModel.getClassName());
		} catch (NoSuchMethodException nsme) {
			// no issues. The control writer chose not to implement this method.
		} catch (InvocationTargetException ite) {
			
		} catch (IllegalAccessException iae) {
			LogManager.logError("Unable to access method for event Type: " + iEventType);
		}
		return false;
	}

	public static void showAlertAndCloseScreen(final Context context, String sTitle, String sMessage) {
		if (null == sTitle && null == sMessage)
			return;
		AlertDialog.Builder alt_bld = new AlertDialog.Builder(context);
		alt_bld.setTitle(sTitle);
		alt_bld.setMessage(sMessage);
		alt_bld.setCancelable(false);
		alt_bld.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
			//  Action for 'NO' Button
			dialog.cancel();
			((Activity)context).finish();
			}
		});
		
		AlertDialog alert = alt_bld.create();
	
		// Icon for AlertDialog
		alert.setIcon(android.R.drawable.btn_star);
		alert.show();
	}
	
	public static float getCustomTitleBarHeight(Vector<ControlModel> vControlsInTitle, float fScreenHeight) {
		if (null == vControlsInTitle || vControlsInTitle.size() == 0)
			return 0;
		float fTitleBottom = 0;
		for (ControlModel controlModel : vControlsInTitle) {
			if (null == controlModel)
				continue;
			float fControlBottom = 0;
			// By now, the controlModel's dimensions have changed to real pixels.
			float fY = Utilities.getDimension(controlModel.sY, fScreenHeight);
			float fHeight = Utilities.getDimension(controlModel.sHeight, fScreenHeight); 
			fControlBottom = fY + fHeight;
			fTitleBottom = fTitleBottom < fControlBottom ? fControlBottom : fTitleBottom;
		}
		return fTitleBottom;
	}
	
	public static int getDefaultTitleBarHeight(Activity activity) {
		Rect rect = new Rect();
	    Window win = activity.getWindow();
	    win.getDecorView().getWindowVisibleDisplayFrame(rect);
	    int contentViewTop = win.findViewById(Window.ID_ANDROID_CONTENT).getTop();
	    return contentViewTop - rect.top; // title bar height
	}
	
	public static boolean setDataSource(String sControlName, String sDataSource, ScreenModel screenModel,
			Vector<ControlModel> vControls) {
		if (null == sControlName)
			return false;
		if (screenModel.sName.equals(sControlName)) {
			screenModel.sRemoteDataSource = sDataSource;
			return true;
		}
		for (ControlModel controlModel: vControls) {
			if (null != controlModel && null != controlModel.sName && controlModel.sName.equals(sControlName)) {
				controlModel.sRemoteDataSource = sDataSource;
				return true;
			}
		}
		return false;
	}
	
	public static boolean setRemoteDataSource(int iControlId, String sDataSource, ScreenModel screenModel,
			Vector<ControlModel> vControls) {
		if (iControlId == Constants.SCREEN_ID) {
			screenModel.sRemoteDataSource = sDataSource;
			return true;
		}
		for (ControlModel controlModel: vControls) {
			if (null != controlModel && controlModel.id == iControlId) {
				controlModel.sRemoteDataSource = sDataSource;
				return true;
			}
		}
		return false;
	}

	public static boolean setLocalDataSource(int iControlId, String sDataSource, ScreenModel screenModel,
			Vector<ControlModel> vControls) {
		if (iControlId == Constants.SCREEN_ID) {
			screenModel.sLocalDataSource = sDataSource;
			return true;
		}
		for (ControlModel controlModel: vControls) {
			if (null != controlModel && controlModel.id == iControlId) {
				controlModel.sLocalDataSource = sDataSource;
				return true;
			}
		}
		return false;
	}
	
	public static boolean closeScreens(AppembleActivity appembleActivity) {
		boolean bReturn = false;
		if (null == appembleActivity)
			return bReturn;
		if (Cache.iNumScreensToClose > 0) {
			Cache.iNumScreensToClose--;
			appembleActivity.finish();
			return true;
		} else if (Cache.sScreenToClose != null) {
			ActivityData activityData = appembleActivity.getActivityData();
			if (null != activityData && null != activityData.screenModel &&
				activityData.screenModel.sName != null) {  
				if (false == appembleActivity.getActivityData().screenModel.sName.
					equalsIgnoreCase(Cache.sScreenToClose)) {  // if the current screen name does not matches the sScreenToClose, close it.
					appembleActivity.finish();
				} else { // If the screen name matches
					Cache.sScreenToClose = null; // set the value to null.
					if (Cache.bScreenCloseUpto) { // if bScreenToCloseUpto
						Cache.bScreenCloseUpto = false;
						appembleActivity.finish();
					}
				}
				return true;
			}
		}
		return false;
	}
	
	public static Bundle getValues(Bundle screenData, View view) {
		if (null == view)
			return screenData;
		if (null == screenData)
			screenData = new Bundle();
		int iNumberOfChildren = 0;
		ControlModel cm = null; Object fieldValue = null;
		if (view instanceof ControlInterface) {
			cm = ((ControlInterface)view).getControlModel();
			if (cm.iPermission != Constants.READWRITE)
				return screenData;
			if (cm.metaData.isGroup == false || 
				Constants.CHILD_CONTROL_DATA_MGMT_SELF == cm.metaData.iChildControlDataMgmt) {
				fieldValue = Action.getValueFromView(view);
				fieldValue = Action.validateFormat(cm, fieldValue);
				Utilities.putData(screenData, cm.iDataType, cm.sFieldName, fieldValue);
				return screenData;
			}
		}
		if (view instanceof ViewGroup) {
			ViewGroup viewGroup = (ViewGroup) view;
			iNumberOfChildren = viewGroup.getChildCount();
			for (int i = 0; i < iNumberOfChildren; i++)
				getValues(screenData, viewGroup.getChildAt(i));
		}
		return screenData;
	}
	
	public static Bundle collectScreenData(AppembleActivity appembleActivity) {
		if (null == appembleActivity)
			return null;
		ActivityData activityData = appembleActivity.getActivityData();
		if (null == activityData)
			return null;
		if (null == activityData.mScreenData)
			activityData.mScreenData = new Bundle();
		else
			activityData.mScreenData.clear();

		activityData.mScreenData = getValues(activityData.mScreenData, appembleActivity.getRootLayout());
		return activityData.mScreenData;
	}

	public static void setDefaultValues(View view) {
		if (null == view)
			return;
		int iNumberOfChildren = 0;
		ControlModel cm = null;
		if (view instanceof ControlInterface) {
			cm = ((ControlInterface)view).getControlModel();
			if (null != cm && cm.metaData.bCanHaveDefaultValue) { // commented 20140109 && (cm.metaData.isGroup == false || 
			//	Constants.CHILD_CONTROL_DATA_MGMT_SELF == cm.metaData.iChildControlDataMgmt)) {
				((ControlInterface) view).setValue(cm.sDefaultValue);
				return;
			}
		}
		if (view instanceof ViewGroup) {
			ViewGroup viewGroup = (ViewGroup) view;
			iNumberOfChildren = viewGroup.getChildCount();
			for (int i = 0; i < iNumberOfChildren; i++) {
				View childView = viewGroup.getChildAt(i);
				if (null != cm && cm.metaData.isGroup == true  && cm.metaData.bCanHaveDefaultValue 
					&& childView instanceof ControlInterface) {
					ControlModel cmChild = ((ControlInterface)childView).getControlModel();
					if (cmChild.iParentId == cm.id && 
						Constants.CHILD_CONTROL_DATA_MGMT_SELF == cm.metaData.iChildControlDataMgmt)
						continue;
				}
				setDefaultValues(childView);
			}
		}
		return;
	}	

    public static boolean showEmptyListControls(View view, ControlModel controlModel, Context context, boolean bVisibility) {
        String sEmptyListControl = controlModel.mExtendedProperties.get("on_empty_show_ids");
        if (null == sEmptyListControl || 0 == sEmptyListControl.length())
            return true;
        String[] sEmptyListControls = Utilities.splitCommaSeparatedString(sEmptyListControl);
//        AppembleActivity appembleActivity = Utilities.getActivity(context, view, null);

        // Bug in the old commented code below: the controls show up even if there is data. 
        // New Algo - Find the view of the empty controls using the rootView of the view.
        View rootView = view.getRootView();
//        Vector<ControlModel> vControls = appembleActivity.getAllControls();
		for (String sControlId : sEmptyListControls) {
//			ControlModel cm = Action.getControl(appembleActivity.getRootLayout(), vControls,
//					sControlName, Constants.FROM_NAME);
//			if (null != cm) {
				View v = rootView.findViewById(Integer.parseInt(sControlId));
			if (null != v)
				v.setVisibility(bVisibility ? View.VISIBLE : View.INVISIBLE);
			else
				LogManager.logError("Unable to find control: " + sControlId + " to show or hide");
		}

//        Vector<ControlModel> vControls = appembleActivity.getAllControls();
//		for (String sControlName : sEmptyListControls) {
//			ControlModel cm = Action.getControl(appembleActivity.getRootLayout(), vControls,
//					sControlName, Constants.FROM_NAME);
//			if (null != cm) {
//				View v = AppembleActivityImpl.findViewInScreen(appembleActivity, cm);
//				if (null != v)
//					v.setVisibility(bVisibility ? View.VISIBLE : View.INVISIBLE);
//			} else
//				LogManager.logError("Unable to find control: " + sControlName + " to show or hide");
//		}
        return true;
    }


    public static void addEmptyControlIds(ControlModel controlModel, ScreenModel screenModel, Vector<ControlModel> vAllControls) {
        String sEmptyListControl = controlModel.mExtendedProperties.get("on_empty_show");
        if (null == sEmptyListControl || 0 == sEmptyListControl.length())
            return;
        String[] sEmptyListControls = Utilities.splitCommaSeparatedString(sEmptyListControl);
		if (null == vAllControls)
			vAllControls = ControlDao.getAllControlsinScreen(controlModel.sSystemDbName, controlModel.sContentDbName, 
			screenModel.sName);
		List<String> listEmptyControls = Arrays.asList(sEmptyListControls);
		StringBuffer sCommaSeparatedEmptyControlIds = new StringBuffer();
		for (ControlModel cm : vAllControls) {
			if (null != cm.sName && listEmptyControls.contains(cm.sName))
				sCommaSeparatedEmptyControlIds.append(sCommaSeparatedEmptyControlIds.length() > 0 ? ", " + cm.id : cm.id);
		}
		controlModel.mExtendedProperties.put("on_empty_show_ids", sCommaSeparatedEmptyControlIds.toString());
    }
}
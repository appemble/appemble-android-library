package com.appemble.avm.dynamicapp;

import java.util.HashMap;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.os.Bundle;

import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class ActivityData {
	public String sScreenName = null;
	public ScreenModel screenModel; 
	public Vector<ControlModel> vControls; // contains all controls 
	// vControls and vControlsInScreenAndTitle share the same instance of contorlModel
	public Vector<ControlModel> vControlsInScreenAndTitle; // contains all controls (parentid == -1 && parendId == -5) 
	public Bundle targetParameterValueList; // Stores the input parameter list
	@SuppressLint("UseSparseArrays")
	public HashMap<Integer, Cursor> cursors = new HashMap<Integer, Cursor>();
	public Bundle mScreenData;
}

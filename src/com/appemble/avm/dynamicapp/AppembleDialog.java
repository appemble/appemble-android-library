/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dynamicapp;

import java.util.HashMap;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.R;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ControlDao;
import com.appemble.avm.models.dao.ScreenDao;
import com.appemble.avm.models.dao.TableDao;

public class AppembleDialog extends Dialog implements AppembleActivity {
//	Vector<ControlModel> vControls;
//	Bundle targetParameterValueList;
	DynamicLayout mBodyLayout;
//	ScreenModel screenModel;
	@SuppressLint("UseSparseArrays")
	HashMap<Integer, Cursor> cursors = new HashMap<Integer, Cursor>();
	boolean isNetworkAvailable = true;
	boolean bBeingCreated = false;
	boolean isDestroyed = false;
	boolean bCloseParent = false;
	public ActivityData mActivityData;
	
	public AppembleDialog(Context context) {
		super(context);
		if (context instanceof Activity)
			setOwnerActivity((Activity) context);
		mActivityData = new ActivityData();
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
	}

	@Override
	public boolean isDestroyed() {
		return isDestroyed;
	}

	@Override
	public void finish() {
		isDestroyed = true;
		Context context = getOwnerActivity();
		AppembleActivity appembleActivity = null;
		if (context instanceof AppembleActivity) {
			appembleActivity = (AppembleActivity)context;
		}
		if (bCloseParent) {
			if (null != appembleActivity) {
				appembleActivity.finish();
			} else {
				Activity activity = getMyParent();
				activity.finish();
			}
		}
	    ActionModel[] actions = mActivityData.screenModel.getActions(Constants.ON_DESTROY_SCREEN);
	    for (int i = 0; null != actions && i < actions.length; i++) {
	    	actions[i].mScreenObject = this;
	    	Action.callAction(getOwnerActivity(), null, mBodyLayout, actions[i], mActivityData.targetParameterValueList);
	    }
		dismiss();
	}

	@Override
	public Vector<ControlModel> getAllControls() {
		return mActivityData.vControls;
	}

	@Override
	public ScreenModel getScreenModel() {
		return mActivityData.screenModel;
	}

	@Override
	public Bundle getTargetParameterValueList() {
		return mActivityData.targetParameterValueList;
	}

	public void onResume() {
	    ActionModel[] actions = mActivityData.screenModel.getActions(DynamicActivity.BEFORE_ON_RESUME_SCREEN); // could be saved in memory as actionOnResume to save DB call (in OnCreate)
	    for (int i = 0; null != actions && i < actions.length; i++) {
	    	actions[i].mScreenObject = this;
	    	Action.callAction(getOwnerActivity(), null, mBodyLayout, actions[i], mActivityData.targetParameterValueList);
	    }
	    AppembleActivityImpl.deparameterizeDataSources(mActivityData.screenModel, mActivityData.vControls, 
	    		mActivityData.targetParameterValueList, mActivityData.screenModel.sContentDbName); // substitute <parameter> with value in targetParameterValueList
		LogManager.logVerbose("Resuming screen: " + mActivityData.screenModel.sName + 
				". \r\n With Local Data Source is : " + mActivityData.screenModel.sDeparameterizedLocalDataSource + 
				". \r\n And Remote Data Source is : " + mActivityData.screenModel.sDeparameterizedRemoteDataSource);
		// Call onResume function for every view in the screen.
//		AppembleActivityImpl.onResume(Constants.TITLE_ID, mBodyLayout, vControls, targetParameterValueList);
		AppembleActivityImpl.onScreenStateChange(Constants.SCREEN_ID, mBodyLayout, mActivityData.vControls, 
				mActivityData.targetParameterValueList, Constants.ON_RESUME_SCREEN);
		long lUpdateTime = 0;
		if (Constants.isDebugMode)
			lUpdateTime = System.currentTimeMillis();
		if (bBeingCreated || mActivityData.screenModel.bOnResumeUpdate) {
		    AppembleActivityImpl.setDefaultValues(mBodyLayout);
			boolean bIsRemoteDataPresent = AppembleActivityImpl.isRemoteDataPresent(mActivityData.screenModel, 
					mActivityData.vControls);
			if (!bIsRemoteDataPresent) {
				if (cursors.size() > 0) {
					LogManager.logVerbose("Cursors should be present at the start of the Activity.onResume");
					cursors.clear();
				}
				AppembleActivityImpl.fetchLocalData(this, null, true); 
				// paint the screen with local data. It is part of lazy loading...
				if (false == AppembleActivityImpl.update(this, null, null)) {
					bBeingCreated = false;
					return; // if the function return 0 means there is an error while executing the function.
				}
			}
			if (Constants.isDebugMode) {
				lUpdateTime = System.currentTimeMillis() - lUpdateTime;
				LogManager.logDebug("Local data update: " + lUpdateTime + "ms");
			}
			// Fetch remote data in the background thread and then use UI thread to update the UI
			if (bIsRemoteDataPresent)
				isNetworkAvailable = AppembleActivityImpl.updateRemoteData(this, null, true, null, isNetworkAvailable,
						bBeingCreated);
			else // do not update bBeingCreated if RemoteDataIsPresent. It will be set to false in onFinish() 
				bBeingCreated = false;
		}
	    actions = mActivityData.screenModel.getActions(Constants.ON_RESUME_SCREEN); // could be saved in memory as actionOnResume to save DB call (in OnCreate)
	    for (int i = 0; actions != null && i < actions.length; i++) {
	    	actions[i].mScreenObject = this;
	    	Action.callAction(getOwnerActivity(), null, mBodyLayout, actions[i], mActivityData.targetParameterValueList);
	    }
		if (mActivityData.screenModel.sInitialFocus != null) {
			View vInitialFocus = AppembleActivityImpl.findViewInScreen(this, 
					AppembleActivityImpl.findControlModelByName(this, mActivityData.screenModel.sInitialFocus));
			if (null != vInitialFocus) {
				vInitialFocus.setFocusableInTouchMode(true);
				vInitialFocus.requestFocus();
			}
		}		
	}

	@Override
	public Activity getMyParent() {
		Context context = getOwnerActivity();
		if (context instanceof Activity)
			return (Activity)context;
		return null;
	}

	public boolean createLayout(String sSystemDbName, String sContentDbName, String sScreenName, 
			Bundle targetParameterValueList) {
		Context context = getOwnerActivity();
		if (null == Cache.context && null != context && false == Cache.initiate(context.getApplicationContext(), true))
			return false;
		if (null == context)
			context = Cache.context;
		bBeingCreated = true;
		mActivityData.screenModel = ScreenDao.getScreenByName(sSystemDbName, sContentDbName, sScreenName);
		mActivityData.vControls = ControlDao.getAllControlsinScreen(sSystemDbName, sContentDbName, sScreenName);
		mActivityData.targetParameterValueList = targetParameterValueList;
		if (null == mActivityData.vControls || null == mActivityData.screenModel)
			return false;
	    ActionModel[] actions = mActivityData.screenModel.getActions(DynamicActivity.BEFORE_ON_CREATE_SCREEN);
	    for (int i = 0; null != actions && i < actions.length; i++) {
	    	actions[i].mScreenObject = this;
	    	Action.callAction(this.getOwnerActivity(), null, null, actions[i], targetParameterValueList);
	    }
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		mActivityData.screenModel.fWidthInPixels = (mActivityData.screenModel.iDimensionType == Constants.PIXELS ? 
				mActivityData.screenModel.fWidth : (mActivityData.screenModel.fWidth * display.getWidth()) / 100); 
		mActivityData.screenModel.fHeightInPixels = (mActivityData.screenModel.iDimensionType == Constants.PIXELS ? 
				mActivityData.screenModel.fHeight : (mActivityData.screenModel.fHeight * display.getHeight()) / 100);
		AppearanceManager.getInstance().setInitialWindowHeight(mActivityData.screenModel.iAllowedLayouts, 
				(int)mActivityData.screenModel.fWidthInPixels, (int)mActivityData.screenModel.fHeightInPixels);
		this.getWindow().setLayout((int)mActivityData.screenModel.fWidthInPixels, (int)mActivityData.screenModel.fHeightInPixels); //Controlling width and height.
		if (null == mActivityData.screenModel.sTitle)
			this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		else
			setTitle(mActivityData.screenModel.sTitle);
		// if (false == dynamicLayout.generateLayout(Constants.DIALOG, Constants.SCREEN_ID, screenModel, vChildControls))
		mBodyLayout = new DynamicLayout(context);
		mBodyLayout.setTag(R.integer.dialog, this);
		mBodyLayout.setTag(R.integer.targetParameterValueList, targetParameterValueList);
		mBodyLayout.setTag(R.integer.screenModel, mActivityData.screenModel);
		// Add a invisible control covering the complete width of the dialog width. This is needed
		// as the dialog's width is calculated based on the maximum width of any child control present.
		addDummyTextFullWidthControl();
		if (false == DynamicLayout.generateLayout(mBodyLayout, Constants.SCREEN_ID, mActivityData.screenModel.fWidthInPixels,
				mActivityData.screenModel.fHeightInPixels, mActivityData.screenModel, mActivityData.vControls))
			return false;
		String sBackground = mActivityData.screenModel.sBackground != null ? mActivityData.screenModel.sBackground :
			TableDao.getColumnValue(sSystemDbName, "_screen_deck", "background", null);
		AppembleActivityImpl.setBackground(mBodyLayout, sBackground);
		// add scroll view if needed
		// Add the body layout
		HorizontalScrollView hsv = null;
		if (mActivityData.screenModel.iScrolling == Constants.HSCROLL || mActivityData.screenModel.iScrolling == Constants.SCROLL_BOTH_WAYS) {
			hsv = new HorizontalScrollView(context);
			hsv.addView(mBodyLayout);
		}
		ScrollView vsv = null;
		if (mActivityData.screenModel.iScrolling == Constants.VSCROLL || 
				mActivityData.screenModel.iScrolling == Constants.SCROLL_BOTH_WAYS) {
			vsv = new ScrollView(context);
			if (null != hsv)
				vsv.addView(hsv);
			else
				vsv.addView(mBodyLayout);
		}
		if (null != vsv) 
			setContentView(vsv);
		else if (null != hsv)
			setContentView(hsv);
		else
			setContentView(mBodyLayout);
				
		// call function to fetch remote data, local data
		// update the controls.
//		setContentView(mBodyLayout);
		this.setCancelable(Utilities.parseBoolean(mActivityData.screenModel.mExtendedProperties.get("cancelable")));
		this.setCanceledOnTouchOutside(Utilities.parseBoolean(mActivityData.screenModel.mExtendedProperties.get("cancelable_on_touch_outside")));
	    actions = mActivityData.screenModel.getActions(DynamicActivity.ON_CREATE_SCREEN);
	    for (int i = 0; null != actions && i < actions.length; i++) {
	    	actions[i].mScreenObject = this;
	    	Action.callAction(this.getOwnerActivity(), null, mBodyLayout, actions[i], targetParameterValueList);
	    }
		return true;
	}

	@Override
	public HashMap<Integer, Cursor> getCursors() {
		return cursors;
	}

	@Override
	public View getRootLayout() {
		return mBodyLayout;
	}

	@Override
	public void setActivityCreateStatus(boolean bCreated) {
		bBeingCreated = bCreated;
	}

	@Override
	public boolean getActivityCreateStatus() {
		return bBeingCreated;
	}

	@Override
	public boolean getActivityRecreateStatus() {
		return false;
	}
	
	@Override
	public boolean getNetworkAvailableFlag() {
		return isNetworkAvailable;
	}
	
	private void addDummyTextFullWidthControl() {
		if (null == mActivityData.vControls || mActivityData.vControls.size() == 0)
			return;
		ControlModel sampleControl = mActivityData.vControls.get(0);
		ControlModel cmDummy = new ControlModel();
		cmDummy.sX = cmDummy.sY = cmDummy.sHeight = "0"; cmDummy.sWidth = String.valueOf(mActivityData.screenModel.fWidth);
		cmDummy.setType("TEXT");cmDummy.iDimensionType = Constants.PERCENTAGE; 
		cmDummy.sContentDbName = mActivityData.screenModel.sContentDbName; cmDummy.sSystemDbName = 
				mActivityData.screenModel.sSystemDbName;
		cmDummy.id = 23423; cmDummy.iAppearanceId = sampleControl.iAppearanceId; 
		cmDummy.iScreenId = sampleControl.iScreenId; cmDummy.mExtendedProperties = new HashMap<String, String>();
		cmDummy.iParentId = Constants.SCREEN_ID; cmDummy.bIsVisible = true;
		mActivityData.vControls.add(cmDummy);
	}
	
	public void setCloseParentActivity(boolean bCloseParent) {
		this.bCloseParent = bCloseParent;
	}
	
	public ActivityData getActivityData() {
		return mActivityData;
	}
}

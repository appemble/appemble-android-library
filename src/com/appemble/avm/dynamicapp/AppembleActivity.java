/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dynamicapp;

import java.util.HashMap;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public interface AppembleActivity {
	public Context getContext();
	public HashMap<Integer, Cursor> getCursors();
	public View getRootLayout();
	public void setActivityCreateStatus(boolean bBoolean);
	public boolean getActivityCreateStatus();
	public boolean getActivityRecreateStatus();
	public boolean getNetworkAvailableFlag();
	void startActivityForResult(Intent intent, int requestCode);
	public boolean isDestroyed();
	public void finish();
	public Vector<ControlModel> getAllControls(); // contains all controls 
	public ScreenModel getScreenModel(); // contains all controls 
	public Bundle getTargetParameterValueList(); 
//	public boolean updateRemoteData(ControlModel controlToUpdate, boolean bRecursive, ActionInterface ai);
//candidate for removal 2013-03-11	public boolean setLocalDataSource(String sControlName, String sDataSource);

	//candidate for removal 2013-03-11		public void onResume();
	public Activity getMyParent();
	public ActivityData getActivityData();
}
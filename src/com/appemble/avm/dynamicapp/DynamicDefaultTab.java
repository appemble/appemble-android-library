/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dynamicapp;

import java.util.Vector;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TabHost;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.ImageManager;
import com.appemble.avm.LogManager;
import com.appemble.avm.models.AppearanceModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ScreenDao;

public class DynamicDefaultTab extends TabActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		String sSystemDbName = null, sContentDbName = null, sStartingScreenName = null;
		Bundle targetParameterValueList; // Stores the input parameter list

		super.onCreate(savedInstanceState);

	    final Window window = getWindow();	    
	    if(window.getContainer() == null) { // If the window has a container, then we are not free to request window features.
	        window.requestFeature(Window.FEATURE_NO_TITLE);
	    }
		TabHost tabHost = getTabHost(); // The activity TabHost
		TabHost.TabSpec spec; // Reusable TabSpec for each tab
		Intent intent; // Reusable Intent for each tab
		if (null == Cache.context && false == Cache.initiate(this.getApplicationContext(), true))
			return;

		if (getIntent().hasExtra(Constants.STR_SYSTEM_DATABASE_NAME))
			sSystemDbName = getIntent().getExtras().getString(Constants.STR_SYSTEM_DATABASE_NAME);
		else 
			sSystemDbName = Cache.bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME);
		if (getIntent().hasExtra(Constants.STR_CONTENT_DATABASE_NAME))
			sContentDbName = getIntent().getExtras().getString(Constants.STR_CONTENT_DATABASE_NAME);
		else 
			sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);

		if (sSystemDbName == null || null == sContentDbName) {
			LogManager.logError("Error in finding system database. Please check res/values/strings.xml for system_dbname and content_dbname. Make sure they are present in the assets folder. Aborting...");
			finish();
			return;
		}
		if (getIntent().hasExtra(Constants.STR_SCREEN_NAME))
			sStartingScreenName = getIntent().getExtras().getString(Constants.STR_SCREEN_NAME);

//		targetParameterValueList = (HashMap<String, String>)getIntent().getSerializableExtra("targetParameterValueList");
//		targetParameterValueList = (HashMap<String, String>) Utilities.deserializeObject(
//			getIntent().getByteArrayExtra("targetParameterValueList"));
		targetParameterValueList = getIntent().getExtras();

		ScreenModel mScreen = ScreenDao.getScreenByName(sSystemDbName, sContentDbName, sStartingScreenName);
		if (null == mScreen) {
			LogManager.logError("Unable to create TABS with starting screen as: " + sStartingScreenName);
			finish();
			return;
		}
		Vector<ScreenModel> tabs = ScreenDao.getScreenGroup(sSystemDbName, sContentDbName, mScreen.sTabGroupName); // get screen group.
		if (null == tabs || tabs.size() == 0) {
			LogManager.logError("Error creating TABS. Cannot find screens with TABS= " + mScreen.sTabGroupName + " from the database: " + sSystemDbName);
			finish();
			return;
		}
		LogManager.logVerbose("Creating tabs: " + mScreen.sTabGroupName + ". SystemDbName=" + sSystemDbName + " sContentDbName = " + sContentDbName);
		int iScreen = 0;
		if (tabs.size() > 1) {
			for (ScreenModel screenModel: tabs) {
				Class<?> cls = ScreenModel.getClass(screenModel, true);
				intent = ScreenModel.getIntent(screenModel, targetParameterValueList, cls);
				// Initialize a TabSpec for each tab and add it to the TabHost
				Drawable drawable = ImageManager.getInstance().getDrawable(null, screenModel.sIcon, null, true, 0);
				spec = tabHost.newTabSpec(screenModel.sMenuName)
						.setIndicator(screenModel.sMenuName, drawable).setContent(intent);
				tabHost.addTab(spec);
				View tabView = tabHost.getTabWidget().getChildAt(iScreen);
				// user has given the tab bar height for the first screen. That is the default for the whole tab bar.
				if (screenModel.fTabBarHeight > 0) {
					Display display = ((WindowManager) this.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
					float fTabBarHeight = (screenModel.iDimensionType == Constants.PIXELS ? 
							screenModel.fTabBarHeight : (screenModel.fTabBarHeight * display.getHeight()) / 100);
					tabView.getLayoutParams().height = (int)fTabBarHeight;
				}
				setTabBackground(screenModel, tabView);
				iScreen++;
			}
//			if (0 == Cache.iTabHeight)
//				Cache.iTabHeight = tabHost.getTabWidget().getChildAt(0).getLayoutParams().height;
			tabHost.setCurrentTab(mScreen.iMenuOrder);
		} else if (tabs.size() == 1){
			// No tabs needed if its just one screen
			ScreenModel screen = tabs.get(0);
			Class<?> cls = ScreenModel.getClass(screen, true);
			// intent.setClass(this, DynamicScrollView.class);
			intent = ScreenModel.getIntent(screen, targetParameterValueList, cls);
			startActivity(intent);
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (Cache.cookieSyncManager != null)
			Cache.cookieSyncManager.sync();		
	}
	
	private void setTabBackground(ScreenModel screenModel, View tabView) {
		if (null == screenModel)
			return;
		// set the tab background (image or appearance)
		String sBackgroundImage = screenModel.mExtendedProperties.get("tab_bar_background_image");
		String sAppearanceName = screenModel.mExtendedProperties.get("tab_bar_appearance_name");
		if (null == sBackgroundImage && null == sAppearanceName)
			return;
		int iAppearanceId = 0;
		if (sAppearanceName != null) {
			AppearanceModel am = AppearanceManager.getInstance().getAppearance(screenModel.sSystemDbName, sAppearanceName);
			if (null != am)
				iAppearanceId = am.id;
		}
		if (null == sBackgroundImage && 0 == iAppearanceId)
			return;
		ControlModel cm = new ControlModel();
		cm.iAppearanceId = iAppearanceId; 
		cm.sSystemDbName = screenModel.sSystemDbName; cm.sContentDbName = screenModel.sContentDbName;
		AppearanceManager.getInstance().updateBackgroundAppearance(tabView, cm, null, sBackgroundImage);
	}
}

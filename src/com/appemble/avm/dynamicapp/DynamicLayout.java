/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dynamicapp;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Vector;

import android.content.Context;
import android.graphics.PointF;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class DynamicLayout extends RelativeLayout {

	public DynamicLayout(Context context) {
		super(context);
	}

	public static boolean generateLayout(ViewGroup viewGroup, int iParentId, float fParentWidthPixels, float fParentHeightPixels,
			ScreenModel screenModel, Vector<ControlModel> vControls) {		
		boolean bResult = true;
		Context context = viewGroup.getContext();
		if (null == context || null == vControls)
			return false; // something is really weird or no child controls.
//		int iCurrentOrientation = context.getResources().getConfiguration().orientation;
		//Now Creating the controls
		for (ControlModel controlModel: vControls)
		{
			View controlView = null;			
			if (controlModel.iParentId != iParentId)
				continue;
			try {
				// TODO this code could be moved to a factory
				// String className = controlModel.getTypeByName();
				String className = controlModel.getClassName();
				if (null == className || className.length() == 0) {
					bResult = false;
					LogManager.logWTF("Cannot create control as its type is missing: " + controlModel.sName);
					continue;
				}
				Vector<ControlModel> vChildControls = new Vector<ControlModel> ();
				if (controlModel.isGroup())
					AppembleActivityImpl.getChildControls(vControls, controlModel.id, vChildControls);
				@SuppressWarnings("rawtypes")
				Class argumentsClass[] = new Class[2];
				argumentsClass[0] = Context.class;
				argumentsClass[1] = ControlModel.class;
				Class<?> cls = Class.forName(className);
				if (null == cls) {
					bResult = false;
					LogManager.logWTF("Cannot create control. Unable to find implementation class for type: " + controlModel.getTypeByName()
							+ " and class name: " + className);
					continue;
				}
				Constructor<?> constructor = cls.getDeclaredConstructor(argumentsClass);
				if (null == constructor) {
					LogManager.logWTF("Cannot create control. The constructor in the implementation class is missing. Class Name:" + className);
					bResult = false;
					continue;
				}
				controlView = (View)constructor.newInstance(context, controlModel);
				Object ret = ((ControlInterface)controlView).initialize(viewGroup, fParentWidthPixels, 
						fParentHeightPixels, vChildControls, screenModel); // pass the child controls to the group to create the group control
				if (ret instanceof Boolean)
					bResult = ((Boolean)ret).booleanValue();
			} catch (ClassNotFoundException cnfe) {
				LogManager.logWTF(cnfe, "Unable to create control. Implementation class not found for control: " + controlModel.sName);
				bResult = false;
			} catch (NoSuchMethodException nsme) {
				LogManager.logWTF(nsme, "Unable to create control. Constructor missing from the implementation class: " + controlModel.sName);
				bResult = false;
			} catch (IllegalArgumentException iae) {
				LogManager.logWTF(iae, "Unable to create control. Arguments missing from the implementation class: " + controlModel.sName);
				iae.printStackTrace();
			} catch (InstantiationException ie) {
				LogManager.logWTF(ie, "Unable to create control. Cannot create instance of the control: " + controlModel.sName);
			} catch (IllegalAccessException iae) {
				LogManager.logWTF(iae, "Unable to create control. Cannot access the constructor: " + controlModel.sName);
			} catch (InvocationTargetException iae) {
				LogManager.logWTF(iae, "Unable to create control. Cannot instantiate the control: " + controlModel.sName);
			}

			try{
				if(controlView!=null) {
					controlView.setId(controlModel.id);
					if(controlModel.bActionYN && controlModel.isClickable())
						controlView.setClickable(true);
					if (controlModel.bIsVisible == false)
						controlView.setVisibility(View.GONE); // Should not take space for layout purpose
					setControlPosition(viewGroup, controlModel, fParentWidthPixels, fParentHeightPixels, 
							controlView);
				} else {
					//TODO:Log Error to Server that control did not get created as expected 
				}
			}
			catch (Exception e) {
				//TODO:Log Error to Server that control did not get created as expected
				e.printStackTrace();
			}
		}
		return bResult;
	}
	
	public static PointF calculateSize(ControlModel controlModel, float fParentWidthPixels, 
			float fParentHeightPixels) {
		PointF fSize = new PointF(fParentWidthPixels, fParentHeightPixels);
//		if (controlModel.iDimensionType == Constants.PERCENTAGE) {
//2013-03-11 to be deleted.			fSize.x = ((fParentWidthPixels * controlModel.fWidth)/100);
//2013-03-11 to be deleted.			fSize.y = ((fParentHeightPixels * controlModel.fHeight)/100);
			fSize.x = Utilities.getDimension(controlModel.sWidth, fParentWidthPixels);
			fSize.y = Utilities.getDimension(controlModel.sHeight, fParentHeightPixels);
//		}
		return fSize;
	}
	
	public static void setControlPosition(ViewGroup parentLayout, ControlModel controlModel, float fParentWidth,
			float fParentHeight, View controlView) {
		float x = Utilities.getDimension(controlModel.sX, fParentWidth);
		float y = Utilities.getDimension(controlModel.sY, fParentHeight);
		controlModel.fWidthInPixels = Utilities.getDimension(controlModel.sWidth, fParentWidth);
		controlModel.fHeightInPixels = Utilities.getDimension(controlModel.sHeight, fParentHeight); 
//		if (controlModel.iDimensionType == Constants.PERCENTAGE) {
//			x = ((fParentWidth * x)/100);
//			y = ((fParentHeight * y)/100);
//			width = ((fParentWidth * width)/100);
//			height = ((fParentHeight * height)/100);
//		}
		
		LayoutParams lParams = new RelativeLayout.LayoutParams((int)controlModel.fWidthInPixels, (int)controlModel.fHeightInPixels);
		lParams.leftMargin = (int)x;
		lParams.topMargin = (int)y;
		
		parentLayout.addView(controlView,lParams);		
	}
}

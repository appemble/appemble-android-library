package com.appemble.avm;

import android.graphics.Color;

public class Attributes {

	static public int getColor(String sInput, int iDefault) {
		if (sInput != null) {
			try {
				return Color.parseColor(sInput);
			} catch (IllegalArgumentException e) {}
		}
		return iDefault;
	}

	static public int getInt(String sInput, int iDefault) {
		if (sInput != null) {
			try {
				return Integer.parseInt(sInput);
			} catch (IllegalArgumentException e) {}
		}
		return iDefault;
	}

	static public float getFloat(String sInput, float fDefault) {
		if (sInput != null) {
			try {
				return Float.parseFloat(sInput);
			} catch (IllegalArgumentException e) {}
		}
		return fDefault;
	}

	static public boolean getBoolean(String sInput, boolean bDefault) {
		if (sInput != null) {
			try {
				return Utilities.parseBoolean(sInput);
			} catch (IllegalArgumentException e) {}
		}
		return bDefault;
	}
}

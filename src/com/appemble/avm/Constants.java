/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import java.io.File;
import java.util.HashMap;
import java.util.regex.Pattern;

import android.os.Environment;

public class Constants {
	private static Constants _constants;
	
	// log codes
    public static final int CRIT    = 0;  // Critical: critical conditions
    public static final int ERR     = 1;  // Error: error conditions
    public static final int WARN    = 2;  // Warning: warning conditions
    public static final int INFO    = 3;  // Informational: informational messages
    public static final int DEBUG   = 4;  // Debug: debug messages
    public static final int ADVANCED_DEBUG   = 5;  // Debug: debug messages + appemble stack trace
    public static final int VERBOSE   = 6;  // verbose: verbose messages

	public static boolean isProdMode = false;
	public static boolean isInfoMode = false;
	public static boolean isDebugMode = false;
	public static boolean isVerboseMode = false;
	public static boolean isAdvancedDebugMode = false;
	public static boolean isExternalStorageAvailable = false;

	// boolean values
	public static String TRUE = "true";
	public static String YES = "yes";
	public static String FALSE = "false";
	public static String NO = "no";
	
	// url request types
	public static final int GET = 1;
	public static final int POST = 2;
	public static final int PUT = 3;
	public static final int DELETE = 4;

	// url response data types.
	public static final int JSON = 1;
	public static final int XML = 2;
	public static final int ATOM = 3;
	public static final int GPROTO = 4;

	// layout types
	public static final int TITLE = 1;
	public static final int BODY = 2;
	public static final int GROUP = 3;
	public static final int LISTITEM = 4;
	
	public static final char urlSeperator = '/';
	public static char pathSeparator;
	
	public static final char BEGIN_PARAMETER_IDENTIFIER = '[';
	public static final char END_PARAMETER_IDENTIFIER = ']';
	public static final char QUOTES = '\"';
	public static final String UTF = "UTF-8";
	
	// dimension type
	public static final int PERCENTAGE = 1;
	public static final int PIXELS = 2;

	// size	
	public static final int SMALL = 1;
	public static final int MEDIUM = 2;
	public static final int LARGE = 3;

	// justify	
	public static final int LEFT = 1;
	public static final int CENTER = 2;
	public static final int RIGHT = 3;
/*
	// align positions
	public static final int ABOVE = 4; 
	public static final int BELOW = 5;
	public static final int TOP = 6;
	public static final int BOTTOM = 7;
	public static final int TOLEFTOF = 8;
	public static final int TORIGHTOF = 9; 
	public static final int PARENTLEFT = 10; 
	public static final int PARENTCENTER = 11; 
	public static final int PARENTRIGHT = 12;
	public static final int CENTERHORIZONTAL = 13; 
	public static final int CENTERVERTICAL = 14;
*/
	// events
	public static final int TAP = "TAP".hashCode();
	public static final int DOUBLE_TAP = "DOUBLE_TAP".hashCode();
	public static final int LONG_PRESS = "LONG_PRESS".hashCode();
	public static final int FLICK = "FLICK".hashCode();
	public static final int TWO_FLICK = "TWO_FLICK".hashCode();
	public static final int THREE_FLICK = "THREE_FLICK".hashCode();
	public static final int ON_CHECKED = "ON_CHECKED".hashCode();
	public static final int ON_UNCHECKED = "ON_UNCHECKED".hashCode();
	public static final int ON_START_SCREEN = "ON_START_SCREEN".hashCode();
	public static final int ON_RESUME_SCREEN = "ON_RESUME_SCREEN".hashCode();
	public static final int ON_PAUSE_SCREEN = "ON_PAUSE_SCREEN".hashCode();
	public static final int ON_STOP_SCREEN = "ON_STOP_SCREEN".hashCode();
	public static final int ON_DESTROY_SCREEN = "ON_DESTROY_SCREEN".hashCode();
	public static final int ON_SLIDE_START = "ON_SLIDE_START".hashCode();
	public static final int ON_SLIDE = "ON_SLIDE".hashCode();
	public static final int ON_SLIDE_STOP = "ON_SLIDE_STOP".hashCode();
	public static final int ON_UPDATE = "ON_UPDATE".hashCode();
	public static final int ON_APP_LAUNCHED = "ON_APP_LAUNCHED".hashCode();
	public static final int ON_FIRST_TIME_APP_LAUNCHED = "ON_FIRST_TIME_APP_LAUNCHED".hashCode();
	public static final int ON_FIRST_TIME_VERSION_APP_LAUNCHED = "ON_FIRST_TIME_VERSION_APP_LAUNCHED".hashCode();

	// field types on the screen
	public static final int READONLY = 1;
	public static final int READWRITE = 2;

	// Data types
//	public static final int AUTOINC = 48;
//	public static final int BIGINT = 2;
//	public static final int BINARY = 3;
//	public static final int BLOB = 4;
//	public static final int BLOB_TEXT = 5;
//	public static final int BOOL = 6;
//	public static final int BOOLEAN = 7;
//	public static final int CHAR = 49;
//	public static final int CLOB = 9;
//	public static final int CURRENCY = 10;
//	public static final int DATE = 11;
//	public static final int DATETEXT = 12;
//	public static final int DATETIME = 13;
//	public static final int DEC = 14;
//	public static final int DECIMAL = 15;
//	public static final int DOUBLE = 16;
//	public static final int DOUBLE_PRECISION = 17;
//	public static final int FLOAT = 18;
//	public static final int GRAPHIC = 19;
//	public static final int GUID = 20;
//	public static final int IMAGE = 8;
//	// public static final int IMAGE = 21;
//	public static final int INT = 22;
//	public static final int INT64 = 23;
//	public static final int INTEGER = 24;
//	public static final int LARGEINT = 25;
//	public static final int MEMO = 26;
//	public static final int MONEY = 27;
//	public static final int NCHAR = 28;
//	public static final int NTEXT = 29;
//	public static final int NUMBER = 30;
//	public static final int NUMERIC = 31;
//	public static final int NVARCHAR = 32;
//	public static final int NVARCHAR2 = 33;
//	public static final int PHOTO = 34;
//	public static final int PICTURE = 35;
//	public static final int RAW = 36;
//	public static final int REAL = 37;
//	public static final int SMALLINT = 38;
//	public static final int SMALLMONEY = 39;
//	public static final int TEXT = 1;
//	// public static final int TEXT  = 40;
//	public static final int TIME = 41;
//	public static final int TIMESTAMP = 42;
//	public static final int TINYINT = 43;
//	public static final int URL = 49;
//	public static final int VARBINARY = 44;
//	public static final int VARCHAR = 45;
//	public static final int VARCHAR2 = 46;
//	public static final int WORD = 47;
//	public static final int JSONARRAY = 56;
//	public static final int JSONOBJECT = 57;
//	public static final int BITMAP = 50;
//	public static final int DRAWABLE = 51;
//	public static final int CURSOR = 52;
//	public static final int ARRAYLIST = 53;
//	public static final int HASHMAP = 54;
//	public static final int STRING_ARRAY = 55;
//	public static final int STRING = 58;

	public static final int AUTOINC = "AUTOINC".hashCode();
	public static final int BIGINT = "BIGINT".hashCode();
	public static final int BINARY = "BINARY".hashCode();
	public static final int BLOB = "BLOB".hashCode();
	public static final int BLOB_TEXT = "BLOB_TEXT".hashCode();
	public static final int BOOL = "BOOL".hashCode();
	public static final int BOOLEAN = "BOOLEAN".hashCode();
	public static final int CHAR = "CHAR".hashCode();
	public static final int CLOB = "CLOB".hashCode();
	public static final int CURRENCY = "CURRENCY".hashCode();
	public static final int DATE = "DATE".hashCode();
	public static final int DATETEXT = "DATETEXT".hashCode();
	public static final int DATETIME = "DATETIME".hashCode();
	public static final int DEC = "DEC".hashCode();
	public static final int DECIMAL = "DECIMAL".hashCode();
	public static final int DOUBLE = "DOUBLE".hashCode();
	public static final int DOUBLE_PRECISION = "DOUBLE_PRECISION".hashCode();
	public static final int FLOAT = "FLOAT".hashCode();
	public static final int GRAPHIC = "GRAPHIC".hashCode();
	public static final int GUID = "GUID".hashCode();
	public static final int IMAGE = "IMAGE".hashCode();
	// public static final int IMAGE = 21;
	public static final int INT = "INT".hashCode();
	public static final int INT64 = "INT64".hashCode();
	public static final int INTEGER = "INTEGER".hashCode();
	public static final int LARGEINT = "LARGEINT".hashCode();
	public static final int MEMO = "MEMO".hashCode();
	public static final int MONEY = "MONEY".hashCode();
	public static final int NCHAR = "NCHAR".hashCode();
	public static final int NTEXT = "NTEXT".hashCode();
	public static final int NUMBER = "NUMBER".hashCode();
	public static final int NUMERIC = "NUMERIC".hashCode();
	public static final int NVARCHAR = "NVARCHAR".hashCode();
	public static final int NVARCHAR2 = "NVARCHAR2".hashCode();
	public static final int PHOTO = "PHOTO".hashCode();
	public static final int PICTURE = "PICTURE".hashCode();
	public static final int RAW = "RAW".hashCode();
	public static final int REAL = "REAL".hashCode();
	public static final int SMALLINT = "SMALLINT".hashCode();
	public static final int SMALLMONEY = "SMALLMONEY".hashCode();
	public static final int TEXT = "TEXT".hashCode();
	// public static final int TEXT  = 40;
	public static final int TIME = "TIME".hashCode();
	public static final int TIMESTAMP = "TIMESTAMP".hashCode();
	public static final int TINYINT = "TINYINT".hashCode();
	public static final int URL = "URL".hashCode();
	public static final int VARBINARY = "VARBINARY".hashCode();
	public static final int VARCHAR = "VARCHAR".hashCode();
	public static final int VARCHAR2 = "VARCHAR2".hashCode();
	public static final int WORD = "WORD".hashCode();
	public static final int JSONARRAY = "JSONARRAY".hashCode();
	public static final int JSONOBJECT = "JSONOBJECT".hashCode();
	public static final int BITMAP = "BITMAP".hashCode();
	public static final int DRAWABLE = "DRAWABLE".hashCode();
	public static final int CURSOR = "CURSOR".hashCode();
	public static final int ARRAYLIST = "ARRAYLIST".hashCode();
	public static final int HASHMAP = "HASHMAP".hashCode();
	public static final int STRING_ARRAY = "STRING_ARRAY".hashCode();
	public static final int STRING = "STRING".hashCode();
	public static final int BYTEARRAY = "BYTEARRAY".hashCode();
	public static final int PARCELABLE = "PARCELABLE".hashCode();

	// data source
	public static final int LOCAL = 1;
	public static final int SERVER = 2;

	public static final int SCREEN_ID = -1;
	public static final int TITLE_ID = -5;

	public static final int NETWORK_CONNECTIVITY_WIFI = 1;
	public static final int NETWORK_CONNECTIVITY_MOBILE = 2;

	public static final String GLOBAL_PARAMETER_PREFIX = "GLOBAL";
	public static final int GLOBAL_PARAMETER_PREFIX_LENGTH = Constants.GLOBAL_PARAMETER_PREFIX.length();
	public static final String CONSTANT_PREFIX = "CONSTANT";
	public static final int CONSTANT_PREFIX_LENGTH = Constants.CONSTANT_PREFIX.length();
	public static final String URL_HTTP_PREFIX = "http-public";
	public static final int URL_HTTP_PREFIX_LENGTH = Constants.URL_HTTP_PREFIX.length();
	public static final String URL_IMAGE_PREFIX = "IMAGE";
	public static final int URL_IMAGE_PREFIX_LENGTH = Constants.URL_IMAGE_PREFIX.length();
	public static final char FIELD_SEPARATER = ':';
	public static final String URL_JSON_PREFIX = "JSON"; 
	public static final String URL_XML_PREFIX = "XML"; 
	public static final String URL_GPROTO_PREFIX = "GPROTO"; 

	// View State
	public static final boolean ENABLE = true;
	public static final boolean DISABLE = false;
	
	// scroll types
	public static final int HSCROLL = 1;
	public static final int VSCROLL = 2;
	public static final int SCROLL_BOTH_WAYS = 3;

	// Deparameter Types
	public static final int SQL = 1;
	public static final int NONE = 2;
	
	// progress bar show and hide.
	public static final int SHOW = 1;
	public static final int HIDE = 0;

	// get control model from name or field_name.
	public static final int FROM_NAME = 0;
	public static final int FROM_FIELD_NAME = 1;

	// cursor mgmt by control or parent activity.
	public static final int CURSOR_MGMT_SCREEN = 0;
	public static final int CURSOR_MGMT_SELF = 1;

	// child control data mgmt by control or parent activity.
	public static final int CHILD_CONTROL_DATA_MGMT_SCREEN = 0;
	public static final int CHILD_CONTROL_DATA_MGMT_SELF = 1;

	public static final int APPEARANCE_ID_NOT_SET = 0;		
	
	public enum Result { SUCCESS, NO_RESULT, FAILURE };
	
	public static String sDbDirExternalStorage = null;
	public static int iNumTriesDbOpen = 5;

	public static final String STR_SCREEN_NAME = "avm_screen_name";
	public static final String STR_SYSTEM_DATABASE_NAME = "system_dbname";
	public static final String STR_CONTENT_DATABASE_NAME = "content_dbname";
	public static final String STR_DATA_TYPES = "data_types";
	public static final String STR_TARGET_PARAMETER_VALUE_LIST = "avm_targetParameterValueList";	
	public static final String STR_PROPERTIES_TABLE = "_properties";	
	public static final String STR_NETWORK_CONNECTION_REQUIRED = 
		"This application requires network connection. Please check your internet connection.";	
	public static final String DB_CONNECTION_ERROR = "Unable to get DB connection";
	public static final String STR_SERVER_ERROR = "Errorl23o90";
	public static final String STR_REQUIRES_NETWORK_CONNECTION = "requires_network_connection";
	
	public static int iOS = 2; // for android the 2nd bit from the right should be on
	public static String sDefaultFilter = " mark_deleted = 0 and ((os & ";//2) > 0) ";

	public static String STORAGE_FORMAT = "storage_format";
	public static String TITLE_STR = "title";
	public static String STORAGE_VALUES = "storage_values";
	public static String DISPLAY_VALUES = "display_values";
	public static String VALUES_QUERY = "values_query";
	public static String IS_MANDATORY_INPUT = "is_mandatory_input";
	public static String MANDATORY_MESSAGE = "mandatory_message";

	public static String MISSING_ACTIVITY = "Unable to find parent activity. Please check the control on which this action is attached to.";
	
	public static HashMap<String, Integer> mRequestType;
	public static HashMap<String, Integer> mRemoteDataFormat;
	
	public static synchronized Constants getInstance() {
		if (_constants != null)
			return _constants;
		_constants = new Constants();
		if (null != Cache.bootstrap) {
			int iLogLevel = Cache.bootstrap.getIntValue("log_level");
			isVerboseMode = Cache.bootstrap.getIntValue("log_level") >= VERBOSE; 
			isAdvancedDebugMode = Cache.bootstrap.getIntValue("log_level") == ADVANCED_DEBUG; 
			isDebugMode = Cache.bootstrap.getIntValue("log_level") >= DEBUG; 
			isInfoMode = (iLogLevel >= INFO);
			isProdMode = (iLogLevel >= CRIT);
			String sValue = android.os.Build.MANUFACTURER;
		    if (null != sValue && Pattern.compile(Pattern.quote("amazon"), Pattern.CASE_INSENSITIVE).matcher(sValue).find())
		    	iOS = 4; // amazon android
		    sDefaultFilter += Integer.toString(iOS) + ") > 0) ";
		}
		pathSeparator = File.separatorChar; 
		File sdcard = Environment.getExternalStorageDirectory();
		if (null != sdcard) {
			sDbDirExternalStorage = sdcard.getAbsolutePath() + File.separatorChar;
			File fExternalStorage = new File(sDbDirExternalStorage);
			if (null != fExternalStorage)
				isExternalStorageAvailable = fExternalStorage.exists();
		}

		mRequestType = new HashMap<String, Integer>();
	    mRequestType.put("GET", Integer.valueOf(Constants.GET));
	    mRequestType.put("POST", Integer.valueOf(Constants.POST));
	    mRequestType.put("PUT", Integer.valueOf(Constants.PUT));
	    mRequestType.put("DELETE", Integer.valueOf(Constants.DELETE));

	    mRemoteDataFormat = new HashMap<String, Integer>();
	    mRemoteDataFormat.put("IMAGE", Integer.valueOf(Constants.IMAGE)); // 8
	    mRemoteDataFormat.put("JSON", Integer.valueOf(Constants.JSON));
	    mRemoteDataFormat.put("XML", Integer.valueOf(Constants.XML));
	    mRemoteDataFormat.put("ATOM", Integer.valueOf(Constants.ATOM));
	    mRemoteDataFormat.put("GPROTO", Integer.valueOf(Constants.GPROTO));
		
		return _constants;
	}	
	
	public static int getRequestType(String sRequestType) {
		if (null == sRequestType)
			return GET;
		int iReturnRequestType = mRequestType.get(sRequestType);
		if (0 == iReturnRequestType)
			iReturnRequestType = GET;
		return iReturnRequestType;
	}

	public static int getRemoteDataFormat(String sRemoteDataFormat) {
		if (null == sRemoteDataFormat)
			return JSON;
		int iReturnRemoteDataFormat = mRemoteDataFormat.get(sRemoteDataFormat);
		if (0 == iReturnRemoteDataFormat)
			iReturnRemoteDataFormat = JSON;
		return iReturnRemoteDataFormat;
	}
}

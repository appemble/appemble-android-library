/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.ContentValues;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.dao.TableDao;

// Need enhancements
//	show a non-modal image loader indicator when the image is loading

// future enhancements
// Instead of creating a background thread for every remote download, 
// one could do in one thread. See https://github.com/thest1/LazyList for details
// cancel image download by user action

public class ImageManager {
	private static ImageManager _imageManager;
	HashMap<String, String> mImageFilesMap; // This map stores the image source to a file in local storage
	int iMaxBitmaps, iMaxImageConcurrentDownloads, nBackgroundTasks = 0, iCacheSize = 0; 
	Stack<RequestParams> queue;

	// TODO This class currently just takes size of items. Instead it should take size of the object.
	public class ConcurrentLRUCache<K, V> {
		private final int maxSize;
		private ConcurrentHashMap<K, V> map;
		private ConcurrentLinkedQueue<K> queue;

		public ConcurrentLRUCache(final int maxSize) {
		    this.maxSize = maxSize;
		    map = new ConcurrentHashMap<K, V>(maxSize);
		    queue = new ConcurrentLinkedQueue<K>();
		}

		/**
		 * @param key - may not be null!
		 * @param value - may not be null!
		 */
		public void put(final K key, final V value) {
			if (null == key || null == value)
				return;
		    if (map.containsKey(key)) {
		        queue.remove(key); // remove the key from the FIFO queue
		    }

		    while (queue.size() >= maxSize) {
		        K oldestKey = queue.poll();
		        if (null != oldestKey) {
		            map.remove(oldestKey);
		        }
		    }
		    queue.add(key);
		    map.put(key, value);
		}

		/**
		 * @param key - may not be null!
		 * @return the value associated to the given key or null
		 */
		public V get(final K key) {
			if (null == key)
				return null;
		    return map.get(key);
		}
	}

	public ConcurrentLRUCache<String, Bitmap> mBitmapMap; // This map stores the local image and its bitmap 
	public ConcurrentLRUCache<String, Drawable> mDrawableMap; // This map stores the (control id or imagename) and its drawable

	String sContentDbName = null;
	public static synchronized ImageManager getInstance() {
		if (_imageManager == null) {
			_imageManager = new ImageManager();
			_imageManager.initialize();
		}
		return _imageManager;
	}

	public ImageManager() {
		mImageFilesMap = new HashMap<String, String>();
		sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);
		Resources res = Cache.context.getResources();
		iMaxBitmaps = res.getInteger(R.integer.maxBitmaps);
//		int iMaxCacheSizePercentage = res.getInteger(R.integer.maxCacheSizePercentage);
//		if (0 == iMaxCacheSizePercentage) iMaxCacheSizePercentage = 20;
		iMaxImageConcurrentDownloads = res.getInteger(R.integer.maxConcurrentImageDownloads);
        queue = new Stack<RequestParams>();
//        // OutOfMemory exception.
//        final int memClass = ((ActivityManager) Cache.context.getSystemService(
//                Context.ACTIVITY_SERVICE)).getMemoryClass();
//
//        // Use % of the available memory for this memory cache.
//        iCacheSize = 1024 * 1024 * memClass / (100/iMaxCacheSizePercentage);
		mBitmapMap = new ConcurrentLRUCache<String, Bitmap>(iMaxBitmaps);
		mDrawableMap = new ConcurrentLRUCache<String, Drawable>(iMaxBitmaps);
	}

	public Bitmap getImage(String sImageSource, boolean bBackground) {
		return get(null, sImageSource, bBackground, Constants.GET, null, null);
	}
	
	// get Drawables. Drawables cannot be obtained in background. Drawables are needed only for 
	// setting StateListDrawable and for that they must be in the UI thread.
	public Drawable getDrawable(View view, String sImageSource, boolean bScaleToDevicePixels,
			int iState) {
		String sLocalImageSource = getImageFile(null, sImageSource, false, Constants.GET, null, null);
		if (null == sLocalImageSource)
			return null;
//		if (sLocalImageSource.contains(".9."))
//			return getNinePatchDrawable(sLocalImageSource);
//		else {
			// create a new bitmap.
			Bitmap bitmap = get(null, sImageSource, false, Constants.GET, null, null);
			if (null != bitmap && sImageSource.contains(".9.")) {
				byte[] chunk = bitmap.getNinePatchChunk();
				if (null == chunk || chunk.length == 0) {
					LogManager.logError("Unable to read the Patch 9 file. Please have the compiled version of the image");
					return null;
				}
				boolean result = NinePatch.isNinePatchChunk(chunk);
				if (false == result) {
					LogManager.logError("Unable to read the Patch 9 file. Please have the compiled version of the image");
					return null;
				}
				NinePatchDrawable patchy = new NinePatchDrawable(bitmap, chunk, new Rect(), null);
				return patchy;
			}
			return getDrawable(view, sImageSource, bitmap, bScaleToDevicePixels, iState);
//		}
	}
	
	public Drawable getNinePatchDrawable(String sImageSource) {
		if (null == sImageSource)
			return null;
		return NinePatchDrawable.createFromPath(sImageSource);
	}
	
	public Drawable getDrawable(View view, String sImageSource, Bitmap bitmap, boolean bScaleToDevicePixels,
			int iState) {
		ControlModel controlModel = null;
		// try to find the drawable in the image map for this control id.
		if (null != view && view instanceof ControlInterface) {
			controlModel = ((ControlInterface)view).getControlModel();
			if (null != controlModel) {	
				Drawable d = (Drawable)mDrawableMap.get(String.valueOf(controlModel.id)+'_'+String.valueOf(iState));
				if (null != d)
					return d;
			}
		}
		if (null == bitmap) 
			return null;
		
		Drawable d = null;
		if (bScaleToDevicePixels && null != view) { // do scaling only if the image is smaller than the view.
			Rect rect = new Rect();
			view.getLocalVisibleRect(rect);
			if (rect.right - rect.left > bitmap.getWidth() && rect.bottom - rect.top > bitmap.getHeight())
				d = new BitmapDrawable(Cache.context.getResources(), 
						Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*Cache.fScaleDPToPixels), 
								(int)(bitmap.getHeight()*Cache.fScaleDPToPixels), false));
			else // no need to scale
				d = new BitmapDrawable(bitmap);				
		} else
			d = new BitmapDrawable(bitmap);
		if (null != controlModel)
			mDrawableMap.put(String.valueOf(controlModel.id+'_'+String.valueOf(iState)), d);
		else
			mDrawableMap.put(sImageSource, d);
		return d;
	}

	public Bitmap getImage(String sImageSource, boolean bBackground, int requestMethod, 
			HashMap<String, String> targetParameterValueList, String sImageName) {
		return get(null, sImageSource, bBackground, requestMethod, targetParameterValueList, sImageName);
	}
	
	public Bitmap UpdateView(View view, String sImageSource, boolean bBackground) {
		return get(view, sImageSource, bBackground, Constants.GET, null, null);
	}
	
	private Bitmap get(View view, String sImageSource, boolean bBackground, 
			int requestMethod, HashMap<String, String> targetParameterValueList, String sImageName) {
		String sLocalImageSource = getImageFile(view, sImageSource, bBackground, requestMethod, targetParameterValueList, 
				sImageName);
		if (null == sLocalImageSource)
			return null;
		return null != view ? _drawView(view, sImageSource, sLocalImageSource) : _getBitmap(sImageSource, sLocalImageSource);
	}
	
//	private Bitmap get(View view, String sImageSource, boolean bBackground, 
//			int requestMethod, HashMap<String, String> targetParameterValueList, String sImageName) {
//		if (null == sImageSource || sImageSource.equalsIgnoreCase("null"))
//			return null != view ? _drawView(view, sImageSource) : null; // this is a request to clear an image from the view (if present)
//		if (requestMethod == 0)
//			requestMethod = Constants.GET;
//		// Remove the IMAGE: prefix
//		if (sImageSource.startsWith(Constants.URL_IMAGE_PREFIX))
//			sImageSource = sImageSource.substring(Constants.URL_IMAGE_PREFIX_LENGTH + 1);
//		// try to find the image in the local storage.
//		String sLocalImageSource = mImageFilesMap.get(((requestMethod == Constants.POST) ? sImageName : sImageSource));
//		if (null != sLocalImageSource)
//			return null != view ? _drawView(view, sLocalImageSource) : _getBitmap(sLocalImageSource);
//
//		if (Utilities.isUrl(sImageSource)) {
//			if (bBackground && null != view) {
//				if (nBackgroundTasks < iMaxImageConcurrentDownloads) {
//					nBackgroundTasks++;
//					DownloadImagesInBackground downloadImagesInBackground = new DownloadImagesInBackground(
//							sImageSource, view, requestMethod, targetParameterValueList, sImageName);
//					downloadImagesInBackground.execute();
//					return null;
//				} else
//					queue.push(new RequestParams(view, sImageSource, bBackground, requestMethod, 
//							targetParameterValueList, sImageName));
//			} else {
//				sLocalImageSource = getRemoteImage(sImageSource, requestMethod, targetParameterValueList, sImageName);
//				return null != view ? _drawView(view, sLocalImageSource) : _getBitmap(sLocalImageSource);
//			}
//		} else {
//			try {
//				// put in code to get image from file://, content://, raw://, assets:// TODO
//				InputStream inputStream = Cache.context.getAssets().open(sImageSource);
//				String sFullDestinationPath = Utilities.getFilesDir() + File.separator + sImageSource;
//				
//				if (Utilities.copyFile(inputStream, sFullDestinationPath, true)) {
//					LogManager.logVerbose("Fetching Local Image: '" + sImageSource + "' from assets folder.");
//					addNewImage(sImageSource, sFullDestinationPath);
//					return null != view ? _drawView(view, sFullDestinationPath) : _getBitmap(sFullDestinationPath);
//				}
//				inputStream.close();
//			} catch (IOException ioe) {
//				LogManager.logError(ioe, "Unable to copy image from assets: " + sImageSource);
//				return null;
//			}
//		}
//		return null;
//	}

	private String getImageFile(View view, String sImageSource, boolean bBackground, 
			int requestMethod, HashMap<String, String> targetParameterValueList, String sImageName) {
		if (null == sImageSource || sImageSource.equalsIgnoreCase("null"))
			return null; 
		if (requestMethod == 0)
			requestMethod = Constants.GET;
		// Remove the IMAGE: prefix
		if (sImageSource.startsWith(Constants.URL_IMAGE_PREFIX))
			sImageSource = sImageSource.substring(Constants.URL_IMAGE_PREFIX_LENGTH + 1);
		// try to find the image in the local storage.
		String sLocalImageSource = mImageFilesMap.get(((requestMethod == Constants.POST) ? sImageName : sImageSource));
		if (null != sLocalImageSource)
			return sLocalImageSource;

		if (Utilities.isUrl(sImageSource)) {
			if (bBackground && null != view) {
				if (nBackgroundTasks < iMaxImageConcurrentDownloads) {
					nBackgroundTasks++;
					DownloadImagesInBackground downloadImagesInBackground = new DownloadImagesInBackground(
							sImageSource, view, requestMethod, targetParameterValueList, sImageName);
					downloadImagesInBackground.execute();
					return null;
				} else
					queue.push(new RequestParams(view, sImageSource, bBackground, requestMethod, 
							targetParameterValueList, sImageName));
			} else {
				sLocalImageSource = getRemoteImage(sImageSource, requestMethod, targetParameterValueList, sImageName);
				return sLocalImageSource;
			}
		} else {
			try {
				// put in code to get image from file://, content://, raw://, assets:// TODO
				InputStream inputStream = Cache.context.getAssets().open(sImageSource);
				String sFullDestinationPath = Utilities.getFilesDir(Environment.DIRECTORY_PICTURES) + File.separator + sImageSource;
				
				if (Utilities.copyFile(inputStream, sFullDestinationPath, true)) {
					LogManager.logVerbose("Fetching Local Image: '" + sImageSource + "' from assets folder.");
					addNewImage(sImageSource, sFullDestinationPath);
					return sFullDestinationPath;
				}
				inputStream.close();
			} catch (IOException ioe) {
				LogManager.logError(ioe, "Unable to copy image from assets: " + sImageSource);
				return null;
			}
		}
		return null;
	}

	public Bitmap UpdateView(View view, String sImageSource, boolean bBackground, 
			int requestMethod, HashMap<String, String> targetParameterValueList, String sImageName) {
		return get(view, sImageSource, bBackground, requestMethod, targetParameterValueList, sImageName);
	}

	private String getRemoteImage(String sRemoteSource, int iRequestMethod, 
			HashMap<String, String> postRequestHashMap, String sImageName) {
		if (null == sRemoteSource || (iRequestMethod == Constants.POST && (sImageName == null || sImageName.length() == 0)))
			return null;
		String sLocalSourceFullPath = mImageFilesMap.get(sRemoteSource);
		if (null != sLocalSourceFullPath)
			return sLocalSourceFullPath;
		// here get the image from the remote server
		DefaultHttpClient httpClient = HttpUtils.getThreadSafeClient();
		URL url; URI uri;
		long lDownloadTime = 0;
		if (Constants.isVerboseMode)
			lDownloadTime = System.currentTimeMillis();
		HttpUriRequest request = null;
		try {
			url = new URL(sRemoteSource);
			uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			switch(iRequestMethod) {
				case Constants.POST:
					List<NameValuePair> nameValuePairs = Utilities.convertHashMapToNameValuePairs(postRequestHashMap);
					if(nameValuePairs == null)
						return null;
					request =  new HttpPost(uri);
					((HttpPost)request).setEntity(new UrlEncodedFormEntity(nameValuePairs));					 
				break;
				case Constants.GET:
					request = new HttpGet(uri);
				break;
			}
	
			if (null == httpClient || null == request) {
				// TODO log fatal error
				return null;
			}

			HttpResponse response = httpClient.execute(request);
			int fileExtensionIndex = sRemoteSource.lastIndexOf('.');
			String sSourceFileName;
			if (iRequestMethod == Constants.POST)
				sSourceFileName = sImageName;
			else {
				if (fileExtensionIndex > -1)
					sSourceFileName = sRemoteSource.substring(fileExtensionIndex);
				else
					return null;
				sSourceFileName = sRemoteSource.hashCode() + sSourceFileName;
			}
			InputStream inputStream = response.getEntity().getContent();
			sLocalSourceFullPath = Utilities.getFilesDir(Environment.DIRECTORY_PICTURES) + File.separator + sSourceFileName;
			if (false == Utilities.copyFile(inputStream, sLocalSourceFullPath,true))
				sLocalSourceFullPath = null;
			inputStream.close();
			if (null != sLocalSourceFullPath) {
				addNewImage((iRequestMethod == Constants.POST) ? sImageName : sRemoteSource, sLocalSourceFullPath);
			} else
				return null;
			// add the image url and the local image source in mImageFilesMap
		} catch (URISyntaxException e) {
			LogManager.logError(e, "Unable to download image from the address: " + sRemoteSource + ". Check the syntax of the URL.");
			return null;
		} catch (ClientProtocolException cpe) {
			LogManager.logError(cpe, "Unable to download image from the address: " + sRemoteSource + ". Check your network connection.");
			return null;
		} catch (MalformedURLException murle) {
			LogManager.logError(murle, "Unable to download image from the address: " + sRemoteSource + ". Check the URL again.");
			return null;
		} catch (IOException ioe) {
			LogManager.logError(ioe, "Unable to download image from the address: " + sRemoteSource);
			return null;
		}
		if (Constants.isVerboseMode)
			lDownloadTime = System.currentTimeMillis() - lDownloadTime;
		LogManager.logVerbose("Downloaded Remote Image: '" + sRemoteSource + "' in " + lDownloadTime + "ms. URL=" + sRemoteSource);
		return sLocalSourceFullPath;
	}

	private class DownloadImagesInBackground extends
			AsyncTask<Void, Void, Integer> {
		String sRemoteSource, sLocalSource, sImageName;
		View viewToUpdate;
		int iRequestMethod;
		HashMap<String, String> targetParameterValueList;

		public DownloadImagesInBackground(String sImgSource, View view, int requestMethod, 
				HashMap<String, String> tplv, String sImgName) {
			sRemoteSource = sImgSource;
			viewToUpdate = view;
			iRequestMethod = requestMethod;
			targetParameterValueList = tplv;
			sImageName = sImgName;
			// add a temp image on the view to show that it is being downloaded.
		}

		public void onPreExecute() {
		}

		public Integer doInBackground(Void... unused) {
			sLocalSource = ImageManager.getInstance().getRemoteImage(sRemoteSource, iRequestMethod, 
					targetParameterValueList, sImageName);
			return sLocalSource != null ? 1 : 0;
		}

		public void onPostExecute(Integer unused) {
			nBackgroundTasks--;
			// get the bitmap
			if (null != sLocalSource && viewToUpdate != null) 
				_drawView(viewToUpdate, sRemoteSource, sLocalSource);
            getNextImage();
		}
	}

    private void getNextImage() {
        if (!queue.isEmpty()) {
            RequestParams item = queue.pop();
            new DownloadImagesInBackground(item.sImageName, item.view, item.requestMethod, 
            		item.targetParameterValueList, item.sImageName).execute();
        }
    }

	private static Bitmap _getBitmap(String sImageSource, String sLocalImageSource) {
		if (null == sLocalImageSource)
			return null;
		Bitmap bitmap = (Bitmap)ImageManager.getInstance().mBitmapMap.get(sLocalImageSource);
		if (null != bitmap)
			return bitmap;
		bitmap = BitmapFactory.decodeFile(sLocalImageSource);
		if (null == bitmap) {
			LogManager.logError("Unable to read the image: " + sImageSource);
			return null;
		}
//		bitmap = new BitmapDrawable(Cache.context.getResources(), Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*Cache.fScaleDPToPixels), 
//	    		(int)(bitmap.getHeight()*Cache.fScaleDPToPixels), false));
		ImageManager.getInstance().mBitmapMap.put(sLocalImageSource, bitmap);
		/*
		//Bitmap d = Bitmap.createFromPath(sImageSource);
		if (null != bitmap) {			
			if (0.0f == MLCache.fScaleDPToPixels)
				MLCache.fScaleDPToPixels = MLCache.context.getResources().getDisplayMetrics().density;
			if (MLCache.fScaleDPToPixels != 1.0f) {
			    Bitmap bitmap = ((BitmapBitmap)bitmap).getBitmap();
			    int iWidth = (int)(bitmap.getWidth()*MLCache.fScaleDPToPixels);
			    int iHeight = (int)(bitmap.getHeight()*MLCache.fScaleDPToPixels); 
			    Bitmap bitmapScaled = Bitmap.createScaledBitmap(bitmap, (int)(bitmap.getWidth()*MLCache.fScaleDPToPixels), 
			    		(int)(bitmap.getHeight()*MLCache.fScaleDPToPixels), false);
			    bitmap = new BitmapBitmap(MLCache.context.getResources(), bitmapScaled);
			}
			ImageManager.getInstance().mImageMap.put(sImageSource,
					bitmap);
		}
		else
			; // TODO we have a serious error. The file is not an image
				// file.
		 */
		return bitmap;
	}
	// This function can receive sLocalImageSoure == null
	private static Bitmap _drawView(View viewToUpdate, String sImageSource, String sLocalImageSource) {
		if (null == viewToUpdate)
			return _getBitmap(sImageSource, sLocalImageSource);
		if (null != sLocalImageSource && viewToUpdate instanceof WebView) {
			((WebView)viewToUpdate).loadUrl("file://" + sLocalImageSource);
			return null;
		}
			
		Bitmap bitmap = _getBitmap(sImageSource, sLocalImageSource);
		if (null == bitmap) // if the bitmap is still null we have a problem.
			return null;
		if (viewToUpdate instanceof ImageView) {
			((ImageView) viewToUpdate).setImageBitmap(bitmap);
		} else {
			Drawable d = _imageManager.getDrawable(viewToUpdate, sLocalImageSource, bitmap, true, 0); // the default state is always 0
			if (null != d)
				viewToUpdate.setBackgroundDrawable((Drawable)d);
		}
		return bitmap;
	}

	private void initialize() {
		// read the table _image_manager and initialize the mImageFilesMap
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sContentDbName);
//		Cursor cur = null;
//		
//		try {
//			cur = db.query("_image_manager", null, null, null, null, null, null, null);
//			while (cur != null && cur.moveToNext()) {
//				mImageFilesMap.put(cur.getString(0), cur.getString(1));
//			}
//		} catch (SQLException e) {
//			LogManager.logError(e, "Unable to initialize ImageManager. Please check if the content database is defined in res/values/strings.xml and is present in the assets folder.");
//		} finally {
//			if (null != cur)
//				cur.close();
//		}
		String[][] sImages = TableDao.getRowsFromDbAsStrings(sContentDbName, "select * from _image_manager", null);
		if (null == sImages)
			return;
		int iCount = sImages.length;
		for (int i = 0; i < iCount; i++) {
			mImageFilesMap.put(sImages[i][0], sImages[i][1]);
		}
		deleteOldFiles(sContentDbName);
	}

	private synchronized void deleteOldFiles(String sContentDbName) {
		// for each row in _image_manager table, if the downloaded_on is > x
		// days, remove the record from the
		// from the db, delete the entry from the mImageFilesMap and then remove
		// the file from the disk.
		// Used to examplify deletion of files more than 1 month old
		// Note the L that tells the compiler to interpret the number as a Long
		String sMaxFileAge = Cache.bootstrap.getValue("maxFileAge");
		if (null == sMaxFileAge || 0 == sMaxFileAge.length())
			return;
		final long MAXFILEAGE = Long.parseLong(sMaxFileAge); // 1 month in milliseconds

		// Get file handle to the directory. In this case the application files dir
		String sFilesDir = null;
		/*if (Constants.isExternalStorageAvailable)
			sFilesDir = Constants.sDbDirExternalStorage + Cache.context.getPackageName();
		else
			sFilesDir = Cache.context.getFilesDir().getAbsolutePath(); */
		sFilesDir = Utilities.getFilesDir(Environment.DIRECTORY_PICTURES);
		if (null == sFilesDir)
			return;
		File dir = new File(sFilesDir);

		// Obtain list of files in the directory.
		// listFiles() returns a list of File objects to each file found.
		File[] files = dir.listFiles();
		if (null == files)
			return;
		// Loop through all files
		for (int i = 0; i < files.length; i++) {
			File f = files[i];
			// Get the last modified date. Milliseconds since 1970
			long lastmodified = f.lastModified();

			// Do stuff here to deal with the file..
			// For instance delete files older than 1 month
			if (lastmodified + MAXFILEAGE < System.currentTimeMillis()) {
				// delete the record in _image_manager table
				String[] params = new String[] { f.getAbsolutePath() };
				TableDao.delete(sContentDbName, "_image_manager", "local_image = ?", params);
				// remove the record from mImageFilesMap
				for(Object key : mImageFilesMap.keySet()) {
				    String value = mImageFilesMap.get(key);
				    if (value.equals(f.getAbsolutePath()))
				    	mImageFilesMap.remove(key);
				}
				f.delete();
			}
		}
	}
	
	private synchronized long addNewImage(String sImageSource, String sFullDestinationPath) {
		mImageFilesMap.put(sImageSource, sFullDestinationPath);
		ContentValues contentValues = new ContentValues();
		contentValues.put("remote_image", sImageSource);
		contentValues.put("local_image", sFullDestinationPath);
		long numRowsAffected = -1;
		numRowsAffected = TableDao.replace1Row(sContentDbName, "_image_manager", contentValues);
		if (-1 == numRowsAffected)
			LogManager.logWTF("Unable to persist the image: " + sImageSource + " in the contest database:" + sContentDbName);
		return numRowsAffected;
	}

    class RequestParams {
        String sImageSource;
		boolean bBackground;
		int requestMethod;
		HashMap<String, String> targetParameterValueList;
		String sImageName;
		View view;
        public RequestParams(View view, String sImageSource, boolean bBackground, int requestMethod, 
        		HashMap<String, String> targetParameterValueList, String sImageName) {
            this.sImageSource = sImageSource;
            this.bBackground = bBackground;
            this.requestMethod = requestMethod;
            this.targetParameterValueList = targetParameterValueList;
            this.sImageName = sImageName;
            this.view = view;
        }
    }
}
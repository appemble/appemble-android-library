/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.appemble.avm.Utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

/**
 * Displays bitmap with rounded corners. <br />
 * <b>NOTE:</b> It's strongly recommended your {@link ImageView} has defined width (<i>layout_width</i>) and height
 * (<i>layout_height</i>) .<br />
 * <b>NOTE:</b> New {@link Bitmap} object is created for displaying. So this class needs more memory and can cause
 * {@link OutOfMemoryError}.
 * 
 * @author Sergey Tarasevich (nostra13[at]gmail[dot]com)
 * @since 1.5.6
 */
public class RoundedBorderBitmapProcessor extends RoundedBitmapProcessor {
	protected final int borderWidth, borderColor;

	public RoundedBorderBitmapProcessor(int iImageWidth, int iImageHeight, ScaleType scaleType,
			int borderWidth, float cornerRadius, int borderColor) {
		super(iImageWidth, iImageHeight, scaleType, cornerRadius);
		this.borderWidth = borderWidth;
		this.borderColor = borderColor;
	}

	@Override
	public Bitmap process(Bitmap bitmap) {
		Bitmap rectBitmap = super.process(bitmap);
		return getRoundedCornerWithBorderBitmap(rectBitmap, borderColor, borderWidth, cornerRadius, 
				srcRect, destRect, imageSize.getWidth(), imageSize.getHeight());		
//		return roundCornersWithBorder(bitmap, iImageWidth, iImageHeight, 
//				scaleType, borderColor, borderWidth, cornerRadius);
	}

//	/**
//	 * Process incoming {@linkplain Bitmap} to make rounded corners according to target {@link ImageView}.<br />
//	 * This method <b>doesn't display</b> result bitmap in {@link ImageView}
//	 * 
//	 * @param bitmap Incoming Bitmap to process
//	 * @param imageView Target {@link ImageView} to display bitmap in
//	 * @param roundPixels
//	 * @return Result bitmap with rounded corners
//	 */
//	public static Bitmap roundCornersWithBorder(Bitmap bitmap, int iImageWidth, int iImageHeight, 
//			ScaleType scaleType, int borderColor, int borderWidth, float cornerRadius) {
//		Bitmap roundBitmap;
//
//		int bw = bitmap.getWidth();
//		int bh = bitmap.getHeight();
//		int vw = iImageWidth;
//		int vh = iImageHeight;
//		if (vw <= 0) vw = bw;
//		if (vh <= 0) vh = bh;
//
//		int width, height;
//		Rect srcRect;
//		Rect destRect;
//		switch (scaleType) {
//			case CENTER_INSIDE:
//				float vRation = (float) vw / vh;
//				float bRation = (float) bw / bh;
//				int destWidth;
//				int destHeight;
//				if (vRation > bRation) {
//					destHeight = Math.min(vh, bh);
//					destWidth = (int) (bw / ((float) bh / destHeight));
//				} else {
//					destWidth = Math.min(vw, bw);
//					destHeight = (int) (bh / ((float) bw / destWidth));
//				}
//				int x = (vw - destWidth) / 2;
//				int y = (vh - destHeight) / 2;
//				srcRect = new Rect(0, 0, bw, bh);
//				destRect = new Rect(x, y, x + destWidth, y + destHeight);
//				width = vw;
//				height = vh;
//				break;
//			case FIT_CENTER:
//			case FIT_START:
//			case FIT_END:
//			default:
//				vRation = (float) vw / vh;
//				bRation = (float) bw / bh;
//				if (vRation > bRation) {
//					width = (int) (bw / ((float) bh / vh));
//					height = vh;
//				} else {
//					width = vw;
//					height = (int) (bh / ((float) bw / vw));
//				}
//				srcRect = new Rect(0, 0, bw, bh);
//				destRect = new Rect(0, 0, width, height);
//				break;
//			case CENTER_CROP:
//				vRation = (float) vw / vh;
//				bRation = (float) bw / bh;
//				int srcWidth;
//				int srcHeight;
//				if (vRation > bRation) {
//					srcWidth = bw;
//					srcHeight = (int) (vh * ((float) bw / vw));
//					x = 0;
//					y = (bh - srcHeight) / 2;
//				} else {
//					srcWidth = (int) (vw * ((float) bh / vh));
//					srcHeight = bh;
//					x = (bw - srcWidth) / 2;
//					y = 0;
//				}
//				width = Math.min(vw, bw);
//				height = Math.min(vh, bh);
//				srcRect = new Rect(x, y, x + srcWidth, y + srcHeight);
//				destRect = new Rect(0, 0, width, height);
//				break;
//			case FIT_XY:
//				width = vw;
//				height = vh;
//				srcRect = new Rect(0, 0, bw, bh);
//				destRect = new Rect(0, 0, width, height);
//				break;
//			case CENTER:
//			case MATRIX:
//				width = Math.min(vw, bw);
//				height = Math.min(vh, bh);
//				x = (bw - width) / 2;
//				y = (bh - height) / 2;
//				srcRect = new Rect(x, y, x + width, y + height);
//				destRect = new Rect(0, 0, width, height);
//				break;
//		}
//
//		try {
//			Bitmap rectBitmap = RoundedBitmapProcessor.getRoundedCornerBitmap(bitmap, 0, srcRect, destRect, width, height);
////			roundBitmap = getRoundedCornerWithBorderBitmap(rectBitmap, borderColor, borderWidth, cornerRadius, 
////					srcRect, destRect, width, height);
//		} catch (OutOfMemoryError e) {
//			L.e(e, "Can't create bitmap with rounded corners. Not enough memory.");
//			roundBitmap = bitmap;
//		}
//
//		return roundBitmap;
//	}

	public static Bitmap getRoundedCornerWithBorderBitmap(Bitmap bitmap, int borderColor, int borderWidth, 
			float cornerRadius, Rect srcRect, Rect destRect, int width, int height) {
		if (null == bitmap)
			return null;
		Drawable imageDrawable = new BitmapDrawable(bitmap);

		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final RectF destRectF = new RectF(destRect);

		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setColor(Color.RED);
		canvas.drawRoundRect(destRectF, cornerRadius, cornerRadius, paint);

		paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
		imageDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());

		// Save the layer to apply the paint
		canvas.saveLayer(destRectF, paint, Canvas.ALL_SAVE_FLAG);
		imageDrawable.draw(canvas);
		canvas.restore();

		// FRAMING THE PHOTO

		// 1. Create offscreen bitmap link: http://www.youtube.com/watch?feature=player_detailpage&v=jF6Ad4GYjRU#t=1035s
		Bitmap framedOutput = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas framedCanvas = new Canvas(framedOutput);
		// End of Step 1

		// Start - TODO IMPORTANT - this section shouldn't be included in the final code
		// It's needed here to differentiate step 2 (red) with the background color of the activity
		// It's should be commented out after the codes includes step 3 onwards
		// Paint squaredPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		// squaredPaint.setColor(Color.BLUE);
		// framedCanvas.drawRoundRect(outerRect, 0f, 0f, squaredPaint);
		// End

		// 2. Draw an opaque rounded rectangle link:
		// http://www.youtube.com/watch?feature=player_detailpage&v=jF6Ad4GYjRU#t=1044s
		RectF innerRect = new RectF(destRect.left+borderWidth, destRect.top+borderWidth, 
				destRect.right - borderWidth, destRect.bottom - borderWidth);

		Paint innerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		innerPaint.setColor(Color.RED);
		framedCanvas.drawRoundRect(innerRect, cornerRadius, cornerRadius, innerPaint);

		// 3. Set the Power Duff mode link:
		// http://www.youtube.com/watch?feature=player_detailpage&v=jF6Ad4GYjRU#t=1056s
		Paint outerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		outerPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT));

		// 4. Draw a translucent rounded rectangle link:
		// http://www.youtube.com/watch?feature=player_detailpage&v=jF6Ad4GYjRU
		outerPaint.setColor(Color.argb(100, 0, 0, 0));
		framedCanvas.drawRoundRect(destRectF, cornerRadius, cornerRadius, outerPaint);

		// 5. Draw the frame on top of original bitmap
		canvas.drawBitmap(framedOutput, 0f, 0f, null);

		return output;
	}
}
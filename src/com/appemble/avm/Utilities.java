/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.Vibrator;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;
import android.view.View;
import android.widget.ImageView;

import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleDialog;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.dao.TableDao;

public class Utilities {
	public static final String STR_EMPTY = "";	

	public static byte[] serializeObject(Object o) {
		if (null == o)
			return null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			ObjectOutput out = new ObjectOutputStream(bos);
			out.writeObject(o);
			out.close();

			// Get the bytes of the serialized object
			byte[] buf = bos.toByteArray();

			return buf;
		} catch (IOException ioe) {
			LogManager.logWTF(ioe, "Unable to serialize the object: " + o.toString());
			return null;
		}
	}

	public static Object deserializeObject(byte[] b) {
		if (null == b)
			return null;
		try {
			ObjectInputStream in = new ObjectInputStream(
					new ByteArrayInputStream(b));
			Object object = in.readObject();
			in.close();

			return object;
		} catch (ClassNotFoundException cnfe) {
			LogManager.logError(cnfe, "Unable to deserialize object.");
			return null;
		} catch (IOException ioe) {
			LogManager.logError(ioe, "Unable to deserialize object.");
			return null;
		}
	}

	public static String objectToString(Serializable object) {
		if (null == object)
			return null;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			new ObjectOutputStream(out).writeObject(object);
			byte[] data = out.toByteArray();
			out.close();

			out = new ByteArrayOutputStream();
			Base64OutputStream b64 = new Base64OutputStream(out, 0);
			b64.write(data);
			b64.close();
			out.close();

			return new String(out.toByteArray());
		} catch (IOException e) {
			LogManager.logError(e, "Unable to convert object to string: " + object.toString());
		}
		return null;
	}

	public static Object stringToObject(String encodedObject) {
		if (null == encodedObject)
			return null;
		try {
			return new ObjectInputStream(new Base64InputStream(
					new ByteArrayInputStream(encodedObject.getBytes()), 0))
					.readObject();
		} catch (Exception e) {
			LogManager.logError(e, "Unable to convert a string to an object: " + encodedObject);
		}
		return null;
	}

	public static HashMap<String, String> ContentValuesToHashMap(ContentValues cv) {
		if (null == cv)
			return null;
		HashMap<String, String> hm = new HashMap<String, String>();
		Set<Entry<String, Object>> s=cv.valueSet();
		for (Entry<String, Object> entry : s) {
            hm.put(entry.getKey(), entry.getValue().toString());
        }
		return hm;
	}

	public static ContentValues HashMapToContentValues(HashMap<String, String>hm) {
		if (null == hm)
			return null;
		ContentValues cv = new ContentValues();
		for (String key : hm.keySet()) {
			cv.put(key, hm.get(key));
		}
		return cv;
	}

	public static HashMap<String, String> BundleToHashMap(Bundle bundle) {
		if (null == bundle)
			return null;
		HashMap<String, String> hm = new HashMap<String, String>();
		Set<String> s = bundle.keySet();
		for (String key : s) {
            hm.put(key, bundle.getString(key));
        }
		return hm;
	}

	public static String getValueFromNameValuePairs(List<NameValuePair> nameValuePairs, String parameterName) {
    	if (parameterName == null)
    		return null;
    	if (null == nameValuePairs)
    		return parameterName;
    	for (NameValuePair nvp : nameValuePairs) {
    		if (nvp != null) {    			
        		String name = nvp.getName();
        		if (null != name && nvp.getName().equalsIgnoreCase(parameterName))
        			return nvp.getValue();
    		}
        }
    	return null;
    }

    public static String replaceParameterisedStrings(String aString, Bundle keyValues, 
    		String sDbName, int iType) {
    	if (null == aString)
    		return null;
    	if (keyValues == null && sDbName == null)
    		return aString;

//    	String[] sTokens = aString.split("((?=<)|(?<=>))(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
    	String[] sTokens = aString.split("((?=\\[)|(?<=\\]))(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

        String sParameter = null, sValue = null; boolean bPrevValueIsNull = false;
        StringBuffer sbResult = new StringBuffer();
        for (int i = 0; i < sTokens.length; i++) {
        	sValue = null;
        	if (sTokens[i].length() == 0) {
        		StringBuffer sb = new StringBuffer();
                for (int j = 0; j < sTokens.length; j++)
                	sb.append(sTokens[j]);
        		continue;
        	}
        	if (sTokens[i].charAt(0) != Constants.BEGIN_PARAMETER_IDENTIFIER && 
        			sTokens[i].charAt(sTokens[i].length() - 1) != Constants.END_PARAMETER_IDENTIFIER) {
            	if (bPrevValueIsNull == true) {
            		if (sTokens[i].charAt(0) == '\'')
            			sbResult.append(sTokens[i].substring(1)); // remove the trailing single quote '
            		else
            			sbResult.append(sTokens[i]); // remove the trailing single quote '
            		bPrevValueIsNull = false;
            	} else 
            		sbResult.append(sTokens[i]);
	        	continue;
        	}
        	int iParameterLength = sTokens[i].length();
        	if (iParameterLength > 0) {
        		sParameter = sTokens[i].substring(1, iParameterLength-1); // remove the angular brackets.
        		iParameterLength -= 2; // reduce the length by 2 for angular brackets.
        	}
        	boolean bIsQuery = false;
        	// remove quotes if present at the beginning and end of the parameter.
        	if (sParameter.charAt(0) == '\"' && sParameter.charAt(iParameterLength - 1) == '\"')
        		bIsQuery = Utilities.isQuery(sParameter.substring(1, iParameterLength - 1));
        	else
        		bIsQuery = Utilities.isQuery(sParameter);

        	// if the parameter is a query, execute the query and get the string from the first column
        	if (bIsQuery) {
	        	// remove the quotes if present
	        	if (sParameter.charAt(0) == '\"' && sParameter.charAt(iParameterLength - 1) == '\"')
	        		sParameter = sParameter.substring(1, iParameterLength - 1);
				sParameter = replaceParameterisedStrings(sParameter, keyValues, sDbName, Constants.SQL);
				sValue = _getParameterValueFromDb(sParameter, sDbName);
        	}
        	else {
	        	// remove the quotes if present
	        	if (sParameter.charAt(0) == '\"' && sParameter.charAt(iParameterLength - 1) == '\"')
	        		sParameter = sParameter.substring(1, iParameterLength - 1);
        		if (null != keyValues)
        			sValue = keyValues.getString(sParameter);
        		if (sValue == null && sParameter.startsWith(Constants.GLOBAL_PARAMETER_PREFIX)) {
            		sParameter = sParameter.substring(Constants.GLOBAL_PARAMETER_PREFIX_LENGTH+1);
        			HashMap<String, String> row = TableDao.get1Row(sDbName, 
        					Constants.STR_PROPERTIES_TABLE, "key = \'" + sParameter + "\'");
        			sValue = row.get("value");
        		} 
        	}
			if (null == sValue) {
				LogManager.logDebug("While deparameterizing: " + aString + " Parameter = " + sParameter + " returned no value.");
				if (iType == Constants.SQL) {
					bPrevValueIsNull = true;
					sValue = "null";
				}
				else
					continue;
			}
			sValue = sValue.trim();
	        try {
	        	if (iType == Constants.URL) {
	        		if (!sValue.startsWith("http") && !sValue.startsWith("www"))
	        			sValue = URLEncoder.encode(sValue, "UTF-8");
					sbResult.append(sValue);
	        	} else if (iType == Constants.SQL) {
	        		if (bPrevValueIsNull) {
	        			if (sbResult.charAt(sbResult.length() - 1) == '\'')
	        				sbResult.setLength(sbResult.length()-1); // remove the last single quote '
	        		}
					sbResult.append(sValue);
	        	} else
					sbResult.append(sValue);
        	}
	        catch (UnsupportedEncodingException uee) {
	        	LogManager.logError(uee, "Unable to deparameterize the source: " + aString);
	        }				
    	}
        return sbResult.toString();
    } 

	public static Drawable getImageFromWeb(String url)
	{
		if (null == url)
			return null;
		try {	
			InputStream is = (InputStream) new URL(url).getContent();
			Drawable d = Drawable.createFromStream(is, "src name");
			return d;
		} catch (MalformedURLException murle) {
			LogManager.logError(murle, "Unable to get image from URL: " + url);
			return null;
		} catch (IOException ioe) {
			LogManager.logError(ioe, "Unable to get image from URL: " + url); 
			return null;
		}
	}

	public static Drawable getImageFromLocal(String ImageName)
	{
		Resources res = Cache.context.getResources();
		int resId = res.getIdentifier(ImageName, "drawable", "com.appemble.avm.dynamicapp");
		if (resId > 0)
			return res.getDrawable(resId);
		return null;
	}

	public static boolean isQuery(String sQuery) {
		String[] keywords = { "SELECT", "INSERT", "UPDATE", "DELETE", "REPLACE" };
		return doesQueryMatches(sQuery, keywords);
	}
	
	public static boolean isUpdateQuery(String sQuery) {
		String[] keywords = { "INSERT", "UPDATE", "DELETE", "REPLACE" };
		return doesQueryMatches(sQuery, keywords);
	}

	public static boolean doesQueryMatches(String sQuery, String[] keywords) {
		if (null == sQuery || sQuery.length() == 0 || null == keywords || keywords.length == 0)
			return false;
		
		String query = sQuery.toUpperCase();
		for (int i = 0; i < keywords.length; i++) { 
			if (query.startsWith(keywords[i])) {
				if (keywords[i].matches("SELECT") || keywords[i].matches("DELETE")) // removed from ex. select CURRENT_DATE
					return true;
				else if ((keywords[i].matches("INSERT") || keywords[i].matches("REPLACE")) && query.contains(" INTO "))
					return true;
				else if (keywords[i].matches("UPDATE") && query.contains(" SET "))
					return true;
			}
		}
		return false;
	}

	public static boolean isUrl(String sString) {
		if (null == sString)
			return false;
		// TODO change this to parseURL and if exception is there 
		try {
			new URL(sString);
			return true;
		} catch (MalformedURLException e) {
			return false;
		}
	}

	public static boolean copyFile(InputStream inputStream, String sDestinationFullPath, 
			boolean bOverwrite) {
		if (null == inputStream || null == sDestinationFullPath || 0 == sDestinationFullPath.length())
			return false;
//		try {
			File outfile = new File(sDestinationFullPath);
			if (outfile.exists()) {
				if (false == bOverwrite)
					return true; // the file already exists and should not be over written
			}
			if (bOverwrite)
				outfile.delete();
			int iLastIndexOfPathSeparator = sDestinationFullPath.lastIndexOf(File.separator);
			if (iLastIndexOfPathSeparator > -1) {
				File parentDir = new File(sDestinationFullPath.substring(0, iLastIndexOfPathSeparator));
				if (false == parentDir.exists())
					parentDir.mkdirs();
			}
			copyFile(inputStream, outfile);
//			inputStream.close();		

//		} 
//		catch (IOException ioe) {
//			LogManager.logError("Error - Unable to copy file to: " + sDestinationFullPath + ". Source file may not be available.");
//			return false;
//		}
		return true;
	}

	public static boolean copyFile(InputStream inputStream, File outfile) {
		// Open the empty db as the output stream
		OutputStream destination;
		try {
			destination = new FileOutputStream(outfile);

			// transfer bytes from the inputfile to the outputfile
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inputStream.read(buffer)) > 0) {
				destination.write(buffer, 0, length);
			}
			// Close the streams
			destination.flush();
			destination.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException ioe) {
			LogManager.logError("Error - Unable to copy file to: " + outfile.getAbsolutePath() + ". Source file may not be available.");
			return false;
		}
		return true;
	}
	private static String _getParameterValueFromDb(String sQuery, String sDbName) {
		if (null == sQuery || sQuery.length() == 0 || null == sDbName || sDbName.length() == 0)
			return null;
		String sReturn = null;
		String[] sValues = TableDao.getValuesFromDb(sQuery, sDbName);
		if (null != sValues) {
			if (sValues.length > 0)
				LogManager.logError("Multiple values returned for a single parameter: " + sQuery);
			sReturn = sValues[0];
		}
		return sReturn;
	}

	public static String format(ControlModel controlModel, String sText) {
		if (null == controlModel)
			return sText;
		if (null == sText)
			return null;
		String sFormat = controlModel.sFormat;
		int iDataType = controlModel.iDataType;
		if (sFormat == null || 0 == sFormat.length() || 0 == iDataType)
			return sText;
		if (iDataType == Constants.DATE || iDataType == Constants.DATETEXT || iDataType == Constants.DATETIME || 
				iDataType == Constants.TIME || iDataType == Constants.TIMESTAMP) {
			String sStorageFormat = controlModel.mExtendedProperties != null ? controlModel.mExtendedProperties.get(Constants.STORAGE_FORMAT) : null;
			return formatDate(sText, iDataType, sStorageFormat != null ? sStorageFormat : sFormat, sFormat);
		}
		else if (iDataType == Constants.TINYINT || iDataType == Constants.VARCHAR || iDataType == Constants.VARCHAR2 ||
				iDataType == Constants.INT64 || iDataType == Constants.INTEGER || iDataType == Constants.LARGEINT || 
				iDataType == Constants.INT || iDataType == Constants.TEXT || iDataType == Constants.DECIMAL ||  
				iDataType == Constants.CHAR || iDataType == Constants.NUMBER || iDataType == Constants.AUTOINC || 
				iDataType == Constants.BIGINT || iDataType == Constants.CLOB || iDataType == Constants.CURRENCY || 
				iDataType == Constants.DEC || iDataType == Constants.DOUBLE || 
				iDataType == Constants.DOUBLE_PRECISION || iDataType == Constants.FLOAT || iDataType == Constants.GUID ||  
				iDataType == Constants.MEMO || iDataType == Constants.MONEY || iDataType == Constants.NCHAR || 
				iDataType == Constants.NTEXT || iDataType == Constants.NUMERIC || 
				iDataType == Constants.NVARCHAR ||iDataType == Constants.NVARCHAR2 || iDataType == Constants.REAL || 
				iDataType == Constants.SMALLINT || iDataType == Constants.SMALLMONEY) {
			 sText = String.format(sFormat, sText);
		}				 
		else if (iDataType == Constants.BOOL || iDataType == Constants.BOOLEAN) {
		}
		
		return sText;
	}
	
	public static String formatDate(String sText, int iDataType, String sInputFormat, String sOutputFormat) {
		if (null == sOutputFormat || null == sText || (sText = sText.trim()).length() == 0 || 0 == iDataType)
			return null;
		
		if (iDataType == Constants.DATE || iDataType == Constants.DATETEXT || iDataType == Constants.DATETIME || 
				iDataType == Constants.TIME || iDataType == Constants.TIMESTAMP) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat();
				Date date = null;
				if ("now".equalsIgnoreCase(sText) || "today".equalsIgnoreCase(sText)) 
					date = new Date();
				else if (null != sInputFormat && (sInputFormat.equals("nn") || sInputFormat.equals("nnnn"))) { // the date is in integer.
					try {
						long lTime = Long.parseLong(sText);
						if (sInputFormat.equals("nn")) // time is represented in seconds.
							lTime *= 1000;
						date = new Date(lTime);
					} catch (NumberFormatException nfe) {
						LogManager.logError("Unable to convert date: " + sText + " using format: " + sInputFormat);
						sText = null;
					}
				} else {
					if( null == sInputFormat || sInputFormat.equals(""))
					{
						if (iDataType == Constants.DATE || iDataType == Constants.DATETEXT)
							sInputFormat = "yyyy-MM-dd";
						else if (iDataType == Constants.DATETIME || iDataType == Constants.TIMESTAMP)
							sInputFormat = "yyyy-MM-dd hh:mm:ss";
						else if (iDataType == Constants.TIME)
							sInputFormat = "hh:mm:ss";
					}
					sdf.applyPattern(sInputFormat);
					date = sdf.parse(sText);
				}
				if (!sInputFormat.equals(sOutputFormat)) {
					if (sOutputFormat.equals("nn"))
						sText = String.valueOf(date.getTime()/1000);
					else if (sOutputFormat.equals("nnnn"))
						sText = String.valueOf(date.getTime());
					else {
						sdf.applyPattern(sOutputFormat);
						sText = sdf.format(date);
					}
				}
			} catch (ParseException pe) {
				LogManager.logError(pe, "Unable to read or write date. Date: " + sText + ". Input Format: " + 
						sInputFormat + ". Output Format: " + sOutputFormat);
				sText = null;
			}
		}
		return sText;
	}

	public static String getDefaultText(CharSequence sTextSequence, ControlModel controlModel, String sImageSource) {
		String sText = sTextSequence.toString();
		if (null == sText)
			sText = sImageSource;
		return sText;
// 		This is being done in Action.validateFormat		
//		String sText = null;
//		if (null != sTextSequence) {
//			sText = sTextSequence.toString();
//			if ((controlModel.iDataType == Constants.DATE || controlModel.iDataType == Constants.DATETEXT || controlModel.iDataType == Constants.DATETIME || 
//				controlModel.iDataType == Constants.TIME || controlModel.iDataType == Constants.TIMESTAMP) && null != controlModel.sMisc1)
//				return Utilities.formatDate(sText, controlModel.iDataType, controlModel.sFormat, controlModel.sMisc1); 
//		} else
//			sText = sImageSource;
//		return sText;
		
	}
	public static boolean isEmail(String email) {
		final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
		          "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
		          "\\@" +
		
		          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
		          "(" +
		          "\\." +
		          "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
		          ")+"
		      );
		return EMAIL_ADDRESS_PATTERN.matcher(email).matches();		
	}

	public static List<NameValuePair> convertBundleToNameValuePairs(Bundle bundle) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		if (null == bundle || bundle.size() == 0)
			return nameValuePairs;
		Bundle dataTypes = bundle.getBundle("data_types");
		if (null != dataTypes)
			bundle.remove("data_types");
		Iterator<String> postKeyItr=bundle.keySet().iterator();
		String key , value = null; int iDataType;
		while(postKeyItr.hasNext()) {
			key = postKeyItr.next();
			value = null;
			if (null != dataTypes) {
				iDataType = dataTypes.getInt(key); 
				if (iDataType == Constants.VARCHAR || iDataType == Constants.VARCHAR2 ||
						iDataType == Constants.TEXT || iDataType == Constants.CHAR || 
						iDataType == Constants.CLOB)
					value = bundle.getString(key);
			} else
				value = bundle.getString(key);
			if (null == value || 0 == value.length())
				continue;
			nameValuePairs.add(new BasicNameValuePair(key, value));
		}
		if (null != dataTypes)
			bundle.putBundle("data_types", dataTypes);
		return nameValuePairs;
	}

	public static List<NameValuePair> convertHashMapToNameValuePairs(HashMap<String, String> hashMap) {
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		if (hashMap != null && hashMap.size() > 0) {
			Iterator<String> postKeyItr=hashMap.keySet().iterator();
			String key , value;
			while(postKeyItr.hasNext()) {
				key=postKeyItr.next();
				value=hashMap.get(key);
				if (null == value || 0 == value.length())
					continue;
				nameValuePairs.add(new BasicNameValuePair(key, value));
			}
		}
		return nameValuePairs;
	}

	public static HashMap<String, String> convertBundleToHashMap(Bundle bundle) {
		HashMap<String, String> hashmap = new HashMap<String, String>();
		if (null == bundle || bundle.size() == 0)
			return hashmap;
		Bundle dataTypes = bundle.getBundle("data_types");
		if (null != dataTypes)
			bundle.remove("data_types");
		Iterator<String> postKeyItr = bundle.keySet().iterator();
		String key, value; int iDataType;
		while(postKeyItr.hasNext()) {
			key = postKeyItr.next();
			value = null;
			if (null != dataTypes) {
				iDataType = dataTypes.getInt(key); 
				if (iDataType == Constants.VARCHAR || iDataType == Constants.VARCHAR2 ||
						iDataType == Constants.TEXT || iDataType == Constants.CHAR || 
						iDataType == Constants.CLOB) 
					value = bundle.getString(key);
			} else
				value = bundle.getString(key);
//				if (null == value || 0 == value.length())
//					continue;
			hashmap.put(key, value);
		}
		if (null != dataTypes)
			bundle.putBundle("data_types", dataTypes);
		return hashmap;
	}

	public static Bundle convertHashMapToBundle(HashMap<String, String> hashMap) {
		if (null == hashMap || hashMap.size() == 0)
			return null;
		Bundle bundle = new Bundle();
		Iterator<String> postKeyItr=hashMap.keySet().iterator();
		String sKey , sValue;
		while(postKeyItr.hasNext()) {
			sKey = postKeyItr.next();
			sValue = hashMap.get(sKey);
//			if (null == sValue || 0 == sValue.length())
//				continue;
			Utilities.putData(bundle, Constants.VARCHAR, sKey, sValue);
		}
		return bundle;
	}

	public static MediaPlayer playSound(Context context, Uri alert, long lDuration) {
		if (null == alert || null == context)
			return null;
		final MediaPlayer mMediaPlayer = new MediaPlayer();
        try {
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
		    	Handler handler = new Handler();
		    	handler.postDelayed(new Runnable() {
		    	    public void run() {
		    	        mMediaPlayer.stop();
		    	    }
		    	}, lDuration);
          }
        } catch (IOException e) {
        	LogManager.logError(e, "Unablet to play sound: " + alert.toString());
        	// TODO log an error here
        }
        return mMediaPlayer;
    }

    public static Uri getAlarmUri(String sFileName) {
		Uri alert = null;
		if (null != sFileName && sFileName.length() > 0) {
			alert = Uri.fromFile(new File("file:///android_asset/" + sFileName));
		}
		if (null == alert)
			alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
	    if (alert == null)
	        alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if (alert == null)
            alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
	    return alert;
	}    

    public static synchronized Object playSoundEffects(Context context, String sFileName) {
    	if (null == context)
    		context = Cache.context;
    	if (null == context)
    		return null;
		// Now play the sound effects.
		AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		switch (am.getRingerMode()) {
		    case AudioManager.RINGER_MODE_SILENT:
		        break;
		    case AudioManager.RINGER_MODE_VIBRATE:
		    	Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
		    	long[] lPattern = { 0, 1500, 500, 1500, 500, 1500, 500, 1500, 500 };
	            vibrator.vibrate(lPattern, -1);
		    	return vibrator;
		    case AudioManager.RINGER_MODE_NORMAL:
				Uri alert = Utilities.getAlarmUri(sFileName);
	    		return Utilities.playSound(context, alert, 20000);
		}
		return null;
    }
    
    public static int getContactInfo(Activity activity, Intent intent, Bundle targetParameterValueList) {
    	if  (null == activity || null == intent)
    		return Activity.RESULT_CANCELED;
    	Uri contactData = intent.getData();
        Cursor cursor = null, nameCursor = null, phoneCursor = null, emailCursor = null, addressCursor = null,
        	organizationCursor = null, eventCursor = null, websiteCursor = null;
        try {
        	cursor = activity.managedQuery(contactData, null, null, null, null);
	        cursor.moveToFirst();
	        
	        String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
	
	        String[] nameColumns = new String[] { StructuredName.PREFIX, StructuredName.GIVEN_NAME, StructuredName.FAMILY_NAME, 
	        		StructuredName.DISPLAY_NAME, StructuredName.MIDDLE_NAME, StructuredName.SUFFIX};
			String whereName = ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " = ?";
			whereName += " AND " + ContactsContract.Data.MIMETYPE + "='" + StructuredName.CONTENT_ITEM_TYPE + "'";
			String[] nameWhereParams = new String[]{ contactId };
			nameCursor = activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI, 
		              nameColumns, whereName, nameWhereParams, null);
	        if (nameCursor != null && nameCursor.moveToNext()) {
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "prefix_name", nameCursor.getString(0));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "given_name", nameCursor.getString(1));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "family_name", nameCursor.getString(2));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "display_name", nameCursor.getString(3));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "middle_name", nameCursor.getString(4));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "suffix_name", nameCursor.getString(5));
	        }
	
			Utilities.putData(targetParameterValueList, Constants.VARCHAR, "name", cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME)));
			
			String hasPhone = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
			
			hasPhone = hasPhone.equalsIgnoreCase("1") ? "true" : "false";
			
			if (Boolean.parseBoolean(hasPhone)) {
				phoneCursor = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, 
						null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
				while (phoneCursor != null && phoneCursor.moveToNext()) {
					String phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					String phoneType = (String) ContactsContract.CommonDataKinds.Phone.getTypeLabel(activity.getResources(), 
							phoneCursor.getInt(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE)), null);
					phoneType = phoneType.toLowerCase().replace(' ', '_');
					Utilities.putData(targetParameterValueList, Constants.VARCHAR, phoneType + "_phone", phoneNumber);
				}
			}
			
			// Find Email Addresses
			emailCursor = activity.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, 
					ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId, null, null);
			while (emailCursor.moveToNext()) {
				String emailAddress = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
				String emailType = (String) ContactsContract.CommonDataKinds.Email.getTypeLabel(activity.getResources(), 
						emailCursor.getInt(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE)), null);
				emailType = emailType.toLowerCase().replace(' ', '_');
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, emailType+"_email", emailAddress);
			}
			
			addressCursor = activity.getContentResolver().query(
					ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + " = " + contactId,
					null, null);
			while (null != addressCursor && addressCursor.moveToNext()) { 
				// These are all private class variables, don't forget to create them.
				String sAddressType = (String) ContactsContract.CommonDataKinds.StructuredPostal.getTypeLabel(activity.getResources(), 
						addressCursor.getInt(addressCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.TYPE)), null);
//				String sAddressType = addressCursor.getString(addressCursor.getColumnIndex(
//						ContactsContract.CommonDataKinds.StructuredPostal.TYPE)); 
				sAddressType = sAddressType.toLowerCase().replace(' ', '_');
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "po_box_" + sAddressType + "_address", 
						addressCursor.getString(addressCursor.getColumnIndex(
								ContactsContract.CommonDataKinds.StructuredPostal.POBOX)));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "street_" + sAddressType + "_address", 
						addressCursor.getString(addressCursor.getColumnIndex(
								ContactsContract.CommonDataKinds.StructuredPostal.STREET)));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "city_" + sAddressType + "_address", 
						addressCursor.getString(addressCursor.getColumnIndex(
						ContactsContract.CommonDataKinds.StructuredPostal.CITY)));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "state_" + sAddressType + "_address", addressCursor.getString(
						addressCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.REGION)));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "postal_code_" + sAddressType + "_address", 
						addressCursor.getString(addressCursor.getColumnIndex(
						ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE)));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "country_" + sAddressType + "_address", 
						addressCursor.getString(addressCursor.getColumnIndex(
								ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY)));
			}

	        String[] organizationColumns = new String[] { 
	        		ContactsContract.CommonDataKinds.Organization.COMPANY, 
	        		ContactsContract.CommonDataKinds.Organization.TYPE, 
	        		ContactsContract.CommonDataKinds.Organization.TITLE, 
	        		ContactsContract.CommonDataKinds.Organization.DEPARTMENT,
	        		ContactsContract.CommonDataKinds.Organization.JOB_DESCRIPTION, 
	        		ContactsContract.CommonDataKinds.Organization.OFFICE_LOCATION
	        };
	        
			String whereOrganization = ContactsContract.CommonDataKinds.Organization.CONTACT_ID + " = ?";
			whereOrganization += " AND " + ContactsContract.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE + "'";
			String[] organizationWhereParams = new String[]{ contactId };
			organizationCursor = activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI, 
		              organizationColumns, whereOrganization, organizationWhereParams, null);
			while (null != organizationCursor && organizationCursor.moveToNext()) { 
			          // These are all private class variables, don't forget to create them.
				String sOrganizationType = (String) ContactsContract.CommonDataKinds.Organization.getTypeLabel(activity.getResources(), 
						organizationCursor.getInt(1), null);
				sOrganizationType = sOrganizationType.toLowerCase().replace(' ', '_');
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "company_name_" + sOrganizationType, organizationCursor.getString(0));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "title_at_" + sOrganizationType, organizationCursor.getString(2));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "department_name_" + sOrganizationType, organizationCursor.getString(3));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "job_description_" + sOrganizationType, organizationCursor.getString(4));
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, "office_location_" + sOrganizationType, organizationCursor.getString(5));
			}

	        String[] eventColumns = new String[] { 
	        		ContactsContract.CommonDataKinds.Event.START_DATE, 
	        		ContactsContract.CommonDataKinds.Event.TYPE 
	        };
			String whereEvent = ContactsContract.CommonDataKinds.Event.CONTACT_ID + " = ?";
			whereEvent += " AND " + ContactsContract.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE + "'";
			String[] eventWhereParams = new String[]{ contactId };
			eventCursor = activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI, 
		              eventColumns, whereEvent, eventWhereParams, null);
			while (null != eventCursor && eventCursor.moveToNext()) {
				String sEventType = null;
				switch (eventCursor.getInt(1)) {
				case ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY:
					sEventType = "anniversary";
					break;
				case ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY:
					sEventType = "birthday";
					break;
				case ContactsContract.CommonDataKinds.Event.TYPE_OTHER:
					sEventType = "other";
					break;
				}
				
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, sEventType + "_date", eventCursor.getString(0));
			}

	        String[] websiteColumns = new String[] { 
	        		ContactsContract.CommonDataKinds.Website.URL, 
	        		ContactsContract.CommonDataKinds.Website.TYPE 
	        };
			String whereWebsite = ContactsContract.CommonDataKinds.Website.CONTACT_ID + " = ?";
			whereWebsite += " AND " + ContactsContract.Data.MIMETYPE + "='" + ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE + "'";
			String[] websiteWhereParams = new String[] { contactId };
			websiteCursor = activity.getContentResolver().query(ContactsContract.Data.CONTENT_URI, 
		              websiteColumns, whereWebsite, websiteWhereParams, null);
			while (null != websiteCursor && websiteCursor.moveToNext()) {
				String sWebsiteType = null;
				switch (websiteCursor.getInt(1)) {
				case ContactsContract.CommonDataKinds.Website.TYPE_BLOG:
					sWebsiteType = "blog";
					break;
				case ContactsContract.CommonDataKinds.Website.TYPE_FTP:
					sWebsiteType = "ftp";
					break;
				case ContactsContract.CommonDataKinds.Website.TYPE_HOME:
					sWebsiteType = "home";
					break;
				case ContactsContract.CommonDataKinds.Website.TYPE_HOMEPAGE:
					sWebsiteType = "homepage";
					break;
				case ContactsContract.CommonDataKinds.Website.TYPE_OTHER:
					sWebsiteType = "other";
					break;
				case ContactsContract.CommonDataKinds.Website.TYPE_PROFILE:
					sWebsiteType = "profile";
					break;
				case ContactsContract.CommonDataKinds.Website.TYPE_WORK:
					sWebsiteType = "work";
					break;
				case ContactsContract.CommonDataKinds.Website.TYPE_CUSTOM:
					sWebsiteType = "custom";
					break;
				}
				Utilities.putData(targetParameterValueList, Constants.VARCHAR, sWebsiteType + "_url", websiteCursor.getString(0));
			}
        } catch (Exception e) {
        	LogManager.logError(e, "Unable to get contact info from the address book");
        } finally {
        	if (nameCursor != null)
        		nameCursor.close();
        	if (phoneCursor != null)
        		phoneCursor.close();
        	if (emailCursor != null)
        		emailCursor.close();
        	if (addressCursor != null)
        		addressCursor.close();
        	if (organizationCursor != null)
        		organizationCursor.close();
        	if (eventCursor != null)
        		eventCursor.close();
        	if (websiteCursor != null)
        		websiteCursor.close();
        	if (cursor != null)
        		cursor.close();        	
        }
		return Activity.RESULT_OK;
    }//getContactInfo
    
    // Thanks to http://stackoverflow.com/questions/3183932/androidhow-to-check-if-application-is-running-in-background
    /**
     * Checks if the application is being sent in the background (i.e behind
     * another application's Activity).
     * 
     * @param context the context
     * @return <code>true</code> if another application will be above this one.
     */
    public static boolean isApplicationSentToBackground(final Context context) {
      ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
      List<RunningTaskInfo> tasks = am.getRunningTasks(1);
      if (!tasks.isEmpty()) {
        ComponentName topActivity = tasks.get(0).topActivity;
        if (!topActivity.getPackageName().equals(context.getPackageName())) {
          return true;
        }
      }

      return false;
    }
    
    public static String[] splitCommaSeparatedString(String sSource) {
    	return splitStrings(sSource, ',');
    }
    public static String[] splitStrings(String sSource, char cSeparator) {
    	if (null == sSource || sSource.length() == 0)
    		return null;
		// source http://stackoverflow.com/questions/1757065/java-splitting-a-comma-separated-string-but-ignoring-commas-in-quotes
        String otherThanQuote = " [^\"] ";
        String quotedString = String.format(" \" %s* \" ", otherThanQuote);
        String regex = String.format("(?x) "+ // enable comments, ignore white spaces
                "%c                         "+ // match a comma
                "(?=                       "+ // start positive look ahead
                "  (                       "+ //   start group 1
                "    %s*                   "+ //     match 'otherThanQuote' zero or more times
                "    %s                    "+ //     match 'quotedString'
                "  )*                      "+ //   end group 1 and repeat it zero or more times
                "  %s*                     "+ //   match 'otherThanQuote'
                "  $                       "+ // match the end of the string
                ")                         ", // stop positive look ahead
                cSeparator, otherThanQuote, quotedString, otherThanQuote);

        String[] sStrings = sSource.split(regex);
        // Added code below to remove the starting and the ending ". TODO this requires regression testing 20120926
        int i = 0;
    	for (String sString : sStrings) {
    		sString = sString.trim();
    		int iLength  = sString.length();
    		if (iLength > 2 && (sString.charAt(0) == '\"' && sString.charAt(iLength-1) == '\"')) // remove the starting and ending quotes.
    			sString = sString.substring(1, iLength-1);
			sStrings[i] = sString;
    		i++;
//    		sStrings[i++] = sString.trim().replaceAll("(^\")|('$)","");
    	}
    	return sStrings;
    }

    public static HashMap<String, String> getNameValuePairMap(String sSource) {
    	HashMap<String, String> map = new HashMap<String, String>();
    	if (null == sSource || sSource.length() == 0)
    		return map;
    	String[] sNameValuePairs = splitCommaSeparatedString(sSource);
    	String[] sNameValue;
    	for (String sNameValuePair : sNameValuePairs) {
    		int iLength = sNameValuePair.length();
    		if (iLength > 2 && (sNameValuePair.charAt(0) == '\"' && sNameValuePair.charAt(iLength-1) == '\"')) // remove the starting and ending quotes.
    			sNameValuePair = sNameValuePair.substring(1, iLength-1);
//    		sNameValuePair = sNameValuePair.trim().replaceAll("^\"|\"$", "");
//    		sNameValuePair = sNameValuePair.trim().replaceAll("(^\")|('$)","");
    		sNameValue = splitStrings(sNameValuePair, '=');
    		if (null == sNameValue || sNameValue.length != 2) {
    			LogManager.logError("Error in parsing the name value pair from the string: " + sNameValue + 
    					". The format should be name=value.");
    			continue;
    		}
    		int i = 0;
        	for (String sString : sNameValue) {
        		sString = sString.trim();
        		iLength  = sString.length();
        		if (iLength > 2 && (sString.charAt(0) == '\"' && sString.charAt(iLength-1) == '\"')) // remove the starting and ending quotes.
        			sString = sString.substring(1, iLength-1);
    			sNameValue[i] = sString;
        		i++;
//        		sStrings[i++] = sString.trim().replaceAll("^\"|\"$", "");
        	}
    		map.put(sNameValue[0], sNameValue[1]);
    	}
    	return map; 
    }
    
    public static boolean isGPSOnBoard() {
	    LocationManager locationManager = (LocationManager) Cache.context.getSystemService(Context.LOCATION_SERVICE);
	    List<String> locationProviders = locationManager.getProviders(true);
	
	    // check if we have a valid list of providers
	    if (locationProviders == null)
	    	return false;
		// check all providers 
		for (String locProvName:locationProviders)
			if (locProvName.equals(LocationManager.GPS_PROVIDER)) {			
			    return true; // we have GPS Hardware on Board
			}
		return false;
    }
    
    public static String getFilesDir(String sType) {
    	File file = null;
    	if (Constants.isExternalStorageAvailable)
    		// return Constants.sDbDirExternalStorage + Cache.context.getPackageName();
    		file = Cache.context.getExternalFilesDir(sType);
    	if (file != null)
    		return file.getAbsolutePath();
    	file = Cache.context.getFilesDir();
    	if (file != null)
    		return file.getAbsolutePath();
    	return null;
    }
    
    public static AppembleActivity getActivity(Context context, View view, View parentView) {
		AppembleActivity appembleActivity = null;
		Context screenContext = null;
		if (parentView instanceof AppembleActivity)
			appembleActivity = (AppembleActivity) appembleActivity;
			
		if (null == appembleActivity && parentView instanceof DynamicLayout) { // treat the dialog separately.
			AppembleDialog ad = (AppembleDialog)parentView.getTag(R.integer.dialog);
			if (null != ad)
				appembleActivity = ad;
		}
		if (null == appembleActivity && null != view) {
			screenContext = view.getContext();
			if (screenContext instanceof AppembleActivity)
				appembleActivity = (AppembleActivity) screenContext;
		}
		if (null == appembleActivity && 
				parentView != null && ((screenContext = parentView.getContext()) != null)) {
			if (screenContext instanceof AppembleActivity)
				appembleActivity = (AppembleActivity) screenContext;
		}
		if (null == appembleActivity && context instanceof AppembleActivity)
			appembleActivity = (AppembleActivity) context;
		return appembleActivity;
    }
    
    public static boolean contains(String[] aStrings, String string) {
    	if (null == aStrings || null == string)
    		return false;
    	for (String str : aStrings)
    		if (null != str && str.contains(string))
    			return true;
    	return false;
    }
    
    public static int indexOf(String[] aStrings, String string) {
    	if (null == aStrings || null == string)
    		return -1;
    	for (int i = 0; i < aStrings.length; i++)
    		if (null != aStrings[i] && aStrings[i].equals(string))
    			return i;
    	return -1;
    }
    
    public static boolean parseBoolean(String sBoolean) {
    	if (null == sBoolean)
    		return false;
    	else if (sBoolean.equalsIgnoreCase("true") || sBoolean.equalsIgnoreCase("1") || sBoolean.equalsIgnoreCase("yes")
    			|| sBoolean.equalsIgnoreCase("y") || sBoolean.equalsIgnoreCase("on"))
    		return true;
    	return false;
    }
    
    public static ImageView.ScaleType getScaleType(String sScaleType) {
    	if (null == sScaleType || sScaleType.length() == 0)
    		return ImageView.ScaleType.CENTER_INSIDE;
		if (null != sScaleType && sScaleType.length() > 0) {
			if (sScaleType.equalsIgnoreCase("CENTER"))
				return ImageView.ScaleType.CENTER;
			else if (sScaleType.equalsIgnoreCase("CENTER_CROP"))
				return ImageView.ScaleType.CENTER_CROP;
			else if (sScaleType.equalsIgnoreCase("FIT_CENTER"))
				return ImageView.ScaleType.FIT_CENTER;
			else if (sScaleType.equalsIgnoreCase("FIT_END"))
				return ImageView.ScaleType.FIT_END;
			else if (sScaleType.equalsIgnoreCase("FIT_START"))
				return ImageView.ScaleType.FIT_START;
			else if (sScaleType.equalsIgnoreCase("FIT_XY"))
				return ImageView.ScaleType.FIT_XY;
			else
				return ImageView.ScaleType.CENTER_INSIDE;
		}
		return ImageView.ScaleType.CENTER_INSIDE;
    }
    
    public static final Pattern DIMENSION_PATTERN =  
//    	Pattern.compile("^\\s*([-|\\+]*)(\\d+(\\.\\d+)*)\\s*([a-zA-Z]+)\\s*$");
    	Pattern.compile("^\\s*([-|\\+]*)(\\d+(\\.\\d+)*)\\s*([a-zA-Z]+|%+|\\s*)\\s*$");
    public static float getDimension(String sDimen, float iScreenDimen) {
    	float fReturn = 0f;
    	if (null == sDimen)
    		return fReturn;
    	if (sDimen.length() == 0)
    		return fReturn;
         // -- Match target against pattern.
        Matcher matcher = Utilities.DIMENSION_PATTERN.matcher(sDimen);
        float value = 0;
        try {
	        if (matcher.matches()) {
	             // -- Match found.
	             // -- Extract value.
	        	String sSign = matcher.group(1);
	            value = Float.valueOf(matcher.group(2)).floatValue();
	            if (sSign != null && sSign.length() > 0 && sSign.charAt(0) == '-')
	            	value = -value;
	             // -- Extract dimension units.
	            String unit = matcher.group(4);
	            if (null == unit || unit.length() == 0 || unit.equalsIgnoreCase("%"))
	        		return iScreenDimen > 0 ? value*(float)(iScreenDimen/100.0) : value;
	            else if (unit.equalsIgnoreCase("dp"))
	         		return value*Cache.fScaleDPToPixels;
	            else if (unit.equalsIgnoreCase("px"))
	        		return value;
	        } else {
		        value = Float.parseFloat(sDimen);
		   		return iScreenDimen > 0 ? value*(float)(iScreenDimen/100.0) : value;
	        }
        } catch(NumberFormatException nfe) {
        	LogManager.logError("Unable to parse dimension: " + sDimen); 
        }
        return 0;
    }

    public static float getDimension(String sDimen, float iScreenDimen, float fDefault) {
    	float fDimension = getDimension(sDimen, iScreenDimen);
    	if (0 == fDimension) {
    		if (null == sDimen || 0 == sDimen.length())
    			fDimension = fDefault;
    		else
    			fDimension = 0;
    	}
    	return fDimension;
    }
    
    public static float getDimensionRaw(String sDimen) {
    	float fReturn = 0f;
    	if (null == sDimen)
    		return fReturn;
    	 if (sDimen.length() == 0)
    		 return fReturn;
    	 try {
	         // -- Match target against pattern.
	         Matcher matcher = Utilities.DIMENSION_PATTERN.matcher(sDimen);
	         if (matcher.matches()) {
	             // -- Match found.
	             // -- Extract value.
	        	String sSign = matcher.group(1);
	            Float value = Float.valueOf(matcher.group(2)).floatValue();
	            if (sSign != null && sSign.length() > 0 && sSign.charAt(0) == '-')
	            	value = -value;
	             return value;
	         } else
	             return Float.valueOf(sDimen);
    	 } catch (NumberFormatException nfe) {
    		 LogManager.logError("Unable to get dimensions from " + sDimen);
    	 }
    	 return 0;
    }
    
    public static String calcDimension(String sDimen, char cOperation, float operand) {
    	if (null == sDimen)
    		return null;
    	 if (sDimen.length() == 0)
    		 return null;
         // -- Match target against pattern.
         Matcher matcher = Utilities.DIMENSION_PATTERN.matcher(sDimen);
         String unit = null;
         float value = 0;
         try { 
	         if (matcher.matches()) {
	             // -- Match found.
	             // -- Extract value.
	        	String sSign = matcher.group(1);
	            value = Float.valueOf(matcher.group(2)).floatValue();
	            if (sSign != null && sSign.length() > 0 && sSign.charAt(0) == '-')
	            	value = -value;
	             // -- Extract dimension units.
	             unit = matcher.group(4);
	             if (null != unit)
	            	 unit = unit.toLowerCase();
	         } else
	        	 value = Float.valueOf(sDimen);
         } catch (NumberFormatException nfe) {
        	 LogManager.logError("Unable to calculate dimensions for " + sDimen);
        	 return null;
         }
         String bReturn = null;
         switch (cOperation) {
         case '+':
        	 bReturn =  Float.toString(value+operand);
        	 break;
         case '-':
        	 bReturn = Float.toString(value-operand);
        	 break;
         case '*':
        	 bReturn = Float.toString(value*operand);
        	 break;
         case '/':
        	 bReturn = Float.toString(value/operand);
        	 break;
         }
         if (null != unit)
        	 bReturn += unit;
         return bReturn;
    }

    public static boolean putData(Bundle bundle, int iDataType, String sKey, Object oValue) {
    	if (null == bundle || 0 == iDataType || null == sKey)
    		return false;
    	Bundle dataTypes = bundle.getBundle("data_types");
    	if (null == dataTypes) {
    		dataTypes = new Bundle();
    		bundle.putBundle("data_types", dataTypes);
    	}
//    	switch (iDataType) {
//    	case Constants.IMAGE:
//    	case Constants.BITMAP:
    	if (iDataType == Constants.IMAGE || iDataType == Constants.BITMAP) {
    		try {
    			bundle.putByteArray(sKey, (byte[]) oValue);
    		} catch (ClassCastException cce) {
    			LogManager.logError("Unable to convert value into byte[] for input parameter: "  + sKey 
    					+ ". with Value: " + oValue.toString());
    			return false;
    		}
//    		break;
//    	case Constants.JSONARRAY:
    	} else if (iDataType == Constants.JSONARRAY) {	
    		try {
    			bundle.putString(sKey, ((JSONArray) oValue).toString());
    		} catch (ClassCastException cce) {
    			LogManager.logError("Unable to convert value into JSONArray for input parameter: "  + sKey 
    					+ ". with Value: " + oValue.toString());
    			return false;
    		}
//    		break;
//    	case Constants.JSONOBJECT:
    	} else if (iDataType == Constants.JSONOBJECT) {
    		try {
    			bundle.putString(sKey, ((JSONObject) oValue).toString());
    		} catch (ClassCastException cce) {
    			LogManager.logError("Unable to convert value into jsonObject for input parameter: "  + sKey 
    					+ ". with Value: " + oValue.toString());
    			return false;
    		}
//    		break;
//    	case Constants.STRING_ARRAY:
    	} else if (iDataType == Constants.STRING_ARRAY) {
    		try {
    			bundle.putStringArray(sKey, ((String[]) oValue));
    		} catch (ClassCastException cce) {
    			LogManager.logError("Unable to convert value into String[] for input parameter: "  + sKey 
    					+ ". with Value: " + oValue.toString());
    			return false;
    		}
    	} else if (iDataType == Constants.PARCELABLE) {
    		try {
    			bundle.putParcelable(sKey, ((Parcelable) oValue));
    		} catch (ClassCastException cce) {
    			LogManager.logError("Unable to convert value into String[] for input parameter: "  + sKey 
    					+ ". with Value: " + oValue.toString());
    			return false;
    		}
//    		break;
//    	default:
    	} else if (iDataType == Constants.AUTOINC || iDataType == Constants.BIGINT || iDataType == Constants.BOOL ||
    			iDataType == Constants.BOOLEAN || iDataType == Constants.CHAR || iDataType == Constants.CURRENCY ||
    			iDataType == Constants.DEC || iDataType == Constants.DECIMAL || iDataType == Constants.DOUBLE ||
    			iDataType == Constants.DOUBLE_PRECISION || iDataType == Constants.FLOAT || iDataType == Constants.INT ||
    			iDataType == Constants.INT64 || iDataType == Constants.INTEGER || iDataType == Constants.LARGEINT ||
    			iDataType == Constants.NUMBER || iDataType == Constants.NUMERIC || iDataType == Constants.REAL ||
    			iDataType == Constants.SMALLINT || iDataType == Constants.SMALLMONEY) {
    		try {
    			if (null == oValue) // if oValue is null, the String.valueOf returns a string "null". We do not want that.
    				bundle.putString(sKey, null);
    			else
    				bundle.putString(sKey, String.valueOf(oValue));
    		} catch (ClassCastException cce) {
    			LogManager.logError("Unable to convert integer value into string for input parameter: "  + sKey 
    					+ ". with Value: " + oValue.toString());
    			return false;
    		}
    	} else {
    		try {
    			bundle.putString(sKey, (String)oValue);
    		} catch (ClassCastException cce) {
    			LogManager.logError("Unable to convert value into String for input parameter: "  + sKey 
    					+ ". with Value: " + oValue.toString());
    			return false;
    		}
//    		break;
    	}
		dataTypes.putInt(sKey, iDataType);
    	return true;
    }
    
    public static Bundle merge(Bundle masterBundle, Bundle slaveBundle) {
    	if (null == masterBundle)
    		return slaveBundle;
    	if (null == slaveBundle)
    		return masterBundle;
    	Bundle slaveDataTypes = slaveBundle.getBundle("data_types");
    	if (null != slaveDataTypes)
    		slaveBundle.remove("data_types");
    	masterBundle.putAll(slaveBundle);
    	Bundle masterDataTypes = masterBundle.getBundle("data_types");
    	if (null != masterDataTypes && null != slaveDataTypes)
    		masterDataTypes.putAll(slaveDataTypes);
    	if (null != slaveDataTypes)
    		slaveBundle.putBundle("data_types", slaveDataTypes);
    	return masterBundle;
    }
    
    public static Object getCursorData(Cursor cursor, int iIndex, int iDataType) {
//    	switch (iDataType) {
//    	case Constants.IMAGE:
//    	case Constants.BITMAP:
    	if (iDataType == Constants.IMAGE || iDataType == Constants.BITMAP)
    		return cursor.getBlob(iIndex);
//    	case Constants.JSONARRAY:
//    	default:
    	else
    		return cursor.getString(iIndex);
//    	}
    }

    public static String[] parseStringArray(String sValue) {
    	if (null == sValue)
    		return null;
    	sValue = sValue.trim();
    	int iLength = sValue.length();
    	if (sValue.charAt(0) != '{' || sValue.charAt(iLength-1) != '}')
    		return null;
    	return splitCommaSeparatedString(sValue.substring(1, iLength-1));
    }

    public static String convertStreamToString(InputStream is)
            throws IOException {
            Writer writer = new StringWriter();

            char[] buffer = new char[2048];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is,
                        "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            String text = writer.toString();
            return text;
    }

    public static String signAndEncodeBase64(String key, String data) {
        try {
	    	final Charset charSet = Charset.forName("US-ASCII");
	        final Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
	
	        final SecretKeySpec secret_key = new javax.crypto.spec.SecretKeySpec(charSet.encode(key).array(), "HmacSHA256");
	        sha256_HMAC.init(secret_key);
	        return Base64.encodeToString(sha256_HMAC.doFinal(data.getBytes()), Base64.DEFAULT);
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchAlgorithmException nsae) {
        	
        }
        return null;
    }
    
    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }
    
    public static String read(Context context, String sFileName) {
    	if (null == context || null == sFileName)
    		return null;
    	
    	StringBuilder buf = new StringBuilder();
    	try {
	        InputStream json=context.getAssets().open(sFileName);
	        BufferedReader in= new BufferedReader(new InputStreamReader(json, "UTF-8"));
	        String str;
	
	        while ((str=in.readLine()) != null)
	            buf.append(str);
	
	        in.close();
    	} catch (IOException ioe) {
    		return null;
    	}
    	return buf.toString();
    }
    
    public static String[] concat(String[] a, String[] b) {
    	if (null == a)
    		return b;
    	else if (null == b)
    		return a;
    	
	   int aLen = a.length;
	   int bLen = b.length;
	   if (0 == aLen)
		   return b;
	   else if (0 == bLen)
		   return a;
	   String[] c= new String[aLen+bLen];
	   System.arraycopy(a, 0, c, 0, aLen);
	   System.arraycopy(b, 0, c, aLen, bLen);
	   return c;
	}
}
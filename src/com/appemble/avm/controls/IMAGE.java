/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class IMAGE extends ImageView implements ControlInterface {
	ControlModel controlModel;
	String sImageSource;
	
	public IMAGE(final Context ctx, final ControlModel controlObject) {
		super(ctx);
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight, 
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		// The image is set at the time of data initialization
		setFocusable(false);
		setScaleType(Utilities.getScaleType(controlModel.mExtendedProperties.get("scale_type")));
		if (controlModel.mExtendedProperties.containsKey("on_pressed_image"))
			setClickable(true);

		if (controlModel != null && controlModel.bActionYN) {
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
				}
			});
		}
		return Boolean.valueOf(true);
	}

	public String getValue() {
		// return (String) getTag();
		return sImageSource;
	}

	public int getControlId() {
		return getId();
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setValue(Object object) {		
		// If called from DynamicActivity::UpdateControl, then
		// this value is obtained in the following order remote data source, local data source, remote data, 
		// local cursor, target parameter list and default value.
		if (object == null) {
			sImageSource = null;
//			ImageManager.getInstance().UpdateView(this, null, true);
//			AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, 
//					null, null);
			AppearanceManager.getInstance().updateImage(this, controlModel, null, null);
			return;
		}	
		if (!(object instanceof String))
			return;
		String sText = (String) object;
		if (null != sImageSource && sImageSource.equals(sText))
			return;
//		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, sText);
		AppearanceManager.getInstance().updateImage(this, controlModel, null, sText);
//		ImageManager.getInstance().UpdateView(this, sText, true);
		sImageSource = sText;
	}
}

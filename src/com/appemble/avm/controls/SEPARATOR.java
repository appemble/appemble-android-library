/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.view.View;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class SEPARATOR extends View implements ControlInterface {
	ControlModel controlModel;
	
	public SEPARATOR(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel=controlObject;		
	}
	
	public Object initialize(View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		this.setFocusable(false);
		this.setBackgroundColor(0xFF080808);
		this.setMinimumHeight(1);

		if (null == controlModel)
			return Boolean.valueOf(false);
		
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		return Boolean.valueOf(true);
	}

	public int getControlId() {
		return controlModel.id;
	}
	
	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return null;
	}
	
	public void setValue(Object object) {
	}
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.AppearanceModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class SEARCH extends Button implements ControlInterface {
	ControlModel controlModel;
//    boolean bOnMeasure = false;
	
	public SEARCH(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		this.setFocusable(false);
		
		if(controlModel.bActionYN) {
			setOnClickListener(new OnClickListener() {				
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
//					final ActionModel[] actions = controlModel.getActions();
//					if (null != actions)
//					for (int i = 0; i < actions.length; i++)
//						if (actions[i].iPrecedingActionId == 0) {
//							Action a = new Action();
//							a.execute(getContext(), v, parentView, actions[i]);
//						}
				}
			});
		}
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		return Boolean.valueOf(true);
	}

//	@Override
//    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        bOnMeasure = true;
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        bOnMeasure = false;
//        return;
//    }

	public int getControlId() {
		return controlModel.id;
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return this.getText().toString();
	}
	
	public void setValue(Object object) {
//	    if (bOnMeasure)
//	    	return;
		// Clear the control...
		CharSequence sExistingText = getText();
		if (object == null) {
			if (null != sExistingText)
				super.setText(null);
			else {
				sExistingText = (String) getTag();
				if (null != sExistingText) // TODO here clear the image
					setTag(null);
			}
			return;
		}
		String sText = (String) object;
		if (sText.startsWith(Constants.URL_IMAGE_PREFIX)) {
			String sAttachedImage = (String) getTag();
			if (null != sAttachedImage && sAttachedImage.equals(sText))
				return;
//			if (null != ImageManager.getInstance().UpdateView(this, sText, true));
			if (null != AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, 
					sText))
				setTag(sText);
		}
		else {
			sText = Utilities.format(controlModel, sText);
			if (null != sExistingText && sExistingText.equals(sText))
				return;
			AppearanceModel appearanceModel = AppearanceManager.getInstance().getAppearance(controlModel.sSystemDbName, 
					controlModel.iAppearanceId);
			if (null != sText && null != appearanceModel && appearanceModel.bFontStyleUnderline) {
				SpannableString content = new SpannableString(sText);
				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				this.setText(content);			
			} else
				super.setText(sText);
		}
	}
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.AppearanceModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class TEXT extends TextView implements ControlInterface {
	ControlModel controlModel;
	String sImageSource;
	boolean bFromHtml; int iMaxLines = 0;

	public TEXT(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;		
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		setFocusable(false);

		this.setMaxWidth(LayoutParams.WRAP_CONTENT);
		if (null != controlModel.mExtendedProperties)
			bFromHtml = Utilities.parseBoolean(controlModel.mExtendedProperties.get("display_as_html"));
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		AppearanceManager.getInstance().updateTextAppearance(this, controlModel, Constants.APPEARANCE_ID_NOT_SET);

	    if (controlModel.bActionYN) {
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
				}
			});
		}
	    
	    try {
	    	if (controlModel.mExtendedProperties.containsKey("max_lines")) {
	    		iMaxLines = Integer.parseInt(controlModel.mExtendedProperties.get("max_lines"));
	    		setMaxLines(iMaxLines);
	    	}
	    } catch (NumberFormatException nfe) {}	    
		return Boolean.valueOf(true);
	}
	 
	void refitText() {
		String text = (String) this.getText();
		int textWidth = this.getWidth();
		if (textWidth > 0) {
			float availableWidth = textWidth - this.getPaddingLeft()
					- this.getPaddingRight();

			TextPaint tp = getPaint();
			Rect rect = new Rect();
			tp.getTextBounds(text, 0, text.length(), rect);
			float size = rect.width();

			if (size > availableWidth)
				setTextScaleX(availableWidth / size);
		}
	}

	public int getControlId() {
		return controlModel.id;
	}
	
	public ControlModel getControlModel() {
		return controlModel;
	}

    public String getValue() {
		return Utilities.getDefaultText(getText(), controlModel, sImageSource);
	}
//  http://stackoverflow.com/questions/2160619/android-ellipsize-multiline-textview
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        CharSequence sText = getText();
        if (null == sText || 0 == iMaxLines)
        	return;
        CharSequence text = sText;
        int iLength = sText.length();
        onPreDraw();

        while(getLineCount() > iMaxLines) {
        	int iLastWordLength = 1;
        	while (iLength - iLastWordLength > 0 && text.charAt(iLength-iLastWordLength) != ' ')
        		iLastWordLength++;
        	iLength -= iLength - iLastWordLength > 0 ? iLastWordLength : 1;
            text = text.subSequence(0, iLength);
            super.setText(text + "...");
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            onPreDraw();
        }
    }

    public void setValue(Object object) {		
		// Clear the control...
		CharSequence sExistingText = getText();
		if (object == null) {
			if (null != sExistingText) {
				super.setText(null);
				}
			// there could be a image
			if (null != sImageSource) {
//				ImageManager.getInstance().UpdateView(this, null, true);
				AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, null);
				sImageSource = null;
			}
			return;
		}
		if (!(object instanceof String))
			return;
		String sText = (String) object;
		if (sText.startsWith(Constants.URL_IMAGE_PREFIX)) {
			if (null != sImageSource && sImageSource.equals(sText))
				return;
//			ImageManager.getInstance().UpdateView(this, sText, true);
			AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, sText);
			sImageSource = sText;
		} else {
			sText = Utilities.format(controlModel, sText);
			if (null != sExistingText && sExistingText.equals(sText))
				return;
			AppearanceModel appearanceModel = AppearanceManager.getInstance().getAppearance(controlModel.sSystemDbName, 
					controlModel.iAppearanceId);
			if (null != appearanceModel && appearanceModel.bFontStyleStrikethrough)
				setPaintFlags(getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			if (bFromHtml) {
				super.setText(Html.fromHtml(sText));
				super.setMovementMethod(LinkMovementMethod.getInstance());
			}
			else if (null != appearanceModel && appearanceModel.bFontStyleUnderline) {
				SpannableString content = new SpannableString(sText);
				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				super.setText(content);			
			} else
				super.setText(sText);
		}
	}
}

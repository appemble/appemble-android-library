/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class VIDEOVIEW extends VideoView implements ControlInterface {
	ControlModel controlModel;
	String sVideoSource;
	private MediaController mMediaController = null;
	boolean bAutoStart = true, bAutoResume = true, bAutoPause = true, bTouchPaused = false;
	PhoneStateListener phoneStateListener = null;
	int iPausedPositionByPhoneCall = -1, iPausedPosition = -1;
	
	public VIDEOVIEW(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		getExtendedParameters();
		
		phoneStateListener = new PhoneStateListener() {
		    @Override
		    public void onCallStateChanged(int state, String incomingNumber) {
		        if (state == TelephonyManager.CALL_STATE_RINGING) {
		            //Incoming call: Pause music
		    		if (isPlaying()) {
		    			VIDEOVIEW.this.pause(); 
		    			iPausedPositionByPhoneCall = getCurrentPosition();
		    		}
		        } else if(state == TelephonyManager.CALL_STATE_IDLE) {
		            //Not in call: Play music
		    		if (false == isPlaying() && iPausedPositionByPhoneCall >= 0) {
		    			if (VIDEOVIEW.this.getCurrentPosition() != iPausedPositionByPhoneCall)
		    				VIDEOVIEW.this.seekTo(iPausedPositionByPhoneCall); 
		    			iPausedPositionByPhoneCall = -1; 
		    			VIDEOVIEW.this.start();
//		    			if (null != mMediaController)
//		    				mMediaController.show(8000);
		    		}
		        } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
		            //A call is dialing, active or on hold
		        }
		        super.onCallStateChanged(state, incomingNumber);
		    }
		};

		// start listening to the telephone state.
		TelephonyManager mgr = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
		if(mgr != null) {
		    mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
		}
		
		if (controlModel.bActionYN) {
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
				}
			});
		}
//		Do not set the background to the video view otherwise the screen remains blank. 		
//		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
//		if (null != controlModel.sDefaultValue)
//			setValue(controlModel.sDefaultValue);
		return Boolean.valueOf(true);
	}

	public void onResume(Bundle targetParameterValueList) {
		if (sVideoSource != null && bAutoResume && false == isPlaying() && bTouchPaused == false) {
			if (iPausedPosition > 0 && getCurrentPosition() != iPausedPosition)
				seekTo(iPausedPosition); 
			iPausedPosition = -1; 
			VIDEOVIEW.this.start();
		}
		if (sVideoSource != null && mMediaController.isShowing() == false)
			mMediaController.show(2000);
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_RESUME_SCREEN); 
		return;
	}

	public void onPause(Bundle targetParameterValueList) {
		if (sVideoSource != null && bAutoPause && isPlaying()) {
			pause(); 
			iPausedPosition = getCurrentPosition();
			if (mMediaController.isShowing())
				mMediaController.hide();
		}
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_PAUSE_SCREEN); 
		return;
	}

	public void onDestroy(Bundle targetParameterValueList) {
		if (isPlaying())
			stopPlayback();
		// stop listening to the telephone manager.
		TelephonyManager mgr = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
		if (mgr != null && null != phoneStateListener) 
		    mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);		
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_DESTROY_SCREEN); 
		return;
	}

    @Override
	public boolean onTouchEvent (MotionEvent ev){	
    	if(ev.getAction() == MotionEvent.ACTION_DOWN && sVideoSource != null) {
    		if(isPlaying()) {
    			pause(); bTouchPaused = true; 
    			iPausedPosition = getCurrentPosition();
    		} else if (bTouchPaused) {
    			if (iPausedPosition > -1 && getCurrentPosition() != iPausedPosition)
    				seekTo(iPausedPosition);
    			iPausedPosition = -1; bTouchPaused = false; 
    			VIDEOVIEW.this.start();
    		}
			if (null != mMediaController && false == mMediaController.isShowing())
				mMediaController.show(5000);
    		return false;
    	} else
    		return false;
  	}
	   
	public int getControlId() {
		return controlModel.id;
	}
	
	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return sVideoSource;
	}

	public void setValue(Object object) {
		if (object == null) { // clear the control
			if (null != sVideoSource) {
				if (isPlaying())
					this.stopPlayback();
				if (Utilities.isUrl(sVideoSource))
					setVideoURI(null);
				else
					setVideoPath(null);
				sVideoSource = null;
			}
			return;
		}
		String sSource = (String) object;
		if (null != sVideoSource && sVideoSource.equals(sSource)) 
			return;// do not set the source if it is the same and it playing.
		if (Utilities.isUrl(sSource)) {
			try {
				Uri uri = Uri.parse(sSource);
				if (null != uri)
					setVideoURI(uri);
			} catch (Exception e) {
				LogManager.logError(e, "Unable to play video. Please check the URI.");
				return;
			}
		} else if (sSource.startsWith("raw:")) {// get it from the raw folder
			Uri uri = Cache.bootstrap.getRawUri(sSource.substring(4));
			if (null == uri)
				return;
			setVideoURI(uri);
		} else if (sSource.charAt(0) != File.separatorChar) { // get it from assets folder
			// for now copying the file. This is not the right solution
			// the right solution is to some how use MEdiaPlayer directly to play the file.
			// see http://stackoverflow.com/questions/8269353/how-to-load-videos-from-assets-folder-to-play-them-with-videoview
			try {
				InputStream inputStream = Cache.context.getAssets().open(sSource);
				String sVideoPath = Utilities.getFilesDir(Environment.DIRECTORY_PICTURES) + File.separatorChar + sSource;
				if (Utilities.copyFile(inputStream, sVideoPath, false))
					setVideoPath(sVideoPath);
				inputStream.close();
			} catch (IOException ioe) {
				LogManager.logError(ioe, "Unable to find file in asset folder" + sSource);
				return;
			}
		} else
			setVideoPath(sSource); // absolute path.
		createMediaController();
		setKeepScreenOn(true);
		sVideoSource = sSource;
		this.requestFocus();
		if (bAutoStart)
			this.start();
	}
    
    private void getExtendedParameters() {
		String sParam = controlModel.mExtendedProperties.get("auto_start");
		if (null != sParam)
			bAutoStart = Utilities.parseBoolean(sParam);
		sParam = controlModel.mExtendedProperties.get("auto_resume");
		if (null != sParam)
			bAutoResume = Utilities.parseBoolean(sParam);
		sParam = controlModel.mExtendedProperties.get("auto_pause");
		if (null != sParam)
			bAutoPause = Utilities.parseBoolean(sParam);
    }
    
    private MediaController createMediaController() {
    	if (null != mMediaController)
    		return mMediaController;
    				
//			mMediaController = new MediaController(this.getContext());
		mMediaController = new MediaController(this.getContext()){
//			    @Override
//			    public void hide() {
//			        this.show(0);
//			    }

		    @Override
		    public void setMediaPlayer(MediaPlayerControl player) {
		        super.setMediaPlayer(player);
		        this.show();
		    }
		};
		mMediaController.setAnchorView(this);
//		mMediaController.setMediaPlayer(this);
		this.setMediaController(mMediaController);
		return mMediaController;
    }
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.MediaController;
import android.widget.MediaController.MediaPlayerControl;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class AUDIOVIEW extends CONTROL_GROUP implements ControlInterface, MediaPlayerControl {
	String sAudioSource;
	private MediaController mMediaController = null;
	private MediaPlayer mMediaPlayer = null;
	boolean bAutoStart = true, bAutoResume = true, bAutoPause = true, bTouchPaused = false;
	private Handler mHandler;
	int iPausedPositionByPhoneCall = -1, iPausedPosition = -1;
	PhoneStateListener phoneStateListener = null;
	
	public AUDIOVIEW(final Context context, final ControlModel controlObject) {
		super(context, controlObject);
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		boolean bReturn = true;
		Object o = super.initialize(parentView, fParentWidth, fParentHeight, vChildControls, screenModel);
		if (o instanceof Boolean)
			bReturn = (Boolean) o;
		if (false == bReturn)
			return Boolean.valueOf(false);
		
		getExtendedParameters();
		mHandler = new Handler();
		
		phoneStateListener = new PhoneStateListener() {
		    @Override
		    public void onCallStateChanged(int state, String incomingNumber) {
		        if (state == TelephonyManager.CALL_STATE_RINGING) {
		            //Incoming call: Pause music
		    		if (mMediaPlayer != null && isPlaying()) {
		    			AUDIOVIEW.this.pause();
		    			iPausedPositionByPhoneCall = getCurrentPosition();
		    		}
		        } else if(state == TelephonyManager.CALL_STATE_IDLE) {
		            //Not in call: Play music
		    		if (mMediaPlayer != null && false == isPlaying() && iPausedPositionByPhoneCall >= 0) {
		    			if (AUDIOVIEW.this.getCurrentPosition() != iPausedPositionByPhoneCall)
		    				AUDIOVIEW.this.seekTo(iPausedPositionByPhoneCall); 
		    			iPausedPositionByPhoneCall = -1; 
		    			mMediaPlayer.start();
//		    			mMediaController.show(8000);
		    		}
		        } else if(state == TelephonyManager.CALL_STATE_OFFHOOK) {
		            //A call is dialing, active or on hold
		        }
		        super.onCallStateChanged(state, incomingNumber);
		    }
		};
		
		if (controlModel.bActionYN) {
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
				}
			});
		}
		return Boolean.valueOf(true);
	}

	public void onResume(Bundle targetParameterValueList) {
		if (sAudioSource != null && mMediaPlayer != null && false == isPlaying() && bAutoResume && 
				false == bTouchPaused) {
			if (iPausedPosition > 0 && getCurrentPosition() != iPausedPosition)
				seekTo(iPausedPosition); 
			iPausedPosition = -1; 
			mMediaPlayer.start();
		}
		if (sAudioSource != null && mMediaController != null && mMediaController.isShowing() == false)
			mMediaController.show(2000);
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_RESUME_SCREEN); 
		return;
	}

	public void onPause(Bundle targetParameterValueList) {
		if (sAudioSource != null && mMediaPlayer != null && isPlaying() && bAutoPause) {
			pause();
			iPausedPosition = getCurrentPosition();
			if (null != mMediaController && mMediaController.isShowing())
				mMediaController.hide();
		}
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_PAUSE_SCREEN); 
		return;
	}

	public void onDestroy(Bundle targetParameterValueList) {
		if (null != mMediaController)
			mMediaController.hide();
		if (null != mMediaPlayer) {
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		// stop listening to the telephone manager.
		TelephonyManager mgr = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
		if (mgr != null && null != phoneStateListener) 
		    mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);		
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_DESTROY_SCREEN); 
		return;
	}

    @Override
	public boolean onTouchEvent (MotionEvent ev){	
    	if(ev.getAction() == MotionEvent.ACTION_DOWN && sAudioSource != null) {
    		if(isPlaying()) {
    			pause(); bTouchPaused = true; 
    			iPausedPosition = getCurrentPosition();
    		} else if (bTouchPaused) {
    			if (iPausedPosition > -1 && getCurrentPosition() != iPausedPosition)
    				seekTo(iPausedPosition);
    			iPausedPosition = -1; bTouchPaused = false; 
    			AUDIOVIEW.this.start();
    		}
//			if (null != mMediaController && false == mMediaController.isShowing())
//				mMediaController.show(3000);
    		return false;
    	} else
    		return false;
  	}

	public int getControlId() {
		return controlModel.id;
	}
	
	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return sAudioSource;
	}

	public void setValue(Object object) {

//		if (null == mMediaController) {
//			mMediaController = new MediaController(this.getContext());
//			mMediaController.setAnchorView(this);
//			mMediaController.setMediaPlayer(this);
//		}
		
		if (object == null) { // clear the control
			if (null != sAudioSource && null != mMediaPlayer) {
				if (isPlaying())
					mMediaPlayer.stop();
				mMediaPlayer.release();
				mMediaPlayer = null;
				sAudioSource = null;
			}
			return;
		}
		
		String sText = (String) object;
		if (null != sAudioSource && sAudioSource.equals(sText))
			return;// do not set the source if it is the same and it playing.
		try {
			Uri uri = null;
			if (Utilities.isUrl(sText))
				uri = Uri.parse(sText);
			if (null == mMediaPlayer)
				mMediaPlayer = createMediaPlayer();
			if (null != uri)
				mMediaPlayer.setDataSource(this.getContext(), uri);
			else if (sText.startsWith("raw:")) {// get it from the raw folder
				uri = Cache.bootstrap.getRawUri(sText.substring(4));
				if (null == uri)
					return;
				mMediaPlayer.setDataSource(this.getContext(), uri);
			} else if (sText.charAt(0) != File.pathSeparatorChar) { // get it from assets folder
				AssetFileDescriptor afd = Cache.context.getAssets().openFd(sText);
				mMediaPlayer.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(), afd.getLength());
//				mMediaPlayer.setDataSource(this.getContext(), Uri.parse("file:///android_asset/" + sText));
			}
			else
				mMediaPlayer.setDataSource(sText); // absolute path.
			mMediaPlayer.prepare();
		} catch (IOException e) {
			LogManager.logError(e, "Unable to play audio. Please check the source.");
			if (null != mMediaPlayer)
				mMediaPlayer.release();
			mMediaPlayer = null;
			return;
		}
		setKeepScreenOn(true);
		sAudioSource = sText;
		this.requestFocus();
		if (bAutoStart)
			this.start();
	}

	@Override
	public boolean canPause() {
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		return false;
	}

	@Override
	public boolean canSeekForward() {
		return false;
	}

	@Override
	public int getBufferPercentage() {
		if (null == mMediaPlayer)
			return 0;
	    int percentage = (mMediaPlayer.getCurrentPosition() * 100) / mMediaPlayer.getDuration();
        return percentage;	
    }

	@Override
	public int getCurrentPosition() {
		if (null != mMediaPlayer)
			return mMediaPlayer.getCurrentPosition();
		return 0;
	}

	@Override
	public int getDuration() {
		if (null != mMediaPlayer)
			return mMediaPlayer.getDuration();
		return 0;
	}

	@Override
	public boolean isPlaying() {
		if (null != mMediaPlayer)
			return mMediaPlayer.isPlaying();
		return false;
	}

	@Override
	public void pause() {
		if (null != mMediaPlayer && mMediaPlayer.isPlaying())
			mMediaPlayer.pause();
	}

	@Override
	public void seekTo(int pos) {
		if (null != mMediaPlayer)
			mMediaPlayer.seekTo(pos);
	}

	@Override
	public void start() {
		if (null != mMediaPlayer)
			mMediaPlayer.start();
	}

    private void getExtendedParameters() {
		String sParam = controlModel.mExtendedProperties.get("auto_start");
		if (null != sParam)
			bAutoStart = Utilities.parseBoolean(sParam);
		sParam = controlModel.mExtendedProperties.get("auto_resume");
		if (null != sParam)
			bAutoResume = Utilities.parseBoolean(sParam);
		sParam = controlModel.mExtendedProperties.get("auto_pause");
		if (null != sParam)
			bAutoPause = Utilities.parseBoolean(sParam);
    }

    private MediaPlayer createMediaPlayer() {
        mMediaController = createMediaController();
		MediaPlayer mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mHandler.post(new Runnable() {
                    public void run() {
//                    	if (null != mMediaController && mMediaController.isShowing() == false && false == mMediaController.isShown())
//                    		mMediaController.show(3000); // 8 sec when the audio source is set...
                        if (bAutoStart)
                        	start();
                    }
                });
            }
        });
//		if (null == mMediaController) {
//			mMediaController = new MediaController(this.getContext());
//			mMediaController.setAnchorView(this);
//			mMediaController.setMediaPlayer(this);
//		}
		return mediaPlayer;
    }

    //KINDLE FIRE FIX (onControllerHide) Message
    //The Kindle fire throws this message when it hides the controller
    //This custom message is not a part of the official Android distribution
    public void onControllerHide() {
        return;
    }
    
    private MediaController createMediaController() {    				
		MediaController mediaController = new MediaController(this.getContext()) {
		    @Override
		    public void setMediaPlayer(MediaPlayerControl player) {
		        super.setMediaPlayer(player);
//		        this.show();
		    }
		};
		mediaController.setAnchorView(this);
		mediaController.setMediaPlayer(this);
		return mediaController;
    }

	@Override
	public int getAudioSessionId() {
		// TODO Auto-generated method stub
		return 0;
	}

}

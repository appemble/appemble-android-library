/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.database.Cursor;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dataconnector.ListCursorAdaptor;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class ARRANGEDLIST extends ListView implements ControlInterface {	
	ControlModel controlModel;
	public Vector<ControlModel> vControls;
	ScreenModel screenModel;
	boolean bOnMeasure = false;
	float fWidth, fHeight;
	
	public ARRANGEDLIST(final Context context, ControlModel controlObject) {
		super(context);
		controlModel=controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel scrModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		// TODO HB this should be rawQuery.
		vControls = vChildControls;
		screenModel = scrModel;
		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fWidth = size.x; fHeight = size.y;
		} else {
			fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
		}
		this.setCacheColorHint(0);
        this.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> listItemView, View listView, int position, long id) {
				Action.callAction(getContext(), listView, listItemView, controlModel, Constants.TAP); 
			}
        });
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, controlModel.sBackgroundImage);
//		if (controlModel.sBackgroundImage != null)
//			ImageManager.getInstance().UpdateView(this, controlModel.sBackgroundImage, true);
		return Boolean.valueOf(true);
	}
	
	public void onDestroy(Bundle targetParameterValueList) {
        //No need to close cursor. Android framework is closing the cursor.
		ListCursorAdaptor adapter = (ListCursorAdaptor)getAdapter();
		if (null != adapter) {
			Cursor c = adapter.getCursor();
			if (c != null && false == c.isClosed())
				c.close();
		}			
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_DESTROY_SCREEN); 
		return;
	}

	public int getControlId() {
		return controlModel.id;
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setControlAppearance() {
	}

	public String getValue() {
		return null;
	}
	
	public void setValue(Object object) {
	    if (bOnMeasure)
	    	return;
		
		// If the default value is set, it is for setting the list box background. 
		// If called from DynamicActivity::UpdateControl, then
		// this value is obtained in the following order remote data source, local data source, remote data, 
		// local cursor, target parameter list and default value.
		if (null == object)
			return;
		if (object instanceof String) {
			String sText = (String) object;
			if (sText.startsWith(Constants.URL_IMAGE_PREFIX)) {
				String sAttachedImage = (String) getTag();
				if (null != sAttachedImage && sAttachedImage.equals(sText))
					return;
				if (null != AppearanceManager.getInstance().updateBackgroundAppearance(this, 
						controlModel, null, sText))
					setTag(sText);
			}
		}
		else if (object instanceof Cursor) {
			setCursor((Cursor) object);
		}
	}

	public boolean setCursor(Cursor c) {
		// c can be null as well
		ListCursorAdaptor adapter = (ListCursorAdaptor)getAdapter();
		if (null == adapter) { // the constructor does not set the adapter.
			adapter = new ListCursorAdaptor(getContext(), this, c, controlModel, vControls, fWidth, fHeight,
					screenModel);
	        setAdapter(adapter); 
		} else {
			adapter.changeCursor(c);
			adapter.notifyDataSetChanged();
		}
		return true;
	}
}
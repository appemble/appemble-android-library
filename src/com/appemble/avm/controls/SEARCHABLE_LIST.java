/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.database.Cursor;
import android.graphics.PointF;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.TableDao;

public class SEARCHABLE_LIST extends DynamicLayout implements ControlInterface {
	public static String SEARCH_CRITERIA = "search_criteria";
	public static String BEGINS_WITH = "begins_with";
	public static String INCLUDES = "includes";
	ControlModel controlModel;
	private LIST list;
	private EDIT edit;
	ControlModel listModel;
	String sDeparameterizedLocalDataSource = null; // stores the listModel.sDeparmeterizedLocalDataSource available at the time of creating the control
	
	public SEARCHABLE_LIST(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;
	}
	
	public Object initialize(View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		float fWidth, fHeight;
		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fWidth = size.x; fHeight = size.y;
		} else {
			fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
		}
		if (false == generateLayout(this, controlModel.id, fWidth, fHeight, screenModel, vChildControls)) {
			LogManager.logWTF("Unable to generate a layout for SEARCHABLE_LIST: " + controlModel.sName);
			return Boolean.valueOf(false); 
		}
		int iControlType = 0;
		for (ControlModel controlModel: vChildControls) {
			iControlType = controlModel.getType();
			if (iControlType == ConfigManager.getInstance().getControlId("EDIT"))
				edit = (EDIT)findViewById(controlModel.id);
			else if (iControlType == ConfigManager.getInstance().getControlId("LIST")) {
				list = (LIST)findViewById(controlModel.id);
				listModel = controlModel;
			}
		}
		if (null == edit || null == list | null == listModel) {
			LogManager.logError("The control SEARCHABLE LIST: " + controlModel.sName + " needs to have an edit box and a list box.");
			return Boolean.valueOf(false);
		}
		// See more for info here. http://www.androidpeople.com/android-listview-searchbox-sort-items
		edit.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (null == edit || null == listModel)
					return;
				String sSearchText = edit.getText().toString();
				if (null == sSearchText || sSearchText.length() == 0)
					sSearchText = "";
				else
					sSearchText = sSearchText.replaceAll("'", "''");
				String sCurrentDataSource = null;
				if (null != sDeparameterizedLocalDataSource) {
					String sSearchCriteria = null;
					if (controlModel != null)
						sSearchCriteria = controlModel.mExtendedProperties != null ? 
							controlModel.mExtendedProperties.get(SEARCH_CRITERIA) : null;
					if (sSearchCriteria != null && sSearchCriteria.equalsIgnoreCase(INCLUDES))
						sSearchText = "%" + sSearchText;
					sCurrentDataSource = sDeparameterizedLocalDataSource.replaceAll("%SEARCH%", sSearchText + "%");
				}
				if (sCurrentDataSource != null) {
//					SQLiteDatabase db = DbConnectionManager.getInstance().getDb(controlModel.sContentDbName);
					Cursor cursor = TableDao.createCursor(controlModel.sContentDbName, sCurrentDataSource, null);
					list.setCursor(cursor);
				}
			}
		});		
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		return Boolean.valueOf(true);
	}

	public void onResume(Bundle targetParameterValueList) {
		if (null == sDeparameterizedLocalDataSource) {// if onResume is called for the first time...
			AppembleActivity appembleActivity = Utilities.getActivity(getContext(), this, (View)getParent());
			if (null != appembleActivity)
				sDeparameterizedLocalDataSource = (Utilities.replaceParameterisedStrings(
					listModel.sLocalDataSource, appembleActivity.getTargetParameterValueList(), 
					listModel.sContentDbName, Constants.SQL));
		}
		String sSearchCriteria = null;
		String sSearchText = "";
		if (null != edit)
			sSearchText = edit.getText().toString();
		if (null != sSearchText)
			sSearchText = sSearchText.replaceAll("'", "''");
		if (controlModel != null)
			sSearchCriteria = controlModel.mExtendedProperties != null ? 
				controlModel.mExtendedProperties.get(SEARCH_CRITERIA) : null;
		if (sSearchCriteria != null && sSearchCriteria.equalsIgnoreCase(INCLUDES))
			sSearchText = "%" + sSearchText;
		listModel.sDeparameterizedLocalDataSource = sDeparameterizedLocalDataSource.replaceAll("%SEARCH%", sSearchText + "%");
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_RESUME_SCREEN); 
		return;
	}

	public int getControlId() {
		return controlModel.id;
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return null;
	}
	
	public void setValue(Object object) {
	}
}
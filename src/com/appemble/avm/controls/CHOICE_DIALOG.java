/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.view.View;

import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.TableDao;

public class CHOICE_DIALOG extends PUSHBUTTON implements ControlInterface {
	private String[] aCodes = null;
	private String[] aDisplayItems = null;
	private String sTitle = null;
	private boolean[] aSelectedItems;
	int iMode = -1;
	float fWidth, fHeight;
	
	public CHOICE_DIALOG(final Context context, final ControlModel controlObject) {
		super(context, controlObject);
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fParentWidth = size.x; fParentHeight = size.y;
		} else {
			fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
		}
		
		super.initialize(parentView, fParentWidth, fParentHeight, vChildControls, screenModel);
		Context screenContext = this.getContext();
		final AppembleActivity appembleActivity = (AppembleActivity) screenContext;
		if (!(appembleActivity instanceof AppembleActivity)) {
			LogManager.logWTF(Constants.MISSING_ACTIVITY);
			return Boolean.valueOf(false);
		}
		setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String sSelectedText = getValue();
				String sDisplayProperties = null;
				String sStorageValues = null;
				String sValuesQuery = null;		
				if (controlModel.mExtendedProperties != null) {
					sDisplayProperties = controlModel.mExtendedProperties.get(Constants.DISPLAY_VALUES);
					sStorageValues = controlModel.mExtendedProperties.get(Constants.STORAGE_VALUES);
					sValuesQuery = controlModel.mExtendedProperties.get(Constants.VALUES_QUERY);
				}
				if (getItems(sDisplayProperties, sStorageValues, sValuesQuery, sSelectedText) > 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder((Context) appembleActivity);
					builder.setTitle(sTitle);
					if (iMode == ConfigManager.getInstance().getControlId("SINGLE_CHOICE_DIALOG"))
						builder.setSingleChoiceItems(aDisplayItems, getFirstSelectedItem(aSelectedItems), singleChoiceListener); // attach the single choice listener
					else
						builder.setMultiChoiceItems(aDisplayItems, aSelectedItems, multiChoiceListener); // attach the single choice listener
					builder.setPositiveButton(android.R.string.ok, okListener) // attach the OK listener
						   .setNegativeButton(android.R.string.cancel, cancelListener) // attach the cancel listener
						   .create() // create the dialog box
						   .show();  // show the dialog box.
				} else {
					Action.callAction(getContext(), v, parentView, controlModel, LIST.ON_EMPTY_LIST); 
				}
			}
		});
		return Boolean.valueOf(true);
	}
	private int getItems(String sDisplayValues, String sStorageValues, String sQuery, String sSelectedValues) {
    	sTitle = controlModel.mExtendedProperties.get(Constants.TITLE_STR);
		if (null == sStorageValues && null == sQuery)
			return -1;		

    	String[] sSelectedTexts = Utilities.splitCommaSeparatedString(sSelectedValues);
    	if (Utilities.isQuery(sQuery)) { // execute the query and append all values to vItems.
			String[][] aaItems = TableDao.getRowsFromDbAsStrings(controlModel.sContentDbName, sQuery, null);
			if (null != aaItems) {
				int j = 0;
				if (aaItems[0].length >= 2) { // means both code and display value has been retrieved
					aCodes = new String[aaItems.length]; aDisplayItems = new String[aaItems.length];
					for (String[] value : aaItems) {
						aCodes[j] = value[0]; 
						aDisplayItems[j] = value[1]; // index 1 is display item.
					    if (null != sSelectedTexts && null != aCodes[j]) {
					    	for (String sSelectedText : sSelectedTexts) {
						    	if (aCodes[j].equals(sSelectedText))
						    		aSelectedItems[j] = true;
					    	}
					    }
					    j++;						
					}
					return aDisplayItems.length;
				}
				if (aaItems[0].length == 1) { // means only the codes has been retrieved. The display items may be in misc2 field
					aCodes = new String[aaItems.length];
					for (String[] value : aaItems)
						aCodes[j++] = value[0];
				}
			}
    	}
    	String[] sTokens = Utilities.splitCommaSeparatedString(sStorageValues);
    	int i = 0;
		String sToken = null;
    	if (sTokens.length == 0)
    		return sTokens.length;
    	// for each token, 
    		// if query, run the query append the results into aCodes (and possible aDisplayItems)
    		// else append the token into aCodes
		for (i = 1; i < sTokens.length; i++) { 
			sToken = sTokens[i].trim();
        	if (sToken.charAt(0) == '\"') {
				int iLength = sToken.length();
				if (sToken.charAt(iLength - 1) == '\"')
					sTokens[i] = sToken.substring(1, iLength - 1).trim();
				else
					sTokens[i] = sToken.substring(1).trim();
        	}

			if (null == aCodes)
				aCodes = new String[sTokens.length-1];
			aCodes[i-1] = (sTokens[i] != null) ? sTokens[i].trim() : null;
		}
		// Get the display items from Misc2 (or Source2)
		if (null != sDisplayValues && sDisplayValues.length() > 0) {
	    	sTokens = Utilities.splitCommaSeparatedString(sDisplayValues);
			for (i = 0; i < sTokens.length; i++) { 
				sToken = sTokens[i].trim();
	        	if (sToken.charAt(0) == '\"') {
					int iLength = sToken.length();
					if (sToken.charAt(iLength - 1) == '\"')
						sTokens[i] = sToken.substring(1, iLength - 1).trim();
					else
						sTokens[i] = sToken.substring(1).trim();
	        	}
				if (null == aDisplayItems)
					aDisplayItems = new String[sTokens.length];
				aDisplayItems[i] = sTokens[i].trim();
			}
		}
		if (null == aCodes || aCodes.length == 0)
			return 0;
		if (null == aDisplayItems)
			aDisplayItems = new String[aCodes.length];
		i = 0;
		for (String code : aCodes) {
			if (aDisplayItems[i] == null) // if Display items are not available in misc2 then they are same as aCodes
				aDisplayItems[i] = aCodes[i];
		    if (null != sSelectedTexts && null != code) {
		    	for (String sSelectedText : sSelectedTexts)
		    		if (code.equals(sSelectedText)) // set the current item selected.
		    			aSelectedItems[i] = true;
		    }
		    i++;
		}
		return aDisplayItems.length;
	}
	
	private DialogInterface.OnClickListener singleChoiceListener = new DialogInterface.OnClickListener() {
	    public void onClick(DialogInterface dialog, int item) {
	        // Toast.makeText(MLCache.context, items[item], Toast.LENGTH_SHORT).show();
	    	aSelectedItems[item] = true;
	    }
	};
		
	private DialogInterface.OnMultiChoiceClickListener multiChoiceListener = new DialogInterface.OnMultiChoiceClickListener() {
	    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
	        // Toast.makeText(MLCache.context, items[item], Toast.LENGTH_SHORT).show();
	    	aSelectedItems[which] = isChecked;
	    }
	};

	/*
	private int getItems(String sSource1, String sSource2, String sDefaultText) {
		if (null == sSource1)
			return -1;

    	String[] sTokens = Utilities.splitCommaSeparatedString(sSource1);
    	String[] sSelectedTexts = Utilities.splitCommaSeparatedString(sDefaultText);
    	int i = 0;
		String sToken = null;
    	if (sTokens.length == 0)
    		return sTokens.length;
    	sTitle = sTokens[0];
    	// for each token, 
    		// if query, run the query append the results into aCodes (and possible aDisplayItems)
    		// else append the token into aCodes
		for (i = 1; i < sTokens.length; i++) { 
			sToken = sTokens[i].trim();
        	if (sToken.charAt(0) == '\"') {
				int iLength = sToken.length();
				if (sToken.charAt(iLength - 1) == '\"')
					sTokens[i] = sToken.substring(1, iLength - 1).trim();
				else
					sTokens[i] = sToken.substring(1).trim();
        	}

        	if (Utilities.isQuery(sTokens[i])) { // execute the query and append all values to vItems.
				String[][] aaItems = TableDao.getRowsFromDbAsStrings(sTokens[i], controlModel.sContentDbName);
				if (null != aaItems) {
					int j = 0;
					if (aaItems[0].length >= 2) { // means both code and display value has been retrieved
						aCodes = new String[aaItems.length]; aDisplayItems = new String[aaItems.length];
						for (String[] value : aaItems) {
							aCodes[j] = value[0]; 
							aDisplayItems[j] = value[1]; // index 1 is display item.
						    if (null != sSelectedTexts && null != aCodes[j]) {
						    	for (String sSelectedText : sSelectedTexts) {
							    	if (aCodes[j].equals(sSelectedText))
							    		aSelectedItems[j] = true;
						    	}
						    }
						    j++;						
						}
						return aDisplayItems.length;
					}
					if (aaItems[0].length == 1) { // means only the codes has been retrieved. The display items may be in misc2 field
						aCodes = new String[aaItems.length];
						for (String[] value : aaItems)
							aCodes[j++] = value[0];
					}
				}
        	}
			else {
				if (null == aCodes)
					aCodes = new String[sTokens.length-1];
				aCodes[i-1] = (sTokens[i] != null) ? sTokens[i].trim() : null;
			}
		}
		// Get the display items from Misc2 (or Source2)
		if (null != sSource2 && sSource2.length() > 0) {
	    	sTokens = Utilities.splitCommaSeparatedString(sSource2);
			for (i = 0; i < sTokens.length; i++) { 
				sToken = sTokens[i].trim();
	        	if (sToken.charAt(0) == '\"') {
					int iLength = sToken.length();
					if (sToken.charAt(iLength - 1) == '\"')
						sTokens[i] = sToken.substring(1, iLength - 1).trim();
					else
						sTokens[i] = sToken.substring(1).trim();
	        	}
				if (Utilities.isQuery(sTokens[i])) // execute the query and append all values to vItems.
					aDisplayItems = TableDao.getValuesFromDb(sTokens[i], controlModel.sContentDbName);
				else {
					if (null == aDisplayItems)
						aDisplayItems = new String[sTokens.length];
					aDisplayItems[i] = sTokens[i].trim();
				}
			}
		}
		if (null == aCodes || aCodes.length == 0)
			return 0;
		if (null == aDisplayItems)
			aDisplayItems = new String[aCodes.length];
		i = 0;
		for (String code : aCodes) {
			if (aDisplayItems[i] == null) // if Display items are not available in misc2 then they are same as aCodes
				aDisplayItems[i] = aCodes[i];
		    if (null != sSelectedTexts && null != code) {
		    	for (String sSelectedText : sSelectedTexts)
		    		if (code.equals(sSelectedText)) // set the current item selected.
		    			aSelectedItems[i] = true;
		    }
		    i++;
		}
		return aDisplayItems.length;
	}
	
	private DialogInterface.OnClickListener singleChoiceListener = new DialogInterface.OnClickListener() {
	    public void onClick(DialogInterface dialog, int item) {
	        // Toast.makeText(MLCache.context, items[item], Toast.LENGTH_SHORT).show();
	    	aSelectedItems[item] = true;
	    }
	};
		
	private DialogInterface.OnMultiChoiceClickListener multiChoiceListener = new DialogInterface.OnMultiChoiceClickListener() {
	    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
	        // Toast.makeText(MLCache.context, items[item], Toast.LENGTH_SHORT).show();
	    	aSelectedItems[which] = isChecked;
	    }
	};
*/
	
	private DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
        	StringBuilder sSelectedText = new StringBuilder();
        	int iIndex = 0; boolean bSomethingSelected = false;
        	for (boolean bSelectedItem : aSelectedItems) {
        		if (bSelectedItem) {
        			bSomethingSelected = true;
        			if (iMode == ConfigManager.getInstance().getControlId("SINGLE_CHOICE_DIALOG"))
                		sSelectedText.append(aCodes[iIndex]);
        			else { // multi choice dialog
	        			if (iIndex > 0)
	        				sSelectedText.append(',');
	        			sSelectedText.append('\"');
	        			sSelectedText.append(aCodes[iIndex]);
	        			sSelectedText.append('\"');
        			}
        		}
        		iIndex++;
        	}
        	if (bSomethingSelected)
        		setValue(sSelectedText.toString());
	        dialog.dismiss();
        }
    };
    
    private DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
	        dialog.dismiss();
        }
    };
    
	public String getValue() {
    	StringBuilder sSelectedText = new StringBuilder();
    	int iIndex = 0; boolean bSomethingSelected = false;
    	for (boolean bSelectedItem : aSelectedItems) {
    		if (bSelectedItem) {
    			bSomethingSelected = true;
    			if (iMode == ConfigManager.getInstance().getControlId("SINGLE_CHOICE_DIALOG"))
            		sSelectedText.append(aCodes[iIndex]);
    			else { // multi choice dialog
        			if (iIndex > 0)
        				sSelectedText.append(',');
        			sSelectedText.append('\"');
        			sSelectedText.append(aCodes[iIndex]);
        			sSelectedText.append('\"');
    			}
    		}
    		iIndex++;
    	}
    	return bSomethingSelected ? sSelectedText.toString() : null;
	}

	public String getDisplayText() {
    	StringBuilder sSelectedText = new StringBuilder();
    	int iIndex = 0; boolean bSomethingSelected = false;
    	for (boolean bSelectedItem : aSelectedItems) {
    		if (bSelectedItem) {
    			bSomethingSelected = true;
    			if (iMode == ConfigManager.getInstance().getControlId("SINGLE_CHOICE_DIALOG"))
            		sSelectedText.append(aDisplayItems[iIndex]);
    			else { // multi choice dialog
        			if (iIndex > 0)
        				sSelectedText.append(',');
        			sSelectedText.append('\"');
        			sSelectedText.append(aDisplayItems[iIndex]);
        			sSelectedText.append('\"');
    			}
    		}
    		iIndex++;
    	}
    	return bSomethingSelected ? sSelectedText.toString() : null;
	}

	public void setValue(String sText) {
		// candidate for removal 2012 04 27
		// Show the default value. Because localDataSource is always present, the appembleActivity will pass
		// the local data source. However, we should show the default value instead.
		if (null != sText && sText.length() > 0) {
			if (false == sText.equals(controlModel.sDefaultValue)) {
				if (null == aDisplayItems || aDisplayItems.length == 0 || 
						getFirstSelectedItem(aSelectedItems) == -1) {
					String sDisplayProperties = null;
					String sStorageValues = null;
					String sValuesQuery = null;		
					if (controlModel.mExtendedProperties != null) {
						sDisplayProperties = controlModel.mExtendedProperties.get(Constants.DISPLAY_VALUES);
						sStorageValues = controlModel.mExtendedProperties.get(Constants.STORAGE_VALUES);
						sValuesQuery = controlModel.mExtendedProperties.get(Constants.VALUES_QUERY);
					}
					if (getItems(sDisplayProperties, sStorageValues, sValuesQuery, sText) <= 0)
						return;
				}
				sText = getDisplayText();
			}
		}
		if (null == sText) // show the default value if present.
			sText = controlModel.sDefaultValue;			
		super.setText(sText);
	}
	
	int getFirstSelectedItem(boolean[] aSelectedItems) {
		int i = 0;
		for (boolean bSelectedItem : aSelectedItems) {
			if (bSelectedItem)
				return i;
			i++;
		}
		return -1; 
	}
	
}
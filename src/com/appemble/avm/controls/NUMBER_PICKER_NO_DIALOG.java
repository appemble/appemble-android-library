package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.graphics.PointF;
import android.os.Handler;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class NUMBER_PICKER_NO_DIALOG extends DynamicLayout implements ControlInterface {

	public static final int ON_DECREMENT = "ON_DECREMENT".hashCode();
	public static final int ON_INCREMENT = "ON_INCREMENT".hashCode();
	
	ControlModel controlModel;
	public Vector<ControlModel> vControls;
	float fWidth, fHeight; // Using which the child control dimensions are to be calculated.

	private final long REPEAT_DELAY = 50;
	
//	private final int ELEMENT_HEIGHT = 60;
//	private final int ELEMENT_WIDTH = ELEMENT_HEIGHT; // you're all squares, yo
	
	private int mMinimum = 0;
	private int mMaximum = 999;
	private int mIncrement = 1;
	private int mDefault = 0;
	private boolean mCircleThrough = false;
	
//	private final int TEXT_SIZE = 30;
	
	public Integer value;
	
	PUSHBUTTON decrement, increment;
	public EDIT valueText;
	
	private Handler repeatUpdateHandler = new Handler();
	
	private boolean autoIncrement = false;
	private boolean autoDecrement = false;

	class RepetetiveUpdater implements Runnable {
		public void run() {
			if( autoIncrement ){
				increment();
				repeatUpdateHandler.postDelayed( new RepetetiveUpdater(), REPEAT_DELAY );
			} else if( autoDecrement ){
				decrement();
				repeatUpdateHandler.postDelayed( new RepetetiveUpdater(), REPEAT_DELAY );
			}
		}
	}
	
	public NUMBER_PICKER_NO_DIALOG(Context context, ControlModel controlModel) {
		super(context);
		this.controlModel = controlModel;
	}
	
	public NUMBER_PICKER_NO_DIALOG(Context context, AttributeSet attributeSet, ControlModel controlModel) {
		super(context);
		this.controlModel = controlModel;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel scrModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		vControls = vChildControls;
		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fWidth = size.x; fHeight = size.y;
		} else {
			fWidth = scrModel.fWidthInPixels; fHeight = scrModel.fHeightInPixels;
		}
		
		if (false == generateLayout(this, controlModel.id, fWidth, fHeight, scrModel, vChildControls)) {
			LogManager.logWTF("Unable to generate a layout for NUMBER_PICKER_NO_DIALOG: " + controlModel.sName);
			return Boolean.valueOf(false); 
		}
		String sIncrementControl = controlModel.mExtendedProperties != null ? 
			controlModel.mExtendedProperties.get("increment_control_name") : null;
		String sDecrementControl = controlModel.mExtendedProperties != null ? 
			controlModel.mExtendedProperties.get("decrement_control_name") : null;
		for (ControlModel controlModel: vChildControls) {
			if (null != sIncrementControl && sIncrementControl.equals(controlModel.sName))
				increment = (PUSHBUTTON)findViewById(controlModel.id);
			else if (null != sDecrementControl && sDecrementControl.equals(controlModel.sName))
				decrement = (PUSHBUTTON)findViewById(controlModel.id);
			else if (controlModel.iType == ConfigManager.getInstance().getControlId("EDIT"))
				valueText = (EDIT)findViewById(controlModel.id);
		}
		if (null == valueText|| null == increment | null == decrement) {
			LogManager.logError("The control NUMBER_PICKER_NO_DIALOG: " + controlModel.sName + " needs to have an edit box and two push buttons.");
			return Boolean.valueOf(false);
		}
		String sProperty = controlModel.mExtendedProperties != null ? 
				controlModel.mExtendedProperties.get("min") : null;
		if (null != sProperty)
			mMinimum = Integer.parseInt(sProperty);
		sProperty = controlModel.mExtendedProperties != null ? 
				controlModel.mExtendedProperties.get("max") : null;
		if (null != sProperty)
			mMaximum = Integer.parseInt(sProperty);
		sProperty = controlModel.mExtendedProperties != null ? 
				controlModel.mExtendedProperties.get("inc") : null;
		if (null != sProperty)
			mIncrement = Integer.parseInt(sProperty);
		if (null != controlModel.sDefaultValue)
			mDefault = Integer.parseInt(controlModel.sDefaultValue);
		mCircleThrough = Utilities.parseBoolean(controlModel.mExtendedProperties.get("circle_through"));
		initIncrementButton(this.getContext());
		initValueEditText(this.getContext());
		initDecrementButton(this.getContext());
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, 
				controlModel.sBackgroundImage);
		
		AppembleActivityImpl.addEmptyControlIds(controlModel, scrModel, null);
		return Boolean.valueOf(true);
	}

	private void initIncrementButton( Context context){
		if (null == increment || false == increment instanceof PUSHBUTTON)
			return;
		increment.setValue( "+" );
		// Increment once for a click
		increment.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	increment();
				Action.callAction(getContext(), NUMBER_PICKER_NO_DIALOG.this, 
					(View)NUMBER_PICKER_NO_DIALOG.this.getParent(), controlModel, 
					NUMBER_PICKER_NO_DIALOG.ON_INCREMENT);
            }
        });
		
		// Auto increment for a long click
		increment.setOnLongClickListener( 
				new View.OnLongClickListener(){
					public boolean onLongClick(View arg0) {
						autoIncrement = true;
						repeatUpdateHandler.post( new RepetetiveUpdater() );
						return false;
					}
				}
		);
		
		// When the button is released, if we're auto incrementing, stop
		increment.setOnTouchListener( new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if( event.getAction() == MotionEvent.ACTION_UP && autoIncrement ){
					autoIncrement = false;
					Action.callAction(getContext(), NUMBER_PICKER_NO_DIALOG.this, 
						(View)NUMBER_PICKER_NO_DIALOG.this.getParent(), 
						controlModel, NUMBER_PICKER_NO_DIALOG.ON_INCREMENT);
				}
				return false;
			}
		});
	}
	
	private void initValueEditText( Context context){
		value = Integer.valueOf(mDefault);
		
		valueText.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int arg1, KeyEvent event) {
				int backupValue = value;
				try {
					value = Integer.parseInt( ((EditText)v).getText().toString() );
				} catch( NumberFormatException nfe ){
					value = backupValue;
				}
				return false;
			}
		});
		
		// Highlight the number when we get focus
		valueText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if( hasFocus ){
					((EditText)v).selectAll();
				}
			}
		});
		valueText.setGravity( Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL );
		if (null != value)
			valueText.setText( value.toString() );
		valueText.setInputType( InputType.TYPE_CLASS_NUMBER );
		valueText.clearFocus();
	}
	
	private void initDecrementButton( Context context){
		if (null == decrement || false == decrement instanceof PUSHBUTTON)
			return;
		decrement.setValue( "-" );
		// Decrement once for a click
		decrement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	decrement();
				Action.callAction(getContext(), NUMBER_PICKER_NO_DIALOG.this, 
					(View)NUMBER_PICKER_NO_DIALOG.this.getParent(), controlModel, 
					NUMBER_PICKER_NO_DIALOG.ON_DECREMENT);
            }
        });

		// Auto Decrement for a long click
		decrement.setOnLongClickListener( 
				new View.OnLongClickListener(){
					public boolean onLongClick(View arg0) {
						autoDecrement = true;
						repeatUpdateHandler.post( new RepetetiveUpdater() );
						return false;
					}
				}
		);
		
		// When the button is released, if we're auto decrementing, stop
		decrement.setOnTouchListener( new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				if( event.getAction() == MotionEvent.ACTION_UP && autoDecrement ){
					autoDecrement = false;
					Action.callAction(getContext(), NUMBER_PICKER_NO_DIALOG.this, 
						(View)NUMBER_PICKER_NO_DIALOG.this.getParent(), controlModel, 
						NUMBER_PICKER_NO_DIALOG.ON_DECREMENT);
				}
				return false;
			}
		});
	}
	
	public void increment(){
		if(value < mMaximum)
			value = value + mIncrement;
		else if (mCircleThrough && value == mMaximum)
			value = mMinimum;
		else if (value > mMaximum)
			value = mMaximum;
		valueText.setText( value.toString() );
			
	}

	public void decrement(){
		if(value > mMinimum)
			value = value - mIncrement;
		else if (mCircleThrough && value == mMinimum)
			value = mMaximum;
		else if (value < mMinimum)
			value = mMinimum;
		valueText.setText( value.toString() );
	}
	
	public int getControlId() {
		return getId();
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public Object getValue(){
		return value;
	}
	
	public void setValue(Object v){
		if (null == v) {
			valueText.setValue(v);
			return;
		}
		int value = 0;
		if (v instanceof String)
			value = Integer.parseInt((String)v);
		else if (v instanceof Integer)
			value = (Integer)v;
		if( value > mMaximum) value = mMaximum;
		else if( value < mMinimum) value = mMinimum;
		this.value = value;
		valueText.setText( this.value.toString() );
	}
	
}

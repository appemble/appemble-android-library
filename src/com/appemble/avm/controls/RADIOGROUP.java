/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class RADIOGROUP extends RadioGroup implements ControlInterface {
	protected ControlModel controlModel;
	
	public RADIOGROUP(Context context, ControlModel controlObject) {
		super(context);
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {		
		return initialize(parentView, fParentWidth, fParentHeight, vChildControls, 
				screenModel, RADIOBUTTON.class);
	}

    public int getControlId() {
		return controlModel.id;
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setControlAppearance() {
	}

	public String getValue() {
		int checkedId=this.getCheckedRadioButtonId();
		int iCount = this.getChildCount();
		for(int i = 0; i < iCount; i++) {
			View view = this.getChildAt(i);
            if(view.getId() == checkedId && view instanceof RADIOBUTTON) {
	            RADIOBUTTON btn = (RADIOBUTTON) view;
                return btn.sStorageValue;
			}
        }
		return null;
	}

	public void setValue(Object object) {

		int iCount = getChildCount();
		if (object == null) {
			for(int i = 0; i < iCount; i++) {
	            RADIOBUTTON radioButtonView = (RADIOBUTTON) this.getChildAt(i);
	            radioButtonView.setChecked(false);
			}
			return;
		}
		if (!(object instanceof String))
			return;
		String sText = (String) object;
		for(int i = 0; i < iCount; i++) {
			View view = this.getChildAt(i);
			if (!(view instanceof RADIOBUTTON)) {
				LogManager.logWTF("RadioGroup: " + controlModel.sName + " can have only RadioButtons as its children");
				return;
			}
            RADIOBUTTON radioButtonView = (RADIOBUTTON) this.getChildAt(i);
            if (radioButtonView.sStorageValue.equals(sText)) {
	            radioButtonView.setChecked(true);
	            return;
            } 
		}
		return ;
	}

	protected void setAppearances(ControlModel parentControlModel, ControlModel childControlModel) {
		if (parentControlModel.iAppearanceId != 0 && 0 == childControlModel.iAppearanceId)
			childControlModel.iAppearanceId = parentControlModel.iAppearanceId; 
		String appearances = parentControlModel.mExtendedProperties.get("appearances");
		childControlModel.mExtendedProperties.put("appearances", appearances);
		return;
	}

	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel, Class<?> className) {
		if (null == controlModel)
	        return Boolean.valueOf(false);
		AppembleActivity appembleActivity = null;
		Context context = getContext();
	    if (context instanceof AppembleActivity)
	        appembleActivity = (AppembleActivity) context;
	    if(appembleActivity != null) 
	        screenModel=appembleActivity.getScreenModel();
	    String sOrientation = controlModel.mExtendedProperties.get("orientation");
	    int iOrientation = RadioGroup.HORIZONTAL;
	    if (null != sOrientation && sOrientation.length() > 0 && sOrientation.equalsIgnoreCase("VERTICAL"))
	      iOrientation = RadioGroup.VERTICAL;
	    setOrientation(iOrientation);
	    Vector <ControlModel> vRadioButtonControls = vChildControls;
	    int i = 0; 
	    String sWidth, sHeight;
	    if (iOrientation == RadioGroup.HORIZONTAL)
//	      fWidth = controlModel.fWidth/vChildControls.size();
		  sWidth = Utilities.calcDimension(controlModel.sWidth, '/', vChildControls.size());
	    else
	      sWidth = controlModel.sWidth;
	    if (iOrientation == RadioGroup.VERTICAL)
	      sHeight = Utilities.calcDimension(controlModel.sHeight, '/', vChildControls.size());
	    else
	      sHeight = controlModel.sHeight;
	    for (ControlModel radioButtonModel : vRadioButtonControls) {
	      setAppearances(controlModel, radioButtonModel);
	        if (iOrientation == RadioGroup.VERTICAL) {
	          radioButtonModel.sX = controlModel.sX;
	          radioButtonModel.sY = Utilities.calcDimension(controlModel.sY , '+', 
	        		  Utilities.getDimensionRaw(sHeight)*i);
	        } else {
	          radioButtonModel.sX = Utilities.calcDimension(controlModel.sX, '+', 
	        		  Utilities.getDimensionRaw(sWidth)*i);
	          radioButtonModel.sY = controlModel.sY;
	        }
	        radioButtonModel.sWidth = sWidth;
	        radioButtonModel.sHeight = sHeight;
	        Class<?> argumentsClass[] = new Class[2];
	        
			argumentsClass[0] = Context.class;
			argumentsClass[1] = ControlModel.class;
			Constructor<?> constructor;
			try {
				constructor = className.getDeclaredConstructor(argumentsClass);
				if (null == constructor) {
					LogManager.logWTF("Cannot create control. The constructor in the implemetation class is missing. Class Name:" + className);
					return Boolean.valueOf(false);
				}
				ControlInterface radioButtonView = (ControlInterface)constructor.newInstance(context, radioButtonModel);

		        radioButtonView.initialize(this, fParentWidth, fParentHeight, null, screenModel);
		        if (radioButtonModel.bIsVisible == false)
		            ((View)radioButtonView).setVisibility(View.GONE); // Should not take space for layout purpose
		        if (i == 0) // only set the dimensions to the first radio button. Rest are calculated by android
		        	DynamicLayout.setControlPosition((ViewGroup)this, radioButtonModel, 
		        			fParentWidth, fParentHeight, (View)radioButtonView);
		//	      addView(radioButtonView); // The view is already added by setControlPosition 
			} catch (SecurityException e) {
				LogManager.logWTF(e, "Unable to create control. Implementation class not found for control: " + radioButtonModel.sName);
			} catch (NoSuchMethodException nsme) {
				LogManager.logWTF(nsme, "Unable to create control. Constructor missing from the implementation class: " + radioButtonModel.sName);
			} catch (IllegalArgumentException iae) {
				LogManager.logWTF(iae, "Unable to create control. Arguments missing from the implementation class: " + radioButtonModel.sName);
				iae.printStackTrace();
			} catch (InstantiationException ie) {
				LogManager.logWTF(ie, "Unable to create control. Cannot create instance of the control: " + radioButtonModel.sName);
			} catch (IllegalAccessException iae) {
				LogManager.logWTF(iae, "Unable to create control. Cannot access the constructor: " + radioButtonModel.sName);
			} catch (InvocationTargetException iae) {
				LogManager.logWTF(iae, "Unable to create control. Cannot instantiate the control: " + radioButtonModel.sName);
			}
		}
		if(controlModel != null && controlModel.bActionYN){
			setOnCheckedChangeListener(new OnCheckedChangeListener() {
				public void onCheckedChanged(RadioGroup radioGroup, int iCheckedId) {
					ActionModel[] actions = controlModel.getActions();
					if (null != actions)
					for (int i = 0; i < actions.length; i++) {
						if (actions[i].iEventList[0] == Constants.TAP) {
							Action a = new Action();
							a.execute(getContext(), radioGroup, parentView, actions[i]);
						}
					}
				}
			});
		}
	    return Boolean.valueOf(true);       
	}
	
	public void setEnabled(boolean bEnabled) {
		for (int i = 0; i < this.getChildCount(); i++) 
		    this.getChildAt(i).setEnabled(false);      
	}
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.graphics.Paint;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.AppearanceModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class EDIT extends EditText implements ControlInterface {
	public static String KEYBOARD_TYPE = "keyboard_type";
	public static String ACTION_STRING = "action_string";
	ControlModel controlModel;

	public EDIT(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight, 
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		String sHint = controlModel.mExtendedProperties.get("hint");
		if (null != sHint) {
			// this.setText(controlModel.sDefaultValue);
			this.setHint(sHint);
		}		
		String sKeyboardType = controlModel.mExtendedProperties != null ? controlModel.mExtendedProperties.get(KEYBOARD_TYPE): null;
		String sActionString = controlModel.mExtendedProperties != null ? controlModel.mExtendedProperties.get(ACTION_STRING) : null;
		int[] iInputActionType = getInputActionType(sKeyboardType, sActionString);
		setInputType(iInputActionType[0]);
		if (iInputActionType[1] != EditorInfo.IME_ACTION_UNSPECIFIED)
			setImeOptions(iInputActionType[1]);
		setSingleLine(false);
		setHorizontallyScrolling(false);
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		AppearanceManager.getInstance().updateTextAppearance(this, controlModel, Constants.APPEARANCE_ID_NOT_SET);
		return Boolean.valueOf(true);
	}

	public int getControlId() {
		return getId();
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setControlAppearance() {
		this.setSingleLine();
	}

	public String getValue() {
		// TODO Auto-generated method stub
		return this.getText().toString();
	}

	public void setValue(Object object) {		
		// Clear the control...
		CharSequence sExistingText = getText();
		if (object == null) {
			if (null != sExistingText)
				super.setText(null);
			return;
		}
		if (!(object instanceof String))
			return;
		String sText = (String) object;
		String sHint = controlModel.mExtendedProperties.get("hint");
		if (sText.equals(sHint))
			return;
		sText = Utilities.format(controlModel, sText);
		if (null != sExistingText && sExistingText.equals(sText))
			return;
		AppearanceModel appearanceModel = AppearanceManager.getInstance().getAppearance(controlModel.sSystemDbName, 
				controlModel.iAppearanceId);
		if (null != appearanceModel && appearanceModel.bFontStyleStrikethrough)
			setPaintFlags(getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		if (null != sText && null != appearanceModel && appearanceModel.bFontStyleUnderline) {
			SpannableString content = new SpannableString(sText);
			content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			super.setText(content);			
		} else
			super.setText(sText);
	}
	
	public int[] getInputActionType(String sKeyboardType, String sActionString) {
		int iInputAction[] = new int[2];
		iInputAction[0] = InputType.TYPE_CLASS_TEXT;
		iInputAction[1] = EditorInfo.IME_ACTION_UNSPECIFIED;
		if (null == sKeyboardType)
			return iInputAction;
		
		if (sKeyboardType.equalsIgnoreCase("TEXT"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT;
		else if (sKeyboardType.equalsIgnoreCase("NUMBER"))
			iInputAction[0] = InputType.TYPE_CLASS_NUMBER;
		else if (sKeyboardType.equalsIgnoreCase("DATE"))
			iInputAction[0] = InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_DATE;
		else if (sKeyboardType.equalsIgnoreCase("DECIMAL"))
			iInputAction[0] = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL;
		else if (sKeyboardType.equalsIgnoreCase("URI"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_URI;
		else if (sKeyboardType.equalsIgnoreCase("TIME"))
			iInputAction[0] = InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_TIME;
		else if (sKeyboardType.equalsIgnoreCase("DATETIME"))
			iInputAction[0] = InputType.TYPE_CLASS_DATETIME;
		else if (sKeyboardType.equalsIgnoreCase("EMAIL_ADDRESS"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS;
		else if (sKeyboardType.equalsIgnoreCase("PHONE"))
			iInputAction[0] = InputType.TYPE_CLASS_PHONE;
		else if (sKeyboardType.equalsIgnoreCase("PASSWORD"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD;
		else if (sKeyboardType.equalsIgnoreCase("AUTO_COMPLETE"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE;
		else if (sKeyboardType.equalsIgnoreCase("AUTO_CORRECT"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT;
		else if (sKeyboardType.equalsIgnoreCase("CAP_CHARACTERS"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS;
		else if (sKeyboardType.equalsIgnoreCase("CAP_SENTENCES"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES;
		else if (sKeyboardType.equalsIgnoreCase("CAP_WORDS"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS;
		else if (sKeyboardType.equalsIgnoreCase("MULTI_LINE"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE;
		else if (sKeyboardType.equalsIgnoreCase("NO_SUGGESTIONS"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
		else if (sKeyboardType.equalsIgnoreCase("EMAIL_SUBJECT"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_SUBJECT;
		else if (sKeyboardType.equalsIgnoreCase("VARIATION_FILTER"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_FILTER;
		else if (sKeyboardType.equalsIgnoreCase("LONG_MESSAGE"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE;
		else if (sKeyboardType.equalsIgnoreCase("PERSON_NAME"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME;
		else if (sKeyboardType.equalsIgnoreCase("PHONETIC"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PHONETIC;
		else if (sKeyboardType.equalsIgnoreCase("POSTAL_ADDRESS"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS;
		else if (sKeyboardType.equalsIgnoreCase("SHORT_MESSAGE"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_SHORT_MESSAGE;
		else if (sKeyboardType.equalsIgnoreCase("VISIBLE_PASSWORD"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD;
		else if (sKeyboardType.equalsIgnoreCase("WEB_EDIT_TEXT"))
			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT;
		else if (sKeyboardType.equalsIgnoreCase("SIGNED"))
			iInputAction[0] = InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED;
//		else if (sKeyboardType.equalsIgnoreCase("NUMBER_PASSWORD"))
//			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_NUMBER_VARIATION_PASSWORD;
//		else if (sKeyboardType.equalsIgnoreCase("WEB_EMAIL_ADDRESS"))
//			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS;
//		else if (sKeyboardType.equalsIgnoreCase("WEB_PASSWORD"))
//			iInputAction[0] = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD;
		
		// set ACTIONS
		if (null == sActionString)
			;
		else if (sActionString.equalsIgnoreCase("DONE"))
			iInputAction[1] = EditorInfo.IME_ACTION_DONE;
		else if (sActionString.equalsIgnoreCase("GO"))
			iInputAction[1] = EditorInfo.IME_ACTION_GO;
		else if (sActionString.equalsIgnoreCase("NEXT"))
			iInputAction[1] = EditorInfo.IME_ACTION_NEXT;
		else if (sActionString.equalsIgnoreCase("NONE"))
			iInputAction[1] = EditorInfo.IME_ACTION_NONE;
//		else if (sActionString.equalsIgnoreCase("PREVIOUS"))
//			iInputAction[1] = EditorInfo.IME_ACTION_PREVIOUS;
		else if (sActionString.equalsIgnoreCase("SEARCH"))
			iInputAction[1] = EditorInfo.IME_ACTION_SEARCH;
		else if (sActionString.equalsIgnoreCase("SEND"))
			iInputAction[1] = EditorInfo.IME_ACTION_SEND;
		else
			iInputAction[1] = EditorInfo.IME_ACTION_UNSPECIFIED;
		return iInputAction;
	}
}

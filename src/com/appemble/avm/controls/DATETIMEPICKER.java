/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.PointF;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class DATETIMEPICKER extends PUSHBUTTON {
	private Date date = null;
	Context context;
	View parentView;
	
	
	public DATETIMEPICKER(final Context ctx, final ControlModel controlObject) {
		super(ctx, controlObject);
	}
	
	public Object initialize(final View pView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		float fWidth, fHeight;
		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fWidth = size.x; fHeight = size.y;
		} else {
			fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
		}
		Boolean bReturn = (Boolean) super.initialize(pView, fWidth, fHeight,
				vChildControls, screenModel);
		this.parentView = pView;
		
		setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Dialog d = null;
				CharSequence sDate = getText();
				if ("now".equals(sDate) || "today".equals(sDate))
					date = new Date();
				else if (null != sDate && sDate.length() > 0) {
					SimpleDateFormat sdf = new SimpleDateFormat();
					sdf.applyPattern(controlModel.sFormat);
					try {
						date = sdf.parse(sDate.toString());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (date == null)
					date = new Date();
				if (controlModel.iDataType == Constants.DATE) {
						d = new DatePickerDialog(v.getContext(), dateListener, date.getYear()+1900, 
								date.getMonth(), date.getDate());
						// select the fields to display based on the format
						chooseDatePickerDialogFields((DatePickerDialog)d);
				} else if (controlModel.iDataType == Constants.TIME) { 
						d = new TimePickerDialog(v.getContext(), timeListener, date.getHours(), date.getMinutes(), false);
						// select the fields to display based on the format
						chooseTimePickerDialogFields((TimePickerDialog)d);
				}
				if (null != d)
					d.show();
			}
		});
		return bReturn;
	}

	public String getValue() {
		return Utilities.getDefaultText(getText(), controlModel, sImageSource);
	}

	private void chooseDatePickerDialogFields(DatePickerDialog d) {
		Field datePickerDialogFields[] = d.getClass().getDeclaredFields();
		boolean bRemovedAField = false;
		if (!controlModel.sFormat.contains("y")) {
			removeDisplayField(d, datePickerDialogFields, "mYearPicker");
			bRemovedAField = true;
		}
		if (!controlModel.sFormat.contains("M")) {
			removeDisplayField(d, datePickerDialogFields, "mMonthPicker");
			bRemovedAField = true;
		}
		if (!controlModel.sFormat.contains("d")) {
			removeDisplayField(d, datePickerDialogFields, "mDayPicker");
			bRemovedAField = true;
		}
		if (bRemovedAField)
			d.setTitle(null);
	}
	
	private void removeDisplayField(DatePickerDialog d, Field datePickerDialogFields[], String sFieldName) {
		if (null == sFieldName)
			return;
		try {
			for (Field datePickerDialogField : datePickerDialogFields) {           					
			    if (datePickerDialogField.getName().equals("mDatePicker")) {
			        datePickerDialogField.setAccessible(true);
			        DatePicker datePicker = (DatePicker) datePickerDialogField.get(d);
			        Field datePickerFields[] = datePickerDialogField.getType().getDeclaredFields();

			        for (Field datePickerField : datePickerFields) {
			            if (sFieldName.equals(datePickerField.getName())) {
			                datePickerField.setAccessible(true);
			                Object fieldPicker = datePickerField.get(datePicker);
			                if (fieldPicker instanceof View)
			                ((View) fieldPicker).setVisibility(View.GONE);
			            }
			        }
			    }
			}
		} catch (IllegalAccessException e) {
			LogManager.logWTF(e, "In control DATETIMEPICKER: " + controlModel.sName + ", cannot access internal data structure.");
		}
	}

	private void chooseTimePickerDialogFields(TimePickerDialog d) {
		boolean bRemovedAField = false;
		Field timePickerDialogFields[] = d.getClass().getDeclaredFields();
		if (!(controlModel.sFormat.contains("H") || controlModel.sFormat.contains("h") || controlModel.sFormat.contains("K"))) {
			removeDisplayField(d, timePickerDialogFields, "mHourPicker");
			bRemovedAField = true;
		}
		if (!controlModel.sFormat.contains("m")) {
			removeDisplayField(d, timePickerDialogFields, "mMinutePicker");
			bRemovedAField = true;
		}
		if (!controlModel.sFormat.contains("a")) {
			removeDisplayField(d, timePickerDialogFields, "mAmPmButton");
			bRemovedAField = true;
		}
		if (bRemovedAField)
			d.setTitle(null);
	}
	
	private void removeDisplayField(TimePickerDialog d, Field timePickerDialogFields[], String sFieldName) {
		if (null == sFieldName)
			return;
		try {
			for (Field timePickerDialogField : timePickerDialogFields) {           

			    if (timePickerDialogField.getName().equals("mTimePicker")) {
			        timePickerDialogField.setAccessible(true);
			        TimePicker timePicker = (TimePicker) timePickerDialogField.get(d);

			        Field timePickerFields[] = timePickerDialogField.getType().getDeclaredFields();

			        for (Field timePickerField : timePickerFields) {

			            if (sFieldName.equals(timePickerField.getName())) {
			                timePickerField.setAccessible(true);
			                Object fieldPicker = new Object();
			                fieldPicker = timePickerField.get(timePicker);
			                ((View) fieldPicker).setVisibility(View.GONE);
			            }
			        }
			    }
			}
		} catch (IllegalAccessException e) {
			LogManager.logWTF(e, "In control DATETIMEPICKER: " + controlModel.sName + ", cannot access internal data structure.");
		}
	}

	private void update() {
		String sText = null;
		String sStorageFormat = (controlModel.mExtendedProperties != null && 
			controlModel.mExtendedProperties.containsKey(Constants.STORAGE_FORMAT)) ? 
			controlModel.mExtendedProperties.get(Constants.STORAGE_FORMAT) : controlModel.sFormat;
		if ("nnnn".equals(sStorageFormat))
			sText = String.valueOf(date.getTime());
		else if ("nn".equals(sStorageFormat))
			sText = String.valueOf(date.getTime()/1000);
		else {
			SimpleDateFormat sdf = new SimpleDateFormat(sStorageFormat);
			sText = sdf.format(date); // format the date to default format. The setValue will format it back.
		}
		setValue(sText);
		ActionModel[] actions = controlModel.getActions();
		if (null != actions)
		for (int i = 0; i < actions.length; i++) {
			if (actions[i].iPrecedingActionId == 0) {
				Action a = new Action();
				a.execute(context, this, parentView, actions[i]);
			}
		}
	}
	
	private DatePickerDialog.OnDateSetListener dateListener = 
		new DatePickerDialog.OnDateSetListener() {

			public void onDateSet(DatePicker view, int year, int month, int day) {
				date.setYear(year-1900); date.setMonth(month); date.setDate(day);
				update();
			}
		};
	
	private TimePickerDialog.OnTimeSetListener timeListener = 
		new TimePickerDialog.OnTimeSetListener() {
			public void onTimeSet(TimePicker view, int hour, int minute) {
				date.setHours(hour); date.setMinutes(minute);
				update();
			}		
		};
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.graphics.Paint;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.AppearanceModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class PUSHBUTTON extends Button implements ControlInterface{
	ControlModel controlModel;
	String sImageSource;
	boolean bLongPressed = false;
	private Handler repeatUpdateHandler = new Handler();
	private long REPEAT_DELAY = 50;
	
	class RepetetiveUpdater implements Runnable {
		View v, parentView; 
		public RepetetiveUpdater(View v, View parentView) {
			this.v = v;
			this.parentView = parentView;
		}
		
		public void run() {
			if(bLongPressed) {
				Action.callAction(PUSHBUTTON.this.getContext(), v, parentView, 
					PUSHBUTTON.this.controlModel, Constants.LONG_PRESS); 
				repeatUpdateHandler.postDelayed( new RepetetiveUpdater(v, parentView), REPEAT_DELAY );
			}
		}
	}

	public PUSHBUTTON(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;		
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		setFocusable(true);
		if (null != screenModel.sInitialFocus && screenModel.sInitialFocus.equals(controlModel.sName))
			requestFocus();
		String sRepeatDelay = controlModel.getExtendedProperty("repeat_delay");
		if (null != sRepeatDelay) {
			try {
				REPEAT_DELAY = Integer.parseInt(sRepeatDelay);
			} catch (NumberFormatException nfe) {}
		}
		if(controlModel.bActionYN){
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
				}
			});
			// Call action LONG_PRESS after a long click
			setOnLongClickListener( 
					new View.OnLongClickListener(){
						public boolean onLongClick(View v) {
							bLongPressed = true;
							repeatUpdateHandler.post( new RepetetiveUpdater(v, parentView));
							return false;
						}
					}
			);
			
			// When the button is released, if we're auto incrementing, stop
			setOnTouchListener( new View.OnTouchListener() {
				public boolean onTouch(View v, MotionEvent event) {
					if( event.getAction() == MotionEvent.ACTION_UP && bLongPressed ){
						bLongPressed = false;
						Action.callAction(getContext(), v, parentView, controlModel, Constants.LONG_PRESS);
					}
					return false;
				}
			});
		}
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		AppearanceManager.getInstance().updateTextAppearance(this, controlModel, Constants.APPEARANCE_ID_NOT_SET);
		return Boolean.valueOf(true);
	}

	public PUSHBUTTON(final Context context, final View parentView, final ControlModel controlObject) {
		super(context);
		this.controlModel = controlObject;
		this.setFocusable(false); // Why? HB 2011 12 16	
//		AppearanceManager.getInstance().updateButtonBackgroundAppearance(this, controlObject);
//		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlObject, null);
		AppearanceManager.getInstance().updateTextAppearance(this, controlObject, Constants.APPEARANCE_ID_NOT_SET);
	}

	public int getControlId() {
		// TODO Auto-generated method stub
		return controlModel.id;
	}
	
	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return Utilities.getDefaultText(getText(), controlModel, sImageSource);
	}

	public void setValue(Object object) {
		// Clear the control...
		CharSequence sExistingText = getText();
		if (object == null) {
			if (null != sExistingText)
				super.setText(null);
			// there could be a image
			if (null != sImageSource) {
//				ImageManager.getInstance().UpdateView(this, null, true);
				AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, null);
				sImageSource = null;
			}
			return;
		}
		if (false == (object instanceof String))
			return; // must be a string.
		// if there is a value to be set...
		String sText = (String) object;
		if (sText.startsWith(Constants.URL_IMAGE_PREFIX)) {
			if (null != sImageSource && sImageSource.equals(sText))
				return;
//			ImageManager.getInstance().UpdateView(this, sText, true);
			AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, sText);
			sImageSource = sText;
		} else {
			sText = Utilities.format(controlModel, sText);
			if (null != sExistingText && sExistingText.equals(sText))
				return;
			AppearanceModel appearanceModel = AppearanceManager.getInstance().getAppearance(controlModel.sSystemDbName,
					controlModel.iAppearanceId);
			if (null != appearanceModel && appearanceModel.bFontStyleStrikethrough)
				setPaintFlags(getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
			if (null != appearanceModel && appearanceModel.bFontStyleUnderline) {
				SpannableString content = new SpannableString(sText);
				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				super.setText(content);			
			} else
				super.setText(sText);
		}
	}
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.R;
import com.appemble.avm.dynamicapp.DynamicActivity;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.quietlycoding.android.picker.NumberPicker;

public class NUMBER_PICKER extends PUSHBUTTON {
	NumberPicker numberPicker;
	int iMin = 0, iMax = 0;
	EditText etInput; // to be passed to the listener.
	
	public NUMBER_PICKER(final Context ctx, final ControlModel controlObject) {
		super(ctx, controlObject);		
		controlModel = controlObject;		
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight, 
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		super.initialize(parentView, fParentWidth, fParentHeight, vChildControls, screenModel);
		Context screenContext = this.getContext();
		final DynamicActivity dynamicActivity = (DynamicActivity) screenContext;
		if (!(dynamicActivity instanceof DynamicActivity)) {
			LogManager.logError(Constants.MISSING_ACTIVITY + " NUMBER_PICKER with Id: " + controlModel.id);
			return Boolean.valueOf(false);
		}
		try {
			String string = controlModel.mExtendedProperties.get("min");
			if (string != null)
				iMin = Integer.parseInt(string);
			string = controlModel.mExtendedProperties.get("max");
			if (string != null)
				iMax = Integer.parseInt(string);
		} catch (NumberFormatException nfe) {
			LogManager.logError("Unable to initiate control NUMBER_PICKER: " + controlModel.sName + 
					". The min/max values should be integer(s).");
			return Boolean.valueOf(false);
		}
		setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(dynamicActivity);
				LayoutInflater inflater = (LayoutInflater) Cache.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View numberPickerLayout = inflater.inflate(R.layout.number_picker_parent_layout,
				                               (ViewGroup) findViewById(R.id.number_picker_parent_layout));
				CharSequence sText = getText(); // get the current value from the push button. 
				etInput = (EditText) numberPickerLayout.findViewById(R.id.numberpicker_input); // get the edit text so that on OK, its value can be obtained.				
				numberPicker = (NumberPicker) numberPickerLayout.findViewById(R.id.number_picker); // get the number picker.
				
				if (iMin != 0 || iMax != 0) // set the min max. 
					numberPicker.setRange(iMin, iMax);
				if (null != sText && sText.length() > 0) {
					try {
						int iValue = Integer.parseInt(sText.toString());
						numberPicker.setCurrent(iValue);
					} catch (NumberFormatException nfe) {
						LogManager.logWTF(nfe, "Unable to convert existing number into an integer." + sText);
					}
				}
				builder.setTitle(controlModel.mExtendedProperties.get(Constants.TITLE))
					   .setView(numberPickerLayout) 
					   .setPositiveButton(android.R.string.ok, okListener) // attach the OK listener
					   .setNegativeButton(android.R.string.cancel, cancelListener) // attach the cancel listener
					   .create() // create the dialog box
					   .show();  // show the dialog box.
			}
		});
		return Boolean.valueOf(true);
	}
	
	private DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {        	
            setValue(etInput.getText().toString());
	        dialog.dismiss();
        }
    };
    
    private DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
	        dialog.dismiss();
        }
    };
    
	public void setValue(Object object) {
		if (!(object instanceof String))
			return;
		String sText = (String) object;
		// Do not show the default value
		if (null != sText && sText.equalsIgnoreCase(controlModel.sDeparameterizedLocalDataSource))
			sText = null;
		super.setText(sText);
	}	
}
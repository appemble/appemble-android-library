/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class IMAGEBUTTON extends ImageButton implements ControlInterface {
	ControlModel controlModel;
	String sImageSource;
	
	public IMAGEBUTTON(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;		
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		setFocusable(false);
		setScaleType(Utilities.getScaleType(controlModel.mExtendedProperties.get("scale_type")));
						
		if(controlModel != null && controlModel.bActionYN){
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
				}
			});
		}
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		return Boolean.valueOf(true);
	}

	public void onStart(Bundle targetParameterValueList) {
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_START_SCREEN); 
		return;
	}

	public void onResume(Bundle targetParameterValueList) {
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_RESUME_SCREEN); 
		return;
	}

	public void onPause(Bundle targetParameterValueList) {
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_PAUSE_SCREEN); 
		return;
	}

	public void onStop(Bundle targetParameterValueList) {
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_STOP_SCREEN); 
		return;
	}

	public void onDestroy(Bundle targetParameterValueList) {
		Action.callAction(getContext(), this, (View)getParent(), controlModel, Constants.ON_DESTROY_SCREEN); 
		return;
	}

	public int getControlId() {
		return controlModel.id;
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setControlAppearance() {
	}
	
	public String getValue() {
		// return (String) getTag();
		return sImageSource;
	}

	public void setValue(Object object) {		
		if (object == null) {
			sImageSource = null;
//			ImageManager.getInstance().UpdateView(this, null, true);
			AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, null);
			AppearanceManager.getInstance().updateImage(this, controlModel, null, null);
			return;
		}
		if (!(object instanceof String))
			return;
		String sText = (String) object;
		if (null != sImageSource && sImageSource.equals(sText))
			return;
		AppearanceManager.getInstance().updateImage(this, controlModel, null, sText);
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, sText);
//		ImageManager.getInstance().UpdateView(this, sText, true);
		sImageSource = sText;
	}
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;

import com.appemble.avm.LogManager;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class SEEKBAR_VERTICAL_DISCRETE extends SEEKBAR_VERTICAL implements ControlInterface {
	int iSlabWidth = 0;
	
	public SEEKBAR_VERTICAL_DISCRETE(final Context context, final ControlModel controlObject) {
		super(context, controlObject);//, null, android.R.style.Widget_ProgressBar_Large); //TODO add style in control model. set style from extended properties.
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		super.initialize(parentView, fParentWidth, fParentHeight, vChildControls, screenModel);
		try {
			if (null == controlModel)
			return Boolean.valueOf(false);
		if (controlModel.mExtendedProperties.containsKey("discrete_values")) {
			try {
				int iNumDiscreteValues = Integer.parseInt(controlModel.mExtendedProperties.get("discrete_values"));
				if (iNumDiscreteValues > 0)
					iSlabWidth = 100/iNumDiscreteValues;
			} catch (NumberFormatException nfe) {
				LogManager.logError("Unable to parse discrete_values");
			}
		}
		} catch (NumberFormatException nfe) {
			LogManager.logError("Please give how many discrete values would this control has: " + controlModel.sName);
		}
//		if (null == controlModel)
//			return Boolean.valueOf(false);
//		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
//		try {
//		if (controlModel.mExtendedProperties.containsKey("min"))
//			this.iMin = Integer.parseInt(controlModel.mExtendedProperties.get("min"));
//		if (controlModel.mExtendedProperties.containsKey("max"))
//			this.setMax(Integer.parseInt(controlModel.mExtendedProperties.get("max"))-iMin);
//		} catch (NumberFormatException nfe) {
//			LogManager.logError("Please give min/max values as integer for control name: " + controlModel.sName);
//		}
//		if (controlModel != null && controlModel.bActionYN) {
//			setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
//				public void onProgressChanged(SeekBar seekBar, int progress,
//                        boolean fromUser) {
//					Action.callAction(getContext(), seekBar, parentView, controlModel, Constants.ON_SLIDE);
//				}
//
//				public void onStartTrackingTouch(SeekBar seekBar) {
//					Action.callAction(getContext(), seekBar, parentView, controlModel, Constants.ON_SLIDE_START);
//				}
//
//				public void onStopTrackingTouch(SeekBar seekBar) {
//					Action.callAction(getContext(), seekBar, parentView, controlModel, Constants.ON_SLIDE_STOP);
//				}
//			});
//		}
		return Boolean.TRUE;
	}

//	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(h, w, oldh, oldw);
//    }
//
//    @Override
//    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
//        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
//    }
//
//    protected void onDraw(Canvas c) {
//        c.rotate(90);
//        c.translate(0, -getWidth());
//
//        super.onDraw(c);
//    }
//
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }	

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE: {
            	int i = 0;
                i=getMax() - (int) (getMax() * event.getY() / getHeight());
                setProgress(100-i);                
                onSizeChanged(getWidth(), getHeight(), 0, 0);
            }
            	break;
            case MotionEvent.ACTION_UP: {
            	int i = 100 - (getMax() - (int) (getMax() * event.getY() / getHeight()));                
                int iSlab = (i / iSlabWidth);
                int iMod = i % iSlabWidth;
                if (iMod > iSlabWidth/2)
                	iSlab++;
                setProgress(iSlabWidth*iSlab);
                onSizeChanged(getWidth(), getHeight(), 0, 0);
            }
                break;

            case MotionEvent.ACTION_CANCEL:
                break;
        }
        return true;
    }    
}

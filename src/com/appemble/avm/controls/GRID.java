/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.database.Cursor;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;

import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dataconnector.GridArrayAdaptor;
import com.appemble.avm.dataconnector.GridCursorAdaptor;
import com.appemble.avm.dataconnector.GridJsonAdaptor;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.DynamicActivity;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class GRID extends GridView implements ControlInterface {
	ControlModel controlModel;
	public Vector<ControlModel> vControls;
	ScreenModel screenModel;
	public boolean bOnMeasure = false;
	public int iColumnWidth = 0;
	float fWidth, fHeight;
	
	public GRID(final Context context, ControlModel controlObject) {
		super(context);
		controlModel = controlObject;		
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel scrModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		vControls = vChildControls;
		screenModel = scrModel;
		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fWidth = size.x; fHeight = size.y;
		} else {
			fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
		}

    	if (false == setAttributes(fWidth, fHeight).booleanValue())
        	return Boolean.valueOf(false);
        
        
        setSmoothScrollbarEnabled (true);
        // Data binding done in Dynamic Activity. See that file for  more details.
        this.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> listItemView, View listView, int arg2, long arg3) {
				Action.callAction(getContext(), listItemView, listView, controlModel, Constants.TAP); 
			}
        });
		AppembleActivityImpl.addEmptyControlIds(controlModel, screenModel, null);        
		return Boolean.valueOf(true);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		bOnMeasure = true;
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		bOnMeasure = false;
		return;
	}
	
    public void onDestroy(Bundle targetParameterValueList) {
        // No need to close the cursor. Android is doing OK in closing the cursor.
		ListAdapter imageAdapter = getAdapter();
		if (null == imageAdapter)
			return;
		if (imageAdapter instanceof GridCursorAdaptor) {
			GridCursorAdaptor adapter = (GridCursorAdaptor)imageAdapter;
			Cursor c = adapter.getCursor();
			if (c != null && false == c.isClosed())
				c.close();
		}
	}

	public int getControlId() {
		return controlModel.id;
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setControlAppearance() {
	}

	public String getValue() {
		return null;
	}
	
	public void setValue(Object object) {
		// If called from DynamicActivity::UpdateControl, then
		// this value is obtained in the following order remote data source, local data source, remote data, 
		// local cursor, target parameter list and default value.
		// candidate for removal 20120330 HB if (null == sText)
		//	sText = controlModel.sDefaultValue;
		if (bOnMeasure)
			return;
		if (null == object) {
			showEmptyListControls(true);
			return;
		}
		try {
			if (controlModel.iDataSourceType == Constants.JSONARRAY) {
				JSONArray jsonArray = null;
				if (null != object)
					jsonArray = new JSONArray((String)object);
				setJson(jsonArray);
			}
		    else if (controlModel.iDataSourceType == Constants.ARRAYLIST)
	        	setArrayList(object);
			else // if (controlModel.iDataType == Constants.CURSOR) TODO uncomment when all lists are compatible...
				setCursor((Cursor) object);
		} catch (JSONException jsone) {
			LogManager.logError("The control: " + controlModel.sName + " expects data type as JSONArray"); 
		} catch (ClassCastException cce) {
			LogManager.logError("The control: " + controlModel.sName + " expects data type as " + ConfigManager.getInstance().getDataTypesString(controlModel.iDataType));
		}
	}
	
	public boolean setCursor(Cursor c) {
		// cursor can be null
		GridCursorAdaptor adapter = (GridCursorAdaptor)getAdapter();
		if (null == adapter) { // the constructor does not set the adapter.
			adapter = new GridCursorAdaptor(getContext(), this, c, controlModel, vControls, fWidth,
					fHeight, screenModel);
	        setAdapter(adapter); 
		} else {
			adapter.changeCursor(c);
			adapter.notifyDataSetChanged();
		}
		return showEmptyListControls(null == c || c.getCount() == 0);
	}

	public boolean setArrayList(Object object) {
		// here parse the string to formulate the multidimensional array. The order of the controls in the list box 
		// determine the order in which they get the value from this multidimensional array.
		
		// String [] aValues = sValues.split(",");
		ArrayList<HashMap<String, Object>> aValues = new ArrayList<HashMap<String, Object>>();
		// to represent multidimensional array, use HashMap<columm name, value> in an ArrayList
		// ArrayList<HashMap<String, String>> aValues = new ArrayList<HashMap<String, String>>();
		if (null == aValues || aValues.size()== 0)
			return false;
		GridArrayAdaptor adaptor = (GridArrayAdaptor)getAdapter();
		if (null == adaptor) { // the constructor does not set the adaptor.
			adaptor = new GridArrayAdaptor(getContext(), this, aValues, controlModel, vControls, fWidth,
					fHeight, screenModel);
	        setAdapter(adaptor); 
		} else {
			adaptor.changeDataset(aValues);
			adaptor.notifyDataSetChanged();
		}
		return showEmptyListControls(null == aValues || aValues.size() == 0);
	}
	
	public boolean setJson(Object object) {		
		JSONArray jsonArray = (JSONArray) object;
		GridJsonAdaptor adaptor = (GridJsonAdaptor)getAdapter();
		if (null == adaptor) { // the constructor does not set the adaptor.
			adaptor = new GridJsonAdaptor(getContext(), this, jsonArray, controlModel, vControls, fWidth,
					fHeight, screenModel);
	        setAdapter(adaptor); 
		} else {
			adaptor.changeDataset(jsonArray);
			final GridJsonAdaptor a = adaptor;
			AppembleActivity appembleActivity = Utilities.getActivity(getContext(), this, null);
			if (appembleActivity instanceof DynamicActivity) {
				DynamicActivity dynamicActivity = (DynamicActivity) appembleActivity;
				dynamicActivity.runOnUiThread(new Runnable() {
				    public void run() {
				        a.notifyDataSetChanged();
				    }
				});			
			}
		}
		return showEmptyListControls(null == jsonArray || jsonArray.length() == 0);
	}
	
    private boolean showEmptyListControls(boolean bVisibility) {
    	return AppembleActivityImpl.showEmptyListControls(this, controlModel, this.getContext(), bVisibility);
    }

	private Boolean setAttributes(float fParentWidth, float fParentHeight) {
        int iVerticalSpacing = 0, iHorizontalSpacing = 0, iNumColumns = 0;
        String sValue = controlModel.mExtendedProperties.get("vertical_spacing");
        if (null != sValue && sValue.length() > 0)
        	iVerticalSpacing = sValue.equalsIgnoreCase("same") ? -1 : 
       			(int)(Utilities.getDimension(sValue, screenModel.fHeightInPixels));
        sValue = controlModel.mExtendedProperties.get("horizontal_spacing");
        if (null != sValue && sValue.length() > 0)
        	iHorizontalSpacing = sValue.equalsIgnoreCase("same") ? -1 : 
    			(int)(Utilities.getDimension(sValue, screenModel.fWidthInPixels));
        if (iVerticalSpacing == -1 && iHorizontalSpacing == -1) {
        	LogManager.logError("Cannot specify both horizontal_spacing and vertical_spacing as \"same\" for Grid: " + controlModel.sName);
        	return Boolean.valueOf(false);
        } else if (iVerticalSpacing == -1)
        	iVerticalSpacing = iHorizontalSpacing;
        else if (iHorizontalSpacing == -1)
        	iHorizontalSpacing = iVerticalSpacing;

        sValue = controlModel.mExtendedProperties.get("column_width");
        if (null != sValue && sValue.length() > 0)
        	iColumnWidth = (int)Utilities.getDimension(sValue, Cache.bDimenRelativeToParent ? 
        			fParentWidth : screenModel.fWidthInPixels);
        sValue = controlModel.mExtendedProperties.get("num_columns");
        if (null != sValue && sValue.length() > 0)
        	iNumColumns = Integer.parseInt(sValue.trim());
        if (iColumnWidth != 0 && iNumColumns != 0) {
        	LogManager.logError("Cannot specify both column_width and num_columns at the same time for Grid: " + controlModel.sName);
        	return Boolean.valueOf(false);
        } else if (iColumnWidth == 0 && iNumColumns == 0) {
        	LogManager.logError("Must specify either column_width and num_columns for Grid: " + controlModel.sName);
        	return Boolean.valueOf(false);
        }
        
        if (iNumColumns > 0) {// calculate the column width
	       	iColumnWidth = (int)((Utilities.getDimension(controlModel.sWidth, 
	       			Cache.bDimenRelativeToParent ? fParentWidth : screenModel.fWidthInPixels) -  
	        		iHorizontalSpacing*(iNumColumns-1))/(float)iNumColumns); // subtract space taken by horizontal spacing
        }
        else // calculate the num of columns
        	iNumColumns = (int)getNumColumns(1, iColumnWidth-iHorizontalSpacing, (-screenModel.fWidthInPixels-1));
        if (iVerticalSpacing > 0)
        	setVerticalSpacing(iVerticalSpacing);
        if (iHorizontalSpacing > 0)
        	setHorizontalSpacing(iHorizontalSpacing);
    	setColumnWidth(iColumnWidth);
   		setNumColumns(iNumColumns);
    	return Boolean.valueOf(true);
	}

    // Applying the quadratic formula
	public static double getNumColumns(double a, double b, double c) {
	     double discr, root1, root2;

	     // Solve the discriminant (SQRT (b^2 - 4ac)
	     discr = Math.sqrt((b * b) - (4 * a * c));
	     System.out.println("Discriminant = " + discr);
	     // Determine number of roots
	     // if discr > 0 equation has 2 real roots
	     // if discr == 0 equation has a repeated real root
	     // if discr < 0 equation has imaginary roots
	     // if discr is NaN equation has no roots

	     // Test for NaN
	     if(Double.isNaN(discr))
//	        System.out.println("Equation has no roots");
	    	 return 0.0;
	     if(discr > 0)
	     {
//	        System.out.println("Equation has 2 roots");
	        root1 = (-b + discr)/2 * a;
	        root2 = (-b - discr)/2 * a;
	        if (root1 > 0)
	        	return root1;
	        else if (root2 > 0)
	        	return root2;
//	        System.out.println("First root = " + root1);
//	        System.out.println("Second roor = " + root2);
	      }

	      if(discr == 0) {
//	        System.out.println("Equation has 1 root");
	        root1 = (-b + discr)/2 * a;
	        return root1;
//	        System.out.println("Root = " + root1);
	      }

	       if(discr < 0)
//	         System.out.println("Equation has imaginary roots");
	    	   return 0.0;
	       return 0.0;
	}
}
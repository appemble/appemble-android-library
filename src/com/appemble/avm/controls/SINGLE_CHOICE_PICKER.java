/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.graphics.PointF;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dataconnector.ListArrayAdaptor;
import com.appemble.avm.dataconnector.ListCursorAdaptor;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class SINGLE_CHOICE_PICKER extends LIST {	
	public String sSelectedValue;
	public int iCheckableControlId;
	public int iSelectionFieldControlId;
	public SINGLE_CHOICE_PICKER(final Context context, ControlModel controlObject) {
		super(context, controlObject);
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel scrModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		vControls = vChildControls;
		screenModel = scrModel;
		
		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fWidth = size.x; fHeight = size.y;
		} else {
			fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
		}
		// Data binding done in Dynamic Activity. See that file for  more details.
		setCacheColorHint(controlModel);
		// get the iSelectionFieldControlId and iCheckableControlId
		String sCheckableControlName = controlModel.mExtendedProperties.get("checkable_control");
		String sSelectionFieldControlName = controlModel.mExtendedProperties.get("selection_control");
		if (null == sCheckableControlName || sCheckableControlName.length() == 0 ||  
				null == sSelectionFieldControlName || sSelectionFieldControlName.length() == 0) {
			LogManager.logError("The SINGLE_CHOICE_PICKER: " + controlModel.sName + " cannot be created. It must have an attribute checkable and selection_field");
			return Boolean.valueOf(false);
		}
		for (ControlModel cm : vControls) { // loop through the vListControls and get the iCheckableControlId and iSelectionFieldControlId
			if (null == cm)
				continue;
			if (null != cm.sName) {
				if (cm.sName.equals(sSelectionFieldControlName))
					iSelectionFieldControlId = cm.id;
				else if (cm.sName.equals(sCheckableControlName))
					iCheckableControlId = cm.id;
			}
			if (iCheckableControlId > 0 && iSelectionFieldControlId > 0)
				break;
		}		
		
        this.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> listView, View listItemView, int position, long id) {
				String sSelection = null; boolean isBeingSelected = false;
				for (ControlModel cm : vControls) { // loop through the vListControls
					if (null == cm)
						continue;
					if (cm.id  == iSelectionFieldControlId) {
						View controlView = listItemView.findViewById(cm.id);
						if (controlView != null)
							sSelection = (String)((ControlInterface)controlView).getValue();
					}
					if (cm.id == iCheckableControlId) {
						View controlView = listItemView.findViewById(cm.id);
						if (controlView != null) {
							if (cm.getType() == ConfigManager.getInstance().getControlId("IMAGE")) {
								isBeingSelected = (controlView.getVisibility() == INVISIBLE);
							}
						}
					}
				}
				sSelectedValue = isBeingSelected ? sSelection : "no selection";
				ListAdapter listAdapter = getAdapter();
				if (listAdapter instanceof ListCursorAdaptor)
					((ListCursorAdaptor)listAdapter).notifyDataSetChanged();
				else if (listAdapter instanceof ListArrayAdaptor)
					((ListArrayAdaptor)listAdapter).notifyDataSetChanged();
				// Call the tap action
				ActionModel[] actions = controlModel.getActions();
				if (null != actions)
				for (int i = 0; i < actions.length; i++) {
					if (actions[i].iEventList[0] == Constants.TAP || 
						(isBeingSelected && actions[i].iEventList[0] == Constants.ON_CHECKED) || 
						(false == isBeingSelected && actions[i].iEventList[0] == Constants.ON_UNCHECKED)) {
						Action a = new Action();
						a.execute(getContext(), listView, listItemView, actions[i]);
					}
				}
			}
        });
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, 
				controlModel.sBackgroundImage);
//		if (controlModel.sBackgroundImage != null)
//			ImageManager.getInstance().UpdateView(this, controlModel.sBackgroundImage, true);
        return Boolean.valueOf(true);
	}

	public String getValue() {
		return (null == sSelectedValue || sSelectedValue.equalsIgnoreCase("no selection")) ? null : sSelectedValue;
	}
}
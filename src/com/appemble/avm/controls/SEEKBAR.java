/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.widget.SeekBar;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class SEEKBAR extends SeekBar implements ControlInterface {
	ControlModel controlModel;
	int progress = 0;
	int iMin = 0;
	
	public SEEKBAR(final Context context, final ControlModel controlObject) {
		super(context);//, null, android.R.style.Widget_ProgressBar_Large); //TODO add style in control model. set style from extended properties.
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		try {
		if (controlModel.mExtendedProperties.containsKey("min"))
			this.iMin = Integer.parseInt(controlModel.mExtendedProperties.get("min"));
		if (controlModel.mExtendedProperties.containsKey("max"))
			this.setMax(Integer.parseInt(controlModel.mExtendedProperties.get("max"))-iMin);
		} catch (NumberFormatException nfe) {
			LogManager.logError("Please give min/max values as integer for control name: " + controlModel.sName);
		}
		if (controlModel != null && controlModel.bActionYN) {
			setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
				public void onProgressChanged(SeekBar seekBar, int progress,
                        boolean fromUser) {
					Action.callAction(getContext(), seekBar, parentView, controlModel, Constants.ON_SLIDE);
				}

				public void onStartTrackingTouch(SeekBar seekBar) {
					Action.callAction(getContext(), seekBar, parentView, controlModel, Constants.ON_SLIDE_START);
				}

				public void onStopTrackingTouch(SeekBar seekBar) {
					Action.callAction(getContext(), seekBar, parentView, controlModel, Constants.ON_SLIDE_STOP);
				}
			});
		}
		return Boolean.valueOf(true);
	}

//	@Override
//	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		bOnMeasure = true;
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//		bOnMeasure = false;
//		return;
//	}

	public String getValue() {
		return Integer.toString(this.getProgress()+iMin);
	}

	public int getControlId() {
		return getId();
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setValue(Object object) {
		if (!((object instanceof String) || (object instanceof Integer)))
			return;
		int progress = 0;
		if (object instanceof String) {
			try {
				progress = Integer.parseInt((String)object);
			} catch (NumberFormatException nfe) {
				LogManager.logError("Unable to set the value for seekbar: " + controlModel.sName);
			}
		}
		else
			progress = (Integer)object;
		progress -= iMin;
		setProgress(progress);
	}
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Arrays;
import java.util.Vector;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.TableDao;

public class SINGLE_CHOICE_DIALOG extends PUSHBUTTON {
	private String[] aCodes = null;
	private String[] aDisplayItems = null;
	private String sTitle = null;
	private int iItemSelected = -1;
	
	public SINGLE_CHOICE_DIALOG(final Context context, final ControlModel controlObject) {
		super(context, controlObject);		
		controlModel = controlObject;		
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		Context screenContext = this.getContext();
		final AppembleActivity appembleActivity = (AppembleActivity) screenContext;
		if (!(appembleActivity instanceof AppembleActivity)) {
			LogManager.logWTF(Constants.MISSING_ACTIVITY + " Control Name: " + controlModel.sName);
			return Boolean.valueOf(false);
		}

		setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String sSelectedText = getValue();
				String sDisplayValues = null;
				String sStorageValues = null;
				String sValuesQuery = null;		
				if (controlModel.mExtendedProperties != null) {
					sDisplayValues = controlModel.mExtendedProperties.get(Constants.DISPLAY_VALUES);
					sStorageValues = controlModel.mExtendedProperties.get(Constants.STORAGE_VALUES);
					sValuesQuery = controlModel.mExtendedProperties.get(Constants.VALUES_QUERY);
				}
				if (getItems(sDisplayValues, sStorageValues, sValuesQuery, sSelectedText) > 0) {
					AlertDialog.Builder builder = new AlertDialog.Builder((Context) appembleActivity);
					builder.setTitle(sTitle);
					builder.setSingleChoiceItems(aDisplayItems, iItemSelected, choiceListener) // attach the single choice listener
						   .setPositiveButton(android.R.string.ok, okListener) // attach the OK listener
						   .setNegativeButton(android.R.string.cancel, cancelListener) // attach the cancel listener
						   .create() // create the dialog box
						   .show();  // show the dialog box.
				} else {
					ActionModel[] actions = controlModel.getActions(LIST.ON_EMPTY_LIST);
					if (null != actions)
					for (int i = 0; i < actions.length; i++) {
						if (actions[i].iPrecedingActionId == 0) {
							Action a = new Action();
							a.execute(getContext(), v, parentView, actions[i]);
						}
					}
				}
			}
		});
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		AppearanceManager.getInstance().updateTextAppearance(this, controlModel, Constants.APPEARANCE_ID_NOT_SET);
		return Boolean.valueOf(true);
	}

	// source1 == storage_values, source2 = display_values
	// title, display_values, storage_values, query => must contain two columns.
	// display_values is a MUST field. If storage_values is not there, display_values are taken as storage_values.
	// When a values_query is given. first Column is display_values and 2nd column is storage values
	private int getItems(String sDisplayValues, String sStorageValues, String sQuery, String sSelectedText) {
    	sTitle = controlModel.mExtendedProperties != null ? 
    			controlModel.mExtendedProperties.get(Constants.TITLE_STR) : null;
		if (null == sDisplayValues && null == sQuery)
			return -1;		

		String[] sTokens;
    	int i = 0; String sToken = null;
    	if (null != sQuery && Utilities.isQuery(sQuery)) { // execute the query and append all values to vItems.
			String[][] aaItems = TableDao.getRowsFromDbAsStrings(controlModel.sContentDbName, sQuery, null);
			if (null != aaItems) {
				int j = 0;
				if (aaItems[0].length >= 2) { // means both code and display value has been retrieved
					aCodes = new String[aaItems.length]; aDisplayItems = new String[aaItems.length];
					for (String[] value : aaItems) {
						aDisplayItems[j] = value[0]; // index 0 is display value.
						aCodes[j] = value[1]; 		 // index 1 is storage value
						if (null == value[0] || null == value[1])
							LogManager.logError("The query: '" + sQuery + "' returned a null value. This control may not work: " + controlModel.sName);
					    if (null != sSelectedText && null != aCodes[j] && aCodes[j].equals(sSelectedText))
					    	iItemSelected = j;
					    j++;						
					}
					return aDisplayItems.length;
				} else if (aaItems[0].length == 1) { // means only the display values been retrieved. The storage items may be in storage_values field
					aDisplayItems = new String[aaItems.length];
					for (String[] value : aaItems)
						aDisplayItems[j++] = value[0];
				}
			}
    	} else { // the display values are in sDisplayValues
			sTokens = Utilities.splitCommaSeparatedString(sDisplayValues);
	    	if (sTokens.length == 0)
	    		return sTokens.length;
	    	// for each token, 
	    		// if query, run the query append the results into aCodes (and possible aDisplayItems)
	    		// else append the token into aCodes
			for (i = 0; i < sTokens.length; i++) { 
				sToken = sTokens[i].trim();
	        	if (sToken.charAt(0) == '\"') {
					int iLength = sToken.length();
					if (sToken.charAt(iLength - 1) == '\"')
						sTokens[i] = sToken.substring(1, iLength - 1).trim();
					else
						sTokens[i] = sToken.substring(1).trim();
	        	}
	
				if (null == aDisplayItems)
					aDisplayItems = new String[sTokens.length];
				aDisplayItems[i] = (sTokens[i] != null) ? sTokens[i].trim() : null;
			}
    	}
		// Get the storage items from storage_values
		if (null != sStorageValues && sStorageValues.length() > 0) {
	    	sTokens = Utilities.splitCommaSeparatedString(sStorageValues);
			for (i = 0; i < sTokens.length; i++) { 
				sToken = sTokens[i].trim();
	        	if (sToken.charAt(0) == '\"') {
					int iLength = sToken.length();
					if (sToken.charAt(iLength - 1) == '\"')
						sTokens[i] = sToken.substring(1, iLength - 1).trim();
					else
						sTokens[i] = sToken.substring(1).trim();
	        	}
				if (null == aCodes)
					aCodes = new String[sTokens.length];
				aCodes[i] = sTokens[i].trim();
			}
		}
		if (null == aDisplayItems || aDisplayItems.length == 0) {
			LogManager.logError("While creating SINGLE_CHOICE_DIALIG: " + controlModel.sName + 
					", the display values are missing.");
			return -1;
		}
		if (null == aCodes)
			aCodes = new String[aDisplayItems.length];
		i = 0;
		for (String displayItem : aDisplayItems) {
			if (aCodes[i] == null) // if code is not available in storage_values then it is same as aDisplayItems
				aCodes[i] = aDisplayItems[i];
		    if (null != sSelectedText && null != displayItem && aCodes[i].equals(sSelectedText)) // set the current item selected.
		    	iItemSelected = i;
		    i++;
		}
		return aDisplayItems.length;
	}
	
	private DialogInterface.OnClickListener choiceListener = new DialogInterface.OnClickListener() {
	    public void onClick(DialogInterface dialog, int item) {
	        // Toast.makeText(MLCache.context, items[item], Toast.LENGTH_SHORT).show();
	    	iItemSelected = item;
	    }
	};
	
	private DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
        	if (iItemSelected >= 0 && iItemSelected < aDisplayItems.length)
        		setValue(aDisplayItems[iItemSelected]);
	        dialog.dismiss();
        }
    };
    
    private DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
	        dialog.dismiss();
        }
    };
    
	public String getValue() {
		// TODO Auto-generated method stub
		if (null != aCodes && iItemSelected >= 0 && iItemSelected < aCodes.length)
			return aCodes[iItemSelected];
		return null;
	}
	
	// This function will receive storate_value when the value is being set from setValue. It will receive display value if the 
	// value is being set from the Single Choice Dialog
	public void setValue(Object object) {
		String sText = null;
		if (null == object)
			sText = controlModel.sDefaultValue;  
		else if (!(object instanceof String))
			return;
		else
			sText = (String) object;
		// Show the default value. Because localDataSource is always present, the appembleActivity will pass
		// the local data source. However, we should show the default value instead.
//		if (null != sText && sText.length() > 0) {
//			if (false == sText.equals(controlModel.sDefaultValue)) {
				if (null == aDisplayItems || aDisplayItems.length == 0 || iItemSelected == -1 || 
					aDisplayItems.length < iItemSelected) {
					String sDisplayValues = null;
					String sStorageValues = null;
					String sValuesQuery = null;		
					if (controlModel.mExtendedProperties != null) {
						sDisplayValues = controlModel.mExtendedProperties.get(Constants.DISPLAY_VALUES);
						sStorageValues = controlModel.mExtendedProperties.get(Constants.STORAGE_VALUES);
						sValuesQuery = controlModel.mExtendedProperties.get(Constants.VALUES_QUERY);
					}
					if (getItems(sDisplayValues, sStorageValues, sValuesQuery, sText) <= 0)
						return;
				}
				iItemSelected = Arrays.asList(aCodes).indexOf(sText);
				if (iItemSelected < 0)
					iItemSelected = Arrays.asList(aDisplayItems).indexOf(sText);
				String sDisplayText = null;
				if (iItemSelected >= 0 && iItemSelected < aDisplayItems.length)
					sDisplayText = aDisplayItems[iItemSelected];
//			}
//		}
		super.setText(sDisplayText);
	}	
}
/*
 * HorizontalListView.java v1.5
 *
 * 
 * The MIT License
 * Copyright (c) 2011 Paul Soucy (paul@dev-smart.com)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package com.appemble.avm.controls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Scroller;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dataconnector.ListArrayAdaptor;
import com.appemble.avm.dataconnector.ListCursorAdaptor;
import com.appemble.avm.dataconnector.ListJsonAdaptor;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.DynamicActivity;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class HORIZONTAL_LIST extends AdapterView<ListAdapter> implements ControlInterface, AVMGroupViewInterface {

	public boolean mAlwaysOverrideTouch = true;
	protected ListAdapter mAdapter;
	private int mLeftViewIndex = -1;
	private int mRightViewIndex = 0;
	protected int mCurrentX;
	protected int mNextX;
	private int mMaxX = Integer.MAX_VALUE;
	private int mDisplayOffset = 0;
	protected Scroller mScroller;
	private GestureDetector mGesture;
	private Queue<View> mRemovedViewQueue = new LinkedList<View>();
	private OnItemSelectedListener mOnItemSelected;
	private OnItemClickListener mOnItemClicked;
	private OnItemLongClickListener mOnItemLongClicked;
	private boolean mDataChanged = false;
	
    ControlModel controlModel;
    public Vector<ControlModel> vControls;
    ScreenModel screenModel;
    public boolean bOnMeasure = false;
    public static final int ON_CREATE_LIST_ITEM = "ON_CREATE_LIST_ITEM".hashCode();
    public static final int ON_EMPTY_LIST = "ON_EMPTY_LIST".hashCode();
    float fWidth, fHeight; // Using which the child control dimensions are to be calculated.

    public HORIZONTAL_LIST(final Context context, ControlModel controlObject) {
        super(context);
        controlModel = controlObject;       
        initView();
    }
  
	public HORIZONTAL_LIST(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView();
	}
	
	private synchronized void initView() {
		mLeftViewIndex = -1;
		mRightViewIndex = 0;
		mDisplayOffset = 0;
		mCurrentX = 0;
		mNextX = 0;
		mMaxX = Integer.MAX_VALUE;
		mScroller = new Scroller(getContext());
		mGesture = new GestureDetector(getContext(), mOnGesture);
	}
	
	@Override
	public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener listener) {
		mOnItemSelected = listener;
	}
	
	@Override
	public void setOnItemClickListener(AdapterView.OnItemClickListener listener){
		mOnItemClicked = listener;
	}
	
	@Override
	public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener listener) {
		mOnItemLongClicked = listener;
	}

	private DataSetObserver mDataObserver = new DataSetObserver() {

		@Override
		public void onChanged() {
			synchronized(HORIZONTAL_LIST.this){
				mDataChanged = true;
			}
			invalidate();
			requestLayout();
		}

		@Override
		public void onInvalidated() {
			reset();
			invalidate();
			requestLayout();
		}
		
	};

	@Override
	public ListAdapter getAdapter() {
		return mAdapter;
	}

	@Override
	public View getSelectedView() {
		//TODO: implement
		return null;
	}

	@Override
	public void setAdapter(ListAdapter adapter) {
		if(mAdapter != null) {
			mAdapter.unregisterDataSetObserver(mDataObserver);
		}
		mAdapter = adapter;
		mAdapter.registerDataSetObserver(mDataObserver);
		reset();
	}
	
	private synchronized void reset(){
		initView();
		removeAllViewsInLayout();
        requestLayout();
	}

	@Override
	public void setSelection(int position) {
		//TODO: implement
	}
	
	private void addAndMeasureChild(final View child, int viewPos) {
		LayoutParams params = child.getLayoutParams();
		if(params == null) {
			params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		}

		addViewInLayout(child, viewPos, params, true);
		child.measure(MeasureSpec.makeMeasureSpec(getWidth(), MeasureSpec.AT_MOST),
				MeasureSpec.makeMeasureSpec(getHeight(), MeasureSpec.AT_MOST));
	}
	
	

	@Override
	protected synchronized void onLayout(boolean changed, int left, int top, int right, int bottom) {
		super.onLayout(changed, left, top, right, bottom);

		if(mAdapter == null){
			return;
		}
		
		if(mDataChanged){
			int oldCurrentX = mCurrentX;
			initView();
			removeAllViewsInLayout();
			mNextX = oldCurrentX;
			mDataChanged = false;
		}

		if(mScroller.computeScrollOffset()){
			int scrollx = mScroller.getCurrX();
			mNextX = scrollx;
		}
		
		if(mNextX <= 0){
			mNextX = 0;
			mScroller.forceFinished(true);
		}
		if(mNextX >= mMaxX) {
			mNextX = mMaxX;
			mScroller.forceFinished(true);
		}
		
		int dx = mCurrentX - mNextX;
		
		removeNonVisibleItems(dx);
		fillList(dx);
		positionItems(dx);
		
		mCurrentX = mNextX;
		
		if(!mScroller.isFinished()){
			post(new Runnable(){
				@Override
				public void run() {
					requestLayout();
				}
			});
			
		}
	}
	
	private void fillList(final int dx) {
		int edge = 0;
		View child = getChildAt(getChildCount()-1);
		if(child != null) {
			edge = child.getRight();
		}
		fillListRight(edge, dx);
		
		edge = 0;
		child = getChildAt(0);
		if(child != null) {
			edge = child.getLeft();
		}
		fillListLeft(edge, dx);
		
		
	}
	
	private void fillListRight(int rightEdge, final int dx) {
		while(rightEdge + dx < getWidth() && mRightViewIndex < mAdapter.getCount()) {
			
			View child = mAdapter.getView(mRightViewIndex, mRemovedViewQueue.poll(), this);
			addAndMeasureChild(child, -1);
			rightEdge += child.getMeasuredWidth();
			
			if(mRightViewIndex == mAdapter.getCount()-1) {
				mMaxX = mCurrentX + rightEdge - getWidth();
			}
			
			if (mMaxX < 0) {
				mMaxX = 0;
			}
			mRightViewIndex++;
		}
		
	}
	
	private void fillListLeft(int leftEdge, final int dx) {
		while(leftEdge + dx > 0 && mLeftViewIndex >= 0) {
			View child = mAdapter.getView(mLeftViewIndex, mRemovedViewQueue.poll(), this);
			addAndMeasureChild(child, 0);
			leftEdge -= child.getMeasuredWidth();
			mLeftViewIndex--;
			mDisplayOffset -= child.getMeasuredWidth();
		}
	}
	
	private void removeNonVisibleItems(final int dx) {
		View child = getChildAt(0);
		while(child != null && child.getRight() + dx <= 0) {
			mDisplayOffset += child.getMeasuredWidth();
			mRemovedViewQueue.offer(child);
			removeViewInLayout(child);
			mLeftViewIndex++;
			child = getChildAt(0);
			
		}
		
		child = getChildAt(getChildCount()-1);
		while(child != null && child.getLeft() + dx >= getWidth()) {
			mRemovedViewQueue.offer(child);
			removeViewInLayout(child);
			mRightViewIndex--;
			child = getChildAt(getChildCount()-1);
		}
	}
	
	private void positionItems(final int dx) {
		if(getChildCount() > 0){
			mDisplayOffset += dx;
			int left = mDisplayOffset;
			for(int i=0;i<getChildCount();i++){
				View child = getChildAt(i);
				int childWidth = child.getMeasuredWidth();
				child.layout(left, 0, left + childWidth, child.getMeasuredHeight());
				left += childWidth + child.getPaddingRight();
			}
		}
	}
	
	public synchronized void scrollTo(int x) {
		mScroller.startScroll(mNextX, 0, x - mNextX, 0);
		requestLayout();
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		boolean handled = super.dispatchTouchEvent(ev);
		handled |= mGesture.onTouchEvent(ev);
		return handled;
	}
	
	protected boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
		synchronized(HORIZONTAL_LIST.this){
			mScroller.fling(mNextX, 0, (int)-velocityX, 0, 0, mMaxX, 0, 0);
		}
		requestLayout();
		
		return true;
	}
	
	protected boolean onDown(MotionEvent e) {
		mScroller.forceFinished(true);
		return true;
	}
	
	private OnGestureListener mOnGesture = new GestureDetector.SimpleOnGestureListener() {

		@Override
		public boolean onDown(MotionEvent e) {
			return HORIZONTAL_LIST.this.onDown(e);
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			return HORIZONTAL_LIST.this.onFling(e1, e2, velocityX, velocityY);
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			
			synchronized(HORIZONTAL_LIST.this){
				mNextX += (int)distanceX;
			}
			requestLayout();
			
			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e) {
			for(int i=0;i<getChildCount();i++){
				View child = getChildAt(i);
				if (isEventWithinView(e, child)) {
					if(mOnItemClicked != null){
						mOnItemClicked.onItemClick(HORIZONTAL_LIST.this, child, mLeftViewIndex + 1 + i, mAdapter.getItemId( mLeftViewIndex + 1 + i ));
					}
					if(mOnItemSelected != null){
						mOnItemSelected.onItemSelected(HORIZONTAL_LIST.this, child, mLeftViewIndex + 1 + i, mAdapter.getItemId( mLeftViewIndex + 1 + i ));
					}
					break;
				}
				
			}
			return true;
		}
		
		@Override
		public void onLongPress(MotionEvent e) {
			int childCount = getChildCount();
			for (int i = 0; i < childCount; i++) {
				View child = getChildAt(i);
				if (isEventWithinView(e, child)) {
					if (mOnItemLongClicked != null) {
						mOnItemLongClicked.onItemLongClick(HORIZONTAL_LIST.this, child, mLeftViewIndex + 1 + i, mAdapter.getItemId(mLeftViewIndex + 1 + i));
					}
					break;
				}

			}
		}

		private boolean isEventWithinView(MotionEvent e, View child) {
            Rect viewRect = new Rect();
            int[] childPosition = new int[2];
            child.getLocationOnScreen(childPosition);
            int left = childPosition[0];
            int right = left + child.getWidth();
            int top = childPosition[1];
            int bottom = top + child.getHeight();
            viewRect.set(left, top, right, bottom);
            return viewRect.contains((int) e.getRawX(), (int) e.getRawY());
        }
	};

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        bOnMeasure = true;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        bOnMeasure = false;
        return;
    }
    

    @Override
    public int getControlId() {
        return controlModel.id;
    }
  
    @Override
    public ControlModel getControlModel() {
        return controlModel;
    }
  
    @Override
    public Object getValue() {
        return null;
    }

    @Override
    public void setValue(Object object) {
        // If called from DynamicActivity::UpdateControl, then
        // this value is obtained in the following order remote data source, local data source, remote data, 
        // local cursor, target parameter list and default value.
        if (bOnMeasure)
            return;
        try {
            if (controlModel.iDataSourceType == Constants.JSONARRAY) {
                JSONArray jsonArray = null;
                if (null != object)
                    jsonArray = new JSONArray((String)object);
                setJson(jsonArray);
            }
            else if (controlModel.iDataSourceType == Constants.ARRAYLIST)
                setArrayList(object);
            else if (controlModel.iDataSourceType == Constants.CURSOR) 
                setCursor((Cursor) object);
        } catch (JSONException jsone) {
            LogManager.logError("The control: " + controlModel.sName + " expects data type as JSONArray"); 
        } catch (ClassCastException cce) {
            LogManager.logError("The control: " + controlModel.sName + " expects data type as " + ConfigManager.getInstance().getDataTypesString(controlModel.iDataSourceType));
        }
    }

    public boolean setCursor(Cursor c) {
        // The cursor can be null. Which means that the data is being removed from the list box
        ListCursorAdaptor adaptor = (ListCursorAdaptor)getAdapter();
        if (null == adaptor) { // the constructor does not set the adaptor.
            adaptor = new ListCursorAdaptor(getContext(), this, c, controlModel, vControls, fWidth,
                    fHeight, screenModel);
            setAdapter(adaptor); 
        } else {
            adaptor.changeCursor(c);
            adaptor.notifyDataSetChanged();
        }
        return showEmptyListControls(null == c || c.getCount() == 0);
    }

    public boolean setArrayList(Object object) {
        // here parse the string to formulate the multidimensional array. The order of the controls in the list box 
        // determine the order in which they get the value from this multidimensional array.
        
        // String [] aValues = sValues.split(",");
        ArrayList<HashMap<String, Object>> aValues = new ArrayList<HashMap<String, Object>>();
        // to represent multidimensional array, use HashMap<columm name, value> in an ArrayList
        // ArrayList<HashMap<String, String>> aValues = new ArrayList<HashMap<String, String>>();
        if (null == aValues || aValues.size()== 0)
            return false;
        ListArrayAdaptor adaptor = (ListArrayAdaptor)getAdapter();
        if (null == adaptor) { // the constructor does not set the adaptor.
            adaptor = new ListArrayAdaptor(getContext(), this, aValues, controlModel, vControls, fWidth,
                    fHeight, screenModel);
            setAdapter(adaptor); 
        } else {
            adaptor.changeDataset(aValues);
            adaptor.notifyDataSetChanged();
        }
        return showEmptyListControls(null == aValues || aValues.size() == 0);
    }
    
    public boolean setJson(Object object) {     
        JSONArray jsonArray = (JSONArray) object;
        ListJsonAdaptor adaptor = (ListJsonAdaptor)getAdapter();
        if (null == adaptor) { // the constructor does not set the adaptor.
            adaptor = new ListJsonAdaptor(getContext(), this, jsonArray, controlModel, vControls, fWidth,
                    fHeight, screenModel);
            setAdapter(adaptor); 
        } else {
            adaptor.changeDataset(jsonArray);
            final ListJsonAdaptor a = adaptor;
            AppembleActivity appembleActivity = Utilities.getActivity(getContext(), this, null);
            if (appembleActivity instanceof DynamicActivity) {
                DynamicActivity dynamicActivity = (DynamicActivity) appembleActivity;
                dynamicActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        a.notifyDataSetChanged();
                    }
                });         
            }
        }
        return showEmptyListControls(null == jsonArray || jsonArray.length() == 0);
    }
    
    private boolean showEmptyListControls(boolean bVisibility) {
    	return AppembleActivityImpl.showEmptyListControls(this, controlModel, this.getContext(), bVisibility);
    }

    public Object initialize(View parentView, float fParentWidth, float fParentHeight,
        Vector<ControlModel> vChildControls, ScreenModel screenModel) {
        if (null == controlModel)
          return Boolean.valueOf(false);
    
        vControls = vChildControls;
        this.screenModel = screenModel;
        if (Cache.bDimenRelativeToParent) {
            PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
            fWidth = size.x; fHeight = size.y;
        } else {
            fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
        }
      
        // Data binding done in Dynamic Activity. See that file for  more details.
        if (this instanceof HORIZONTAL_LIST)
        this.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> listView, View listItemView, int arg2, long arg3) {
                Action.callAction(getContext(), listView, listItemView, controlModel, Constants.TAP); 
            }
        });
        AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, 
                controlModel.sBackgroundImage);
        if (null != controlModel.sDefaultValue)
            setValue(controlModel.sDefaultValue);

		AppembleActivityImpl.addEmptyControlIds(controlModel, screenModel, null);
        return Boolean.valueOf(true);
    }

    public void onDestroy(Bundle targetParameterValueList) {
        // No need to close the cursor. Android is doing OK in closing the cursor.
        ListAdapter listAdapter = getAdapter();
        if (null == listAdapter)
            return;
        if (listAdapter instanceof ListAdapter) {
            ListCursorAdaptor adaptor = (ListCursorAdaptor)listAdapter;
            Cursor c = adaptor.getCursor();
            if (c != null && false == c.isClosed())
                c.close();
        }
    }

    @Override
    public Boolean isBeingMeasured() {
      return bOnMeasure;
    }
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.widget.ToggleButton;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class TOGGLEBUTTON extends ToggleButton implements ControlInterface{
	public static String TEXT_ON = "text_on";
	public static String TEXT_OFF = "text_off";
	
	ControlModel controlModel;
//    boolean bOnMeasure = false;
	String sImageSource;
	
	public TOGGLEBUTTON(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;		
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight, 
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		setFocusable(true);
		String sTextOn = null, sTextOff = null;
		if (controlModel.mExtendedProperties != null) {
			sTextOn = controlModel.mExtendedProperties.get(TEXT_ON);
			sTextOff = controlModel.mExtendedProperties.get(TEXT_OFF);
		}
			
		if (null != sTextOn) { // text_on="ON" text_off="OFF"
			setTextOn(sTextOn);
			setTextOff(sTextOff);
		} else { // text_on="ON" text_off="OFF"
			setTextOn("On");
			setTextOff("Off");
		}
			
		if (null != screenModel.sInitialFocus && screenModel.sInitialFocus.equals(controlModel.sName))
			requestFocus();
		if(controlModel.bActionYN){
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					ActionModel[] actions = controlModel.getActions();
					if (null != actions)
					for (int i = 0; i < actions.length; i++) {
						if (actions[i].iEventList[0] == Constants.TAP || 
							(((ToggleButton)v).isChecked() == true && actions[i].iEventList[0] == Constants.ON_CHECKED) || 
							(((ToggleButton)v).isChecked() == false && actions[i].iEventList[0] == Constants.ON_UNCHECKED)) {
							Action a = new Action();
							a.execute(getContext(), v, parentView, actions[i]);
						}
					}
				}
			});
		}
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		AppearanceManager.getInstance().updateTextAppearance(this, controlModel, Constants.APPEARANCE_ID_NOT_SET);
		return Boolean.valueOf(true);
	}

	public int getControlId() {
		return controlModel.id;
	}
	
	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return Boolean.toString(isChecked());
	}

	public void setValue(Object object) {
	    if (null == controlModel)
	    	return;
		// Clear the control...
		boolean bChecked = false;
	    if (null == object) {
	    	if (controlModel.sDefaultValue != null)
	    		bChecked = Utilities.parseBoolean(controlModel.sDefaultValue);
	    }
	    else if (object instanceof Boolean)
			bChecked = (Boolean)object;
		else if (object instanceof String)
			bChecked = Utilities.parseBoolean((String)object);
		setChecked(bChecked);		
	}
}
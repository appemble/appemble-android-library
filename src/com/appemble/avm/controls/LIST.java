/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dataconnector.ListArrayAdaptor;
import com.appemble.avm.dataconnector.ListCursorAdaptor;
import com.appemble.avm.dataconnector.ListJsonAdaptor;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.DynamicActivity;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class LIST extends ListView implements ControlInterface {
	ControlModel controlModel;
	public Vector<ControlModel> vControls;
	ScreenModel screenModel;
	public boolean bOnMeasure = false;
	public static final int ON_CREATE_LIST_ITEM = "ON_CREATE_LIST_ITEM".hashCode();
	public static final int BEFORE_ON_CREATE_LIST_ITEM = "BEFORE_ON_CREATE_LIST_ITEM".hashCode();
	public static final int ON_EMPTY_LIST = "ON_EMPTY_LIST".hashCode();
	float fWidth, fHeight; // Using which the child control dimensions are to be calculated.
	
	public LIST(final Context context, ControlModel controlObject) {
		super(context);
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel scrModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);

		vControls = vChildControls;
		screenModel = scrModel;
		if (Cache.bDimenRelativeToParent) {
			PointF size = DynamicLayout.calculateSize(controlModel, fParentWidth, fParentHeight);
			fWidth = size.x; fHeight = size.y;
		} else {
			fWidth = screenModel.fWidthInPixels; fHeight = screenModel.fHeightInPixels;
		}
		
		// Data binding done in Dynamic Activity. See that file for  more details.
		setCacheColorHint(controlModel);
        if (this instanceof LIST)
        this.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> listView, View listItemView, int arg2, long arg3) {
				Action.callAction(getContext(), listView, listItemView, controlModel, Constants.TAP); 
			}
        });
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, 
				controlModel.sBackgroundImage);
		
		AppembleActivityImpl.addEmptyControlIds(controlModel, screenModel, null);
		return Boolean.valueOf(true);
	}

	@Override
	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		bOnMeasure = true;
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		bOnMeasure = false;
		return;
	}
	
    public void onDestroy(Bundle targetParameterValueList) {
        // No need to close the cursor. Android is doing OK in closing the cursor.
		ListAdapter listAdapter = getAdapter();
		if (null == listAdapter)
			return;
		if (listAdapter instanceof ListCursorAdaptor) {
			ListCursorAdaptor adaptor = (ListCursorAdaptor)listAdapter;
			Cursor c = adaptor.getCursor();
			if (c != null && false == c.isClosed())
				c.close();
		}
	}

	public int getControlId() {
		return controlModel.id;
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setControlAppearance() {
		// TODO Auto-generated method stub
		
	}

	public Object getValue() {
		return Integer.valueOf(this.getCount());
	}
	
	public void setValue(Object object) {
		// If called from DynamicActivity::UpdateControl, then
		// this value is obtained in the following order remote data source, local data source, remote data, 
		// local cursor, target parameter list and default value.
		// candidate for removal 20120330 HB if (null == sText)
		//	sText = controlModel.sDefaultValue;
		if (bOnMeasure)
			return;
		try {
			if (controlModel.iDataSourceType == Constants.JSONARRAY) {
				JSONArray jsonArray = null;
				if (null != object)
					jsonArray = new JSONArray((String)object);
				setJson(jsonArray);
			}
		    else if (controlModel.iDataSourceType == Constants.ARRAYLIST)
	        	setArrayList(object);
			else if (controlModel.iDataType == Constants.CURSOR || controlModel.iDataSourceType == Constants.CURSOR) 
				setCursor((Cursor) object);
		} catch (JSONException jsone) {
			LogManager.logError("The control: " + controlModel.sName + " expects data type as JSONArray"); 
		} catch (ClassCastException cce) {
			LogManager.logError("The control: " + controlModel.sName + " expects data type as " + ConfigManager.getInstance().getDataTypesString(controlModel.iDataType));
		}
	}
	
	public boolean setCursor(Cursor c) {
		// The cursor can be null. Which means that the data is being removed from the list box
		ListCursorAdaptor adaptor = (ListCursorAdaptor)getAdapter();
		if (null == adaptor) { // the constructor does not set the adaptor.
			adaptor = new ListCursorAdaptor(getContext(), this, c, controlModel, vControls, fWidth,
					fHeight, screenModel);
	        setAdapter(adaptor); 
		} else {
			adaptor.changeCursor(c);
			adaptor.notifyDataSetChanged();
		}
		return showEmptyListControls(null == c || c.getCount() == 0);
	}

	public boolean setArrayList(Object object) {
		// here parse the string to formulate the multidimensional array. The order of the controls in the list box 
		// determine the order in which they get the value from this multidimensional array.
		
		// String [] aValues = sValues.split(",");
		ArrayList<HashMap<String, Object>> aValues = new ArrayList<HashMap<String, Object>>();
		// to represent multidimensional array, use HashMap<columm name, value> in an ArrayList
		// ArrayList<HashMap<String, String>> aValues = new ArrayList<HashMap<String, String>>();
		if (null == aValues || aValues.size()== 0)
			return false;
		ListArrayAdaptor adaptor = (ListArrayAdaptor)getAdapter();
		if (null == adaptor) { // the constructor does not set the adaptor.
			adaptor = new ListArrayAdaptor(getContext(), this, aValues, controlModel, vControls, fWidth,
					fHeight, screenModel);
	        setAdapter(adaptor); 
		} else {
			adaptor.changeDataset(aValues);
			adaptor.notifyDataSetChanged();
		}
		return showEmptyListControls(null == aValues || aValues.size() == 0);
	}
	
	public boolean setJson(Object object) {		
		JSONArray jsonArray = (JSONArray) object;
		ListJsonAdaptor adaptor = (ListJsonAdaptor)getAdapter();
		if (null == adaptor) { // the constructor does not set the adaptor.
			adaptor = new ListJsonAdaptor(getContext(), this, jsonArray, controlModel, vControls, fWidth,
					fHeight, screenModel);
	        setAdapter(adaptor); 
		} else {
			adaptor.changeDataset(jsonArray);
			final ListJsonAdaptor a = adaptor;
			AppembleActivity appembleActivity = Utilities.getActivity(getContext(), this, null);
			if (appembleActivity instanceof DynamicActivity) {
				DynamicActivity dynamicActivity = (DynamicActivity) appembleActivity;
				dynamicActivity.runOnUiThread(new Runnable() {
				    public void run() {
				        a.notifyDataSetChanged();
				    }
				});			
			}
		}
		return showEmptyListControls(null == jsonArray || jsonArray.length() == 0);
	}

    private boolean showEmptyListControls(boolean bVisibility) {
    	return AppembleActivityImpl.showEmptyListControls(this, controlModel, this.getContext(), bVisibility);
    }
	
	protected void setCacheColorHint(ControlModel controlModel) {
		int iCacheColorHint = Color.TRANSPARENT;
		try {
			String sCacheColorHint = controlModel.mExtendedProperties.get("cache_color_hint");
			if (null != sCacheColorHint)
				iCacheColorHint = Color.parseColor(sCacheColorHint);
		} catch (IllegalArgumentException iae) {
			LogManager.logError("Unable to parse cache_color_hint");
		} finally {
	        this.setCacheColorHint(iCacheColorHint);			
		}
	}
}
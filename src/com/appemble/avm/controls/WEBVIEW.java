/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.http.SslError;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

@SuppressLint("SetJavaScriptEnabled")
public class WEBVIEW extends WebView implements ControlInterface {
	public static String JAVASCRIPT = "javascript"; 
	ControlModel controlModel;
	int iExistingHashCode;

	public WEBVIEW(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		setClickable(true);
		setVerticalScrollBarEnabled(true); // TODO: should this be a parameter in the control table?
		setHorizontalScrollBarEnabled(true); // TODO: should this be a parameter in the control table?
		setVerticalScrollbarOverlay(true);
		setHorizontalScrollbarOverlay(true);
		setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		boolean bJavaScript = Utilities.parseBoolean(controlModel.mExtendedProperties.get(JAVASCRIPT));
		if (bJavaScript)
			getSettings().setJavaScriptEnabled(true);
		if (controlModel.mExtendedProperties.containsKey("web_view_client")) {
			String sClassName = controlModel.mExtendedProperties.get("web_view_client");
			try {
				Class<?> cls = Class.forName(sClassName);
				if (null == cls) {
					LogManager.logWTF("Cannot get WebViewClient. Unable to find implementation class for and class name: " + 
							sClassName);
					return Boolean.valueOf(false);
				}
				Constructor<?> constructor = cls.getDeclaredConstructor();
				if (null == constructor) {
					LogManager.logWTF("Cannot create control. The constructor in the implementation class is missing. Class Name:" + sClassName);
					return Boolean.valueOf(false);
				}
				WebViewClient wvc = (WebViewClient)constructor.newInstance();
				if (null != wvc)
					setWebViewClient(wvc);
				else {
					LogManager.logWTF("Cannot get WebViewClient. Unable to instantiate WebViewClient for class name: " + 
						sClassName);
					return Boolean.valueOf(false);
				}
			
			} catch (ClassNotFoundException cnfe) {
				LogManager.logWTF(cnfe, "Unable to create WebVew control. Implementation class not found for WebViewClient: " + sClassName);
				return Boolean.valueOf(false);
			} catch (NoSuchMethodException nsme) {
				LogManager.logWTF(nsme, "Unable to create WevViewClient. Constructor missing from the implementation class: " + sClassName);
				return Boolean.valueOf(false);
			} catch (InstantiationException ie) {
				LogManager.logWTF(ie, "Unable to create WebViewClient. Cannot create instance of the WebViewClient: " + sClassName);
			} catch (IllegalAccessException iae) {
				LogManager.logWTF(iae, "Unable to create WebViewClient. Cannot access the constructor of the class: " + sClassName);
			} catch (InvocationTargetException iae) {
				LogManager.logWTF(iae, "Unable to create WebViewClient. Cannot instantiate the WebViewClient with class: " + sClassName);
			}
		} else
			setWebViewClient(new WebViewClient(){
			    @Override
			    public boolean shouldOverrideUrlLoading(WebView view, String url){
			      view.loadUrl(url);
			      return true;
			    }
			    
			    @Override
			    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
					Boolean bIgnoreErrors = Utilities.parseBoolean(
							controlModel.mExtendedProperties.get("ignore_ssl_errors"));
			    	if (bIgnoreErrors)
			    		handler.proceed(); // Ignore SSL certificate errors
			    }
			});
		
		if (controlModel.bActionYN) {
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
				}
			});
		}
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		return Boolean.valueOf(true);
	}

	public int getControlId() {
		return controlModel.id;
	}
	
	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return null;
	}

	public void setValue(Object object) {
		if (object == null) { // clear the control
			this.loadData(null, "text/html", null);
			iExistingHashCode = 0;
			return;
		}
		String sText = (String) object;
		int iNewHashCode = sText.hashCode();
		if (iNewHashCode == iExistingHashCode)
			return; // nothing changed
		else
			iExistingHashCode = iNewHashCode;
		if (sText.startsWith(Constants.URL_IMAGE_PREFIX))
			AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null, sText);
		else if (Utilities.isUrl(sText))
			loadUrl(sText);
		else
			this.loadDataWithBaseURL(null, sText, "text/html", "UTF-8", "about:blank");
	}

//	private class Callback extends WebViewClient {  //HERE IS THE MAIN CHANGE. 
//		@Override
//		public boolean shouldOverrideUrlLoading(WebView view, String url) {
//			return (false);
//		}
//	}
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.graphics.Paint;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.RadioButton;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.AppearanceModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class RADIOBUTTON extends RadioButton implements ControlInterface{
	protected ControlModel controlModel;
	protected String sImageSource;
	protected String sStorageValue;
	
	public RADIOBUTTON(final Context context, final ControlModel controlObject) {
		super(context);
		controlModel=controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		
		if (!(parentView instanceof RADIOGROUP))
			return Boolean.valueOf(false);
		
		this.setSelected(false);
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
		AppearanceManager.getInstance().updateTextAppearance(this, controlModel, Constants.APPEARANCE_ID_NOT_SET);
		setLabel();
		// Call the tap action
		if(controlModel != null && controlModel.bActionYN){
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					ActionModel[] actions = controlModel.getActions();
					if (null != actions)
					for (int i = 0; i < actions.length; i++)
						if (actions[i].iEventList[0] == Constants.TAP || 
								(((RadioButton)v).isChecked() == true && actions[i].iEventList[0] == Constants.ON_CHECKED) || 
								(((RadioButton)v).isChecked() == false && actions[i].iEventList[0] == Constants.ON_UNCHECKED)) {
							Action a = new Action();
							a.execute(getContext(), v, parentView, actions[i]);
						}
				}
			});
		}
		return Boolean.valueOf(true);
	}

	public int getControlId() {
		return getId();
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public String getValue() {
		return Boolean.toString(isChecked());
	}
	public void setValue(Object object) {
	}
	
	protected void setLabel() {
		if (null == controlModel)
			return;
		String sDisplayValue = controlModel.mExtendedProperties.get("display_value");
		AppearanceModel appearanceModel = AppearanceManager.getInstance().getAppearance(controlModel.sSystemDbName,
				controlModel.iAppearanceId);
		if (null != appearanceModel && appearanceModel.bFontStyleStrikethrough)
			setPaintFlags(getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
		if (null != appearanceModel && appearanceModel.bFontStyleUnderline) {
			SpannableString content = new SpannableString(sDisplayValue);
			content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			super.setText(content);			
		} else
			super.setText(sDisplayValue);
		sStorageValue = controlModel.mExtendedProperties.get("storage_value");
		if (null == sStorageValue)
			sStorageValue = sDisplayValue;
	}
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.controls;

import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Constants;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class PROGRESSBAR extends ProgressBar implements ControlInterface {
	ControlModel controlModel;
//	boolean bOnMeasure = false;
	
	public PROGRESSBAR(final Context context, final ControlModel controlObject) {
		super(context);//, null, android.R.style.Widget_ProgressBar_Large); //TODO add style in control model. set style from extended properties.
		controlModel = controlObject;
	}
	
	public Object initialize(final View parentView, float fParentWidth, float fParentHeight,
			Vector<ControlModel> vChildControls, ScreenModel screenModel) {
		if (null == controlModel)
			return Boolean.valueOf(false);
		controlModel.bIsVisible = false; // Initially make the progress control invisible.
		this.setFocusable(false); // Do not allow focus on this control
		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);

		if (controlModel != null && controlModel.bActionYN) {
			setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Action.callAction(getContext(), v, parentView, controlModel, Constants.TAP); 
//					ActionModel[] actions = controlModel.getActions();
//					if (null != actions)
//					for (int i = 0; i < actions.length; i++)
//						if (actions[i].iPrecedingActionId == 0) {
////							new Action(getContext(), v, parentView, actions[i]);
//							Action a = new Action();
//							a.execute(getContext(), v, parentView, actions[i]);
//						}
				}
			});
		}
		return Boolean.valueOf(true);
	}

//	@Override
//	public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		bOnMeasure = true;
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//		bOnMeasure = false;
//		return;
//	}

	public String getValue() {
		return (String) getTag();
	}

	public int getControlId() {
		return getId();
	}

	public ControlModel getControlModel() {
		return controlModel;
	}

	public void setValue(Object object) {
//	    if (bOnMeasure)
//	    	return;
//		// If called from DynamicActivity::UpdateControl, then
//		// this value is obtained in the following order remote data source, local data source, remote data, 
//		// local cursor, target parameter list and default value.
//		String sAttachedImage = (String) getTag();
//		if (object == null) {
//			if (null != sAttachedImage)
//				super.setTag(null);
//			ImageManager.getInstance().UpdateView(this, null, true);
//			return;
//		}
//		String sText = (String) object;
//		if (null != sAttachedImage && sAttachedImage.equals(sText))
//			return;
//		ImageManager.getInstance().UpdateView(this, sText, true);
//		setTag(sText);
//		AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, null);
////		drawable = AppearanceManager.getInstance().updateBackgroundAppearance(this, controlModel, drawable);
	}
}

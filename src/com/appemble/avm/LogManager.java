/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import java.util.HashMap;

import android.util.Log;

public class LogManager {

	private static String addPrefix(String message) {
		StringBuffer sb = new StringBuffer();
		if (null != message)
			sb.append(message);
		sb.append(" - ");
		sb.append(Thread.currentThread().getStackTrace()[4].getClassName());
		sb.append('.');
//        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        sb.append(Thread.currentThread().getStackTrace()[4].getMethodName());
        sb.append("(): ");
        sb.append(Thread.currentThread().getStackTrace()[4].getLineNumber());		
		sb.append("\r\n");
		return sb.toString();
	}

//	private static String getPrefix() {
//		StringBuffer sb = new StringBuffer();
//		sb.append(Thread.currentThread().getStackTrace()[4].getClassName());
//		sb.append('.');
////        String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
//        sb.append(Thread.currentThread().getStackTrace()[4].getMethodName());
//        sb.append("(): ");
//        sb.append(Thread.currentThread().getStackTrace()[4].getLineNumber());		
//		sb.append("- ");
//		return sb.toString();
//	}
	
	public static void logWTF(Throwable tr, String sMessage){
		//add time with seconds & also memory
		if (Constants.isAdvancedDebugMode)
			Log.wtf(Cache.APPEMBLE, addPrefix(sMessage), tr);
		else if (Constants.isProdMode)
			Log.e(Cache.APPEMBLE, addPrefix(sMessage)+tr.toString());
	}
	
	public static void logWTF(String sMessage){
		//add time with seconds & also memory
		if (Constants.isProdMode)
			Log.wtf(Cache.APPEMBLE, addPrefix(sMessage));
	}

	public static void logError(Throwable tr, String sMessage){
		//add time with seconds & also memory
		if (Constants.isAdvancedDebugMode)
			Log.e(Cache.APPEMBLE, addPrefix(sMessage), tr);
		else if (Constants.isProdMode)
			Log.e(Cache.APPEMBLE, addPrefix(sMessage)+tr.toString());
	}

	public static void logError(String message){
		//add time with seconds & also memory
		if (Constants.isProdMode)
			Log.e(Cache.APPEMBLE, addPrefix(message));
	}

	public static void logWarn(String message){
		//add time with seconds & also memory
		if (Constants.isProdMode)
			Log.w(Cache.APPEMBLE, addPrefix(message));
	}
	
	public static void logInfo(String message){
		//add time with seconds & also memory
		if (Constants.isInfoMode)
			Log.i(Cache.APPEMBLE, addPrefix(message));
	}
	
	public static void logDebug(String message ){
		//add time with seconds & also memory		
		if (Constants.isDebugMode)
			Log.d(Cache.APPEMBLE, addPrefix(message));
	}
	
	public static void logVerbose(String message ){
		//add time with seconds & also memory		
		if (Constants.isVerboseMode)
			Log.v(Cache.APPEMBLE, addPrefix(message));
	}
	
	public HashMap<String,String> getAllErrorLogs(){
		//Call this function from JSON connector and send this to server (if not null) with every connection
		return null;		
	}
	
	public void clearLogBase(){
		
	}
}

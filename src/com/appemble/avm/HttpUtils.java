/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.loopj.android.http.AsyncHttpClient;

// Source http://foo.jasonhudgins.com/2009/08/http-connection-reuse-in-android.html
public class HttpUtils {
	//private static CookieStore cookieStore;
	private static DefaultHttpClient client = null;
	private static AsyncHttpClient asyncClient;
	
	public static AsyncHttpClient getAsyncHttpClient() {
		if (null == asyncClient)
			asyncClient = new AsyncHttpClient();
		return asyncClient;
	}
	
	public synchronized static DefaultHttpClient getThreadSafeClient() {
    	if (null != client)
    		return client;
		DefaultHttpClient localClient = new DefaultHttpClient();
        ClientConnectionManager mgr = localClient.getConnectionManager();
        HttpParams params = localClient.getParams();
    	SchemeRegistry registry = mgr.getSchemeRegistry();

    	int iConnectionTimeout = 0, iDataResponseTimeout = 0;
		Resources res = null;
		if (null != Cache.context)
			res = Cache.context.getResources();
		if (res != null) {
			iConnectionTimeout = Cache.bootstrap.getIntValue("connection_time_out");
			iDataResponseTimeout = Cache.bootstrap.getIntValue("data_response_time_out");
		}

		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used. 
		if (iConnectionTimeout > 0) {
			HttpConnectionParams.setConnectionTimeout(params, iConnectionTimeout);
		}			
		// Set the default socket timeout (SO_TIMEOUT) 
		// in milliseconds which is the timeout for waiting for data.
		if (iDataResponseTimeout > 0) {
			HttpConnectionParams.setSoTimeout(params, iDataResponseTimeout);
		}
		
        client = new DefaultHttpClient(
            new ThreadSafeClientConnManager(params, registry), params);
      
        return client;
    } 

	public static JSONObject getJSONResponse(String sUrl, int requestMethod, List<NameValuePair> nameValuePairs){
		// prepare name value pair from the hash map.
		if(sUrl == null || sUrl.length() == 0 || 0 == requestMethod)
			return null;

		URL url;
		try {
			url = new URL(sUrl);
		} catch (MalformedURLException e) {
			LogManager.logError(e, "Check the format for the URL: " + sUrl);
			return null;
		}
		URI uri;
		try {
			uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
			if (null != Cache.context) {
				ConnectivityManager connectivityManager 
	        		= (ConnectivityManager) Cache.context.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
				if (activeNetworkInfo == null) {
					JSONObject jReturn = new JSONObject();
					try {
						StringBuffer sError = new StringBuffer();
						sError.append("Network connection not available. Please check your device. URL: "); sError.append(url.toString());
						jReturn.put(Constants.STR_SERVER_ERROR, sError);
					} catch (JSONException jsone) {
						LogManager.logWTF(jsone, "Error in executing a URL: " + uri);
					}
					return jReturn;
				}
			}
			return getJSONResponse(uri, requestMethod, nameValuePairs);
		} catch (URISyntaxException e) {
			LogManager.logError(e, "Check the format for the URL: " + sUrl);
			return null;
		}
	}
	
	public static JSONObject getJSONResponse(URI uri, int requestMethod, List<NameValuePair> nameValuePairs){
		// prepare name value pair from the hash map.
		if(uri == null || 0 == requestMethod)
			return null;

		String sResponse = null;
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		HttpUriRequest request = null;
		JSONObject jReturn = null;
		
		try {
			switch(requestMethod) {
				case Constants.POST:
					if(nameValuePairs == null)
						return null;
					request =  new HttpPost(uri);
					UrlEncodedFormEntity entity = new UrlEncodedFormEntity(nameValuePairs);
					entity.setContentEncoding(HTTP.UTF_8);
					((HttpPost)request).setEntity(entity);					 
				break;
				case Constants.GET:
					request = new HttpGet(uri);
				break;
			}

			DefaultHttpClient client = getThreadSafeClient();
			if (null == client) {
			 // TODO log fatal error
				return null;
			}
			if (Cache.cookieStore != null)
				client.setCookieStore(Cache.cookieStore);
			long retrievalTime = 0;
			if (Constants.isVerboseMode)
				retrievalTime = System.currentTimeMillis();
			sResponse=client.execute(request, responseHandler);
			if (Constants.isVerboseMode) {
				retrievalTime = System.currentTimeMillis() - retrievalTime;
				LogManager.logVerbose("The URL: " + uri + " took: " + retrievalTime + "ms for fetching response.");
			}

			// check if the response status code is 200. Otherwise log the error message
			// check the response type here it should be application\json
			// if (getHeader)
	        List<Cookie> newCookies = client.getCookieStore().getCookies();
	        	for (Cookie newCookie : newCookies) {
	    	        List<Cookie> storedCookies = Cache.cookieStore.getCookies();
	    	        Cookie storedCookie = null;
	    	        if (null != storedCookies) {
		        		for ( Iterator<Cookie> iter = storedCookies.iterator(); iter.hasNext();) {
		        			storedCookie = iter.next();
		        			if (null != storedCookie && null != newCookie && 
		        				null != storedCookie.getName() && null != newCookie.getName() &&
		        				storedCookie.getName().equals(newCookie.getName())) {
		        				iter.remove();
		        				break;
		        			}		        					
		        		}
	        			if (!newCookie.isExpired(new Date()))
	        				storedCookies.add(newCookie);
	    	        }
	        	}
	        List<Cookie> storedCookies= Cache.cookieStore.getCookies();
	        if (storedCookies.isEmpty()) {
	            LogManager.logVerbose("cookieStore has no stored cookies.");
	        } else {
	            for (int i = 0; i < storedCookies.size(); i++) {
	            	if (i == 0)
	    	        	LogManager.logVerbose("The list of cookies stored on the device are: ");
	                LogManager.logVerbose("- " + storedCookies.get(i).toString());
	            }
	        }
	        
			if (Constants.isDebugMode)
				retrievalTime = System.currentTimeMillis();
//			LogManager.logDebug("The URL " + uri.toString() + " returned response: " + sResponse);
	        JSONObject jsonObject  =  null;
			if (null == sResponse) {
				jsonObject = new JSONObject();
				jsonObject.put(Constants.STR_SERVER_ERROR, "Server did not respond to URL: " + uri);
				LogManager.logWTF("Error in executing a URL: " + uri);
			} else {
		        jsonObject  =  new JSONObject(sResponse);
				if (Constants.isDebugMode) {
			        retrievalTime = System.currentTimeMillis() - retrievalTime;
					LogManager.logVerbose("The jSON response for URL: " + uri + " took " + retrievalTime + 
							"ms for creating json");
				}
			}
			return jsonObject;
		} catch (JSONException jsone) {
			jReturn = new JSONObject();
			StringBuffer sError = new StringBuffer(); 
			sError.append("Error in reading server response: ");
			sError.append(jsone.getMessage());
			sError.append(". Server Response: ");
			sError.append(sResponse);
			LogManager.logWTF(jsone, sError.toString());
			try {
				jReturn.put(Constants.STR_SERVER_ERROR, sError);
			} catch (JSONException e) {
				LogManager.logWTF("Too many errors in executing a URL: " + uri);
			}
		} catch (IllegalArgumentException ite) {
			StringBuffer sError = new StringBuffer();
			sError.append("Invalid Arguments in URL: "); sError.append(uri.toString()); 
			sError.append(": "); sError.append(ite.getMessage());
			LogManager.logWTF(ite, sError.toString());
			jReturn = new JSONObject();
			try {
				jReturn.put(Constants.STR_SERVER_ERROR, sError);
			} catch (JSONException jsone) {
				LogManager.logWTF(ite, "Error in executing a URL: " + uri);
			}
		} catch (UnknownHostException jhe) {
			StringBuffer sError = new StringBuffer();
			sError.append("Unknown host or error Reaching host: "); sError.append(jhe.getMessage());
			sError.append(". Please check your network connection.");
			LogManager.logWTF(jhe, sError.toString());
			
			jReturn = new JSONObject();
			try {
				jReturn.put(Constants.STR_SERVER_ERROR, sError);
			} catch (JSONException jsone) {
				LogManager.logWTF(jhe, "Error in executing a URL: " + uri);
			}
		} catch (HttpResponseException hre) {
			StringBuffer sError = new StringBuffer();
			sError.append("Error in getting response for URL: "); sError.append(uri.toString());
			sError.append(". Message: "); sError.append(hre.getMessage());
			
			LogManager.logWTF(hre, sError.toString());
			jReturn = new JSONObject();
			try {
				jReturn.put(Constants.STR_SERVER_ERROR, sError);
			} catch (JSONException jsone) {
				LogManager.logWTF(jsone, "Error in executing a URL: " + uri);
			}
		} catch (ClientProtocolException cpe) {
			StringBuffer sError = new StringBuffer();
			sError.append("Error in getting communicating with the server: "); sError.append(uri.toString());
			sError.append(". Message: "); sError.append(cpe.getMessage());
			LogManager.logWTF(cpe, sError.toString());
			jReturn = new JSONObject();
			try {
				jReturn.put(Constants.STR_SERVER_ERROR, sError);
			} catch (JSONException jsone) {
				LogManager.logWTF(jsone, "Error in executing a URL: " + uri);
			}
		} catch (IOException ioe) {
			StringBuffer sError = new StringBuffer();
			sError.append("Error in getting response for URL: "); sError.append(uri.toString());
			sError.append(". Message: "); sError.append(ioe.getMessage());
			LogManager.logWTF(ioe, "Error in executing a URL: " + uri);
			jReturn = new JSONObject();
			try {
				jReturn.put(Constants.STR_SERVER_ERROR, sError);
			} catch (JSONException jsone) {
				LogManager.logWTF(jsone, "Error in executing a URL: " + uri);
			}
		}
		
		return jReturn;
	}    

	public static JSONObject getJSONResponse(String url, int requestMethod, Bundle postHashMap){
		List<NameValuePair> nameValuePairs = Utilities.convertBundleToNameValuePairs(postHashMap);
		return getJSONResponse(url, requestMethod, nameValuePairs);
	}

	public static JSONObject getJSONResponse(String url, int requestMethod, HashMap<String, String> postHashMap){
		List<NameValuePair> nameValuePairs = Utilities.convertHashMapToNameValuePairs(postHashMap);
		return getJSONResponse(url, requestMethod, nameValuePairs);
	}

	public static String getStringResponse(HttpResponse response) {
        HttpEntity entity = response.getEntity();
        String sResponse = null;
        try {
	        if (entity != null) {
	            InputStream instream = entity.getContent();
	            sResponse = convertStreamToString(instream);
	            // Closing the input stream will trigger connection release
	            instream.close();
	        }
        } catch (IOException ioe) {
	        	LogManager.logError(ioe, "Unable to parse response");
	    }
        return sResponse;
	}
	
    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dataconnector;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.R;
import com.appemble.avm.actions.Action;
import com.appemble.avm.actions.ActionBase;
import com.appemble.avm.controls.AVMGroupViewInterface;
import com.appemble.avm.controls.CONTROL_GROUP;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.controls.LIST;
import com.appemble.avm.controls.SINGLE_CHOICE_PICKER;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

// This article helped in formulating the algorithm for arranged list
// https://github.com/monxalo/android-section-adapter/blob/master/src/com/monxalo/android/widget/SectionCursorAdapter.java
public class AVMListAdaptor {
	ControlModel listModel;
	Vector<ControlModel> vListControls;
	ControlModel headerModel = null;
	Vector<ControlModel> vHeaderControls = null;
	Context context; // TODO remove this
	View mView;
	ScreenModel screenModel;
	String[] headerFieldNames = null; 
	LinkedHashMap<Integer, String[]> sectionsIndexer;
	float fWidth, fHeight; // Using which the child control dimensions are to be calculated.

	static final int TYPE_NORMAL = 1;
    static final int TYPE_HEADER = 0;
    static final int TYPE_COUNT = 2;
    static final String SECTION_HEADER = "section_header";
    AVMAdaptorInterface avmDataSource = null;
    static final int LIST_ITEM_ID_OFFSET = 23457;
    ActionModel[] actionsAfterCreateListItem = null, actionsBeforeCreateListItem = null;
    ActionBase[] aisBeforeCreateListItem = null;
    
	public AVMListAdaptor(Context ctx, View lView, ControlModel lModel, Vector<ControlModel> vectorControls, 
			ScreenModel scrModel, float fParentWidth, float fParentHeight, AVMAdaptorInterface avmDataSource) {
		vListControls = vectorControls;
		context = ctx;
		listModel = lModel;
		mView = lView;
		screenModel = scrModel;
		this.fWidth = fParentWidth;
		this.fHeight = fParentHeight;
		this.avmDataSource = avmDataSource;
		sectionsIndexer = new LinkedHashMap<Integer, String[]>();
		String sSectionHeaderName = listModel.mExtendedProperties.get(SECTION_HEADER);
	    actionsAfterCreateListItem = listModel.getActions(LIST.ON_CREATE_LIST_ITEM);
        actionsBeforeCreateListItem = listModel.getActions(LIST.BEFORE_ON_CREATE_LIST_ITEM);        
	    if (null != actionsBeforeCreateListItem && actionsBeforeCreateListItem.length > 0) {
	    	aisBeforeCreateListItem = new ActionBase[actionsBeforeCreateListItem.length];
	    	for (int i = 0; i < actionsBeforeCreateListItem.length; i++) {
	    		try {
		    		aisBeforeCreateListItem[i] = (ActionBase) Class.forName(actionsBeforeCreateListItem[i].getClassName())
	    				.getDeclaredConstructor().newInstance();
	    		} catch (NoSuchMethodError nsme) {
				LogManager.logWTF(nsme, "Unable to find method execute in class: " + 
						actionsBeforeCreateListItem[i].getClassName());
	    		} catch (InvocationTargetException ite) {
					LogManager.logWTF(ite, "Unable to invoke target method in class: " + 
							actionsBeforeCreateListItem[i].getClassName());
	    		} catch (InstantiationException ie) {
					LogManager.logWTF(ie, "Unable to instantiate class: " + actionsBeforeCreateListItem[i].getClassName());
				} catch (ClassNotFoundException cnfe) {
					LogManager.logWTF(cnfe, "Class not found: " + actionsBeforeCreateListItem[i].getClassName());
				} catch (IllegalAccessException iae) {
					LogManager.logWTF(iae, "Unable to access constructor: " + actionsBeforeCreateListItem[i].getClassName());
				} catch (NoSuchMethodException nsme) {
					LogManager.logWTF(nsme, "Unable to find default constructor: " + actionsBeforeCreateListItem[i].getClassName());
	    		}
	    	}
	    }
		if (null != sSectionHeaderName && sSectionHeaderName.length() > 0) { // if header exists...
			for (ControlModel cm : vListControls) { // loop through the vListControls
				if (cm.sName.equals(sSectionHeaderName)) {
					headerModel = cm; // store the headerModel
					vListControls.remove(cm); // remove the header...
					vHeaderControls = new Vector<ControlModel>();
					AppembleActivityImpl.getChildControls(vListControls, cm.id, vHeaderControls); // extract the child controls of the header
					break;
				}
			}			 
		}
	}

	public void calculateSectionHeaders() {
		int i = 0;
		String[] previous = null;
		int count = 0;

		if (false == avmDataSource.hasData() || avmDataSource.getCount() == 0 || null == listModel  || 
				null == sectionsIndexer || null == vHeaderControls)
			return;
		
		sectionsIndexer.clear();
		if (null == headerFieldNames) {
			count = 0; i = 0;
			for (ControlModel controlModel : vHeaderControls) {
		    	if (controlModel.iParentId != headerModel.id)
		    		continue;
				if (null != controlModel.sFieldName)
					count++;
			}
			headerFieldNames = new String[count];
			for (ControlModel controlModel : vHeaderControls) {
		    	if (controlModel.iParentId != headerModel.id)
		    		continue;
				if (null != controlModel.sFieldName)
					headerFieldNames[i++] = controlModel.sFieldName;
			}
			
			if (headerFieldNames == null || headerFieldNames.length == 0)
				return;
			count = avmDataSource.getColumnCount();
			i = 0;
		}
		
		count = 0; i = 0;
		avmDataSource.moveToPosition(-1);
		while (avmDataSource.moveToNext()) {
			String[] group = new String[headerFieldNames.length];
			for (int j = 0; j < headerFieldNames.length; j++)
				group[j] = (headerFieldNames[j] != null) ? 
						(String) avmDataSource.getValue(headerFieldNames[j], Constants.VARCHAR) : null;

			if (!isEqual(group, previous)) {
				sectionsIndexer.put(i + count, group);
				previous = group;
				count++;
			}
			i++;
		}
		return;
	}
	
	private static boolean isEqual(String[] newer, String[] previous) {
		if (newer == previous)
			return true;
		
		if ((null != newer && null == previous) || (null != previous && null == newer))
			return false;
		return Arrays.deepEquals(newer, previous);
//		if (newer.length != previous.length)
//			return false;
//		boolean bResult = true;
//		int iCount = previous.length;
//		for (int i = 0; i < iCount; i++) {
//			if ((null != newer[i] && null == previous[i]) || (null != previous[i] && null == newer[i]))
//				return false;
//			if (null == previous[i] && null == newer[i])
//				continue;
//			if (!previous[i].equals(newer[i]))
//				return false;
//		}
//		return bResult;
	}
	
	public String getGroupCustomFormat(Object obj) {
		return null;
	}
	
	// Article reference: http://stackoverflow.com/questions/5183813/android-issue-with-newview-and-bindview-in-custom-simplecursoradapter 
	public View getView(int position, View convertView, ViewGroup parent) {
		int viewType = getItemViewType(position);
		
	    DynamicLayout listItemLayout = null, headerLayout = null;
	    if (null == convertView) { // if the layout is not present for this list item, then create it.
	    	listItemLayout = new CONTROL_GROUP(context, listModel);
	    	((CONTROL_GROUP)listItemLayout).initialize(mView, fWidth, fHeight, 
	    			vListControls, screenModel);
	    	listItemLayout.setId(listModel.id+position+LIST_ITEM_ID_OFFSET);  
	    	// http://code.google.com/p/android/issues/detail?id=3414. However it does not fix the issue.
	    	listItemLayout.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
	    	// if (false == listItemLayout.generateLayout(Constants.LISTITEM, listModel.id, screenModel, vListControls)) candidate for delete 2012 04 30
			//	return null; // TODO Log error here
			if (null != headerModel && null != vHeaderControls && vHeaderControls.size() > 0) {
				headerLayout = new CONTROL_GROUP(context, headerModel);
				((CONTROL_GROUP)headerLayout).initialize(mView, fWidth, fHeight, 
						vHeaderControls, screenModel);
		    	headerLayout.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
			}
			// if the current cursor row and previous cursor row have different values for list headers fields
			// return headerLayout
			
			listItemLayout.setTag(R.integer.listitems, listItemLayout);
			if (null != headerLayout) {
				listItemLayout.setTag(R.integer.listheader, headerLayout);
				headerLayout.setTag(R.integer.listitems, listItemLayout);
				headerLayout.setTag(R.integer.listheader, headerLayout);
			}
	    }
        else {
            listItemLayout = (DynamicLayout) convertView.getTag(R.integer.listitems);
            if (null != headerModel)
            	headerLayout = (DynamicLayout) convertView.getTag(R.integer.listheader);
        }
	    convertView = (viewType == TYPE_NORMAL) ? listItemLayout : headerLayout;

        if (mView instanceof AVMGroupViewInterface && ((AVMGroupViewInterface)mView).isBeingMeasured())
	    	return convertView;

        Bundle tplv = null;
		if (viewType == TYPE_NORMAL) {
			final int mapPos = getSectionForPosition(position);
	        boolean bHasData = avmDataSource.hasData();
	        boolean bMovedToPosition = false;
	        if (bHasData)
	          bMovedToPosition = avmDataSource.moveToPosition(mapPos);
	        if (bMovedToPosition && null != actionsBeforeCreateListItem && actionsBeforeCreateListItem.length > 0) {
		        tplv = new Bundle();
	            HashMap<String, Object> values = avmDataSource.getValues();
	            for (Map.Entry<String, Object> entry : values.entrySet()) {
	                Object value = entry.getValue();
	                if (value instanceof String)
	                    tplv.putString(entry.getKey(), (String)entry.getValue());
	                else if (value instanceof byte[])
	                    tplv.putByteArray(entry.getKey(), (byte[])entry.getValue());
	                else if (value instanceof Long)
	                    tplv.putLong(entry.getKey(), (Long)entry.getValue());
	                else if (value instanceof Integer)
	                    tplv.putInt(entry.getKey(), (Integer)entry.getValue());
	            }
	            for (int i = 0; i < actionsBeforeCreateListItem.length; i++)
	                if (actionsBeforeCreateListItem[i].iPrecedingActionId == 0)
	                    aisBeforeCreateListItem[i].execute(context, convertView, mView, actionsBeforeCreateListItem[i], tplv);
	        }

//			if (false == bHasData || false == bMovedToPosition) 
//		    	return convertView;
//			final int mapPos = getSectionForPosition(position);
//			if (false == avmDataSource.hasData() || false == avmDataSource.moveToPosition(mapPos)) 
//		    	return convertView;
			View checkableView = null; String sSelectionFieldValue = null; boolean bSelected = false;
			Object columnVal = null;
			// Update the control values for the views present in the list item layout.
		    for (ControlModel controlModel : vListControls) {
		    	if (controlModel.iParentId != listModel.id || controlModel.bIsVisible == false)
		    		continue;
		    	columnVal = null;
				if(controlModel != null) {
					if (controlModel.sFieldName != null && bHasData && bMovedToPosition) {
						if (null != tplv)
							columnVal = tplv.getString(controlModel.sFieldName);
						else
							columnVal = avmDataSource.getValue(controlModel.sFieldName, controlModel.iDataType);
					}
					if (null == columnVal) {
						// a control may have a remote image or a local image
						if (controlModel.sDeparameterizedRemoteDataSource != null)
							columnVal = controlModel.sDeparameterizedRemoteDataSource;
						else if (controlModel.sDeparameterizedLocalDataSource != null)
							columnVal = controlModel.sDeparameterizedLocalDataSource;
						else 	
							columnVal = controlModel.sDefaultValue;
					}
					if (null == columnVal)
						LogManager.logDebug("While updating List: " + listModel.sName 
								+ ", cannot find value for field_name: " + controlModel.sFieldName);
					//	continue; 
					View controlView = listItemLayout.findViewById(controlModel.id);
					if (null != controlView) {
						// for a Single Choice Picker, store the checkableView. It is later made visible or invisible
						// based on SINGLE_CHOICE_PICKER.sSelectedValue
						if (parent instanceof SINGLE_CHOICE_PICKER && 
								controlModel.id == ((SINGLE_CHOICE_PICKER)parent).iCheckableControlId) {
							checkableView = controlView;
							if (controlModel.sDeparameterizedRemoteDataSource != null)
								((ControlInterface)controlView).setValue(controlModel.sDeparameterizedRemoteDataSource);
							else if (controlModel.sDeparameterizedLocalDataSource != null)
								((ControlInterface)controlView).setValue(controlModel.sDeparameterizedLocalDataSource);
							else
								((ControlInterface)controlView).setValue(controlModel.sDefaultValue);
						} else
							((ControlInterface)controlView).setValue(columnVal);
					}
					// A SINGLE_CHOICE_PICKER has always a selection_field and a checkable controls. 
					// store the sSelectionFieldValue and if this list item is selected.
					if (parent instanceof SINGLE_CHOICE_PICKER) {
						try {
						if (controlModel.id == ((SINGLE_CHOICE_PICKER)parent).iSelectionFieldControlId)
							sSelectionFieldValue = (String) columnVal;
						if (controlModel.id == ((SINGLE_CHOICE_PICKER)parent).iCheckableControlId && ((String)columnVal).length() > 0)
							bSelected = Integer.parseInt((String) columnVal) == 1;
						} catch (ClassCastException cce) {
							LogManager.logWTF("Unable to set values for SINGLE_CHOICE_PICKER: " + listModel.sName + 
									". The Selection Control should have the value type as String only");
						}
					}
				}
			}
		    if (parent instanceof SINGLE_CHOICE_PICKER && checkableView != null) {
		    	if (bSelected && ((SINGLE_CHOICE_PICKER)mView).sSelectedValue == null)
		    		((SINGLE_CHOICE_PICKER)mView).sSelectedValue = sSelectionFieldValue;
		    	boolean iVisibility = ((SINGLE_CHOICE_PICKER)mView).sSelectedValue != null &&
    				sSelectionFieldValue != null && 
    				((SINGLE_CHOICE_PICKER)mView).sSelectedValue.equals(sSelectionFieldValue);
		    	checkableView.setVisibility(iVisibility ? View.VISIBLE : View.INVISIBLE);
		    }
		    		    
			if (null != actionsAfterCreateListItem)
			for (int i = 0; i < actionsAfterCreateListItem.length; i++)
				if (actionsAfterCreateListItem[i].iPrecedingActionId == 0) {
					Action a = new Action();
					a.execute(context, mView, convertView, actionsAfterCreateListItem[i]);
				}
			

		} else {
			int iHeaderId = headerModel.id;
			// Update the control values for the views present in the list item layout.
	    	final String[] groupValues = sectionsIndexer.get(position);
		    for (ControlModel controlModel : vHeaderControls) {
		    	if (controlModel.iParentId != iHeaderId)
		    		continue;
		    	String columnVal = null;
				if(controlModel != null) {
					String sFieldName = controlModel.sFieldName;
					int iIndex = -1;
					if (sFieldName != null) {
						for (iIndex = 0; iIndex < headerFieldNames.length; iIndex++) {
							if (sFieldName.equals(headerFieldNames[iIndex]))
								break;
						}
						if (iIndex > -1 && iIndex < groupValues.length)
							columnVal = groupValues[iIndex];
					}
					if (null == columnVal) {
						if (controlModel.sDeparameterizedRemoteDataSource != null)
							columnVal = controlModel.sDeparameterizedRemoteDataSource;
						else if (controlModel.sDeparameterizedLocalDataSource != null)
							columnVal = controlModel.sDeparameterizedLocalDataSource;
						else	
							columnVal = controlModel.sDefaultValue;
					}
					View controlView = headerLayout.findViewById(controlModel.id);
					if (null != controlView)
						((ControlInterface)controlView).setValue(columnVal);
				}
		    }
		}
	    return convertView;
	}	

	public int getViewTypeCount() {
		return TYPE_COUNT;
	}

	public int getCount() {
		return avmDataSource.getCount() + sectionsIndexer.size();
	}

	public boolean isEnabled(int position) {
		return getItemViewType(position) == TYPE_NORMAL;
	}

	public int getPositionForSection(int section) {
		if(sectionsIndexer.containsKey(section)) {
			return section + 1;
		}
		return section;
	}

	public int getSectionForPosition(int position) {
		int offset = 0;
		for(Integer key: sectionsIndexer.keySet()) {
			if(position > key) {
				offset++;
			} else {
				break;
			}
		}

		return position - offset;
	}

	public Object getItem(int position) {
		if (getItemViewType(position) == TYPE_NORMAL){
            return avmDataSource.getItem(getSectionForPosition(position));
        } 
        return avmDataSource.getItem(position);
	}

	public int getItemViewType(int position) {
		if (position == getPositionForSection(position)) {
			return TYPE_NORMAL;
		}
		return TYPE_HEADER;
	}

	public void changeDataset() {
		if (headerModel != null && vHeaderControls.size() > 0)
			calculateSectionHeaders();
	}
	
	public void bindView(View view, Context context, Cursor cursor) {
	}

	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return null;
	}

}
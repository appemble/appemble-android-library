/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dataconnector;

import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.appemble.avm.LogManager;
import com.appemble.avm.actions.Action;
import com.appemble.avm.controls.CONTROL_GROUP;
import com.appemble.avm.controls.ControlInterface;
import com.appemble.avm.controls.GRID;
import com.appemble.avm.controls.IMAGE;
import com.appemble.avm.controls.LIST;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

public class AVMGridAdaptor {
	ControlModel gridItemModel;
	Vector<ControlModel> vGridControls;
	Context context;
	GRID gridView;
	ScreenModel screenModel;
	float fWidth, fHeight; // Using which the child control dimensions are to be calculated.
    AVMAdaptorInterface avmDataSource = null;
    
	public AVMGridAdaptor(Context ctx, View gridView, ControlModel controlModel, 
			Vector<ControlModel> vControls, ScreenModel screenModel, 
			float fParentWidth, float fParentHeight, AVMAdaptorInterface avmDataSource) {
		vGridControls = vControls;
		context = ctx;
		gridItemModel = new ControlModel(controlModel);
		this.gridView = (GRID)gridView;
		this.screenModel = screenModel;
		this.fWidth = fParentWidth;
		this.fHeight = fParentHeight;
		this.avmDataSource = avmDataSource;
	}

	// Article reference: http://stackoverflow.com/questions/5183813/android-issue-with-newview-and-bindview-in-custom-simplecursoradapter 
	public View getView(int position, View convertView, ViewGroup parent) {
		boolean bViewBeingCreated = false;
		
	    View gridItemLayout = convertView;
	    if (null == convertView) { // if the layout is not present for this list item, then create it.
	    	bViewBeingCreated = true;
	    	if (vGridControls.size() > 0) {
//	    		gridItemModel.fWidth = /= gridView.iColumnWidth;
//	    		gridItemModel.sWidth = Utilities.calcDimension(gridItemModel.sWidth, '/', gridView.iColumnWidth);
//	    		gridItemModel.fHeight = gridItemModel.fWidth; 
	    		gridItemModel.sX = gridItemModel.sY = "0px"; 
	    		gridItemModel.sHeight = gridItemModel.sWidth = Integer.toString(gridView.iColumnWidth)+"px"; 
	    		gridItemLayout = new CONTROL_GROUP(context, gridItemModel);
		    	((CONTROL_GROUP)gridItemLayout).initialize(gridView, fWidth, fHeight,
		    			vGridControls, screenModel);
		    	((DynamicLayout)gridItemLayout).setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
	    	}
	    	else {
	    		gridItemLayout = new IMAGE(context, gridItemModel);
		    	((IMAGE)gridItemLayout).initialize(gridView, fWidth, fHeight, 
		    			vGridControls, screenModel);			
	    	}
		    convertView = gridItemLayout;
	    }
			
	    if (gridView.bOnMeasure)
	    	return convertView;
	    boolean bHasData = avmDataSource.hasData();
	    boolean bMovedToPosition = avmDataSource.moveToPosition(position);
		if (false == bHasData || false == bMovedToPosition) 
	    	return convertView;
		Object columnVal = null;
		// Update the control values for the views present in the list item layout.
	    for (ControlModel controlModel : vGridControls) {
	    	if (controlModel.iParentId != gridItemModel.id)
	    		continue;
	    	columnVal = null;
			if(controlModel != null) {
				if (controlModel.sFieldName != null && bHasData && bMovedToPosition) {
					columnVal = avmDataSource.getValue(controlModel.sFieldName, controlModel.iDataType);
//					int iIndex = avmDataSource.getColumnIndex(controlModel.sFieldName);
//					if (iIndex > -1)
//						columnVal = Utilities.getCursorData(c, iIndex, controlModel.iDataType); // .getString(iIndex);
				}
				if (null == columnVal) {
					// a control may have a remote image or a local image
					if (controlModel.sDeparameterizedRemoteDataSource != null)
						columnVal = controlModel.sDeparameterizedRemoteDataSource;
					else if (controlModel.sDeparameterizedLocalDataSource != null)
						columnVal = controlModel.sDeparameterizedLocalDataSource;
					else 	
						columnVal = controlModel.sDefaultValue;
				}
				if (null == columnVal)
					LogManager.logError("While updating List: " + gridItemModel.sName 
							+ ", cannot find value for field_name: " + controlModel.sFieldName);
				//	continue; 
				View controlView = gridItemLayout.findViewById(controlModel.id);
				if (null != controlView)
					((ControlInterface)controlView).setValue(columnVal);
			}
		}
	    if (bViewBeingCreated) {
		    ActionModel[] actions = gridItemModel.getActions(LIST.ON_CREATE_LIST_ITEM);
		    
			if (null != actions)
			for (int i = 0; i < actions.length; i++)
				if (actions[i].iPrecedingActionId == 0) {
					Action a = new Action();
					a.execute(context, convertView, gridView, actions[i]);
					// capture the result of a.execute and see if any action can be taken.
				}
	    }
		return convertView;
	}
}
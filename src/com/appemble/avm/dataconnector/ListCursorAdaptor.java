/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dataconnector;

import java.util.HashMap;
import java.util.Vector;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import com.appemble.avm.Utilities;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

// This article helped in formulating the algorithm for arranged list
// https://github.com/monxalo/android-section-adapter/blob/master/src/com/monxalo/android/widget/SectionCursorAdapter.java
public class ListCursorAdaptor extends CursorAdapter implements AVMAdaptorInterface {
    AVMListAdaptor avmListAdaptor;
//    Cursor cursor;
	public ListCursorAdaptor(Context ctx, View listView, Cursor c, ControlModel controlModel, 
			Vector<ControlModel> vControls, float fParentWidth, float fParentHeight, 
			ScreenModel screenModel) {
		super(ctx, c);
//		cursor = c;
		avmListAdaptor = new AVMListAdaptor(ctx, listView, controlModel, vControls, screenModel, fParentWidth,
				fParentHeight, this);
		avmListAdaptor.calculateSectionHeaders();
		if (c != null)
			c.registerDataSetObserver(mDataSetObserver);
	}

	private DataSetObserver mDataSetObserver = new DataSetObserver() {
		public void onChanged() {
			if (avmListAdaptor.headerModel != null && avmListAdaptor.vHeaderControls.size() > 0)
				avmListAdaptor.calculateSectionHeaders();
		};

		public void onInvalidated() {
			avmListAdaptor.sectionsIndexer.clear();
		};
	}; 	
	
	// Article reference: http://stackoverflow.com/questions/5183813/android-issue-with-newview-and-bindview-in-custom-simplecursoradapter 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return avmListAdaptor.getView(position, convertView, parent);
	}	

	@Override
	public int getViewTypeCount() {
		return AVMListAdaptor.TYPE_COUNT;
	}

	@Override
	public int getCount() {
		return super.getCount() + avmListAdaptor.sectionsIndexer.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return getItemViewType(position) == AVMListAdaptor.TYPE_NORMAL;
	}

	@Override
	public Object getItem(int position) {
		if (getItemViewType(position) == AVMListAdaptor.TYPE_NORMAL){
            return super.getItem(avmListAdaptor.getSectionForPosition(position));
        } 
        return super.getItem(position);
	}

	@Override
	public int getItemViewType(int position) {
		if (position == avmListAdaptor.getPositionForSection(position)) {
			return AVMListAdaptor.TYPE_NORMAL;
		}
		return AVMListAdaptor.TYPE_HEADER;
	}

	@Override
	public void changeCursor(Cursor cursor) {
		Cursor oldCursor = getCursor(); 
		if (oldCursor == cursor)
			return;
		if(oldCursor != null && false == oldCursor.isClosed())
			oldCursor.unregisterDataSetObserver(mDataSetObserver);

		super.changeCursor(cursor);
//		this.cursor = cursor;
		if (avmListAdaptor.headerModel != null && avmListAdaptor.vHeaderControls.size() > 0)
			avmListAdaptor.calculateSectionHeaders();
		if (cursor != null && false == cursor.isClosed())
			cursor.registerDataSetObserver(mDataSetObserver);
		if(oldCursor != null && false == oldCursor.isClosed()) {
			oldCursor.close();
		}
	}
	
	public void changeDataset(Object object) {
		Cursor c = (Cursor)object;
		changeCursor(c);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return null;
	}

	public boolean hasData() { 
		Cursor cursor = getCursor();
		return null != cursor && cursor.getCount() > 0; 
	}
	
	public int getColumnCount() {
		Cursor cursor = getCursor();
		return null != cursor ? cursor.getColumnCount() : 0;
	}
	
	public int getColumnIndex(String sColumnName) {
		Cursor cursor = getCursor();
		return null != cursor ? cursor.getColumnIndex(sColumnName) : -1;
	}
	
	public boolean moveToPosition(int iPosition) {
		Cursor cursor = getCursor();
		return cursor.moveToPosition(iPosition);
	}
	
	public boolean moveToNext() {
		Cursor cursor = getCursor();
		return cursor.moveToNext();
	}

	public Object getValue(String sColumnName, int iDataType) {
		if (null == sColumnName)
			return null;
		int iColumnIndex = getColumnIndex(sColumnName);
		Cursor cursor = getCursor();
		return iColumnIndex >= 0 && iColumnIndex < cursor.getColumnCount() ? 
				Utilities.getCursorData(cursor, iColumnIndex, iDataType) : null; // .getString(iIndex);
//				cursor.getString(iColumnIndex) : null;
	}

	@Override
	public HashMap<String, Object> getValues() {
        Cursor cursor = getCursor();
        int iColumns = cursor.getColumnCount();
        HashMap<String, Object> map = new HashMap<String, Object>();
        for (int i = 0; i < iColumns; i++)
            map.put(cursor.getColumnName(i), cursor.getString(i));
        return map;
	}
	
}
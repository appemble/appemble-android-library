package com.appemble.avm.dataconnector;

import java.util.HashMap;

public interface AVMAdaptorInterface {
	public int getCount();
	public boolean hasData();
	public int getColumnCount();
//	public int getColumnIndex(String sColumnName);
	public boolean moveToPosition(int iPosition);
	public boolean moveToNext();
	public Object getValue(String sColumnName, int iDataType);
	public HashMap<String, Object> getValues();
	public boolean isEnabled(int iPosition);
	public Object getItem(int iPosition);
	public void changeDataset(Object object);
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dataconnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

// This article helped in formulating the algorithm for arranged list
// https://github.com/monxalo/android-section-adapter/blob/master/src/com/monxalo/android/widget/SectionCursorAdapter.java
public class ListArrayAdaptor extends ArrayAdapter<HashMap<String, Object>> implements AVMAdaptorInterface {
    AVMListAdaptor avmAdapter;
    ArrayList<HashMap<String, Object>> aValues; int iCurrentPosition = -1;

    public ListArrayAdaptor(Context ctx, View listView, ArrayList<HashMap<String, Object>> aVals, 
    		ControlModel controlModel, Vector<ControlModel> vControls, float fParentWidth,
    		float fParentHeight, ScreenModel screenModel) {
		super(ctx, 0);
		aValues = aVals;
		avmAdapter = new AVMListAdaptor(ctx, listView, controlModel, vControls, screenModel, fParentWidth,
				fParentHeight, this);
		avmAdapter.calculateSectionHeaders();
	}
	
	// Article reference: http://stackoverflow.com/questions/5183813/android-issue-with-newview-and-bindview-in-custom-simplecursoradapter 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return avmAdapter.getView(position, convertView, parent);
	}	

	@Override
	public int getViewTypeCount() {
		return AVMListAdaptor.TYPE_COUNT;
	}

	@Override
	public int getCount() {
		return aValues.size() + avmAdapter.sectionsIndexer.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return getItemViewType(position) == AVMListAdaptor.TYPE_NORMAL;
	}

	@Override
	public HashMap<String, Object> getItem(int position) {
		if (getItemViewType(position) == AVMListAdaptor.TYPE_NORMAL)
			position = avmAdapter.getSectionForPosition(position);
		if (position < aValues.size()) 
			return aValues.get(position);
		return null;
	}

	@Override
	public int getItemViewType(int position) {
		if (position == avmAdapter.getPositionForSection(position)) {
			return AVMListAdaptor.TYPE_NORMAL;
		}
		return AVMListAdaptor.TYPE_HEADER;
	}

	@SuppressWarnings("unchecked")
	public void changeDataset(Object object) {
		if (!(object instanceof ArrayList))
			return;			
		aValues = (ArrayList<HashMap<String, Object>>) object;
		if (avmAdapter.headerModel != null && avmAdapter.vHeaderControls.size() > 0)
			avmAdapter.calculateSectionHeaders();
	}
	
	public boolean hasData() { 
		return null != aValues && aValues.size() > 0; 
	}
	
	public int getColumnCount() {
		if (null != aValues) {
			HashMap<String, Object> item = aValues.get(0);
			return item.size();
		}
		return 0;
	}
	
	public boolean moveToPosition(int iPosition) {
		if (iPosition < aValues.size()) {
			iCurrentPosition = iPosition;
			return true;
		}
		return false;
	}
	
	public boolean moveToNext() {
		return moveToPosition(iCurrentPosition+1);
	}

	public Object getValue(String sColumnName, int iDataType) {
		if (aValues == null || aValues.size() < iCurrentPosition)
			return null;
		return aValues.get(iCurrentPosition).get(sColumnName);
	}

	public HashMap<String, Object> getValues() {
		if (aValues == null || aValues.size() < iCurrentPosition)
			return null;
		return aValues.get(iCurrentPosition);
	}
}
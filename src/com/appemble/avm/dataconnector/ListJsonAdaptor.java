/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dataconnector;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.appemble.avm.LogManager;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;

// This article helped in formulating the algorithm for arranged list
// https://github.com/monxalo/android-section-adapter/blob/master/src/com/monxalo/android/widget/SectionCursorAdapter.java
public class ListJsonAdaptor extends BaseAdapter implements AVMAdaptorInterface {
    AVMListAdaptor avmAdapter;
    JSONArray jsonArray; int iCurrentPosition = -1;
	public ListJsonAdaptor(Context ctx, View listView, JSONArray jsonA, ControlModel controlModel, 
			Vector<ControlModel> vControls, float fParentWidth, float fParentHeight, ScreenModel screenModel) {
		super();
		jsonArray = jsonA;
		avmAdapter = new AVMListAdaptor(ctx, listView, controlModel, vControls, screenModel, fParentWidth,
				fParentHeight, this);
		avmAdapter.calculateSectionHeaders();
	}

	// Article reference: http://stackoverflow.com/questions/5183813/android-issue-with-newview-and-bindview-in-custom-simplecursoradapter 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return avmAdapter.getView(position, convertView, parent);
	}	

	@Override
	public int getViewTypeCount() {
		return AVMListAdaptor.TYPE_COUNT;
	}

	@Override
	public int getCount() {
		return jsonArray != null ? jsonArray.length() + avmAdapter.sectionsIndexer.size() : 0;
	}

	@Override
	public boolean isEnabled(int position) {
		return getItemViewType(position) == AVMListAdaptor.TYPE_NORMAL;
	}

	@Override
	public Object getItem(int position) {
		if (getItemViewType(position) == AVMListAdaptor.TYPE_NORMAL)
			position = avmAdapter.getSectionForPosition(position);
		if (jsonArray != null && position < jsonArray.length()) {
			try {
				return jsonArray.get(position);
			} catch (JSONException e) {
				LogManager.logWTF(e, "Unable to get jsonObject at position: " + position + " for control: " + 
						avmAdapter.listModel.sName);
			}
		}
		return null;
	}

	@Override
	public int getItemViewType(int position) {
		if (position == avmAdapter.getPositionForSection(position)) {
			return AVMListAdaptor.TYPE_NORMAL;
		}
		return AVMListAdaptor.TYPE_HEADER;
	}

	public void changeDataset(Object object) {
		jsonArray = (JSONArray) object;
//		if (!(object instanceof JSONArray))
//			return;
		if (avmAdapter.headerModel != null && avmAdapter.vHeaderControls.size() > 0)
			avmAdapter.calculateSectionHeaders();
		moveToPosition(-1);
	}
	
	public boolean hasData() { 
		return null != jsonArray && jsonArray.length() > 0; 
	}
	
	public int getColumnCount() {
		if (null == jsonArray || jsonArray.length() == 0)
			return 0;
		try {
			return jsonArray.getJSONObject(0).length();
		} catch (JSONException e) {
			LogManager.logWTF(e, "Unable to get column count for Json object of control: " + avmAdapter.listModel.sName);
		}
		return 0;
	}
	
//	public int getColumnIndex(String sColumnName) {
//		return null != jsonArray ? jsonArray.getJSONObject(iCurrentPosition).get(sColumnName) : -1;
//	}
//	
	public boolean moveToPosition(int iPosition) {
		if (iPosition == -1)
			iCurrentPosition = iPosition;
		if (jsonArray != null && iCurrentPosition < jsonArray.length()) {
			iCurrentPosition = iPosition;
			return true;
		}
		return false;
	}
	
	public boolean moveToNext() {
		return moveToPosition(iCurrentPosition+1);
	}

	public Object getValue(String sColumnName, int iDataType) {
		if (null == sColumnName || iCurrentPosition < 0 || jsonArray == null || 
				iCurrentPosition > jsonArray.length())
			return null;
		try {
			return jsonArray.getJSONObject(iCurrentPosition).getString(sColumnName);
		} catch (JSONException e) {
			LogManager.logWTF(e, "Unable to value for column: " + sColumnName + " for postion: " + 
					iCurrentPosition + " of control: " + avmAdapter.listModel.sName);
			return null;
		}
	}

	@Override
	public HashMap<String, Object> getValues() {
		if (iCurrentPosition < 0 || jsonArray == null || iCurrentPosition > jsonArray.length())
			return null;
		JSONObject jsonObject = null;
		HashMap<String, Object> map = new HashMap<String, Object>();
		try {
			jsonObject = jsonArray.getJSONObject(iCurrentPosition);
			Iterator<String> keys = (Iterator<String>) jsonObject.keys();
			while(keys.hasNext()) {
				String key = (String) keys.next();
				map.put(key, jsonObject.getString(key));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return map.size() > 0 ? map : null;
	}
	
	public long getItemId(int position) {
		if (position < 0 || jsonArray == null || position > jsonArray.length())
			return 0;
		try {
			String sId = jsonArray.getJSONObject(position).getString("_id");
			if (null == sId || sId.length() == 0)
				return 0;
			try {
				return Long.parseLong(sId);
			} catch (NumberFormatException nme) {
				return sId.hashCode();				
			}
		} catch (JSONException e) {
			LogManager.logWTF(e, "Unable to get ItemId for position: " + 
					iCurrentPosition + " of control: " + avmAdapter.listModel.sName);
			return 0;
		}
	}
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.dataconnector;

import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.ContentValues;
import android.os.Bundle;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.dao.db.DbConnectionManager;

public class JSONProcessor {
	// This is a possible algorithm. Has lot of flaws. It is not implemented in the function called processJSONObject()
	// declare a vector of tablecv's
	// for each control in the screen as thiscontrol
		// if thiscontrol.parent_id == -1
			// if field_name = null then table_name = screen.tablename else table_name = field_name
			// now try to add all its children to this tablecv
			// is_parent = false;
			//for all controls with parent_id = thiscontrol._id as childcontrol 
				//	tablecv[1][tablename] = childcontrol.field_name and is_parent = true
			// if thiscontrol is not a parent i.e. is_parent == false
				// tablecv[0][tablename] = thiscontrol.field_name
	// Now that we have all tablecvs populate them with incoming data from JSON
	// Traverse the JSON array
	// At level 0, populate the tablecv[0][tablename]
	// At level 1, populate the tablecv[1][tablename]
	//Vector<String> columnNames;
	//HashMap <String, Object> table;
	//Vector<Object> tables;
	// traverse through the JSON object. If it is a array, it represents the table name.

	// detects JSONArrays in the jsonObject and saves them in the table with same name as JSONArray name.
	// return the top level key/value pairs.
	public static boolean saveJSONObjectIntoDb(JSONObject aJSONObject, String sDbName, 
			String sTableName, Bundle result, int iRecursionLevel) {
		if (null == aJSONObject)
			return false;
		@SuppressWarnings("unchecked")
		Iterator<String> keys = (Iterator<String>)aJSONObject.keys();
		ContentValues tablecv = null;
		String[] aColumnNames = null;
		if (null != sDbName && null != sTableName) {
			// && table name exists in the list of tables in database)
			aColumnNames = DbConnectionManager.getInstance().getColumnNames(sDbName, sTableName);
			if (aColumnNames != null)
				tablecv = new ContentValues();
			else
				; // TODO log the error and also write the json object in the logs

		}

		while (keys.hasNext()) {
			String key = keys.next();
			
			JSONArray aChildJSONArray = aJSONObject.optJSONArray(key);
			if (null != aChildJSONArray) {
				// if (key exists in the list table names)
				if (false == processJSONArray(aChildJSONArray, sDbName, key, result, iRecursionLevel+1))
					; // TODO log an error. The function should return null.
				if (0 == iRecursionLevel)
					Utilities.putData(result, Constants.JSONARRAY, key, aChildJSONArray);	
				continue;
			}

			JSONObject aChildJSONObject = aJSONObject.optJSONObject(key);
			if (null != aChildJSONObject) {
				// if (key exists in the list table names)
				if (false != saveJSONObjectIntoDb(aChildJSONObject, sDbName, key, result, iRecursionLevel+1))
					; // TODO log error. The function should return null
				if (0 == iRecursionLevel)
					Utilities.putData(result, Constants.JSONOBJECT, key, aChildJSONObject);	
				continue;
			}

			String value = aJSONObject.optString(key);
			if (null != value && value.length() > 0) {
				int i = 0;
				if (value.equalsIgnoreCase("null"))
					value = null;
				if (tablecv != null) {
					for (i = 0; i < aColumnNames.length; i++) {
						if(key.matches(aColumnNames[i])) {
							tablecv.put(key, value);
							break;
						}
					}
					if (i == aColumnNames.length)
						; // log error here if i == aColumnNames.length as JSON key did not match with a column name in the DB					
				}
				else if (null != result && iRecursionLevel == 0) // only save the top level name value pairs of the JSON object
//					result.putString(key, value);	
					Utilities.putData(result, Constants.VARCHAR, key, value);	
				continue;
			}
			
		}
		// Here see if the table exists, then insert these values in the table.
		// The tablecv is only initialized when there is a table name existing in the db schema.
		if (tablecv != null) {
			long new_row_id = DbConnectionManager.getInstance().getDb(sDbName).replace(sTableName, null, tablecv);
			if (new_row_id <= 0)
				LogManager.logError("Error while saving JSON response from a URL, table name = " + sTableName + 
						" and Response = " + aJSONObject.toString());
		}
		return true;
	}

	public static boolean processJSONArray(JSONArray aJSONArray, String sDbName, 
			String sTableName, Bundle result, int iRecursionLevel) {
		if (null == aJSONArray)
			return false;
		boolean bResult = true;
		int iCount = aJSONArray.length();
		for(int i = 0; i < iCount; i++) {
// if there is another array within the array.
			JSONArray aChildJSONArray = aJSONArray.optJSONArray(i);
			if (null != aChildJSONArray) {
				// if (key exists in the list table names)
				bResult &= processJSONArray(aChildJSONArray, sDbName, aJSONArray.optString(i), result, iRecursionLevel+1);	
				continue;
			}

			JSONObject aChildJSONObject = aJSONArray.optJSONObject(i);
			if (null != aChildJSONObject) {
				// if (key exists in the list table names)
				if (false != saveJSONObjectIntoDb(aChildJSONObject, sDbName, sTableName, result, iRecursionLevel+1))
					; // TODO log an error here.
				continue;
			}
		}
		return bResult;
	}
}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models;

import java.util.Date;
import java.util.HashMap;

public class ScreenDeckModel {
	public int id;
	public String sVersion;
	public String sName;
	public String sShortDescription;
	public String sLongDescription;
	public int iSize;
	public String sPublisherName;
	public Date dPublishDate;
	public String sBackground;
	public String sTitleBackground;
	public String sMisc1;
	public String sMisc2;	
	public String sDbName;
	public String sStartingScreenType;
	public String sStartingScreenName;	
	public HashMap <String, String> mExtendedProperties;
	public int iDefaultTitleBar = -1; // -1 = not set. 0 = false 1 = true
	
	public ScreenDeckModel() {
		mExtendedProperties = new HashMap<String, String>();
	}
}
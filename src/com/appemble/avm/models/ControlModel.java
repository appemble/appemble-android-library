/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models;

import java.util.HashMap;

import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.dao.ActionDao;

public class ControlModel {
 	public int id;
 	public int iScreenId;
// 	public int iScreenDeckId;
 	public String sName;
 	public int iType;
 	public String sX;
 	public String sY;
 	public String sWidth;
 	public String sHeight;
 	public float fWidthInPixels;
 	public float fHeightInPixels;
 	public int iZOrder;
 	
 	public int iDimensionType;
 	public boolean bIsVisible;
 	public int iPermission;
 	public int iAppearanceId;
// 	public int iAlignPosition;
// 	public int iAlignId;
// 	public int iSpacingX;
// 	public int iSpacingY;
 	public int iDataType;
 	public int iDataSourceType;
 	public String sFormat;
 	public String sFieldName;
 	public int iSize;
 	public String sDefaultValue;
 	public boolean bWordWrap;
 	public boolean bEllipsized;
 	public int iParentId;
// 	public String sListCellScreenName;
// 	public int iFieldType;
// 	public String sFieldRefreshSec;
 	public boolean bActionYN;
 	public String sRemoteDataSource;
 	public String sLocalDataSource;
	public int iRemoteRequestType;
	public int iRemoteResponseType;
	public boolean bMarkDeleted;
	public String sBackgroundImage;
	public boolean bOnResumeUpdate;
	public String sMisc1;
	public String sMisc2;
	public HashMap <String, String> mExtendedProperties;
	public int[] iDisableOnEventList;
	
 	public String sSystemDbName;
 	public String sContentDbName;
 	public ConfigManager.ControlMetaData metaData;
 	public boolean bRemoteDataSaveLocally;
 	// Only when there is an action. Commented to have to access dynamically.
 	// private ActionModel ControlAction;
	
	//Used only for listView
 	// private String TableName;  //now used for all controls
 	public String sDeparameterizedRemoteDataSource;
 	public String sDeparameterizedLocalDataSource;
 	
 	public ControlModel() {
		mExtendedProperties = new HashMap<String, String>();
	}

 	@SuppressWarnings("unchecked")
	public ControlModel(ControlModel cm) {
 	 	id = cm.id;
 	 	iScreenId = cm.iScreenId;
 	 	if (null != cm.sName)
 	 		sName = new String(cm.sName);
 	 	iType = cm.iType;
 	 	
 	 	if (null != cm.sX) // child controls like RADIOBUTTON do not have sX. They are calculated at run time
 	 		sX = new String(cm.sX);
 	 	if (null != cm.sY) // child controls like RADIOBUTTON do not have sX. They are calculated at run time
 	 		sY = new String(cm.sY);
 	 	if (null != cm.sWidth)
 	 		sWidth = new String(cm.sWidth);
 	 	if (null != cm.sHeight)
 	 		sHeight = new String(cm.sHeight);
 	 	iZOrder = cm.iZOrder;
 	 	
 	 	iDimensionType = cm.iDimensionType;
 	 	bIsVisible = cm.bIsVisible;
 	 	iPermission = cm.iPermission;
 	 	iAppearanceId = cm.iAppearanceId;
 	 	iDataType = cm.iDataType;
 	 	if (null != cm.sFormat)
 	 		sFormat = new String(sFormat);
 	 	if (null != cm.sFieldName)
 	 		sFieldName = new String(sFieldName);
 	 	iSize = cm.iSize;
 	 	if (null != cm.sDefaultValue)
 	 		sDefaultValue = new String(sDefaultValue);
 	 	bWordWrap = cm.bWordWrap;
 	 	bEllipsized = cm.bEllipsized;
 	 	iParentId = cm.iParentId;
 	 	bActionYN = cm.bActionYN;
 	 	if (null != cm.sRemoteDataSource)
 	 		sRemoteDataSource = new String(cm.sRemoteDataSource);
 	 	if (null != cm.sLocalDataSource)
 	 		sLocalDataSource = new String(cm.sLocalDataSource);
 		iRemoteRequestType = cm.iRemoteRequestType;
 		iRemoteResponseType = cm.iRemoteResponseType;
 		bMarkDeleted = cm.bMarkDeleted;
 	 	if (null != cm.sBackgroundImage)
 			sBackgroundImage = new String(sBackgroundImage);
 		bOnResumeUpdate = cm.bOnResumeUpdate;
 	 	if (null != cm.sMisc1)
 			sMisc1 = new String(cm.sMisc1);
 	 	if (null != cm.sMisc2)
 			sMisc2 = new String(cm.sMisc2);
 	 	if (null != cm.mExtendedProperties)
			mExtendedProperties = (HashMap<String, String>)cm.mExtendedProperties.clone();
 	 	if (null != iDisableOnEventList)
 			iDisableOnEventList = cm.iDisableOnEventList.clone();
 		
 	 	if (null != cm.sSystemDbName)
 	 		sSystemDbName = new String(cm.sSystemDbName);
 	 	if (null != cm.sContentDbName)
 	 		sContentDbName = new String(cm.sContentDbName);
 	 	metaData = cm.metaData; // this is a static copy anyway.
 	 	bRemoteDataSaveLocally = cm.bRemoteDataSaveLocally;
 	 	if (null != cm.sDeparameterizedRemoteDataSource)
 	 		sDeparameterizedRemoteDataSource = new String(cm.sDeparameterizedRemoteDataSource);
 	 	if (null != cm.sDeparameterizedLocalDataSource)
 	 		sDeparameterizedLocalDataSource = new String(cm.sDeparameterizedLocalDataSource);
	}

 	public int getType() {
		return iType;
	}
	
	public void setType(String type){
		if (null == type || 0 == type.length())
			return;
		metaData = ConfigManager.getInstance().getControlMetaData(type);
		if (null == metaData) {
			LogManager.logWTF("Cannot instantiate the control: " + sName + ". The defintion is missing in the configuration file for control type = " + type);
			return; // TODO Log severe error here
		}
		iType = metaData.id;
	}

	public void setDimensionType(String dimensionType){
		if (null == dimensionType)
			return;
		if (dimensionType.equalsIgnoreCase("PERCENTAGE"))
			iDimensionType = Constants.PERCENTAGE;
		else if (dimensionType.equalsIgnoreCase("PIXELS"))
			iDimensionType = Constants.PIXELS;

	}

	public void setPermission(String permission){
		if (null == permission || 0 == permission.length())
			return;
		iPermission = Constants.READONLY;
		if (permission.equalsIgnoreCase("READWRITE"))
			iPermission = Constants.READWRITE;
	}

	public String getDataType(int iDataTypeHashCode){
		return ConfigManager.getInstance().getDataTypesString(iDataTypeHashCode);
	}

	public void setDataType(String dataType){
		iDataType = convertDataType(dataType);
	}
	
	private int convertDataType(String dataType) {
		if (null == dataType || 0 == dataType.length())
			return 0;
		return dataType.trim().hashCode();
//		if (dataType.equalsIgnoreCase("AUTOINC"))
//			return Constants.AUTOINC;				
//		else if (dataType.equalsIgnoreCase("BIGINT"))
//			return Constants.BIGINT;
//		else if (dataType.equalsIgnoreCase("BINARY"))
//			return Constants.BINARY;				
//		else if (dataType.equalsIgnoreCase("BLOB"))
//			return Constants.BLOB;				
//		else if (dataType.equalsIgnoreCase("BLOB_TEXT"))
//			return Constants.BLOB_TEXT;				
//		else if (dataType.equalsIgnoreCase("BOOL"))
//			return Constants.BOOL;				
//		else if (dataType.equalsIgnoreCase("BOOLEAN"))
//			return Constants.BOOLEAN;				
//		else if (dataType.equalsIgnoreCase("CHAR"))
//			return Constants.CHAR;				
//		else if (dataType.equalsIgnoreCase("CLOB"))
//			return Constants.CLOB;				
//		else if (dataType.equalsIgnoreCase("CURRENCY"))
//			return Constants.CURRENCY;				
//		else if (dataType.equalsIgnoreCase("DATE"))
//			return Constants.DATE;				
//		else if (dataType.equalsIgnoreCase("DATETEXT"))
//			return Constants.DATETEXT;				
//		else if (dataType.equalsIgnoreCase("DATETIME"))
//			return Constants.DATETIME;				
//		else if (dataType.equalsIgnoreCase("DEC"))
//			return Constants.DEC;				
//		else if (dataType.equalsIgnoreCase("DECIMAL"))
//			return Constants.DECIMAL;				
//		else if (dataType.equalsIgnoreCase("DOUBLE"))
//			return Constants.DOUBLE;				
//		else if (dataType.equalsIgnoreCase("DOUBLE_PRECISION"))
//			return Constants.DOUBLE_PRECISION;				
//		else if (dataType.equalsIgnoreCase("FLOAT"))
//			return Constants.FLOAT;				
//		else if (dataType.equalsIgnoreCase("GRAPHIC"))
//			return Constants.GRAPHIC;				
//		else if (dataType.equalsIgnoreCase("GUID"))
//			return Constants.GUID;				
//		else if (dataType.equalsIgnoreCase("IMAGE"))
//			return Constants.IMAGE;				
//		else if (dataType.equalsIgnoreCase("INT"))
//			return Constants.INT;				
//		else if (dataType.equalsIgnoreCase("INT64"))
//			return Constants.INT64;				
//		else if (dataType.equalsIgnoreCase("INTEGER"))
//			return Constants.INTEGER;				
//		else if (dataType.equalsIgnoreCase("LARGEINT"))
//			return Constants.LARGEINT;				
//		else if (dataType.equalsIgnoreCase("MEMO"))
//			return Constants.MEMO;				
//		else if (dataType.equalsIgnoreCase("MONEY"))
//			return Constants.MONEY;				
//		else if (dataType.equalsIgnoreCase("NCHAR"))
//			return Constants.NCHAR;				
//		else if (dataType.equalsIgnoreCase("NTEXT"))
//			return Constants.NTEXT;				
//		else if (dataType.equalsIgnoreCase("NUMBER"))
//			return Constants.NUMBER;				
//		else if (dataType.equalsIgnoreCase("NUMERIC"))
//			return Constants.NUMERIC;				
//		else if (dataType.equalsIgnoreCase("NVARCHAR"))
//			return Constants.NVARCHAR;				
//		else if (dataType.equalsIgnoreCase("NVARCHAR2"))
//			return Constants.NVARCHAR2;				
//		else if (dataType.equalsIgnoreCase("PHOTO"))
//			return Constants.PHOTO;				
//		else if (dataType.equalsIgnoreCase("PICTURE"))
//			return Constants.PICTURE;				
//		else if (dataType.equalsIgnoreCase("RAW"))
//			return Constants.RAW;				
//		else if (dataType.equalsIgnoreCase("REAL"))
//			return Constants.REAL;				
//		else if (dataType.equalsIgnoreCase("SMALLINT"))
//			return Constants.SMALLINT;				
//		else if (dataType.equalsIgnoreCase("SMALLMONEY"))
//			return Constants.SMALLMONEY;				
//		else if (dataType.equalsIgnoreCase("TEXT"))
//			return Constants.TEXT;				
//		else if (dataType.equalsIgnoreCase("TIME"))
//			return Constants.TIME;				
//		else if (dataType.equalsIgnoreCase("TIMESTAMP"))
//			return Constants.TIMESTAMP;				
//		else if (dataType.equalsIgnoreCase("TINYINT"))
//			return Constants.TINYINT;				
//		else if (dataType.equalsIgnoreCase("VARBINARY"))
//			return Constants.VARBINARY;				
//		else if (dataType.equalsIgnoreCase("VARCHAR"))
//			return Constants.VARCHAR;				
//		else if (dataType.equalsIgnoreCase("VARCHAR2"))
//			return Constants.VARCHAR2;				
//		else if (dataType.equalsIgnoreCase("WORD"))
//			return Constants.WORD;				
//		else if (dataType.equalsIgnoreCase("JSONARRAY"))
//			return Constants.JSONARRAY;
//		else if (dataType.equalsIgnoreCase("JSONOBJECT"))
//			return Constants.JSONOBJECT;
//		else if (dataType.equalsIgnoreCase("CURSOR"))
//			return Constants.CURSOR;
//		else if (dataType.equalsIgnoreCase("BITMAP"))
//			return Constants.BITMAP;
//		else if (dataType.equalsIgnoreCase("DRAWABLE"))
//			return Constants.DRAWABLE;
//		else if (dataType.equalsIgnoreCase("ARRAYLIST"))
//			return Constants.ARRAYLIST;
//		else if (dataType.equalsIgnoreCase("HASHMAP"))
//			return Constants.HASHMAP;
//		else
//			return 0;
	}

//	public void setFieldType(String fieldType){
//		if (null == fieldType || 0 == fieldType.length())
//			return;
//		if (fieldType.equalsIgnoreCase("LOCAL"))
//			iFieldType=Constants.LOCAL;
//		else if (fieldType.equalsIgnoreCase("SERVER"))
//			iFieldType=Constants.SERVER;
//	}

	public void setRemoteRequestType(String sRequestType){
		if (sRequestType == null || 0 == sRequestType.length())
			return;
		iRemoteRequestType = Constants.mRequestType.get(sRequestType);
	}

	public ActionModel[] getActions(){
		//ActionModel a=MLCache.globalConn(this.DatabaseName).getActionForControl(MLCache.ctx, this.getId());
		ActionModel[] a=ActionDao.getActionsForControl(sSystemDbName, sContentDbName, id, 0);
		return a;
	}

	public ActionModel[] getActions(int iEventType){
		ActionModel[] a=ActionDao.getActionsForControl(sSystemDbName, sContentDbName, id, iEventType);
		return a;
	}

	public boolean isClickable() {
		if (null == metaData)
			return false;
		return metaData.isClickable;
	}
	public boolean isGroup() {
		if (null == metaData)
			return false; // log error here.
		return metaData.isGroup;
	}
	
	public String getClassName() {
		if (null == metaData)
			return null;
		return metaData.sClass;
	}
	
	public String getTypeByName() {
		if (null == metaData)
			return null;
		return metaData.sName;
	}

	public void interpretExtendedProperties() {
		if (null == mExtendedProperties || 0 == mExtendedProperties.size())
			return;
		String sDataSourceType = mExtendedProperties.get("data_source_type");
		if (null != sDataSourceType && sDataSourceType.length() != 0) {
			iDataSourceType = convertDataType(sDataSourceType);
			mExtendedProperties.remove("data_source_type");
		}
		String sDisableOnEventList = mExtendedProperties.get("disable_on");
		if (null != sDisableOnEventList) {
			String[] sEventList = sDisableOnEventList.split(",");
			iDisableOnEventList = new int[sEventList.length];
			for (int i = 0; i < sEventList.length && sEventList[i] != null; i++) {
				iDisableOnEventList[i] = sEventList[i].trim().hashCode();
			}
			mExtendedProperties.remove("disable_on");
		}
		bRemoteDataSaveLocally = Utilities.parseBoolean(mExtendedProperties.get("remote_data_save_locally"));
		mExtendedProperties.remove("remote_data_save_locally");
	}

	public String getExtendedProperty(String sKey) {
		if (null == mExtendedProperties || null == sKey)
			return null;
		return mExtendedProperties.get(sKey);
	}	
}

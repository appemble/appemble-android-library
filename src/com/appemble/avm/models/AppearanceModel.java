/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models;

import java.util.HashMap;
import java.util.Vector;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.StateSet;
import android.view.Gravity;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;

public class AppearanceModel {
	public int id;
	public int  iScreenDeckId;
	public String  sName;
	public String  sFontFamily;
	public String  sFontName;
	public String sFontSize;
	public int iFontColor;
	public int[]  iBackgroundColors;
	public int  iAlpha; // range 0-255 (fully transparent to fully opaque)
	public int  iJustify;
	public String  sFontStyle;
	public boolean  bFontStyleBold;
	public boolean  bFontStyleItalic;
	public boolean  bFontStyleUnderline;

	public String sPaddingLeft;
	public String sPaddingTop;
	public String sPaddingRight;
	public String sPaddingBottom;

	public int iBorderColor;
	public int iBorderWidth;
	public float[] fBorderRadii; 
	
	public boolean  bFontStyleStrikethrough;
	public boolean  bFontStyleDoubleStrikethrough;
	public boolean  bFontStyleSuperscript;
	public boolean  bFontStyleSubscript;
	public boolean  bFontStyleShadow;
	public boolean  bFontStyleOutline;
	public boolean  bFontStyleEmboss;
	public boolean  bFontStyleEngrave;
	public boolean bMarkDeleted;
	public HashMap <String, String> mExtendedProperties;
	public int states[][];
	public String sImageName;
	
	public float fShadowRadius;
	public float fShadowHorizontalOffset;
	public float fShadowVerticalOffset;
	public int iShadowColor;
	public GradientDrawable.Orientation mGradientOrientation;
	public int iGradientShape;
	public int iGradientType;
//	public GradientDrawable mGradientDrawable;
	
	public AppearanceModel() {
		mExtendedProperties = new HashMap<String, String>();
//		mGradientDrawable = null;
	}
/*
	public int getId(){
		return id;
	}
	
	public int getiScreenDeckId(){
		return iScreenDeckId;
	}
	
	public String getFont_family(){
		return sFontFamily;
	}
	
	public String getFont_name(){
		return sFontName;
	}
	
	public float getFont_size(){
		return fFontSize;
	}
	
	public int getFont_color(){
		return iFontColor;
	}
	
	public int getBackground_color(){
		return iBackgroundColor;
	}
	
	public int getAlpha(){
		return iAlpha;
	}
	
	public int getJustify(){
		return iJustify;
	}
	
	public String getFont_style(){
		return sFontStyle;
	}
	
	public boolean getFont_style_bold(){
		return bFontStyleBold;
	}
	
	public boolean getFont_style_italic(){
		return bFontStyleItalic;
	}
	
	public boolean getFont_style_underline(){
		return bFontStyleUnderline;
	}
	
	public boolean getFont_style_strikethrough(){
		return bFontStyleStrikethrough;
	}
	
	public boolean getFont_style_double_strikethrough(){
		return bFontStyleDoubleStrikethrough;
	}
	
	public boolean getFont_style_superscript(){
		return bFontStyleSuperscript;
	}
	
	public boolean getFont_style_subscript(){
		return bFontStyleSubscript;
	}
	
	public boolean getFont_style_shadow(){
		return bFontStyleShadow;
	}
	
	public boolean getFont_style_outline(){
		return bFontStyleOutline;
	}
	
	public boolean getFont_style_emboss(){
		return bFontStyleEmboss;
	}
	
	public boolean getFont_style_engrave(){
		return bFontStyleEngrave;
	}


	public void setId(int i){
		id = i;
	}
	
	public void setiScreenDeckId(int screen_deck_id){
		iScreenDeckId = screen_deck_id;
	}
	
	public void setFont_family(String font_family){
		sFontFamily = font_family;
	}
	
	public void setFont_name(String font_name){
		sFontName = font_name;
	}
	
	public void setFont_size(float font_size){
		fFontSize = font_size;
	}
*/	
	public void setFont_color(String font_color){
		try {
			if (null != font_color)
				iFontColor = Color.parseColor(font_color);
		} catch (IllegalArgumentException iae) {
			LogManager.logError(iae, "Unable to parse font color: " + font_color);
		}
	}
	
	public void setBackground_color(String sBackground_color) {
		if (null == sBackground_color || sBackground_color.length() == 0)
			return;
		String[] colors = sBackground_color.split(",");
		iBackgroundColors = new int[colors.length];
		int i = 0;
		for (String color : colors) {
			color = color.trim();			
			try {
				iBackgroundColors[i++] = Color.parseColor(color);
			} catch (IllegalArgumentException iae) {
				LogManager.logError(iae, "Unable to parse background font color: " + sBackground_color);
			}
		}
	}
/*	
	public void setAlpha(int alpha){
		iAlpha = alpha;
	}
*/	
	public void setJustify(String justify){
		if (null == justify || justify.length() == 0)
			return;
		iJustify = Gravity.NO_GRAVITY; // NOT SET
//		boolean bVertical = false, bHorizontal = false;
		String[] justifies = justify.split("\\|");
		for (int i = 0; i < justifies.length; i++) {			
			if (justifies[i].trim().equalsIgnoreCase("LEFT"))
				iJustify |= Gravity.LEFT;
			else if (justifies[i].trim().equalsIgnoreCase("CENTER"))
				iJustify |= Gravity.CENTER;
			else if (justifies[i].trim().equalsIgnoreCase("RIGHT"))
				iJustify |= Gravity.RIGHT;
			else if (justifies[i].trim().equalsIgnoreCase("TOP"))
				iJustify |= Gravity.TOP;
			else if (justifies[i].trim().equalsIgnoreCase("BOTTOM"))
				iJustify |= Gravity.BOTTOM;
			else if (justifies[i].trim().equalsIgnoreCase("CENTER_VERTICAL"))
				iJustify |= Gravity.CENTER_VERTICAL;
			else if (justifies[i].trim().equalsIgnoreCase("CENTER_HORIZONTAL"))
				iJustify |= Gravity.CENTER_HORIZONTAL;
		}
//		if (iJustify == Gravity.CENTER) {
//			if (bVertical)
//				iJustify = Gravity.CENTER_VERTICAL;
//			else if (bHorizontal)
//				iJustify = Gravity.CENTER_HORIZONTAL;
//		}
	}

	public void setBorderColor(String borderColor){
		if (null == borderColor || borderColor.length() == 0)
			return;
		borderColor = borderColor.trim();
		try {
			if (null != borderColor)
				iBorderColor = Color.parseColor(borderColor);
		} catch (IllegalArgumentException iae) {
			LogManager.logError(iae, "Unable to parse border color: " + borderColor);
		}
	}
	
	public void setPadding(String padding){
		if (null == padding || padding.length() == 0)
			return;
		String[] paddings = padding.split(",");
		if (paddings.length != 4)
			return;
		try {
			sPaddingLeft = paddings[0].trim();
			sPaddingTop = paddings[1].trim();
			sPaddingRight = paddings[2].trim();
			sPaddingBottom = paddings[3].trim();
		} catch (NumberFormatException nfe) {
			LogManager.logError("unable to parse appearance. Padding: " + padding);
		}
	}
	
	public void setBorderRadius(String sRadii){
		if (null == sRadii || sRadii.length() == 0)
			return;
		String[] radii = sRadii.split(",");
		if (!(radii.length == 1 || radii.length == 8))
			LogManager.logError("Unable to read border_radius: " + sRadii + ". It must have either 1 or 8 values");
		fBorderRadii = new float[radii.length];
		int i = 0;
		for (String radius : radii) {
			radius = radius.trim();
			if (radius.length() == 0)
				continue;
			try {
				fBorderRadii[i++] = Float.parseFloat(radius);
			} catch (IllegalArgumentException iae) {
				LogManager.logError(iae, "Unable to parse border radii: " + sRadii);
			}
		}
	}
	/*	
	public void setFont_style(String font_style){
		sFontStyle=font_style;
	}
	
	public void setFont_style_bold(boolean font_style_bold){
			bFontStyleBold = font_style_bold;
	}
	
	public void setFont_style_italic(boolean font_style_italic){
		bFontStyleItalic = font_style_italic;
	}
	
	public void setFont_style_underline(boolean font_style_underline){
		bFontStyleUnderline = font_style_underline;
	}
	
	public void setFont_style_strikethrough(boolean font_style_strikethrough){
		bFontStyleStrikethrough = font_style_strikethrough;
	}
	
	public void setFont_style_double_strikethrough(boolean font_style_double_strikethrough){
		bFontStyleDoubleStrikethrough = font_style_double_strikethrough;
	}
	
	public void setFont_style_superscript(boolean font_style_superscript){
		bFontStyleSuperscript = font_style_superscript;
	}
	
	public void setFont_style_subscript(boolean font_style_subscript){
		bFontStyleSubscript = font_style_subscript;
	}
	
	public void setFont_style_shadow(boolean font_style_shadow){
		bFontStyleShadow = font_style_shadow;
	}
	
	public void setFont_style_outline(boolean font_style_outline){
			bFontStyleOutline = font_style_outline;
	}
	
	public void setFont_style_emboss(boolean font_style_emboss){
		bFontStyleEmboss = font_style_emboss;
	}
	
	public void setFont_style_engrave(boolean font_style_engrave){
		bFontStyleEngrave = font_style_engrave;
	}
	*/
	
	public boolean parseExtendedProperties() {
	    String sAttr = null;
	    boolean bReturn = true;
	    try {
	    	sAttr = mExtendedProperties.get("text_shadow_radius");
	    	if (sAttr != null)
		    	fShadowRadius = Float.parseFloat(sAttr);
	    } catch (NumberFormatException nfe) {
	    	LogManager.logError("Unable to read appearance: " + sName + ". text_shadow_radius must be of type float.");
	    	bReturn = false;
	    }
	    try {
	    	sAttr = mExtendedProperties.get("text_shadow_color");
		    if (sAttr != null)
		    	iShadowColor = Color.parseColor(sAttr);
	    } catch (NumberFormatException nfe) {
	    	LogManager.logError("Unable to read appearance: " + sName + ". text_shadow_color cannot be interpreted.");
	    	bReturn = false;
	    }
	    try {
		    sAttr = mExtendedProperties.get("text_shadow_horizontal_offset");
		    if (sAttr != null)
		    	fShadowHorizontalOffset = Float.parseFloat(sAttr);
	    } catch (NumberFormatException nfe) {
	    	LogManager.logError("Unable to read appearance: " + sName + ". text_shadow_horizontal_offset must be of type float.");
	    	bReturn = false;
	    }
	    try {
		    sAttr = mExtendedProperties.get("text_shadow_vertical_offset");
		    if (sAttr != null)
		    	fShadowVerticalOffset = Float.parseFloat(sAttr);
	    } catch (NumberFormatException nfe) {
	    	LogManager.logError("Unable to read appearance: " + sName + ". text_shadow_vertical_offset must be of type float.");
	    	bReturn = false;
	    }
	    sAttr = mExtendedProperties.get("gradient_orientation");
	    if (sAttr != null) {
	    	if (sAttr.equalsIgnoreCase("BL_TR"))
    			mGradientOrientation = GradientDrawable.Orientation.BL_TR;
	    	else if (sAttr.equalsIgnoreCase("BOTTOM_TOP"))
	    		mGradientOrientation = GradientDrawable.Orientation.BOTTOM_TOP;
	    	else if (sAttr.equalsIgnoreCase("BR_TL"))
	    		mGradientOrientation = GradientDrawable.Orientation.BR_TL;
	    	else if (sAttr.equalsIgnoreCase("LEFT_RIGHT"))
	    		mGradientOrientation = GradientDrawable.Orientation.LEFT_RIGHT;
	    	else if (sAttr.equalsIgnoreCase("LEFT_RIGHT"))
	    		mGradientOrientation = GradientDrawable.Orientation.RIGHT_LEFT;
	    	else if (sAttr.equalsIgnoreCase("TL_BR"))
	    		mGradientOrientation = GradientDrawable.Orientation.TL_BR;
	    	else if (sAttr.equalsIgnoreCase("TR_BL"))
	    		mGradientOrientation = GradientDrawable.Orientation.TR_BL;
	    	else 
	    		mGradientOrientation = GradientDrawable.Orientation.TOP_BOTTOM;
		} 
    	else
    		mGradientOrientation = GradientDrawable.Orientation.TOP_BOTTOM;

	    sAttr = mExtendedProperties.get("gradient_shape");
	    if (sAttr != null) {
	    	if (sAttr.equalsIgnoreCase("LINE"))
		    	iGradientShape = GradientDrawable.LINE;
	    	else if (sAttr.equalsIgnoreCase("OVAL"))
		    	iGradientShape = GradientDrawable.OVAL;
	    	else if (sAttr.equalsIgnoreCase("RING"))
		    	iGradientShape = GradientDrawable.RING;
	    	else
		    	iGradientShape = GradientDrawable.RECTANGLE;
	    } else
	    	iGradientShape = GradientDrawable.RECTANGLE;
	    
	    sAttr = mExtendedProperties.get("gradient_type");
	    if (sAttr != null) {
	    	if (sAttr.equalsIgnoreCase("RADIAL_GRADIENT"))
		    	iGradientType= GradientDrawable.RADIAL_GRADIENT;
	    	else if (sAttr.equalsIgnoreCase("SWEEP_ORIENTATION"))
		    	iGradientType = GradientDrawable.SWEEP_GRADIENT;
	    	else
		    	iGradientType = GradientDrawable.LINEAR_GRADIENT;
	    } else
	    	iGradientType = GradientDrawable.LINEAR_GRADIENT;
	    
	    if (mExtendedProperties.containsKey("image"))
	    	sImageName = mExtendedProperties.get("image");
	    sAttr = mExtendedProperties.get("applies_when");
	    states = parseStates(sAttr);
	    
		return bReturn;
	}
	
	private int[][] parseStates(String sData) {
		int[][] states = null;
//		String[] sStateGroups = sData != null ? sData.split("((?=<)|(?<=>))(?=([^\"]*\"[^\"]*\")*[^\"]*$)") : null;
    	String[] sStateGroups = sData != null ? sData.split("((?=\\[)|(?<=\\]))(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)") : null;
		if (null == sStateGroups || sStateGroups.length == 0) {
			states = new int[1][];
			states[0] = StateSet.WILD_CARD;
		} else {
			Vector<int[]> vStates = new Vector<int[]>();
			
//			states = new int[sStateGroups.length][];
			for (int i = 0; i < sStateGroups.length; i++) {
				if (null == sStateGroups[i])
					continue;
				sStateGroups[i] = sStateGroups[i].trim();
				if (sStateGroups[i].length() > 0 && sStateGroups[i].charAt(0) == Constants.BEGIN_PARAMETER_IDENTIFIER && 
					sStateGroups[i].charAt(sStateGroups[i].length()-1) == Constants.END_PARAMETER_IDENTIFIER)
					vStates.add(getStates(Utilities.splitCommaSeparatedString(sStateGroups[i].substring(1, sStateGroups[i].length()-1))));
			}
			if (vStates.size() > 0) {
				states = new int[vStates.size()][];
				states = (int[][])vStates.toArray(new int[vStates.size()][]);
			}
		}
		return states;
	}
	
	private int[] getStates(String[] sStates) {
		if (null == sStates)
			return null;
		
		int[] states = new int[sStates.length];
		for (int j = 0; j < sStates.length; j++) {		
			if (sStates[j].equalsIgnoreCase("disabled"))
				states[j] = -android.R.attr.state_enabled;
			else if (sStates[j].equalsIgnoreCase("enabled"))
				states[j] = android.R.attr.state_enabled;
			else if (sStates[j].equalsIgnoreCase("not_pressed"))
				states[j] = -android.R.attr.state_pressed;
			else if (sStates[j].equalsIgnoreCase("pressed"))
				states[j] = android.R.attr.state_pressed;
			else if (sStates[j].equalsIgnoreCase("not_focused"))
				states[j] = -android.R.attr.state_focused;
			else if (sStates[j].equalsIgnoreCase("focused"))
				states[j] = android.R.attr.state_focused;
			else if (sStates[j].equalsIgnoreCase("not_selected"))
				states[j] = -android.R.attr.state_selected;
			else if (sStates[j].equalsIgnoreCase("selected"))
				states[j] = android.R.attr.state_selected;
			else if (sStates[j].equalsIgnoreCase("not_checked"))
				states[j] = -android.R.attr.state_checked;
			else if (sStates[j].equalsIgnoreCase("checked"))
				states[j] = android.R.attr.state_checked;
		}
		return states;
	}
}

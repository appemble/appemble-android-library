/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import android.database.Cursor;
import android.database.SQLException;

import com.appemble.avm.ConfigManager;
import com.appemble.avm.ConfigManager.Validation;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ScreenDeckModel;

public class ScreenDeckDao {

	static public ScreenDeckModel getScreenDeck(String sDbName) {
		if (null == sDbName)
			return null;
		Cursor cursor = null; ScreenDeckModel newScreenDeck = null;
		try {
			// here set the limit to 1
			cursor = TableDao.selectData(sDbName, "_screen_deck", null, null, null, null, null, null);
	        if (null == cursor || false == cursor.moveToNext())
	        	LogManager.logError("Unable to get the table _screendeck in the database: " + sDbName);// we have a problem. TODO Log error here;
	        else
	        	newScreenDeck = getScreenDeckModel(cursor, sDbName);
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch screendeck from the database.");
		} finally {
			if (null != cursor)
		        cursor.close();
		}
		return newScreenDeck;
	}

	public static ScreenDeckModel getScreenDeckModel(Cursor cursor, String sDbName) {
		if (null == cursor)
			return null;
		int iColumnIndex = -1;		
    	ScreenDeckModel newScreenDeck = new ScreenDeckModel();
    	newScreenDeck.id = (cursor.getInt(cursor.getColumnIndex("_id")));
    	newScreenDeck.sVersion = (cursor.getString(cursor.getColumnIndex("version")));
    	newScreenDeck.sName = (cursor.getString(cursor.getColumnIndex("name")));
    	newScreenDeck.sShortDescription = (cursor.getString(cursor.getColumnIndex("short_description")));
    	//newScreenDeck.sLongDescription = (cursor.getString(cursor.getColumnIndex("long_description")));
    	newScreenDeck.iSize = (cursor.getInt(cursor.getColumnIndex("size")));
    	newScreenDeck.sPublisherName = (cursor.getString(cursor.getColumnIndex("publisher_name")));
    	String sDate = cursor.getString(cursor.getColumnIndex("publish_date"));
    	Validation screenDeckValidation = ConfigManager.getInstance().mScreenDeckValidation.get("publish_date");
    	String sDateFormat = null;
    	if (null != screenDeckValidation)
    		sDateFormat = screenDeckValidation.sFormat;
    	if (sDate != null) {
	    	SimpleDateFormat sdf = new SimpleDateFormat(sDateFormat != null ? sDateFormat : "yyyy-MM-dd");
			try {
				newScreenDeck.dPublishDate = sdf.parse(sDate);
			} catch (ParseException pe) {
				LogManager.logError(pe, "Unable to parse publish_date. It should be formatted as yyyy-MM-dd.");
			}
    	}
       	newScreenDeck.sTitleBackground = (cursor.getString(cursor.getColumnIndex("title_background")));
    	newScreenDeck.sBackground = (cursor.getString(cursor.getColumnIndex("background")));
	    if ((iColumnIndex = cursor.getColumnIndex("starting_screen_type")) > -1)
	    	newScreenDeck.sStartingScreenType = (cursor.getString(cursor.getColumnIndex("starting_screen_type")));
	    if ((iColumnIndex = cursor.getColumnIndex("misc1")) > -1)
	    	newScreenDeck.sMisc1 = (cursor.getString(iColumnIndex));
	    if ((iColumnIndex = cursor.getColumnIndex("misc2")) > -1)
	    	newScreenDeck.sMisc2 = (cursor.getString(iColumnIndex));    	
	    if ((iColumnIndex = cursor.getColumnIndex("starting_screen_name")) > -1)
	    	newScreenDeck.sStartingScreenName = (cursor.getString(cursor.getColumnIndex("starting_screen_name")));
	    if ((iColumnIndex = cursor.getColumnIndex("extended_properties")) > -1)
	    	newScreenDeck.mExtendedProperties = Utilities.getNameValuePairMap(cursor.getString(
	    			cursor.getColumnIndex("extended_properties")));
	    if (null == newScreenDeck.mExtendedProperties)
	    	newScreenDeck.mExtendedProperties = new HashMap<String, String>();
	    String sDefaultTitleBar = newScreenDeck.mExtendedProperties.get("default_title_bar");
	    if (null != sDefaultTitleBar)
	    	newScreenDeck.iDefaultTitleBar = Utilities.parseBoolean(sDefaultTitleBar) ? 1 : 0; // 0 = false, 1 = true 
    	newScreenDeck.sDbName = (sDbName);
   		return newScreenDeck;
	}
}

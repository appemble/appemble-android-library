/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao;

import java.util.HashMap;

import android.database.Cursor;
import android.database.SQLException;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.AppearanceModel;

public class AppearanceDao {

	public static AppearanceModel getAppearance(String sDbName, String sAppearanceName){
	    String sSQL = "SELECT * FROM _appearance WHERE " + Constants.sDefaultFilter + " and name = ?";
		String[] bindArgs = new String[]{ sAppearanceName };
		return retrieveAppearance(sDbName, sSQL, bindArgs);
	}
	
	public static AppearanceModel getAppearance(String sDbName, int appearanceId){
	    String sSQL = "SELECT * FROM _appearance WHERE " + Constants.sDefaultFilter + " and _id = ?";
		String[] bindArgs = new String[]{Integer.toString(appearanceId)};
		return retrieveAppearance(sDbName, sSQL, bindArgs);
	}
	
	public static AppearanceModel retrieveAppearance(String sDbName, String sSQL, String[] bindArgs){
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
//		if (null == db) {
//			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
//			return null; 
//		}
		AppearanceModel appearance=null;
		Cursor cur = null;
		try {
//		    cur = db.rawQuery(sSQL, bindArgs);
		    cur = (Cursor)TableDao.executeSQL(sDbName, sSQL, bindArgs);
			if (null == cur || false == cur.moveToNext())
				return null;
			appearance = new AppearanceModel();
			int iColumnIndex = 0;
			
			appearance.id = cur.getInt(cur.getColumnIndex("_id"));
			appearance.bMarkDeleted = (cur.getInt(cur.getColumnIndex("mark_deleted")) == 1);
			// appearance.setScreen_deck_id(cur.getInt(cur.getColumnIndex("screen_deck_id")));
			if ((iColumnIndex = cur.getColumnIndex("name")) > -1)
				appearance.sName = (cur.getString(cur.getColumnIndex("name")));
			appearance.sFontFamily = (cur.getString(cur.getColumnIndex("font_family")));
			appearance.sFontName = (cur.getString(cur.getColumnIndex("font_name")));
//			appearance.fFontSize = (cur.getFloat(cur.getColumnIndex("font_size")));
			appearance.sFontSize = (cur.getString(cur.getColumnIndex("font_size")));
			appearance.setFont_color(cur.getString(cur.getColumnIndex("font_color")));
			appearance.setBackground_color(cur.getString(cur.getColumnIndex("background_color")));
			appearance.iAlpha = (cur.getInt(cur.getColumnIndex("alpha")));
			appearance.setJustify(cur.getString(cur.getColumnIndex("justify")));
			appearance.sFontStyle = (cur.getString(cur.getColumnIndex("font_style")));
			appearance.bFontStyleBold = (cur.getInt(cur.getColumnIndex("font_style_bold")) == 1);
			appearance.bFontStyleItalic = (cur.getInt(cur.getColumnIndex("font_style_italic")) == 1);
			appearance.bFontStyleUnderline = (cur.getInt(cur.getColumnIndex("font_style_underline")) == 1);
			if ((iColumnIndex = cur.getColumnIndex("border_width")) > -1)
				appearance.iBorderWidth = (int)(cur.getInt(iColumnIndex) * Cache.fScaleDPToPixels);
		     if ((iColumnIndex = cur.getColumnIndex("border_color")) > -1) {
		    	 String sBorderColor = cur.getString(iColumnIndex);
		    	 appearance.setBorderColor(sBorderColor);
		     }
		     if ((iColumnIndex = cur.getColumnIndex("border_radius")) > -1)
		    	 appearance.setBorderRadius(cur.getString(iColumnIndex));
		     if ((iColumnIndex = cur.getColumnIndex("padding")) > -1)
		    	 appearance.setPadding(cur.getString(iColumnIndex));
			 				 
			appearance.bFontStyleDoubleStrikethrough = (cur.getInt(cur.getColumnIndex("font_style_double_strikethrough")) == 1);
			appearance.bFontStyleEmboss = (cur.getInt(cur.getColumnIndex("font_style_emboss")) == 1);
			appearance.bFontStyleEngrave = (cur.getInt(cur.getColumnIndex("font_style_engrave")) == 1);
			appearance.bFontStyleOutline = (cur.getInt(cur.getColumnIndex("font_style_outline")) == 1);
			appearance.bFontStyleShadow = (cur.getInt(cur.getColumnIndex("font_style_shadow")) == 1);
			appearance.bFontStyleStrikethrough = (cur.getInt(cur.getColumnIndex("font_style_strikethrough")) == 1);
			appearance.bFontStyleSubscript = (cur.getInt(cur.getColumnIndex("font_style_subscript")) == 1);
			appearance.bFontStyleSuperscript = (cur.getInt(cur.getColumnIndex("font_style_superscript")) == 1);
		    if ((iColumnIndex = cur.getColumnIndex("extended_properties")) > -1)
			   	appearance.mExtendedProperties = Utilities.getNameValuePairMap(cur.getString(
			   			cur.getColumnIndex("extended_properties")));
		    if (null == appearance.mExtendedProperties)
		    	appearance.mExtendedProperties = new HashMap<String, String>();
		    if (false == appearance.parseExtendedProperties())
		    	return null;
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Error in fetching appearance from the database.");
		} finally {
			if (null != cur)
				cur.close();
		}
		return appearance;
	}
}

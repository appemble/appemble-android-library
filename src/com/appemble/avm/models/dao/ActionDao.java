/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao;

import java.util.HashMap;
import java.util.Vector;

import android.database.Cursor;
import android.database.SQLException;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ActionModel;

public class ActionDao {
	public static ActionModel[] getActionsForControl(String sSystemDbName,
			String sContentDbName, int iControlId, int iEventType) {
	    String sSQL = "SELECT * FROM _action WHERE " + Constants.sDefaultFilter + 
	    	" and (preceding_action is null or preceding_action = 0) and control_id = ? ";
		if (iEventType != 0)
			sSQL += " and event_list like ?";
	    
		String[] bindArgs = null;
		if (iEventType != 0)
			bindArgs = new String[] { Integer.toString(iControlId), "%"+ActionModel.getEventString(iEventType)+"%" };
		else
			bindArgs = new String[] { Integer.toString(iControlId) };
				
		return getActions(sSystemDbName, sContentDbName, sSQL, bindArgs);
	}

	public static ActionModel[] getActions(String sSystemDbName,
			String sContentDbName, int iEventType) {
		if (null == sSystemDbName || null == sContentDbName || 0 == iEventType)
			return null;
		String sEvent = ActionModel.getEventString(iEventType);
		if (null == sEvent)
			return null;
		sEvent = "%"+sEvent+"%";
	    String sSQL = "SELECT * FROM _action WHERE " + Constants.sDefaultFilter + 
	    	" and (preceding_action is null or preceding_action = 0) and (control_id=0 or control_id is null) and (screen_id is null or screen_id=0) and event_list like ?";
		String[] bindArgs =  { sEvent };
	    
		return getActions(sSystemDbName, sContentDbName, sSQL, bindArgs);
	}

	public static ActionModel[] getActionsForScreen(String sSystemDbName,
			String sContentDbName, int screenId, String eventType, boolean bScreenOnly) {
		if (0 == screenId)
			return null;
	    String sSQL = "SELECT * FROM _action WHERE " + Constants.sDefaultFilter + 
			" and (preceding_action is null or preceding_action = 0) and screen_id = ? ";
	    if (bScreenOnly)
	    	sSQL += "and (control_id=0 or control_id is null) ";
		String[] bindArgs = null;
		if (eventType != null) {
			sSQL += " and event_list like ?";
			bindArgs = new String[] { Integer.toString(screenId), "%"+eventType+"%" };
		}
		else
			bindArgs = new String[] { Integer.toString(screenId) };
				
		return getActions(sSystemDbName, sContentDbName, sSQL, bindArgs);
	}

	public static ActionModel getActionForId(String sSystemDbName,
			String sContentDbName, int iActionId) {
	    String sSQL = "SELECT * FROM _action WHERE " + Constants.sDefaultFilter + " and _id = ?";	    
	    String[] bindArgs = new String[] { Integer.toString(iActionId) };
			
	    ActionModel[] actions = getActions(sSystemDbName, sContentDbName, sSQL, bindArgs);
	    if (null != actions)
	    	return actions[0];
	    return null;
	}

	public static ActionModel[] getNextActions(String sSystemDbName,
			String sContentDbName, int iActionId) {
	    String sSQL = "SELECT * FROM _action WHERE " + Constants.sDefaultFilter + " and preceding_action = ?";	    
	    String[] bindArgs = new String[] { Integer.toString(iActionId) };
			
	    return getActions(sSystemDbName, sContentDbName, sSQL, bindArgs);
	}

	public static ActionModel[] getActions(String sSystemDbName, String sContentDbName, 
			String sQuery, String[] bindArgs) {
		ActionModel[] actions = null;
		Cursor cur = null;

		try {
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sQuery, bindArgs);
		    actions = getActions(sSystemDbName, sContentDbName, cur);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to fetch actions from the database: " + sQuery);
		} finally {
			if (null != cur)
				cur.close();
		}
		return actions;
	}

	public static ActionModel[] getActions(String sSystemDbName, String sContentDbName, 
			String sFilter) {
		ActionModel[] actions = null;
		Cursor cur = null;

		try {
			cur = TableDao.selectData(sSystemDbName, "_action", null, sFilter, null, null, null, null);
			actions = getActions(sSystemDbName, sContentDbName, cur);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to get actions with the following condition: " + sFilter);
		} finally {
			if (null != cur)
				cur.close();
		}
		return actions;
	}

	static ActionModel[] getActions(String sSystemDbName, String sContentDbName, Cursor cur) {
		int iIndex = 0;
		ActionModel action = null;
		ActionModel[] actions = null;
		while (null != cur && cur.moveToNext()) {
			if (null == actions)
				actions = new ActionModel[cur.getCount()];
			action = new ActionModel(sSystemDbName, sContentDbName);

			action.iId = (cur.getInt(cur.getColumnIndex("_id")));
			action.iScreenDeckId = (cur.getInt(cur.getColumnIndex("screen_deck_id")));
			action.iScreenId = (cur.getInt(cur.getColumnIndex("screen_id")));
			action.iControlId = (cur.getInt(cur.getColumnIndex("control_id")));
			action.setActionName((cur.getString(cur.getColumnIndex("action_name"))));
			action.setEventList(cur.getString(cur.getColumnIndex("event_list")));

			if (null == action.metaData)
				LogManager.logError("Unable to recognize action " + action.getActionName() + ". Its definition is missing in the configuration file.");
			action.sTarget = (cur.getString(cur.getColumnIndex("target")));

			// Move this logic to respective actions if performance
			// improvement needed
			action.vInputParameters = parseParameterList(cur.getString(cur.getColumnIndex("input_parameter_list")));
			action.vTargetParameters = parseParameterList(cur.getString(cur.getColumnIndex("target_parameter_list")));
			int iColumnIndex = -1;
			if ((iColumnIndex = cur.getColumnIndex("preceding_action")) > -1)
				action.iPrecedingActionId = (cur.getInt(iColumnIndex));
			action.bMarkDeleted = (cur.getInt(cur
					.getColumnIndex("mark_deleted")) == 1);
			if ((iColumnIndex = cur.getColumnIndex("misc1")) > -1)
				action.sMisc1 = (cur.getString(iColumnIndex));
			if ((iColumnIndex = cur.getColumnIndex("misc2")) > -1)
				action.sMisc2 = (cur.getString(iColumnIndex));
		    if ((iColumnIndex = cur.getColumnIndex("extended_properties")) > -1)
			   	action.mExtendedProperties = Utilities.getNameValuePairMap(cur.getString(
			    	cur.getColumnIndex("extended_properties")));
		     if (null == action.mExtendedProperties)
		    	 action.mExtendedProperties = new HashMap<String, String>(); 
			actions[iIndex++] = action;
		}
		return actions;
	}

	public static Vector<String> parseParameterList(String sParameterList) {
		if (null == sParameterList || sParameterList.length() == 0)
			return null;
		String[] sFieldList = Utilities
				.splitCommaSeparatedString(sParameterList);
		Vector<String> fieldListVector = new Vector<String>();
		for (int i = 0; i < sFieldList.length; i++) {
			if (null != sFieldList[i]) {
				String sField = sFieldList[i].trim();
				if (sField.charAt(0) == '\"') // remove the single
												// or double quotes.
					sField = sField.substring(1,
							sField.length() - 1);
				fieldListVector.add(sField);
			}
		}
		return (fieldListVector);		
	}
}
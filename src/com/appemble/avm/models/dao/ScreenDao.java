/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao;

import java.util.HashMap;
import java.util.Vector;

import android.database.Cursor;
import android.database.SQLException;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ScreenModel;

public class ScreenDao {

	public static Vector <ScreenModel> getScreenGroup(String sSystemDbName, String sContentDbName, String sGroupName) {
		if (null == sGroupName) {
			LogManager.logError("Unable to get the TABS screen as the Tab Group Name is missing");
			return null;
		}
	    String sSQL = "SELECT * FROM _screen WHERE " + Constants.sDefaultFilter + " and tab_group_name =  ? and menuOrder >= 0 ORDER BY menuOrder";
		String[] bindArgs = new String[]{sGroupName};
		Vector<ScreenModel> vScreens = getScreens(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vScreens;
	}

	public static ScreenModel getScreenByName(String sSystemDbName, String sContentDbName, String screenName) {
		if (null == sSystemDbName || null == sContentDbName || null == screenName)
			return null;
	    String sSQL = "SELECT * FROM _screen WHERE " + Constants.sDefaultFilter + " and name =  ?";
		String[] bindArgs = new String[]{screenName};
		Vector<ScreenModel> vScreens = getScreens(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return (null != vScreens && vScreens.size() > 0) ? vScreens.get(0) : null;
	}

	public static ScreenModel getScreenById(String sSystemDbName, String sContentDbName, int id) {
		if (null == sSystemDbName || null == sContentDbName || 0 == id)
			return null;
	    String sSQL = "SELECT * FROM _screen WHERE " + Constants.sDefaultFilter + " and _id =  ?";
		String[] bindArgs = new String[]{Integer.toString(id)};
		Vector<ScreenModel> vScreens = getScreens(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vScreens.get(0);
	}

	static Vector <ScreenModel> getScreens(String sSystemDbName, String sContentDbName, String sSQL, String[] bindArgs) {
		if (null == sSystemDbName || null == sContentDbName || null == sSQL)
			return null;
		ScreenModel newScreen = null;
	    Cursor cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
		Vector <ScreenModel> vScreens = null; 
		try {
			// here set the limit to 1
	        while (null != cur && cur.moveToNext()) {
				if (null == vScreens)
					vScreens = new Vector <ScreenModel>();
	        	newScreen = getScreenModel(cur, sSystemDbName, sContentDbName);
	        	vScreens.add(newScreen);
	        }
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Error in fetching a screen: " + sSQL);
		} finally {
			if (null != cur)
		        cur.close();
		}
		return vScreens;
	}

	public static ScreenModel getScreenModel(Cursor cur, String sSystemDbName, String sContentDbName) {
		if (null == cur)
			return null;
		int iColumnIndex = -1;
    	ScreenModel newScreen=new ScreenModel();
    	newScreen.id = (cur.getInt(cur.getColumnIndex("_id")));
    	newScreen.setType(cur.getString(cur.getColumnIndex("screen_type")));
    	newScreen.sName = (cur.getString(cur.getColumnIndex("name")));
    	newScreen.sTitle = (cur.getString(cur.getColumnIndex("title")));
    	newScreen.sIcon = (cur.getString(cur.getColumnIndex("icon")));
    	newScreen.iMenuOrder = (cur.getInt(cur.getColumnIndex("menuOrder")));
    	newScreen.sMenuName = (cur.getString(cur.getColumnIndex("menuName")));
//    	newScreen.setInitialLayout(cur.getString(cur.getColumnIndex("initial_layout")));
    	newScreen.setAllowedLayouts(cur.getString(cur.getColumnIndex("allowed_layouts")));
    	newScreen.fWidth = (cur.getFloat(cur.getColumnIndex("width")));
    	newScreen.fHeight = (cur.getFloat(cur.getColumnIndex("height")));
    	newScreen.setScrolling(cur.getString(cur.getColumnIndex("scroll")));
    	newScreen.setDimensionType(cur.getString(cur.getColumnIndex("dimension_type")));
    	newScreen.sTitleBackground = (cur.getString(cur.getColumnIndex("title_background")));
    	newScreen.sBackground = (cur.getString(cur.getColumnIndex("background")));
    	newScreen.sRemoteDataSource = (cur.getString(cur.getColumnIndex("remote_data_source")));
    	newScreen.setRemoteRequestType(cur.getString(cur.getColumnIndex("remote_request_type")));
    	newScreen.sLocalDataSource = (cur.getString(cur.getColumnIndex("local_data_source")));
    	newScreen.sInitialFocus = (cur.getString(cur.getColumnIndex("initial_focus")));
    	newScreen.bActionYN = (cur.getInt(cur.getColumnIndex("action_yn")) == 1);
    	newScreen.bMarkDeleted = (cur.getInt(cur.getColumnIndex("mark_deleted")) == 1);
    	if ((iColumnIndex = cur.getColumnIndex("on_resume_update")) > -1)
    		newScreen.bOnResumeUpdate = (cur.getInt(iColumnIndex) == 1);
    	if ((iColumnIndex = cur.getColumnIndex("misc1")) > -1)
    		newScreen.sMisc1 = (cur.getString(iColumnIndex));
    	if ((iColumnIndex = cur.getColumnIndex("misc2")) > -1)
    		newScreen.sMisc2 = (cur.getString(iColumnIndex));
    	if ((iColumnIndex = cur.getColumnIndex("tab_group_name")) > -1)
    		newScreen.sTabGroupName = (cur.getString(iColumnIndex));
	    if ((iColumnIndex = cur.getColumnIndex("extended_properties")) > -1)
	    	newScreen.mExtendedProperties = Utilities.getNameValuePairMap(cur.getString(
	    			cur.getColumnIndex("extended_properties")));
	    if (null == newScreen.mExtendedProperties)
	    	newScreen.mExtendedProperties = new HashMap<String, String>(); 
    	newScreen.setAllowReorientation(newScreen.mExtendedProperties.get("allow_reorientation"));
    	newScreen.bRemoteDataSaveLocally = Utilities.parseBoolean(
    			newScreen.mExtendedProperties.get("remote_data_save_locally"));
    	String sTabBarHeight = newScreen.mExtendedProperties.get("tab_bar_height");
    	try {
    	if (null != sTabBarHeight)
    		newScreen.fTabBarHeight = Utilities.getDimension(sTabBarHeight.trim(), 0);
    	} catch(NumberFormatException nfe) {
    		LogManager.logError(nfe, "Unable to parse tar_bar_height");
    	}
	    String sDefaultTitleBar = newScreen.mExtendedProperties.get("default_title_bar");
	    if (null != sDefaultTitleBar)
	    	newScreen.iDefaultTitleBar = Utilities.parseBoolean(sDefaultTitleBar) ? 1 : 0; 
    	newScreen.setControlHeightBasedUpon(newScreen.mExtendedProperties.get("control_height_based_upon"));
    	newScreen.sSystemDbName = (sSystemDbName);
    	newScreen.sContentDbName = (sContentDbName);
   		return newScreen;
	}

	public static String getScreenType(String sSystemDbName, String screenName) {
		String screenType=null; String sSQL = null;

        Cursor cur = null;
		try {
		    sSQL = "SELECT screen_type FROM _screen WHERE " + Constants.sDefaultFilter + " and name =  ?";
			String[] bindArgs = new String[]{screenName};
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        if (null != cur && cur.moveToNext())
	        	screenType=cur.getString(cur.getColumnIndex("screen_type"));
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
				cur.close();
		}
		return screenType;
	}
	
	public static String getScreenName(String sSystemDbName, int screenID) {
		if (screenID <= 0)
			return null;
		String screenName=""; Cursor cur = null;String sSQL = null;
		try {
		    sSQL = "SELECT name FROM _screen WHERE " + Constants.sDefaultFilter + " and _id =  ?";
			String[] bindArgs = new String[]{Integer.toString(screenID)};
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        if (null != cur && cur.moveToNext())
	        	screenName=cur.getString(cur.getColumnIndex("name"));
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
				cur.close();
		}
		return screenName;
	}
	
	public static int getScreenID(String sSystemDbName, String sContentDbName, String screenName) {
		int screenID=1; Cursor cur = null; String sSQL = null;
		try {
		    sSQL = "SELECT _id FROM _screen WHERE " + Constants.sDefaultFilter + " and name = ?";
			String[] bindArgs = new String[]{screenName};
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        if (null != cur && cur.moveToNext())
	        	screenID=cur.getInt(cur.getColumnIndex("_id"));
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
		        cur.close();
		}
		return screenID;
	}

	public static ScreenModel getSplashScreenModel(String sSystemDbName, String sContentDbName) {
		Cursor cur = null; String sSQL = null;
		try {
		    sSQL = "SELECT * FROM _screen WHERE " + Constants.sDefaultFilter + " and screen_type='SPLASH'";
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, null);
			if(null != cur && cur.moveToFirst()) {
	        	return getScreenModel(cur, sSystemDbName, sContentDbName);
		    } else {
		    	return null;
		    }
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
				cur.close();
		}
		return null;
	}

	public static String getFirstViewName(String sSystemDbName) {
		Cursor cur = null; String screenName = null, sSQL = null;
		try {
		    sSQL = "SELECT name FROM _screen WHERE " + Constants.sDefaultFilter + " and id = 1";
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, null);
			if (null != cur && cur.moveToFirst())
				screenName = cur.getString(cur.getColumnIndex("name"));
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
				cur.close();
		}
		return screenName;
	}

	public static HashMap<String, String> get1Row(String sSystemDbName, String screenTableName, String filter) {
		Cursor cur = null;
		HashMap<String, String> fieldListHashMap=new HashMap<String, String>();
		try{
			if(filter==null)
				filter="1=1";
			cur = TableDao.selectData(sSystemDbName, screenTableName, null, Constants.sDefaultFilter + " and " + filter, 
					null, null, null, null);
			if (null != cur && cur.moveToNext()) {
				int iColumnCount = cur.getColumnCount();
				for(int i = 0; i < iColumnCount; i++) {
					fieldListHashMap.put(cur.getColumnName(i), cur.getString(i));	
				}
			}
		} catch(SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + filter);
		} finally {
			if (null != cur)
				cur.close();			
		}
		return fieldListHashMap;
	}
}

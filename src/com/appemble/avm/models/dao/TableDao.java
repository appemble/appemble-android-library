/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
//SQLCipher import net.sqlcipher.database.SQLiteDatabase;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.dao.db.DbConnectionManager;

public class TableDao {
	public static String getColumnValue(String sDbName, String sTableName, String sColumnName, String row_id) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cur = null; String columnValue = null;
		try {
			String[] columns = { sColumnName };
			cur = db.query(sTableName, columns, null != row_id ? "_id=?" : null, 
					null != row_id ? new String[] { row_id } : null, null, null, null, null);
			if (cur.moveToFirst())
				columnValue = cur.getString(cur.getColumnIndex(sColumnName));
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to get a column value. TableName: " + sTableName + ". ColumnName:" + sColumnName);
		} finally {
			if (null != cur)
				cur.close();
		}
		return columnValue;
	}

	public static String getColumnValue(String sDbName, String sTableName, String sColumnName, 
			String sWhereClause, String[] params) {
		Vector<String> values = getColumnValues(sDbName, sTableName, new String[] { sColumnName }, 
				sWhereClause, params);
		if (null != values)
			return values.get(0);
		return null;
	}

	public static HashMap<String, String> get1Row(String sDbName, String sTableName, String sFilter) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cur = null;
		HashMap<String, String> fieldListHashMap = new HashMap<String, String>();
		try {
			if (sFilter == null) {
				sFilter = "1=1";
			}
			cur = db.query(sTableName, null, sFilter, null, null, null,
					null, "1");
			if (cur.moveToNext()) {
				int iCount = cur.getColumnCount();
				for (int i = 0; i < iCount; i++) {
					fieldListHashMap.put(cur.getColumnName(i), cur.getString(i));
				}
			}
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to get a row from content database: " + sTableName + ". Filter: " + sFilter);
		} finally {
			if (null != cur)
				cur.close();			
		}
		return fieldListHashMap;
	}

	public static int getRowCount(String sDbName, String sTableName, String sFilter) {
		if (null == sDbName || null == sTableName)
			return 0;
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return 0;
		}
		Cursor cur = null;String rawQuery = null;
		int iCount = 0; 
		try {
			if (sFilter == null) {
				sFilter = "1=1";
			}
			rawQuery = "select count(*) from " + sTableName + " where " + sFilter;
			cur = db.rawQuery(rawQuery, null);
			if (null != cur && cur.moveToFirst())
				iCount = cur.getInt(0);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to getRowCount: " + rawQuery);
		} finally {
			if (null != cur)
				cur.close();			
		}
		return iCount;
	}

	public static int getRowCount(String sDbName, String sTableName, String sFilter, String[] params) {
		if (null == sDbName || null == sTableName)
			return 0;
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return 0;
		}
		Cursor cursor = null;String rawQuery = null;
		int iCount = 0; 
		try {
			if (sFilter == null) {
				sFilter = "1=1";
			}
			cursor = db.query(sTableName, new String[] { "count(*)" } , sFilter, params, null, null, null);
			if (null != cursor && cursor.moveToFirst())
				iCount = cursor.getInt(0);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to getRowCount: " + rawQuery);
		} finally {
			if (null != cursor)
				cursor.close();			
		}
		return iCount;
	}

	public static Vector<String> getColumnValues(String sDbName,
			String sTableName, String[] columns, String whereClause, String[] params) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cursor = null;
		Vector<String> values = new Vector<String>();
		try {
			cursor = db.query(sTableName, columns, whereClause, params, null, null, null);
			if (cursor.moveToNext()) {
				for (int i = 0; i < columns.length; i++) {
					int iColumnIndex = cursor.getColumnIndex(columns[i]);
					if (iColumnIndex > -1) {
						String columnValue = cursor.getString(iColumnIndex);
						values.add(columnValue);
					}
				}
			}
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to getColumnValues from table: " + sTableName);
		} finally {
			if (null != cursor)
				cursor.close();
		}
		return values.size() > 0 ? values : null;
	}

	public String[] getColumnListValues(String sDbName, String sTableName,
			String[] columnList, int row_id) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cur = null; String columnValueList[] = null;

		try {
			cur = db.query(sTableName, columnList, "_id=" + row_id, null, null,
					null, null);
			if (cur.moveToFirst()) {
				columnValueList = new String[columnList.length];
				for (int i = 0; i < columnList.length; i++) {
					columnValueList[i] = cur.getString(cur.getColumnIndex(columnList[i]));
				}
			}
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to getColumnListValues from table: " + sTableName);
		} finally {
			if (null != cur)
				cur.close();
		}
		return columnValueList;
	}

	public static Boolean tableExists(String sDbName, String sTableName) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cur = null; boolean bExists = false;

		try {
			cur = db.query(sTableName, null, "_id=" + 1, null, null, null, null);
			if (cur.moveToFirst())
				bExists = true; 
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to find table:" + sTableName);
		} finally {
			if (null != cur)
				cur.close();
		}
		return bExists;
	}

	public static HashMap<String, String> getColumnValues(String sDbName,
			String sTableName, String columnName[], int row_id) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		String[] columns = columnName; HashMap<String, String> FieldValues = null;
		Cursor cur = null;
		try {
			cur = db.query(sTableName, columns, "_id=?", new String[] { String.valueOf(row_id) }, null, null,
					null, null);
			if (cur.moveToFirst()) {
				String columnValue;
				if (null == FieldValues)
					FieldValues = new HashMap<String, String>();
				for (int i = 0; i < columns.length; i++) {
					columnValue = cur.getString(cur.getColumnIndex(columns[i]));
					FieldValues.put(columns[i], columnValue);
				}
			}
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to getColumnValues from table: " + sTableName);
		} finally {
			if (null != cur)
				cur.close();
		}
		return FieldValues;
	}

	public static HashMap<String, String> getScreenColumnValues(String sDbName,
			String sTableName, int row_id) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cur = null;
		HashMap<String, String> FieldValues = new HashMap<String, String>();

		try {
			cur = db.query(sTableName, null, "_id=" + row_id, null, null, null,
					null);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to getColumnValues from table: " + sTableName);
		}
		if (cur.getCount() > 0) {
			cur.moveToFirst();
			String columnValue;
			String columnName;
			int iCount = cur.getColumnCount();
			for (int i = 0; i < iCount; i++) {
				columnName = cur.getColumnName(i);
				columnValue = cur.getString(i);
				FieldValues.put(columnName, columnValue);
			}
			cur.close();
		} else {
			cur.close();
		}
		return FieldValues;
	}

	// Insert / Update functions
	public static long insert1Row(String sDbName, String sTableName,
			ContentValues valueList) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return 0; 
		}
		long lNewRowId = 0;

		try {
			lNewRowId = db.insert(sTableName, "", valueList);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to insert a row in table: " + sTableName);
		}
		return lNewRowId;
	}

	public static long replace1Row(String sDbName, String sTableName,
			ContentValues valueList) {
		if (null == sTableName || valueList.size() == 0)
			return 0;
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return 0; 
		}
		long lNewRowId = 0;
		try {
			lNewRowId = db.replace(sTableName, "", valueList);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to replace1Row for table name: " + sTableName);
		}
		return lNewRowId;
	}

	public static long insertRows(String sDbName, String sTableName, JSONArray val_list,
			String sWhereClause, String[] sWhereArgs) {
		if (null == val_list || val_list.length() == 0)
			return -1;
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return 0; 
		}
		int iRowsChanged = 0;
		ContentValues cv = new ContentValues();
		int iCount = val_list.length();
	    String key = null;
        String value = null;
        try {
			for (int i = 0; i < iCount; i++) {
				JSONObject row = val_list.getJSONObject(i);
				if (null == row || row.length() == 0)
					continue;
				@SuppressWarnings("unchecked")
				Iterator<String> iterator = (Iterator<String>)row.keys();
				while (iterator.hasNext()) {
				    key = (String)iterator.next();
			        value = row.getString(key);
			        cv.put(key,value);
		    	}
				iRowsChanged += db.update(sTableName, cv, sWhereClause, sWhereArgs);			
				cv.clear();
			}
        } catch (JSONException jsone) {
        	LogManager.logError(jsone, jsone.getMessage());
        }
		return iRowsChanged;
	}

	public static int update(String sDbName, String sQuery, String[] bindArgs) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return 0; 
		}
		Cursor cursor = null;
		int iUpdatedRows = 0;
		try {
			db.beginTransaction();
			db.execSQL(sQuery, bindArgs);
			cursor = db.rawQuery("select changes()", null);
			if (null != cursor && cursor.moveToFirst())
				iUpdatedRows = cursor.getInt(0);
			if (0 == iUpdatedRows)
				LogManager.logDebug("The query " + sQuery + " did not result any updates in the table");
			db.setTransactionSuccessful();
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to create a cursor for the query: " + sQuery);
		} finally {
			db.endTransaction();
			if (cursor != null)
				cursor.close();
		}
		return iUpdatedRows;
	}

	public static void write2DB(String sDbName, String sTableName,
			String columnName, int row_id, String value) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return; 
		}
		Cursor cur = null;
		try {
			ContentValues values = new ContentValues(1);
			values.put(columnName, value);
			String columnList[] = new String[1];
			columnList[0] = "_id";
			cur = db.query(sTableName, columnList, "_id=" + row_id, null, null, null, null);

			if (cur.getCount() > 1 || cur.getCount() == 0) {
				LogManager.logDebug("ERROR: Multiple row update or No rows to update!");
			}
			db.update(sTableName, values, "_id=" + row_id, null);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to write2DB for table name: " + sTableName);
		} finally {
			if (null != cur)
				cur.close();
		}		
	}

	public static void updateColumn(String sDbName, String sTableName,
			String columnName, String newColumnValue, String whereClause) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return; 
		}
		ContentValues cv = new ContentValues();
		cv.put(columnName, newColumnValue);
		try {
			db.update(sTableName, cv, whereClause, null);
		} catch (SQLException e) {
			LogManager.logError(e, "Unable to update table: " + sTableName + ", column name: " + columnName);
		}
	}

	public static boolean updateColumn(String sDbName, String sTableName,
			ContentValues cv, String whereClause) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return false; 
		}
		try {
			return db.update(sTableName, cv, whereClause, null) > 0;
		} catch (SQLException e) {
			LogManager.logWTF(e, "Unable to update table: " + sTableName);
			return false;
		}

	}

	public static int delete(String sDbName, String sTableName, String sWhereClause, String[] sWhereArgs) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return 0; 
		}
		int del_rows = db.delete(sTableName, sWhereClause, sWhereArgs);
		return del_rows;
	}

	public static String[] getValuesFromDb(String sQuery, String sDbName) {
		if (null == sQuery || sQuery.length() == 0 || null == sDbName || sDbName.length() == 0)
			return null;
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cursor = null;
		String[] sReturnValues = null;
		try {
			Object object = executeSQL(db, sQuery, null);
			if (object instanceof Cursor)
				cursor = (Cursor)object;
			if (null == cursor)
				return null; // error in creating cursor or no rows returned.
			if (cursor.getCount() == 0)
				return null;
			sReturnValues = new String[cursor.getCount()];
			int i = 0;
			cursor.moveToFirst();
			while (i < sReturnValues.length) {
				sReturnValues[i++] = cursor.getString(0);
				cursor.moveToNext();
			}
		} catch (SQLException e) {
			LogManager.logWTF(e, "Unable to get values from db: " + sQuery);
		} finally {
			if (null != cursor)
				cursor.close();
		}
		return sReturnValues;
	}

	public static String[][] getRowsFromDbAsStrings(String sDbName, String sQuery, String[] args) {
		if (null == sQuery || sQuery.length() == 0 || null == sDbName || sDbName.length() == 0)
			return null;
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cursor = null;
		String[][] sReturnValues = null;
		try {
			Object object = executeSQL(db, sQuery, args);
			if (object instanceof Cursor)
				cursor = (Cursor)object;
			if (null == cursor) {
				LogManager.logError("Cannot create cursor for the query: " + sQuery);
				return null; // error in creating cursor or no rows returned.
			}
//			if (cursor.getCount() == 0) {
//				LogManager.logVerbose("The query " + sQuery + " did not return any result.");
//				return null;
//			}
			int i = 0, iColumnCount = cursor.getColumnCount();
			sReturnValues = new String[cursor.getCount()][iColumnCount];
			iColumnCount = cursor.getColumnCount();
			cursor.moveToFirst();
			while (i < sReturnValues.length) {
				int j = 0;
				while (j < iColumnCount)
					sReturnValues[i][j] = cursor.getString(j++);
				i++;
				cursor.moveToNext();
			}
		} catch (SQLException e) {
			LogManager.logWTF(e, "Unable to get rows from db: " + sQuery);
		} finally {
			if (null != cursor)
				cursor.close();
		}
		return sReturnValues;
	}

	public static Object executeSQL(String sDbName, String sQuery, String[] bindArgs) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		return executeSQL(db, sQuery, bindArgs);
	}
	
	public static Cursor createCursor(String sDbName, String sQuery, String[] bindArgs) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Object object = executeSQL(db, sQuery, bindArgs);
		if (object instanceof Cursor) {
			Cursor cursor = (Cursor)object;
			cursor.moveToFirst(); // still move to first as all the callers are assuming this.
			return cursor;
		}
		return null; // no need to log error
	}
	
	public static Object executeSQL(SQLiteDatabase db, String sQuery, String[] bindArgs) {
		if (null == sQuery || null == db)
			return null;
		Cursor cursor = null;

		if (Utilities.isUpdateQuery(sQuery)) {
			int iUpdatedRows = 0;
			try {
				db.beginTransaction();
				if (null != bindArgs)
					db.execSQL(sQuery, bindArgs);
				else
					db.execSQL(sQuery);

				cursor = db.rawQuery("select changes()", null);
				if (null != cursor && cursor.moveToFirst())
					iUpdatedRows = cursor.getInt(0);
				if (0 == iUpdatedRows)
					LogManager.logDebug("The query " + sQuery + " did not result any updates in the table");
				db.setTransactionSuccessful();
			} catch (SQLException sqle) {
				LogManager.logError(sqle, "Error in executing the query: " + sQuery);
				return sqle.getMessage();
			} finally {
				db.endTransaction();
				if (cursor != null)
					cursor.close();
				cursor = null;
			}
			return Integer.valueOf(iUpdatedRows);
		}
		else {
			try {
				cursor = db.rawQuery(sQuery, bindArgs);
			} catch (SQLException sqle) {
				LogManager.logWTF(sqle, "Unable to create a cursor for the query: " + sQuery);
				return null;
			}
//			if (null == cursor) {
//				LogManager.logError("Unable to create cursor for the query" + sQuery);
//				return null; 
//			}
			// commented the code below. It is OK to have a cursor set as null to a list box to show it empty.
			// The fix was made in the List.SetCursor and ArrangedList.setCursor.
			//if (isList) {// for a list (or arrangedlist) control do not check if the data is present or not.
			//	return cursor;
			//}
//			cursor.moveToFirst(); // still move to first as all the callers are assuming this.
// 2012 10 13. Cursor should always be return even if it does not have any content. 			
//			if (false == cursor.moveToFirst()) { 
//				// Data not present in the local database. May be it needs to be fetched from the server. 
//				// Anyway proceed to set default values.
//				cursor.close();
//				cursor = null; 
//			}
			return cursor;
		}
	}
	
	public static Cursor selectData(String sDbName, String table, String[] columns, String selection, 
			String[] selectionArgs, String groupBy, String having, String orderBy) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		return db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
	}

	public static String get1stColumn(String sDbName, String table, String[] columns, String selection, 
			String[] selectionArgs, String groupBy, String having, String orderBy) {
		Cursor cursor = selectData(sDbName, table, columns, selection, selectionArgs, groupBy, having, orderBy);
		String sValue = null;
		try {
			if (null != cursor && cursor.moveToFirst())
				sValue = cursor.getString(0);
		} catch (SQLException sqle) {
			LogManager.logWTF(sqle, "Unable to create a cursor for the query.");
			return null;
		} finally {
			if (null != cursor)
				cursor.close();
		}
		return sValue;
	}

	public static String[] getColumnValues(String sDbName, String sQuery, String[] bindArgs) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		if (null == db) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return null; 
		}
		Cursor cursor = db.rawQuery(sQuery, bindArgs);
		String[] sValues = null;
		try {
			if (null == cursor || false == cursor.moveToFirst())
				return null;
			int iColumnCount = cursor.getColumnCount();
			sValues = new String[iColumnCount];
			
			for (int i = 0; i < iColumnCount; i++)
				sValues[i] = cursor.getString(i);
		} catch (SQLException sqle) {
			LogManager.logWTF(sqle, "Unable to create a cursor for the query.");
			return null;
		} finally {
			if (null != cursor)
				cursor.close();
		}
		return sValues;
	}

	public static int upsert(String sDbName, String sInsertQuery, String[] sInsertBindArgs, 
			String sUpdateQuery, String[] sUpdateBindArgs) {
		int iRowsUpdated = 0;
		if ((iRowsUpdated = TableDao.update(sDbName, sUpdateQuery, sUpdateBindArgs)) > 0)
			return iRowsUpdated;
		return TableDao.update(sDbName, sInsertQuery, sInsertBindArgs);
	}
		
	public static int upsert(String sDbName, String sTableName, String[] sPrimaryColumns, 
		String[] sOtherColumns, String[] sPrimaryValues, String[] sOtherColumnValues, 
		String sModifiedColumnName, String sModifiedColumnValue) {
		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sDbName);
		int iRowsAffected = 0;
		if (null == db || null == sTableName || sPrimaryColumns.length!= sPrimaryValues.length || 
			sOtherColumns.length  != sOtherColumnValues.length) {
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
			return 0; 
		}
		StringBuffer sb = new StringBuffer();
		sb.append("UPDATE ");
		sb.append(sTableName);
		sb.append(" SET ");
		generateNameParameterString(sb, sOtherColumns, '?');
		if (null != sModifiedColumnName && null != sModifiedColumnValue) {
			sb.append(", ");
			sb.append(sModifiedColumnName);
			sb.append("=");
			sb.append(sModifiedColumnValue);
		}
		sb.append(" WHERE ");
		generateNameParameterString(sb, sPrimaryColumns, '?');
		
		// execute the update query
		String[] bindArgs = Utilities.concat(sOtherColumnValues, sPrimaryValues);
		Object o = TableDao.executeSQL(sDbName, sb.toString(), bindArgs);
		if (o instanceof Integer) 
			iRowsAffected = ((Integer)o).intValue();
		if (iRowsAffected > 0)
			return iRowsAffected;
		
		// if 0 rows are inserted, do an insert
		sb.delete(0, sb.length());
		sb.append("INSERT INTO ");
		sb.append(sTableName);
		sb.append(" (");
		generateCSV(sb, sOtherColumns);
		sb.append(", ");
		generateCSV(sb, sPrimaryColumns);
		sb.append(") VALUES (");
		generateParameterCharacter(sb, sPrimaryColumns.length + sOtherColumns.length, ',');
		sb.append(")");
		// execute the insert query
		o = TableDao.executeSQL(sDbName, sb.toString(), bindArgs);
		if (o instanceof Integer)
			iRowsAffected = ((Integer)o).intValue();
		
		// return the result;
		return iRowsAffected;
	}
	
	public static void generateCSV(StringBuffer sb, String[] sFields) {
		if (null == sFields || null == sb)
			return;
		for (int ii = 0; ii < sFields.length; ii++) {
			if (ii > 0)
				sb.append(", ");
			sb.append(sFields[ii]);
		}
	}

	public static void generateParameterCharacter(StringBuffer sb, int iTimes, char c) {
		if (null == sb || iTimes == 0)
			return;
		for (int ii = 0; ii < iTimes; ii++) {
			if (ii > 0)
				sb.append(", ");
			sb.append(c);
		}
	}

	public static boolean isNumericOrNull(String str) {
		if (null == str || "null".equalsIgnoreCase(str))
			return true;
		String sLowerCase = str.toLowerCase();
		if (sLowerCase.startsWith("date"))
			return true;
	    return str.matches("[+-]?\\d*(\\.\\d+)?");
	}
	
	public static void generateNameParameterString(StringBuffer sb, 
		String[] sColumns, char cParameter) {
		if (null == sb || null == sColumns || 0 == cParameter)
			return;
		for (int ii = 0; ii < sColumns.length; ii++) {
			if (ii > 0)
				sb.append(", ");
			sb.append(sColumns[ii]);
			sb.append("= ?");
//			boolean bNumeric = isNumericOrNull(sValues[ii]);
//			if (bNumeric)
//				sb.append('\'');
//			sb.append(sValues[ii]);
//			if (bNumeric)
//				sb.append('\'');			
		}			
	}

}

/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.HttpUtils;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;

public class DbConnectionManager {

	private static DbConnectionManager _dbManager;
	private int iOpenDbTries = 0;

	public static synchronized DbConnectionManager getInstance() {
		if (_dbManager == null) {
// SQLCipher			SQLiteDatabase.loadLibs(Cache.context);
			_dbManager = new DbConnectionManager();
			if (null != Cache.context && null != Cache.bootstrap) {
				Resources res = Cache.context.getResources();
				if (res != null)
					_dbManager.iOpenDbTries = Cache.bootstrap.getIntValue("num_open_db_tries");
				String sOverwrite;
				sOverwrite = Cache.bootstrap.getValue("overwrite_system_db");
				boolean bSystemDbOverwrite = sOverwrite != null && sOverwrite.equalsIgnoreCase("yes");
				sOverwrite = Cache.bootstrap.getValue("overwrite_content_db");
				boolean bContentDbOverwrite = sOverwrite != null && sOverwrite.equalsIgnoreCase("yes");
				_dbManager.copyDefaultDb(bSystemDbOverwrite, bContentDbOverwrite); // Overwrite if debug mode == true
			}
		}
		return _dbManager;
	}

	Map<String, DbHelperInternalStorage> mDatabases = new HashMap<String, DbHelperInternalStorage>();

	/**
	 * Constructs the database. Does not open
	 */
	public DbConnectionManager() {
	}

	/**
	 * Checks the database state and throws an {@link IllegalStateException} if
	 * database isn't open. Should always be used before starting to access the
	 * database.
	 * 
	 * @param type
	 *            Type of the database. Can be INTERNAL or EXTERNAL.
	 */
	public void checkDbState(String sDbName) {
		SQLiteDatabase db = getDb_(sDbName);
		if (null == db || !db.isOpen())
			throw new IllegalStateException("The database has not been opened");
	}

	/**
	 * Closes the database of the given type.
	 * 
	 * @param sDbName
	 *            : name of the database.
	 */
	public void close(String sDbName) {
		DbHelperInternalStorage db = mDatabases.get(sDbName);
		if (null != db)
			db.close();
		mDatabases.remove(sDbName);
	}

	/**
	 * @param type
	 *            Type of the database. Can be INTERNAL or EXTERNAL.
	 * @return true if the database is open, false otherwise.
	 */
	public boolean isOpen(String sDbName) {
		SQLiteDatabase db = getDb_(sDbName);
		return (db != null && db.isOpen());
	}

	/**
	 * Opens the default database.
	 * 
	 * @param type
	 *            Type of the database. Can be INTERNAL or EXTERNAL.
	 */
	public SQLiteDatabase open(String sDbName) {
		if (null == sDbName)
			return null;
		if (null == Cache.context)
			return null;
		if (mDatabases.get(sDbName) == null) {
			int versionNumber = 0;
			String sContentDbEncryptionKeys = null;
			try {
				String sSystemDbName = Cache.bootstrap.getValue("system_dbname");
				if (null == sSystemDbName)
					return null;
				if (sDbName.equalsIgnoreCase(sSystemDbName)) {
					PackageInfo pinfo;
					pinfo = Cache.context.getPackageManager().getPackageInfo(Cache.context.getPackageName(), 0);
					versionNumber = pinfo.versionCode;
				} else {
					String sContentDbName = Cache.bootstrap.getValue("content_dbname");
					if (null == sContentDbName)
						return null;
					if (sDbName.equalsIgnoreCase(sContentDbName)) {
						versionNumber = Cache.bootstrap.getIntValue("content_db_version");
						sContentDbEncryptionKeys = Cache.bootstrap.getContentDbEncryptionKeys();
					}
				}
			} catch (NameNotFoundException e) {
				LogManager.logWTF(e, "Error while opening a database connection. Application Packaget not found");
			}
			String sDbNameFullPath = null;
			File file = Cache.context.getDatabasePath(sDbName);
			sDbNameFullPath = file.getAbsolutePath();
			DbHelperInternalStorage dbHelper = new DbHelperInternalStorage(Cache.context,
					sDbNameFullPath, versionNumber, sContentDbEncryptionKeys);
			mDatabases.put(sDbName, dbHelper);
		}

		SQLiteDatabase db = getDb_(sDbName);
		if (null == db) { // try to open it
			DbHelperInternalStorage databaseObject = mDatabases.get(sDbName);
			db = databaseObject.openDatabase();
		}
		if (null == db)
			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
		return db;
	}

	/**
	 * gets the database for a given name.
	 * 
	 * @param sDbName
	 *            : name of the database.
	 */
	public SQLiteDatabase getDb(String sDbName) {
		int iTry = 0;
		SQLiteDatabase db = getDb_(sDbName);

		while (db == null && iTry++ < iOpenDbTries) {
			close(sDbName);
			db = open(sDbName);
			if (null != db)
				return db;
		}
		return db;
	}

	private SQLiteDatabase getDb_(String sDbName) {
		SQLiteDatabase db = null;
		DbHelperInternalStorage databaseObject = mDatabases.get(sDbName);
		if (databaseObject == null)
			return null;
		db = databaseObject.getDb();
		if (db.isOpen() == false) { // if the db is closed because of inactivity, return false.
			close(sDbName);
			return null;
		}
		Cursor cursor = null;
		try {
			String sQuery = "SELECT count(*) FROM sqlite_master WHERE type='table';";
			cursor = db.rawQuery(sQuery, null);
			if (null != cursor) {
				cursor.moveToFirst();
				cursor.close();
			}
		} catch (SQLException sqle) {
			close(sDbName);
			LogManager.logWTF(sqle, "Unable to get database connection for: " + sDbName);
			return null;
		}
		return db;
	}

	public String copyDb(String sSource, boolean onExternalStorage, boolean bOverwrite) {
		if (null == sSource)
			return null;
		String sSourceFileName = null, sDestinationFullPath = null;
		InputStream inputStream = null;
		if (Utilities.isUrl(sSource)) {
			try { // try the URL
				HttpGet request = new HttpGet(sSource);
				DefaultHttpClient httpClient = HttpUtils.getThreadSafeClient();
				if (null == httpClient || null == request) {
					// TODO log fatal error
					return null;
				}
				HttpResponse response = httpClient.execute(request);
				Header contentDispositionHeader = response
						.getFirstHeader("Content-Disposition");
				if (null == contentDispositionHeader)
					return null;
				String sContentDisposition = contentDispositionHeader
						.getValue();

				if (null == sContentDisposition)
					return null; // TODO log error here.
				if (sContentDisposition.lastIndexOf("attachment") > -1) {
					String token = "filename=\"";
					int iIndex = sContentDisposition.lastIndexOf(token);
					if (iIndex > -1) {
						sContentDisposition = sContentDisposition.substring(iIndex + token.length());
						sSourceFileName = sContentDisposition.substring(0, sContentDisposition.length() - 1);
					} else
						return null; // malformed Content-Disposition
				} else
					return null;
				inputStream = response.getEntity().getContent();
				if (null == inputStream)
					return null; // TODO log error here. unable to get the input stream.

			} catch (MalformedURLException murle) {
				LogManager.logWTF(murle, "Unable to retrieve screen deck from the server. Malformed URL: " + sSource);
				return null;
			} catch (IOException ioe) {
				LogManager.logWTF("Unable to retrieve system database : " + sSource);
				return null;
			}
		} else { // if sSource is a local file name
			int iLastIndexOfPathSeparator = sSource.lastIndexOf(File.pathSeparator);
			if (iLastIndexOfPathSeparator > -1) {
				sSourceFileName = sSource.substring(iLastIndexOfPathSeparator + 1);
				if (sSourceFileName.equalsIgnoreCase(sSource))
					return sSource; // in case of open db from next screen deck, return the same path.
				try { // try the path.
					inputStream = new FileInputStream(sSource);
				} catch (FileNotFoundException fnfe) {
					LogManager.logWTF(fnfe, "Unable to copy database from source: " + sSource);
					return null;
				}
			} else { // Must be a filename only, try the assets folder
				try {
					sSourceFileName = sSource;
					inputStream = Cache.context.getAssets().open(sSourceFileName);
				} catch (IOException ioe) {
					LogManager.logWTF(ioe, "Unable to fetch screen deck database from file source: " + sSource);
					return null;
				}
			}
		}
		if (null == sSourceFileName)
			return null;
		File file = Cache.context.getDatabasePath(sSourceFileName);
		sDestinationFullPath = file.getAbsolutePath();
		
		// sDestinationFullPath += sSourceFileName;
		if (false == Utilities.copyFile(inputStream, sDestinationFullPath, bOverwrite))
			sDestinationFullPath = null;
		try {
			if (null != inputStream)
				inputStream.close();
		} catch (IOException ioe) {}
		
		if (bOverwrite)
			close(sSourceFileName);
		return sDestinationFullPath != null ? sSourceFileName : null;
	}

	public String copyDefaultDb(boolean bSystemDbOverwrite, boolean bContentDbOverwrite) {
		String sSystemDbName = null, sReturnDbName = null;
		if (null == Cache.context)
			return null;
		sSystemDbName = Cache.bootstrap.getValue("system_dbname");
		if (null == sSystemDbName) {
			LogManager.logWTF("Cannot instantiate your app. Please check res/strings.xml for the entry system_dbname. Also make sure BootStrap.java that it has a statement import com.yourcompanyname.yourprojectname.R");
			return null;
		}
		sReturnDbName = copyDb(sSystemDbName, false, bSystemDbOverwrite); // copy the db if it does not exists else return the full path
		if (false == bSystemDbOverwrite) { 	// open the database and get the version. If the existing version is old, force copy the default system db
			// String sDbNameFullPath = Cache.bootstrap.getValue("internal_database_dir") + sSystemDbName;
			File file = Cache.context.getDatabasePath(sSystemDbName);
			String sDbNameFullPath = file.getAbsolutePath();
// SQLCipher			SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(sDbNameFullPath, "", null);
			SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(sDbNameFullPath, null);
			if (null != db) {
				int iOldVersion = db.getVersion();
				// if the existing version of the system db is less than the version in the string
				PackageInfo pinfo = null;
				try {
					pinfo = Cache.context.getPackageManager()
						.getPackageInfo(Cache.context.getPackageName(), 0);
					int iNewVersion = 0;
					if (pinfo != null)
						iNewVersion = pinfo.versionCode;
					if (iNewVersion > iOldVersion)
						sReturnDbName = copyDb(sSystemDbName, false, true); // force copy the db
				} catch (NameNotFoundException e) {
					LogManager.logWTF(e, "Unable to copy default database. Application package not found.");
				} finally {
					db.close();
				}
			}
		}
		// Now copy the content db if not present. Force overwrite if bOrverwrite is true.
		String sContentDbName = Cache.bootstrap.getValue("content_dbname");
		copyDb(sContentDbName, false, bContentDbOverwrite);
		return sReturnDbName;
	}

	public void reset() {
		for(String sDbName : mDatabases.keySet()) {
			DbHelperInternalStorage db = mDatabases.get(sDbName);
			if (null != db) 
				db.close();
        }
		mDatabases.clear();
	}
	/**
	 * gets the schema for a given database name.
	 * 
	 * @param sDbName
	 *            : name of the database.
	 */
	public HashMap<String, ColumnDefinition[]> getSchema(String sDbName) {
		if (mDatabases.get(sDbName) == null)
			return null;
		return mDatabases.get(sDbName).schema;
	}

	public String[] getColumnNames(String sDbName, String sTableName) {
		if (null == sDbName || sDbName.length() == 0 || null == sTableName
				|| sTableName.length() == 0) {
			LogManager.logWTF("Cannot instantiate your app. Missing System database: " + sDbName + ". Or the database is corrupt and cannot find table: " + sTableName);
			return null;
		}
		HashMap<String, ColumnDefinition[]> schema = getSchema(sDbName);
		if (null == schema) {
			LogManager.logWTF("Cannot instantiate your app. Missing System database: " + sDbName + ". Or the database is corrupt and cannot find table: " + sTableName);
			return null;
		}
		ColumnDefinition[] aColumnDefinitions = schema.get(sTableName);
		if (null == aColumnDefinitions || aColumnDefinitions.length == 0) {
			LogManager.logWTF("Cannot instantiate your app. Missing System database: " + sDbName + ". Or the database is corrupt and cannot find table: " + sTableName);
			return null;
		}
		String[] aColumnNames = new String[aColumnDefinitions.length];
		for (int i = 0; i < aColumnDefinitions.length; i++)
			aColumnNames[i] = aColumnDefinitions[i].name;
		return aColumnNames;
	}

	public Vector<String> getPrimaryKeyColumnNames(String sDbName,
			String sTableName) {
		HashMap<String, ColumnDefinition[]> schema = getSchema(sDbName);
		ColumnDefinition[] aColumnDefinitions = schema.get(sTableName);
		Vector<String> vColumnNames = new Vector<String>();
		for (int i = 0; i < aColumnDefinitions.length; i++)
			if (aColumnDefinitions[i].isPrimaryKey)
				vColumnNames.add(aColumnDefinitions[i].name);
		return vColumnNames;
	}

	public static HashMap<String, ColumnDefinition[]> getSchema(SQLiteDatabase db) {
		String[] columns = { "tbl_name" };
		String selection = "type = \'table\' and tbl_name not like \'\\_%\' escape '\\' and tbl_name not like \'sqlite%\' and tbl_name not like \'android_metadata\'";
		Cursor cursorTableNames = null;
		HashMap<String, ColumnDefinition[]> schema = new HashMap<String, ColumnDefinition[]>();
		try {
			cursorTableNames = db.query("sqlite_master", columns, selection, null, null, null, null);
			cursorTableNames.moveToFirst();

			while (cursorTableNames.isAfterLast() == false) {
				String sQueryString = "pragma table_info(" + cursorTableNames.getString(0) + ")";
				Cursor cursorColumnNames = db.rawQuery(sQueryString, null);
				ColumnDefinition[] aColumnNames = new ColumnDefinition[cursorColumnNames
						.getCount()];
				int i = 0;
				cursorColumnNames.moveToFirst();
				while (cursorColumnNames.isAfterLast() == false) {
					// There are 6 columns returns, cid, name, type, notnull,
					// dflt_value, pk
					// int iColumnCount = cursorColumnNames.getColumnCount();
					// String[] aColumnName =
					// cursorColumnNames.getColumnNames();
					aColumnNames[i] = new ColumnDefinition();
					// String cid = cursorColumnNames.getInt(0);
					aColumnNames[i].cid = cursorColumnNames.getInt(0);
					aColumnNames[i].name = cursorColumnNames.getString(1);
					aColumnNames[i].type = cursorColumnNames.getString(2).toLowerCase();
					int iBeginRoundBracket = -1, iEndRoundBracket = -1;
					if ((iBeginRoundBracket = aColumnNames[i].type.indexOf('(')) > -1
							&& (iEndRoundBracket = aColumnNames[i].type.indexOf(')')) > -1) {
						String sTypeSizePrecision = aColumnNames[i].type;
						aColumnNames[i].type = sTypeSizePrecision.substring(0, iBeginRoundBracket);
						String sSizePrecision = sTypeSizePrecision.substring(iBeginRoundBracket + 1, iEndRoundBracket);
						String[] s = sSizePrecision.split(",");
						try {
							aColumnNames[i].size = Integer.parseInt(s[0].trim());
							if (s.length > 1)
								aColumnNames[i].precision = Integer.parseInt(s[1].trim());
						} catch (NumberFormatException nfe) {
							LogManager.logWTF(nfe, "Error while reading Db schema. Unable to parse the size field." + sQueryString);
						}
					}
					aColumnNames[i].isNull = cursorColumnNames.getInt(3) == 1;
					aColumnNames[i].default_value = cursorColumnNames.getString(4);
					aColumnNames[i++].isPrimaryKey = cursorColumnNames.getInt(5) > 0;

					// String sColumnName = cursorColumnNames.getString(1);
					cursorColumnNames.moveToNext();
				}
				cursorColumnNames.close();
				schema.put(cursorTableNames.getString(0), aColumnNames);
				cursorTableNames.moveToNext();
			}
		} catch (SQLException sqle) {
			LogManager.logWTF(sqle, "Error while reading Db schema.");
		} finally {
			if (cursorTableNames != null)
				cursorTableNames.close();
		}
		return schema;
	}
}
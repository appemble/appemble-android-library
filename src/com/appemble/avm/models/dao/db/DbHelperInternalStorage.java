/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao.db;

import java.util.HashMap;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
//SQLCipher import net.sqlcipher.database.SQLiteDatabase;
//SQLCipher import net.sqlcipher.database.SQLiteOpenHelper;

import com.appemble.avm.Cache;
import com.appemble.avm.LogManager;

public class DbHelperInternalStorage extends SQLiteOpenHelper {
    /**
     * Name of the database file.
     * 
     * <p>
     * Example: "myDatabase.db"
     * </p>
     * 
     */
    public String sDbName = null, sContentDbEncryptionKeys = "";
    public HashMap<String, ColumnDefinition[]> schema = null; // to read complete schema,  
    private SQLiteDatabase _db = null;
    /**
     * Constructor Takes and keeps a reference of the passed context in order to
     * access to the application assets and resources.
     * 
     * @param context
     */
    public DbHelperInternalStorage(Context context, String sDatabaseName, int version, 
    		String sContentDbKeys) {
        super(context, sDatabaseName, null, version);
        // int lastIndexOf = sDatabaseName.lastIndexOf(File.separator);
        // sDbName = sDatabaseName.substring(lastIndexOf+1);
        sDbName = sDatabaseName;
        if (null != sContentDbKeys)
            sContentDbEncryptionKeys = sContentDbKeys;
        // Here fill the schema
		_db = getWritableDatabase();
//SQLCipher        _db = getWritableDatabase(sContentDbEncryptionKeys);
        schema = DbConnectionManager.getSchema(_db);
    }

    public SQLiteDatabase getDb() {
    	if (null == _db)
//SQLCipher            _db = getWritableDatabase(sContentDbEncryptionKeys);
    		_db = getWritableDatabase();
    	return _db;
    	// return this.getWritableDatabase(sDbName);
    }

    public synchronized SQLiteDatabase openDatabase() throws SQLException {
        return getDb();
    }

    @Override
    public synchronized void close() {
        super.close();
        _db = null;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// get the SystemDbName
		String sSystemDbName = Cache.bootstrap.getValue("system_dbname");
		if (sDbName.contains(sSystemDbName)) {
    		Cache.bootstrap.onSystemDbUpgrade(db, oldVersion, newVersion);
    		return;
		}
		String sContentDbName = Cache.bootstrap.getValue("content_dbname");
		HashMap<String, ColumnDefinition[]> systemDbSchema = DbConnectionManager.getInstance().getSchema(sSystemDbName);
		if (null != systemDbSchema && systemDbSchema.containsKey("_upgrade_content_db")) {
			// upgrade the content db. First open the SystemDb
			SQLiteDatabase systemDb = DbConnectionManager.getInstance().getDb(sSystemDbName);
			if (null != systemDb) {
				Cursor cursor = null; String sQuery = null;
				try {
					String[] columns = { "sql" };
					cursor = db.query("_upgrade_content_db", columns, 
						"old_version >= " + oldVersion +  
							" and new_version <= " + newVersion, 
						null, null, null, "old_version, new_version, serialno");
					while (cursor.moveToNext()) {
						sQuery = cursor.getString(0);
						db.execSQL(sQuery);
					}
				} catch (SQLException e) {
					if (null == cursor)
						LogManager.logError(e, "Unable to find table _upgrade_content_db in the system database: " + sSystemDbName);
					else
						LogManager.logError(e, "Unable to execute upgrade content database. Please contact technical support. Query=" + 
							sQuery != null ? sQuery : null);
				} finally {
					if (null != cursor)
						cursor.close();
				}
			} else {
				LogManager.logWTF("Unable to connect to system db");
			}
		}
		// Now call the onUpgrade function in the bootstrap file.
    	if (null != sContentDbName && sDbName.contains(sContentDbName))
    		Cache.bootstrap.onContentDbUpgrade(db, oldVersion, newVersion);
    }
}
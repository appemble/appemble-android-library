/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao.db;

import java.io.File;
import java.util.HashMap;

import com.appemble.avm.Constants;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DbHelperExternalStorage {
    /**
     * Name of the database file.
     * 
     * <p>
     * Example: "db.db"
     * </p>
     * 
     */
    public String sDbName = null;
    public HashMap<String, ColumnDefinition[]> schema = null; 
    // replace ArrayList with HashMap<String (represent columnName), ClassRepresentingTypeSizeIsPrimaryKey>>

    /**
     * Full absolute path of the database.
     * 
     * <p>
     * Example: "/sdcard/myapp/db/db.db"
     * </p>
     */
    public String sDbFullPath = null;

    private SQLiteDatabase db;

    /**
     * Constructor Takes and keeps a reference of the passed context in order to
     * access to the application assets and resources.
     * 
     * @param context
     */
    public DbHelperExternalStorage(Context context, String sDatabaseName, int version) {
        int lastIndexOf = sDatabaseName.lastIndexOf(File.separator);
        if (lastIndexOf >= 0)
        	sDbName = sDatabaseName.substring(lastIndexOf+1);
        else
            throw new Error("Incorrect database path");
        sDbFullPath = sDatabaseName;
        // open the database
        openDatabase(); // opens the db and sets the class variable db
        // here fill the schema.
        schema = DbConnectionManager.getInstance().getSchema(sDatabaseName);
        //openDatabase();
    }

    /**
     * Creates a empty database on the system and rewrites it with your own
     * database.
     * 
    public void createDatabase() throws IOException {        
        if (!checkDatabase()) this.getWritableDatabase();
    }
     */
    /**
     * Check if the database already exist to avoid re-copying the file each
     * time you open the application.
     * 
     * @return true if it exists, false if it doesn't
    private boolean checkDatabase() {
    	if (db != null && db.isOpen())
    		return false;
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(sDbFullPath, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            // database does't exist yet.
            throw new Error("database does't exists yet.");
        }
        if (checkDB != null) {
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }
     */

    public synchronized SQLiteDatabase openDatabase(int mode) throws SQLException {     
    	if (db != null)
    		return db;
    	int i = 0;
    	do {
	        try {
	            db = SQLiteDatabase.openDatabase(sDbFullPath, null, mode);
	            return db;
	        } catch(IllegalStateException e) {
	            // Sometimes, esp. after application upgrade, the database will be non-closed, raising a IllegalStateException
	            // below. Try to avoid by simply opening it again.
	            // TODO replace with log manager Log.d(MyApp.APP, "Database non-closed. Reopening.");
	            try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	        }
    	} while (i++ < Constants.iNumTriesDbOpen);
        return null;
    }

    public synchronized SQLiteDatabase openDatabase() throws SQLException {
        return openDatabase(SQLiteDatabase.OPEN_READWRITE);
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    //@Override
    public synchronized void close() {
        if (db != null)
            db.close();
        //super.close();
    }

    //@Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
 
    }
 
    //@Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
 
    }    
}

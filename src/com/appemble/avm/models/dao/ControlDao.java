/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models.dao;

import java.util.HashMap;
import java.util.Vector;

import android.database.Cursor;
import android.database.SQLException;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.ControlModel;

public class ControlDao {

	public static ControlModel getControl(String sSystemDbName, String sContentDbName, int iControlId) {
//		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, "_id = " + controlID);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and _id = ? ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iControlId)};
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls.get(0);
	}

	public static Vector <ControlModel> getControls(String sSystemDbName, String sContentDbName, String screenName) {
	    int iScreenId = ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);
//		String whereClause = "(screen_id = -1 or screen_id = " + ScreenID + ")"; // include control whose screen_id = -1 (represents control for all screens in the screen_deck)
//		if (null != filter && filter.length() > 0)
//			whereClause += " and " + filter;
//		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, whereClause);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and (screen_id = -1 or screen_id = ?) ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iScreenId)};		
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls;
	}
	
	public static Vector <ControlModel> getControls(String sSystemDbName, String sContentDbName, String sSQL, String[] bindArgs) {
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sSystemDbName);
//		if (null == db) {
//			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
//			return null; //TODO add fatal error here
//		}
		Vector <ControlModel> vControls = null;
//		Cursor cursor = null;
//	    Cursor cursor = db.rawQuery(sSQL, bindArgs);
	    Cursor cursor = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
		try {
//	        cursor = db.query("_control", null, Constants.sDefaultFilter + " and " + filter, null, null, null, "is_visible desc, zorder");
//	        cursor = db.execSQL(sSQL);
	        vControls = new Vector <ControlModel>();
			int iColumnIndex = -1;
	        while (null != cursor && cursor.moveToNext()) {
			 ControlModel newControl = new ControlModel();
				 newControl.id = (cursor.getInt(cursor.getColumnIndex("_id")));
				 newControl.bActionYN = (cursor.getInt(cursor.getColumnIndex("action_yn")) == 1);
				 //newControl.bMarkDeleted = (cursor.getInt(cursor.getColumnIndex("mark_deleted")) == 1);
				 newControl.iScreenId = (cursor.getInt(cursor.getColumnIndex("screen_id")));
				 newControl.iParentId = (cursor.getInt(cursor.getColumnIndex("parent_id")));
				 //newControl.iScreenDeckId = (cursor.getInt(cursor.getColumnIndex("screen_deck_id")));
				 newControl.sName = (cursor.getString(cursor.getColumnIndex("name")));
				 newControl.setType(cursor.getString(cursor.getColumnIndex("type")));
				 newControl.sX = cursor.getString(cursor.getColumnIndex("x"));
				 newControl.sY = cursor.getString(cursor.getColumnIndex("y"));
				 newControl.sWidth = cursor.getString(cursor.getColumnIndex("width"));
				 newControl.sHeight = cursor.getString(cursor.getColumnIndex("height"));
			     if ((iColumnIndex = cursor.getColumnIndex("zorder")) > -1)
			    	 newControl.iZOrder = (cursor.getInt(iColumnIndex));
				 newControl.setDimensionType(cursor.getString(cursor.getColumnIndex("dimension_type")));
				 newControl.bIsVisible = (cursor.getInt(cursor.getColumnIndex("is_visible")) == 1);
				 newControl.bWordWrap = (cursor.getInt(cursor.getColumnIndex("word_wrap")) == 1);
				 newControl.bEllipsized = (cursor.getInt(cursor.getColumnIndex("ellipsized")) == 1);
				 newControl.iAppearanceId = (cursor.getInt(cursor.getColumnIndex("appearance_id")));
				 newControl.setPermission(cursor.getString(cursor.getColumnIndex("permission")));
				 //newControl.setAlignPosition(cursor.getInt(cursor.getColumnIndex("align_position")));
				 //newControl.setAlignId(cursor.getInt(cursor.getColumnIndex("align_id")));
				 //newControl.setSpacingX(cursor.getInt(cursor.getColumnIndex("spacing_x")));
				 //newControl.setSpacingY(cursor.getInt(cursor.getColumnIndex("spacing_y")));
				 newControl.setDataType(cursor.getString(cursor.getColumnIndex("data_type")));
				 newControl.sFormat = (cursor.getString(cursor.getColumnIndex("format_type")));
				 //newControl.setFieldType(cursor.getString(cursor.getColumnIndex("field_type")));
				 //newControl.setFieldRefreshSec(cursor.getString(cursor.getColumnIndex("field_refresh_sec")));
				 newControl.sFieldName = (cursor.getString(cursor.getColumnIndex("field_name")));
				 newControl.iSize = (cursor.getInt(cursor.getColumnIndex("size")));
				 newControl.sDefaultValue = (cursor.getString(cursor.getColumnIndex("default_value")));
				 
				 newControl.sRemoteDataSource = (cursor.getString(cursor.getColumnIndex("remote_data_source")));
				 newControl.setRemoteRequestType(cursor.getString(cursor.getColumnIndex("remote_request_type")));
				 newControl.sLocalDataSource = (cursor.getString(cursor.getColumnIndex("local_data_source")));
			     if ((iColumnIndex = cursor.getColumnIndex("background_image")) > -1)
			    	 newControl.sBackgroundImage = (cursor.getString(iColumnIndex));
			     if ((iColumnIndex = cursor.getColumnIndex("on_resume_update")) > -1)
			    	 newControl.bOnResumeUpdate = (cursor.getInt(cursor.getColumnIndex("on_resume_update")) == 1);
			     if ((iColumnIndex = cursor.getColumnIndex("misc1")) > -1)
			    	 newControl.sMisc1 = (cursor.getString(iColumnIndex));
			     if ((iColumnIndex = cursor.getColumnIndex("misc2")) > -1)
			    	 newControl.sMisc2 = (cursor.getString(iColumnIndex));
			     if ((iColumnIndex = cursor.getColumnIndex("extended_properties")) > -1)
			    	newControl.mExtendedProperties = Utilities.getNameValuePairMap(cursor.getString(
			    			cursor.getColumnIndex("extended_properties")));
			     if (null == newControl.mExtendedProperties)
			    	 newControl.mExtendedProperties = new HashMap<String, String>();
			     newControl.interpretExtendedProperties(); // extract the data source type from extended properties.
				 newControl.sSystemDbName = (sSystemDbName);
				 newControl.sContentDbName = (sContentDbName);
				 vControls.add(newControl);
	        }
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Error in fetching a control from the system database: " + sSQL);
		} finally {
			if (null != cursor)
				cursor.close();
		}		
		return vControls;
	}

	public static Vector <ControlModel> getFieldControlsInScreenAndTitle(String sSystemDbName, String sContentDbName, String screenName) {
//		Vector <ControlModel> vControls=getControls(sSystemDbName, sContentDbName, screenName, "field_name <> 'null' and field_name is not null");
	    int iScreenId = ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and screen_id = ? and (field_name <> 'null' and field_name is not null) ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iScreenId)};		
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls;
	}

	public static Vector <ControlModel> getFieldControlsInGroup(String sSystemDbName, String sContentDbName, String screenName, int iParentId) {
		if (iParentId <= 0)
			return null;
//		Vector <ControlModel> vControls=getControls(sSystemDbName, sContentDbName, "field_name <> 'null' and field_name is not null and parent_id = " + parentId);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and (parent_id = ?) and (field_name <> 'null' and field_name is not null) ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iParentId)};
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls;
	}
	
	public static Vector <ControlModel> getControlsinScreenAndTitle(String sSystemDbName, String sContentDbName, String screenName) {
//		Vector <ControlModel> vControls=getControls(sSystemDbName, sContentDbName, screenName, "parent_id = -1");
	    int iScreenId = ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and (parent_id = -1 or parent_id = -5) and screen_id = ? ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iScreenId)};
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls;
	}

	public static Vector <ControlModel> getAllControlsinScreen(String sSystemDbName, String sContentDbName, String screenName) {
//		Vector <ControlModel> vControls=getControls(sSystemDbName, sContentDbName, screenName, null);
	    int iScreenId = ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and (screen_id = ?) ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iScreenId)};
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls;
	}

	public static Vector <ControlModel> getControlsinScreen(String sSystemDbName, String sContentDbName, String screenName) {
//		Vector <ControlModel> vControls=getControls(sSystemDbName, sContentDbName, screenName, "parent_id = -1 and y >= 0");
	    int iScreenId = ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and (parent_id = -1 and y >= 0 and screen_id = ?) ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iScreenId)};
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls;
	}
	
	public static Vector <ControlModel> getControlsinGroup(String sSystemDbName, String sContentDbName, String screenName, int iParentId) {
//		Vector <ControlModel> vControls=getControls(sSystemDbName, sContentDbName, screenName, "parent_id = " + iParentId);
	    int iScreenId = ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and (parent_id = ? and screen_id = ?) ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iScreenId), Integer.toString(iParentId)};
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls;
	}
	
	public static Vector <ControlModel> getControlsinTitle(String sSystemDbName, String sContentDbName, String screenName) {
//		Vector <ControlModel> vControls=getControls(sContentDbName, screenName, "parent_id = -1 and y < 0");
	    int iScreenId = ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);
	    String sSQL = "SELECT * FROM _control WHERE " + Constants.sDefaultFilter + " and (parent_id = -1 and y < 0 and screen_id = ?) ORDER BY is_visible desc, zorder";
		String[] bindArgs = new String[]{Integer.toString(iScreenId)};
		Vector<ControlModel> vControls = getControls(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vControls;
	}

	public static String[] getColumns(String sSystemDbName, String sContentDbName, String screenName, String filter) {
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sSystemDbName);
//		if (null == db) {
//			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
//			return null; //TODO add fatal error here
//		}
		int iScreenId = ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);
		String sSQL = null;
		String[] columns=null;
		Cursor cursor = null;
		try{
			if(filter==null) {
				filter="1=1";
			}
//		    cursor = db.query("_control", null, filter + " and " + Constants.sDefaultFilter + " and field_name != '' and field_name is not null and field_name != '<null>' and screen_id = " + ScreenID, null, null, null, null);
	        sSQL = "SELECT * FROM _control WHERE " + filter + " and " + Constants.sDefaultFilter + " and screen_id = ? and field_name != '' and field_name is not null and field_name != '<null>'"; 
			String[] bindArgs = new String[]{Integer.toString(iScreenId)};
//		    cursor = db.rawQuery(sSQL, bindArgs);
		    cursor = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
		    if (null == cursor)
		    	return null;
			String column= new String();
			columns=new String[cursor.getCount()+1];
			int i=0;
			columns[i]="_id";
			i++;
			while (cursor.moveToNext()) {
				column=cursor.getString(cursor.getColumnIndex("field_name"));
					columns[i]=column;
					i++;
			}
			cursor.close();
		} catch(Exception e) {
			LogManager.logError(e, "Unable to get columns for for query: " + sSQL);
		} finally {
			if (null != cursor)
				cursor.close();
		}
		return columns;
	}

	public static Boolean localColumnExists(String sSystemDbName, String sContentDbName, String screenName, String filter) {
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sSystemDbName);
//		if (null == db) {
//			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
//			return null; //TODO add fatal error here
//		}
		Cursor cursor=null; Boolean exists = false; String sSQL = null;
		try{
			int ScreenID=ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);	    
		
			if(filter==null) {
				filter="1=1";
			}
	        sSQL = "SELECT * FROM _control where " + filter + " and " + Constants.sDefaultFilter + " and screen_id = ?  and field_type = 'LOCAL' and field_name != '' and field_name is not null and field_name != '<null>'"; 
			String[] bindArgs = new String[]{Integer.toString(ScreenID)};
//		    cursor = db.rawQuery(sSQL, bindArgs);
		    cursor = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
			exists = null != cursor && cursor.getCount()>0;
		} catch(Exception e) {
			LogManager.logError(e, "Error in fetching a control: " + sSQL);
		} finally {
			if (null != cursor)
				cursor.close();
		}
		
		return exists;
	}
	
	public static Boolean serverColumnExists(String sSystemDbName, String sContentDbName, String screenName, String filter) {
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sSystemDbName);
//		if (null == db) {
//			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
//			return null; //TODO add fatal error here
//		}
		Cursor cursor=null; boolean bExists = false; String sSQL = null;
		try{
			int ScreenID=ScreenDao.getScreenID(sSystemDbName, sContentDbName, screenName);	    
	
			if(filter==null) {
				filter="1=1";
			}
//		    cursor = db.query("_control", null, filter + " and " + Constants.sDefaultFilter + " and field_name!='' and field_name is not null and field_name!='<null>' and screen_id="+ScreenID +" and field_type='SERVER'", null, null, null, null);
	        sSQL = "SELECT * FROM _control where " + filter + " and " + Constants.sDefaultFilter + " and screen_id = ?  and field_type='SERVER' and field_name != '' and field_name is not null and field_name != '<null>'"; 
			String[] bindArgs = new String[]{Integer.toString(ScreenID)};
//		    cursor = db.rawQuery(sSQL, bindArgs);
		    cursor = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
			bExists = null != cursor && cursor.getCount()>0;
		} catch(Exception e) {
			LogManager.logError(e, "Error while fetching a control: " + sSQL);
		} finally {
			if (null != cursor)
				cursor.close();
		}
		
		return bExists;
	}

	public static String[] getColumnsInScreen(String sSystemDbName, String sContentDbName, String screenName) {
		String[] columns=getColumns(sSystemDbName, sContentDbName, screenName,"y >= 0");
		return columns;
	}
	
	public static boolean localColumnExistsInScreen(String sSystemDbName, String sContentDbName, String screenName) {
		Boolean exists=localColumnExists(sSystemDbName, sContentDbName, screenName,"y >= 0");
		return exists;
	}

	public static boolean serverColumnExistsInScreen(String sSystemDbName, String sContentDbName, String screenName) {
		Boolean exists=serverColumnExists(sSystemDbName, sContentDbName, screenName,"y >= 0");
		return exists;
	}
	
	public static String[] getColumnsInTitle(String sSystemDbName, String sContentDbName, String screenName) {
		String[] columns=getColumns(sSystemDbName, sContentDbName, screenName,"y < 0");
		return columns;
	}
	
	public static boolean localColumnExistsInTitle(String sSystemDbName, String sContentDbName, String screenName) {
		Boolean exists=localColumnExists(sSystemDbName, sContentDbName, screenName,"y < 0");
		return exists;
	}

	public static boolean serverColumnExistsInTitle(String sSystemDbName, String sContentDbName, String screenName) {
		Boolean exists=serverColumnExists(sSystemDbName, sContentDbName, screenName,"y < 0");
		return exists;
	}
/*	
	public static String[] getColumnsInGroup(String sSystemDbName, String sContentDbName, String screenName, int controlId) {
		String[] columns=getColumns(sSystemDbName, sContentDbName, screenName,"parent_id = " + controlId);
		return columns;
	}

	public static boolean localColumnExistsInGroup(String sSystemDbName, String sContentDbName, String screenName, int controlId) {
		Boolean exists=localColumnExists(sSystemDbName, sContentDbName, screenName,"parent_id="+controlId);
		return exists;
	}

	public static boolean serverColumnExistsInGroup(String sSystemDbName, String sContentDbName, String screenName, int controlId) {
		Boolean exists=serverColumnExists(sSystemDbName, sContentDbName, screenName,"parent_id = " + controlId);
		return exists;
	}
*/
	public static int getScreenID(String sSystemDbName, int controlId) {
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sSystemDbName);
		// TODO Auto-generated method stub
		int screenID=1; Cursor cursor = null; String sSQL = null;
		try {
	        sSQL = "SELECT screen_id FROM _control where " + " _id = ?"; 
			String[] bindArgs = new String[]{Integer.toString(controlId)};
//		    cursor = db.rawQuery(sSQL, bindArgs);	        
		    cursor = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        while (null != cursor && cursor.moveToNext()) {
	        	screenID=cursor.getInt(cursor.getColumnIndex("screen_id"));
	        }
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Error in fetching a control: " + sSQL);
		} finally {
			if (null != cursor)
				cursor.close();			
		}
		return screenID;

	}

	public static int getControlID(String sSystemDbName, String controlName, int screenID) {
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sSystemDbName);
//		if (null == db) {
//			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
//			return 0; //TODO add fatal error here
//		}
		int controlID=0;
        Cursor cursor = null;
        String sSQL = null;
        try {
//        	cursor = db.query("_control", null, Constants.sDefaultFilter + " and screen_id = " + screenID + " and name='" + controlName + "'", null, null, null, null);
	        sSQL = "SELECT _id FROM _control where " + " screen_id = ? and name = ?"; 
			String[] bindArgs = new String[]{Integer.toString(screenID), controlName};
//		    cursor = db.rawQuery(sSQL, bindArgs);	        
		    cursor = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        while (null != cursor && cursor.moveToNext()) {
	        	controlID=cursor.getInt(cursor.getColumnIndex("_id"));
	        }
        } catch (SQLException sqle) {
        	LogManager.logError(sqle, "Error in fetching a control: " + sSQL);
        } finally {
        	if (null != cursor)
        		cursor.close();        	
        }
		return controlID;
	}

	public static int getParentControlID(String sSystemDbName, int control_id) {
//		SQLiteDatabase db = DbConnectionManager.getInstance().getDb(sSystemDbName);
//		if (null == db) {
//			LogManager.logWTF(Constants.DB_CONNECTION_ERROR);
//			return 0; //TODO add fatal error here
//		}
		int parentID=0; Cursor cursor = null; String sSQL = null;
		try {
//	        cursor = db.query("_control", null, Constants.sDefaultFilter + " and _id = " + control_id, null, null, null, null);
	        sSQL = "SELECT parent_id FROM _control where " + " _id = ?"; 
			String[] bindArgs = new String[]{Integer.toString(control_id)};
//		    cursor = db.rawQuery(sSQL, bindArgs);	        
		    cursor = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        while (null != cursor && cursor.moveToNext()) {
	        	parentID=cursor.getInt(cursor.getColumnIndex("parent_id"));
	        }
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Error while fetching a control: " + sSQL);
		} finally {
			if (null != cursor)
		        cursor.close();
		}
		return parentID;
	}
}

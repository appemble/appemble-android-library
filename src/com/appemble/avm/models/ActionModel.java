/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models;

import java.util.HashMap;
import java.util.Vector;

import android.os.Bundle;

import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.dao.ActionDao;

public class ActionModel {
	public int iId;
	public int iScreenDeckId;
	public int iScreenId;
	public int iControlId;
	public int[] iEventList;
	
	private String sActionName;
	public String sTarget;
	public Vector<String> vInputParameters;
	public Vector<String> vTargetParameters;
	public int iPrecedingActionId;
	public boolean bMarkDeleted;
	public String sMisc1;
	public String sMisc2;
	public HashMap <String, String> mExtendedProperties;
	
	public String sSystemDbName;
	public String sContentDbName;
 	public ConfigManager.ActionMetaData metaData;
 	public Object mScreenObject;
	
	protected ActionModel() {
		mExtendedProperties = new HashMap<String, String>();
	}
	
	public ActionModel(String sSystemDbName, String sContentDbName) {
		mExtendedProperties = new HashMap<String, String>();
		this.sSystemDbName = sSystemDbName;
		this.sContentDbName = sContentDbName;
	}
	
	public ActionModel(String sSysDbName, String sConDbName, String actionName, String target, Vector<String> sInputParamList, 
			Vector<String> sTargetParamList) {
		sSystemDbName = sSysDbName;
		sContentDbName = sConDbName;
		sActionName = actionName;
		metaData = ConfigManager.getInstance().getActionMetaData(actionName);
		sTarget = target;
		vInputParameters = sInputParamList;
		vTargetParameters = sTargetParamList;
	}
	
	public String getActionName() {
		return sActionName;
	}
	
	public void setActionName(String actionName) {
		sActionName = actionName;
		metaData = ConfigManager.getInstance().getActionMetaData(actionName);
	}
	
	public void setEventList(String eventList) {
		iEventList = null;
		if (null == eventList)
			return;
		String[] sEventList = eventList.split(",");
		iEventList = new int[sEventList.length];
		for (int i = 0; i < sEventList.length && sEventList[i] != null; i++) {
			iEventList[i] = sEventList[i].trim().hashCode();
		}
	}
	
	public static String getEventString(int iEventType) {
		// Here find the Event String from iEventId.
		return ConfigManager.getInstance().getEventString(iEventType);
	}

	public ActionModel[] getNextActions() {
		return ActionDao.getNextActions(sSystemDbName, sContentDbName, iId);
	}


	// This function returns newTargetParameterValueList containing those target parameters that were in the 
	// existingTargetParameterValueList.
	// It modifies the inputParameterList and removes those parameters which are found in existingTargetParameterValueList
	public Bundle transformTargetParameterList(Bundle existingTargetParameterValueList) {
		Bundle targetParameterValueList = new Bundle();
		if (null == existingTargetParameterValueList || null == vTargetParameters || null == vInputParameters || 
				vInputParameters.size() == 0)
			return targetParameterValueList;
		int i = 0;
		Vector<String> vUnresolvedInputParameters = new Vector<String>();
		Vector<String> vUnresolvedTargetParameters = new Vector<String>();
		for(String sInputParameter: vInputParameters) {
			if (false == existingTargetParameterValueList.containsKey(sInputParameter)) {
				vUnresolvedInputParameters.add(sInputParameter);
				vUnresolvedTargetParameters.add(vTargetParameters.get(i));
				i++;
				continue;
			}
			Object value = existingTargetParameterValueList.get(sInputParameter);
			String sTargetParameter = vTargetParameters.get(i);
			Bundle dataTypes = existingTargetParameterValueList.getBundle(Constants.STR_DATA_TYPES);
			Utilities.putData(targetParameterValueList, 
				dataTypes != null ? dataTypes.getInt(sInputParameter, Constants.VARCHAR) : Constants.VARCHAR, 
				sTargetParameter, value);
			i++;
		}
		// modify the inputParameterList to contain only those parameters whose values were not found 
		// in the existingTargetParameterValueList
		vInputParameters = vUnresolvedInputParameters;
		vTargetParameters = vUnresolvedTargetParameters;
		return targetParameterValueList;
	}

	public String getClassName() {
		if (null == metaData)
			return null;
		return metaData.sClass;
	}
	
	public String getExtendedProperty(String sKey) {
		if (null == mExtendedProperties || null == sKey)
			return null;
		return mExtendedProperties.get(sKey);
	}
	
	static public ActionModel[] getActions(String systemDbName, String contentDbName, int iEventType){
		ActionModel[] a = ActionDao.getActions(systemDbName, contentDbName, iEventType);
		return a;
	}
}
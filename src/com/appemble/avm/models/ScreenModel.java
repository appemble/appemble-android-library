/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.models;

import java.util.HashMap;
import java.util.Vector;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.dynamicapp.DynamicActivity;
import com.appemble.avm.dynamicapp.DynamicDefaultTab;
import com.appemble.avm.models.dao.ActionDao;
import com.appemble.avm.models.dao.ScreenDao;
import com.appemble.avm.models.dao.ScreenDeckDao;

public class ScreenModel {
	public int id;
	public String sName;
	public String sTitle;
	public int iScreenType;
	public String sIcon;
	public int iMenuOrder;
	public String sMenuName;
	public boolean bAllowReorientation;
	public int iControlHeightBasedUpon;
	public int iAllowedLayouts;
	public int iDimensionType;
	public float fWidth;
	public float fWidthInPixels;
	public float fHeight;
	public float fHeightInPixels;
	public int iScrolling;
	public String sRemoteDataSource;
	public String sLocalDataSource;
	public int iRemoteRequestType;
	public String sBackground;
	public String sTitleBackground;
	public String sInitialFocus;
	public boolean bActionYN;

	public String sDeparameterizedRemoteDataSource;
	public String sDeparameterizedLocalDataSource;
	public boolean bMarkDeleted;
	public boolean bOnResumeUpdate;
	public String sMisc1;
	public String sMisc2;
	public String sTabGroupName;
	public HashMap <String, String> mExtendedProperties;

	public String sSystemDbName;
	public String sContentDbName;
 	public ConfigManager.ActivityMetaData metaData;
 	public boolean bRemoteDataSaveLocally;
 	public float fTabBarHeight = 0;
 	public int iDefaultTitleBar = -1;
 	
 	public ScreenModel() {
		mExtendedProperties = new HashMap<String, String>();
	}
 	
	public void setType(String sScreenType) {
		// iScreenType = Constants.SCREEN;
		if (sScreenType == null || sScreenType.length() == 0) {
			metaData = ConfigManager.getInstance().getActivityMetaData("SCREEN");
			iScreenType = metaData.id;
			return;
		}
		metaData = ConfigManager.getInstance().getActivityMetaData(sScreenType);
		if (null == metaData) {
			LogManager.logWTF("Cannot instantiate the screen: " + sName + ". The defintion is missing in the configuration file for control type = " + sScreenType);
			return;
		}
		iScreenType = metaData.id;
	}

	public void setControlHeightBasedUpon(String controlHeightBasedUpon) {
		iControlHeightBasedUpon = Configuration.ORIENTATION_UNDEFINED;
		if (null == controlHeightBasedUpon || controlHeightBasedUpon.length() == 0)
			return;
		if (controlHeightBasedUpon.equalsIgnoreCase("PORTRAIT"))
			iControlHeightBasedUpon = Configuration.ORIENTATION_PORTRAIT;
		else if (controlHeightBasedUpon.equalsIgnoreCase("LANDSCAPE"))
			iControlHeightBasedUpon = Configuration.ORIENTATION_LANDSCAPE;
	}

	public void setAllowReorientation(String layoutType) {
		bAllowReorientation = false;
		if (null == layoutType || layoutType.length() == 0)
			return;
		bAllowReorientation = Utilities.parseBoolean(layoutType);
	}

	public void setAllowedLayouts(String layoutType) {
		iAllowedLayouts = Configuration.ORIENTATION_UNDEFINED;
		if (null == layoutType || layoutType.length() == 0)
			return;
		if (layoutType.equalsIgnoreCase("LANDSCAPE"))
			iAllowedLayouts = Configuration.ORIENTATION_LANDSCAPE;
		else if (layoutType.equalsIgnoreCase("PORTRAIT"))
			iAllowedLayouts = Configuration.ORIENTATION_PORTRAIT;
	}

	public void setDimensionType(String dimensionType) {
		if (null == dimensionType || dimensionType.length() == 0)
			return;
		if (dimensionType.equalsIgnoreCase("PERCENTAGE"))
			iDimensionType = Constants.PERCENTAGE;
		else if (dimensionType.equalsIgnoreCase("PIXELS"))
			iDimensionType = Constants.PIXELS;
	}

	public void setScrolling(String sScrolling) {
		iScrolling = 0;
		if (null == sScrolling || sScrolling.length() == 0)
			return;
		if (sScrolling.equalsIgnoreCase("HORIZONTAL"))
			iScrolling = Constants.HSCROLL;
		else if (sScrolling.equalsIgnoreCase("VERTICAL"))
			iScrolling = Constants.VSCROLL;
		else if (sScrolling.equalsIgnoreCase("SCROLL_BOTH_WAYS"))
			iScrolling = Constants.SCROLL_BOTH_WAYS;
	}

	public void setRemoteRequestType(String sRequestType) {
		if (null == sRequestType || sRequestType.length() == 0)
			return;
		if (sRequestType.equalsIgnoreCase("POS"))
			iRemoteRequestType = Constants.POST;
		else
			iRemoteRequestType = Constants.GET;
	}
	
	public String getClassName() {
		if (null == metaData) {
			LogManager.logWTF("Cannot instantiate the screen: " + sName + ". The defintion is missing in the configuration file for control type = " + iScreenType);
			return null;
		}
		return metaData.sClass;
	}
	
	public ActionModel[] getActions(int iEventType) {
		return getActions(iEventType, true);
	}
	
	public ActionModel[] getActions(int iEventType, boolean bScreenOnly){
		String sEventType = ActionModel.getEventString(iEventType);
		ConfigManager.EventMetaData event = ConfigManager.getInstance().getEventMetaData(iEventType);
		if (null == event) {
			LogManager.logWTF("The config file(s) does not contain an event:" + iEventType);
			return null;
		}
		ActionModel[] a = null;
		if (null == event.sActionName)
			a = ActionDao.getActionsForScreen(sSystemDbName, sContentDbName, id, sEventType, bScreenOnly);
		else {
			ActionModel action = new ActionModel(sSystemDbName, sContentDbName);
			action.setActionName(event.sActionName);
			action.sTarget = event.sTarget;
			action.vInputParameters = event.vInputParameter;
			action.vTargetParameters = event.vTargetParameter;
			if (null != action.vInputParameters && null == action.vTargetParameters)
				action.vTargetParameters = new Vector<String>();
			a = new ActionModel[1];
			a[0] = action;
		}
		return a;
	}

	public String getExtendedProperty(String sKey) {
		if (null == mExtendedProperties || null == sKey)
			return null;
		return mExtendedProperties.get(sKey);
	}
	
   public static Intent getIntent(String sSystemDbName, String sContentDbName, String sScreenName, 
    		Bundle tplv) {
    	ScreenModel screenModel = getScreenModel(sSystemDbName, sContentDbName, sScreenName);
    	Class <?> cls = getClass(screenModel, false);
    	return getIntent(screenModel, tplv, cls);
    }

    public static ScreenModel getScreenModel(String sSystemDbName, String sContentDbName, String sScreenName) {
    	if (null == sSystemDbName) {
			LogManager.logError("Cannot instantiate the screen. The system database is not found: " + sSystemDbName);
    		return null;
    	}
    	if (null == sScreenName) { // fetch from database
	    	ScreenDeckModel screenDeckModel = ScreenDeckDao.getScreenDeck(sSystemDbName);
	    	if (null == screenDeckModel || null == screenDeckModel.sStartingScreenName)
	    		return null;
	    	sScreenName = screenDeckModel.sStartingScreenName;
    	}
		ScreenModel screenModel = ScreenDao.getScreenByName(sSystemDbName, sContentDbName, sScreenName);
		if (null == screenModel) {
			LogManager.logError("Cannot instantiate the screen: " + sScreenName + ". Please check the screen name.");
			return null;
		}
    	return screenModel;
    }
    
    public static Class<?> getClass(ScreenModel screenModel, boolean bForTabScreen) {
    	if (null == screenModel)
    		return null;
		Class<?> cls = null;
		String sClassName = screenModel.getClassName();
		if (bForTabScreen == false && screenModel.sTabGroupName != null && screenModel.sTabGroupName.length() > 0)
			cls = DynamicDefaultTab.class; // this has to be hard coded
		else if (null == sClassName)
			cls = DynamicActivity.class; // default is always DynamicActivity
		else {
			try {
				cls = Class.forName(sClassName);
			} catch (ClassNotFoundException e) {
	    		LogManager.logError("Cannot initiate the screen: " + screenModel.sName + ". Implementation class not found: " + sClassName);
			}
		}
		return cls;
    }
    
    public static Intent getIntent(ScreenModel screenModel, Bundle targetParameterValueList, Class<?> cls) {
    	if (null == screenModel || null == cls)
    		return null;
    	if (null == Cache.context) {
    		LogManager.logError("Cannot initiate the screen: " + screenModel.sName + ". Application context not available.");
    		return null;
    	}
    	Intent intent = new Intent(Cache.context, cls);
    	if (null != targetParameterValueList)
    		intent.putExtras(targetParameterValueList);
		intent.putExtra(Constants.STR_SCREEN_NAME, screenModel.sName);
		intent.putExtra(Constants.STR_SYSTEM_DATABASE_NAME, screenModel.sSystemDbName);
		intent.putExtra(Constants.STR_CONTENT_DATABASE_NAME, screenModel.sContentDbName);
		return intent;
    }	
}
/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Looper;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ScreenDeckModel;
import com.appemble.avm.models.dao.ScreenDeckDao;
import com.loopj.android.http.PersistentCookieStore;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

public class Cache {
	public static String TAG = "Cache";
	private static final String LAST_APP_VERSION = "last_app_version";
	
	public static Context context = null;
	public static String TokenId="";
	public static final String APPEMBLE = "AVM";
	public static CookieSyncManager cookieSyncManager;
	public static PersistentCookieStore cookieStore;
	public static Bootstrap bootstrap;
	public static boolean isApplicationInBackground = false;
	public static float fScaleDPToPixels = 0.0f;
	public static String mUserAgent = null;
	public static int iNumScreensToClose = 0;
	public static String sScreenToClose = null;
	public static boolean bScreenCloseUpto = false;
	public static int iStatusBarHeight = 0;
	public static String sCompatibleAVMLibraryVersion = null,
			sAVMLibraryVersion = null;
	public static boolean bDimenRelativeToParent = true;
	public static int iDefaultTitleBar;
	public static SharedPreferences prefs = null;
	
	public enum AppStart {
	    FIRST_TIME, FIRST_TIME_VERSION, NORMAL;
	}	
	
	private final static int ON_FIRST_TIME_APP_LAUNCHED = "ON_FIRST_TIME_APP_LAUNCHED".hashCode();
	private final static int ON_FIRST_TIME_VERSION_APP_LAUNCHED = "ON_FIRST_TIME_VERSION_APP_LAUNCHED".hashCode();
	private final static int ON_APP_LAUNCHED = "ON_APP_LAUNCHED".hashCode();
	
	public Cache(Context context, boolean bFromService){
		initiate(context, bFromService);
		return;
	}	
	
	public static boolean initiate(Context ctx, boolean bFromActivity){
		if(context != null)
			return true; // already initiated. No need to initiate again.
		if (ctx == null)
			return false;
		context = ctx;
		// initialize encrypted shared preference
		prefs = new ObscuredSharedPreferences( 
			    context, context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE) );
		cookieStore = new PersistentCookieStore(ctx);
		Resources res = ctx.getResources();
		String sClassName = res.getString(R.string.bootstrap);
		try {
			bootstrap = (Bootstrap) Class.forName(sClassName).getDeclaredConstructor().newInstance();
			if (false == bootstrap.execute()) {
				context = null;
				return false;
			}
		} catch (ClassNotFoundException cnfe) {
			LogManager.logError(cnfe, "Unable to initiate application. Check appemble library's path.");
			context = null;
			return false;
		} catch (IllegalArgumentException e) {
			LogManager.logError(e, "Unable to initiate application. Check appemble library's path.");
			context = null;
			return false;
		} catch (SecurityException e) {
			LogManager.logError(e, "Unable to initiate application. Check appemble library's path.");
			context = null;
			return false;
		} catch (InstantiationException e) {
			LogManager.logError(e, "Unable to initiate application. Check appemble library's path.");
			context = null;
			return false;
		} catch (IllegalAccessException e) {
			LogManager.logError(e, "Unable to initiate application. Check appemble library's path.");
			context = null;
			return false;
		} catch (InvocationTargetException e) {
			LogManager.logError(e, "Unable to initiate application. Check appemble library's path.");
			context = null;
			return false;
		} catch (NoSuchMethodException e) {
			LogManager.logError(e, "Unable to initiate application. Check appemble library's path.");
			context = null;
			return false;
		} catch (Exception e) {
			LogManager.logError(e, "Unable to initiate application. Check appemble library's path.");
			context = null;
			return false;
		}
		
		try {
			sCompatibleAVMLibraryVersion = bootstrap.getValue("compatible_avm_library_version");
			sAVMLibraryVersion = ctx.getResources().getString(R.string.avm_version);
			if (null != sCompatibleAVMLibraryVersion && 
				Version.compare(sAVMLibraryVersion, sCompatibleAVMLibraryVersion) < 0) {// if library version is > App's AVM compatible version
				LogManager.logError("AVM library is not compatible with your app. Please update compatible_avm_library_version in your app's strings.xml.");
				context = null;
				return false;
			}
		} catch (IllegalArgumentException iae) {
			LogManager.logWTF(iae, "Unable to initiate application.");
		}
		Constants.getInstance(); // initialize Constants class only after bootstrap has been initialized.
		// google_maps_api_key = MLCache.bootstrap.getValue("google_maps_api_key");			
	    Cache.cookieSyncManager = CookieSyncManager.createInstance(ctx);
	    // Now set the default db filter for the screen_size
	    DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
		fScaleDPToPixels = metrics.density;

		float fHeightInches = ((float)metrics.heightPixels)/metrics.ydpi;
		float fWidthInches = ((float)metrics.widthPixels)/metrics.xdpi;
	    float iMaxSize = fHeightInches > fWidthInches ? fHeightInches : fWidthInches; 
	    if (iMaxSize > 10.0)
	    	Constants.sDefaultFilter += " and ((screen_size & 4) > 0) ";
	    else if (iMaxSize > 7.0)
	    	Constants.sDefaultFilter += " and ((screen_size & 2) > 0) ";
	    else
	    	Constants.sDefaultFilter += " and ((screen_size & 1) > 0) ";
	    ImageManager.getInstance(); // This is to initialize the image manager before creating any activity.
	    initializeImageLoader();
		iStatusBarHeight = getStatusBarHeight(ctx);

		// store the user setting of default title bar from screen deck.
		ScreenDeckModel screenDeckModel = ScreenDeckDao.getScreenDeck(bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME));
		if (null == screenDeckModel) {
			LogManager.logWTF("Unable to find screendeck in the database. Please clean and recompile the project");
			context = null;
			return false;
		}
			
		iDefaultTitleBar = screenDeckModel.iDefaultTitleBar;
		
		String sDimensionsRelativeTo = screenDeckModel.mExtendedProperties.get("dimensions_relative_to");
		if (null != sDimensionsRelativeTo && sDimensionsRelativeTo.equalsIgnoreCase("screen"))
			bDimenRelativeToParent = false; 
		LogManager.logDebug("Cache Initiated");
		
		// Raise event ON_FIRST_TIME_APP_LAUNCHED
		final String sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);
		final String sSystemDbName = Cache.bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME);
		Bundle targetParameterValueList = new Bundle();
		if (bFromActivity) { // This can be called from a service as well. In that case, fromActivity will be false...
			AppStart appStart = checkAppStart();
			if (appStart == AppStart.FIRST_TIME) {
	        	ActionModel[] actions = ActionModel.getActions(sSystemDbName, sContentDbName,
	    	    		ON_FIRST_TIME_APP_LAUNCHED);
	    	    for (int i = 0; null != actions && i < actions.length; i++)
	    	    	Action.callAction(ctx, null, null, actions[i], targetParameterValueList);
			} else if (appStart == AppStart.FIRST_TIME_VERSION) {
	        	ActionModel[] actions = ActionModel.getActions(sSystemDbName, sContentDbName,
	    	    		ON_FIRST_TIME_VERSION_APP_LAUNCHED);
	    	    for (int i = 0; null != actions && i < actions.length; i++)
	    	    	Action.callAction(ctx, null, null, actions[i], targetParameterValueList);
			} else {
	        	ActionModel[] actions = ActionModel.getActions(sSystemDbName, sContentDbName,
	    	    		ON_APP_LAUNCHED);
	    	    for (int i = 0; null != actions && i < actions.length; i++)
	    	    	Action.callAction(ctx, null, null, actions[i], targetParameterValueList);
			}
		}
		return true;
	}

	public static void Alert(String title, String message){
		AlertDialog.Builder alt_bld = new AlertDialog.Builder(context);
		alt_bld.setTitle(title);
		alt_bld.setMessage(message);
		alt_bld.setCancelable(false);
		alt_bld.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int id) {
			//  Action for 'NO' Button
			dialog.cancel();
		}
		});
		
		AlertDialog alert = alt_bld.create();
	
		// Icon for AlertDialog
		alert.setIcon(android.R.drawable.alert_light_frame);
		alert.show();
	}

    public static String getUserAgent() {
    	if (null != mUserAgent)
    		return mUserAgent;
        try {
            Constructor<WebSettings> constructor = WebSettings.class.getDeclaredConstructor(Context.class, WebView.class);
            constructor.setAccessible(true);
            try {
                WebSettings settings = constructor.newInstance(context, null);
                return settings.getUserAgentString();
            } finally {
                constructor.setAccessible(false);
            }
        } catch (Exception e) {
            String ua;
            if(Thread.currentThread().getName().equalsIgnoreCase("main")){
                WebView m_webview = new WebView(context);
                ua = m_webview.getSettings().getUserAgentString();
            }else{
                Thread thread = new Thread(){
                    public void run(){
                        Looper.prepare();
                        WebView m_webview = new WebView(context);
                        Cache.mUserAgent = Cache.bootstrap.getValue("user_agent") + ";";
                        Cache.mUserAgent += m_webview.getSettings().getUserAgentString();
                        Looper.loop();
                    }
                };
                thread.start();
                return Cache.mUserAgent;
            }
            return ua;
        }
    }

	//helper method for clearCache() , recursive
    //returns number of deleted files
    static int clearCacheFolder(final File dir, final int numDays) {
	    int deletedFiles = 0;
	    if (dir!= null && dir.isDirectory()) {
	        try {
	            for (File child:dir.listFiles()) { //first delete subdirectories recursively		                  
	            	if (child.isDirectory()) {
	            		deletedFiles += clearCacheFolder(child, numDays);
	            	}
	
	            	//then delete the files and subdirectories in this dir
	            	//only empty directories can be deleted, so subdirs have been done first
	            	if (child.lastModified() < new Date().getTime() - numDays * DateUtils.DAY_IN_MILLIS) {
	            		if (child.delete()) {
	            			deletedFiles++;
	            		}
	            	}
	            }
	        }
	        catch(Exception e) {
	        	LogManager.logError(e, String.format("Failed to clean the cache, error %s", e.getMessage()));
	          	}
	      	}
	      	return deletedFiles;
	    }
	
	  /*
	   * Delete the files older than numDays days from the application cache
	   * 0 means all files.
	   */
	@SuppressLint("DefaultLocale")
	public static void clearCache(final Context context, final int numDays) {
		LogManager.logInfo(String.format("Starting cache prune, deleting files older than %d days", numDays));
		int numDeletedFiles = clearCacheFolder(context.getCacheDir(), numDays);
		LogManager.logInfo(String.format("Cache pruning completed, %d files deleted", numDeletedFiles));
	}
	
//	public static boolean isFirstTimeAppLaunch(Context context) {
//		return PreferenceManager.getDefaultSharedPreferences(
//				context.getApplicationContext()).getBoolean(context.getPackageName()+
//						ActionModel.getEventString(ON_FIRST_TIME_APP_LAUNCHED), true);		
//    }

//	public static void setAppLaunchedFlag(Context context) {
//		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
//        SharedPreferences.Editor edit = prefs.edit();
//        edit.putBoolean(context.getPackageName()+
//        		ActionModel.getEventString(ON_FIRST_TIME_APP_LAUNCHED), true);
//        edit.commit();
//    }
	
	public static int getStatusBarHeight(Context context) {
	      int result = 0;
	      int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
	      if (resourceId > 0)
	          result = context.getResources().getDimensionPixelSize(resourceId);
	      return result;
	}
	
	public static int getKindleSoftKeyBarHeight() {
		if (android.os.Build.MODEL.equalsIgnoreCase("KFAPWA") || android.os.Build.MODEL.equalsIgnoreCase("KFAPWI"))
			return 122;
		else if (android.os.Build.MODEL.equalsIgnoreCase("KFTHWA") || android.os.Build.MODEL.equalsIgnoreCase("KFTHWI"))
			return 117;
		else if (android.os.Build.MODEL.equalsIgnoreCase("KFSOWI"))
			return 78;
		else if (android.os.Build.MODEL.equalsIgnoreCase("KFJWA") || android.os.Build.MODEL.equalsIgnoreCase("KFJWI"))
			return 90;
		else if (android.os.Build.MODEL.equalsIgnoreCase("KFTT"))
			return 78;
		else if (android.os.Build.MODEL.equalsIgnoreCase("KFOT"))
			return 60;
		else if (android.os.Build.MODEL.equalsIgnoreCase("Kindle Fire"))
			return 60;
		else
	      return 0;
	}

	private static void initializeImageLoader() {
		Display display = ((WindowManager) Cache.context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int iMaxSize = display.getHeight() > display.getWidth() ? display.getHeight() : display.getWidth();
				
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(Cache.context)
        	.memoryCacheExtraOptions(iMaxSize, iMaxSize) // default = device screen dimensions
	        .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
	        .memoryCacheSize(2 * 1024 * 1024)

	        .diskCacheExtraOptions(iMaxSize, iMaxSize, null)
	        .diskCacheSize(50 * 1024 * 1024) // 50M
	        .diskCacheFileCount(100)
	        
	        .threadPoolSize(15) // default is 3
	        .build();
        ImageLoader.getInstance().init(config);
	}
	
	/**
	 * Finds out started for the first time (ever or in the current version).
	 * 
	 * @return the type of app start
	 */
	public static AppStart checkAppStart() {
	    AppStart appStart = null;
        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            long lastVersionCode = getPreferenceLong(LAST_APP_VERSION);
            // String versionName = pInfo.versionName;
            long currentVersionCode = pInfo.versionCode;
            appStart = checkAppStart(currentVersionCode, lastVersionCode);
            // Update version in preferences
            setPreference(LAST_APP_VERSION, currentVersionCode);
        } catch (NameNotFoundException e) {
            LogManager.logInfo(
                    "Unable to determine current app version from pacakge manager. Defenisvely assuming normal app start.");
        }
	    return appStart;
	}

	public static AppStart checkAppStart(long currentVersionCode, long lastVersionCode) {
	    if (lastVersionCode <= 0) {
	        return AppStart.FIRST_TIME;
	    } else if (lastVersionCode < currentVersionCode) {
	        return AppStart.FIRST_TIME_VERSION;
	    } else if (lastVersionCode > currentVersionCode) {
	        LogManager.logInfo("Current version code (" + currentVersionCode
	                + ") is less then the one recognized on last startup ("
	                + lastVersionCode
	                + "). Defenisvely assuming normal app start.");
	        return AppStart.NORMAL;
	    } else {
	        return AppStart.NORMAL;
	    }
	}	

	public static void setPreference(String key, String value) {
		prefs.edit().putString(key,value).commit();
	}
	
	public static void setPreference(String key, boolean value) {
		prefs.edit().putBoolean(key,value).commit();
	}

	public static void setPreference(String key, long value) {
		prefs.edit().putLong(key,value).commit();
	}

	public static String getPreferenceString(String key) {
		return prefs.getString(key, null);
	}
	
	public static boolean getPreferenceBoolean(String key) {
		return prefs.getBoolean(key, false);
	}

	public static long getPreferenceLong(String key) {
		return prefs.getLong(key, 0L);
	}
}

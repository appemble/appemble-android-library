package com.appemble.avm;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;

import com.appemble.avm.Cache;
import com.appemble.avm.LogManager;

public class SecureHttpClient extends DefaultHttpClient {
	private int securePort;
	InputStream isClientTrustStore, isClient;

	public SecureHttpClient(final int port, InputStream isClientTrustStore, InputStream isClient) {
	    this.securePort = port;
	    this.isClientTrustStore = isClientTrustStore;
	    this.isClient = isClient;
	}
	  
	private SSLSocketFactory createSSLSocketFactory() {
//		  Log.d(TAG, "Creating SSL socket factory");

		final KeyStore truststore = this.loadStore(isClientTrustStore, Cache.bootstrap.getSValue("CLIENT_TRUST_STORE_PWD"), "BKS");
		final KeyStore keystore = this.loadStore(isClient, Cache.bootstrap.getSValue("KEY_STORE_PWD"), "BKS");

		return this.createFactory(keystore, Cache.bootstrap.getSValue("KEY_STORE_PWD"), truststore);
	}

	private KeyStore loadStore(InputStream isBKSCertificate, String sPassword, String sStoreType) {
		KeyStore keystore = null;
	  	try {
			keystore = KeyStore.getInstance(sStoreType);
	        keystore.load(isBKSCertificate, sPassword.toCharArray());
	    } catch (KeyStoreException kse) {
	        LogManager.logError(kse, kse.getMessage());	
	    } catch (CertificateException ce) {
	        LogManager.logError(ce, ce.getMessage());		        	
        } catch (NoSuchAlgorithmException nsae) {
	        LogManager.logError(nsae, nsae.getMessage());	
        } catch (IOException ioe) {
	        LogManager.logError(ioe, ioe.getMessage());	
        } finally {
	            //inputStream.close();
        }
	  		return keystore;
  	}
	  	
	private SSLSocketFactory createFactory(final KeyStore keystore, final String keystorePassword, 
		final KeyStore truststore) {

		SSLSocketFactory factory = null;
		try {
		    factory = new SSLSocketFactory(keystore, Cache.bootstrap.getSValue("CLIENT_TRUST_STORE_PWD"), truststore);
		    factory.setHostnameVerifier((X509HostnameVerifier) SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		} catch (Exception e) {
//		    Log.e(TAG, "Caught exception when trying to create ssl socket factory. Reason: " +
//		        e.getMessage());
			throw new RuntimeException(e);
		}

		return factory;
	}
		
	@Override
	protected ClientConnectionManager createClientConnectionManager() {
//		  Log.d(TAG, "Creating client connection manager");

		final SchemeRegistry registry = new SchemeRegistry();

//		  Log.d(TAG, "Adding https scheme for port " + securePort);
		registry.register(new Scheme("https", this.createSSLSocketFactory(), this.securePort));

		return new SingleClientConnManager(getParams(), registry);
	}
}

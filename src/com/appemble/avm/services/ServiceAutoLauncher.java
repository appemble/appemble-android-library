/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ServiceAutoLauncher extends BroadcastReceiver {
	@Override
	  public void onReceive(Context context, Intent intent) {
		//if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
		//}
		Intent serviceIntent = new Intent(context,AlarmService.class);
		serviceIntent.setAction(intent.getAction());
		if ("android.intent.action.TIMEZONE_CHANGED".equalsIgnoreCase(intent.getAction()))
			serviceIntent.putExtra("time-zone", intent.getStringExtra("time-zone"));
	    context.startService(serviceIntent);
	  }
}

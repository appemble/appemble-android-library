/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.avm.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;

import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ScreenDao;
import com.appemble.avm.models.dao.TableDao;

public class AlarmService extends Service {
	// _alarm_table column indices
	private final int ID = 0;
	private final int SYSTEM_DBNAME = 6;
	private final int CONTENT_DBNAME = 7;
	final int DATA = 8;
	static public final String STR_FORMAT = "format";
	static public final String STR_DATE = ("date");		
	static public final String STR_REPEAT = ("repeat");
	static public final String STR_DATA = ("data");
	static public final String STR_INTERVAL = ("interval");
	static public final String STR_HOURLY = ("hourly");
	static public final String STR_DAILY = ("daily");
	static public final String STR_WEEKLY = ("weekly");
	static public final String STR_MONTHLY = ("monthly");
	static public final String STR_YEARLY = ("yearly");
	static public final int ON_PLAY_ALARM = "ON_PLAY_ALARM".hashCode();
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	// check the cause of the onStartCommand. 
	// If reboot, set all alarms again
		// go thru the _alarm_manager table and set all alarms
	// if time change, time zone change, adjust the alarms
		// go thru the _alarm_manager table and set all alarms
	// if the alarm manager is calling the service, then react to the intent playAlarm()
	// if the framework is calling the service, modify the alarms (set or remove)
	// To check if the alarm is set, refer to http://stackoverflow.com/questions/4713592/how-to-check-if-alarm-is-set
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		super.onStartCommand(intent, flags, startId);
		String sAction = intent.getAction();
//		Candidate for deletion. 20130426		
//		if (null == Cache.context)
//			Cache.initiate(this.getApplicationContext(), false); // false means not being called from activity.
////			Constants.getInstance(getBaseContext()); // This will initialize the constants.
////		}
		if (null == Cache.context && false == Cache.initiate(this.getApplicationContext(), true)) {
			stopSelf(); // stop the service...
			return startId;
		}
		if (null == sAction) {
			LogManager.logDebug("Cannot execute service as there is no action");
			return startId;
		}
		else if (sAction.equalsIgnoreCase(ConfigManager.getInstance().getActionClass("PLAY_ALARM")))
			playAlarm(intent);
		else if (sAction.equalsIgnoreCase(ConfigManager.getInstance().getActionClass("SET_ALARM")))
			setAlarm(intent);
		else if (sAction.equalsIgnoreCase(ConfigManager.getInstance().getActionClass("CANCEL_ALARM")))
			cancelAlarm(intent);
		else if (sAction.equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED))
			bootCompleted(intent);
		else if (sAction.equalsIgnoreCase(Intent.ACTION_TIME_CHANGED) ||
				sAction.equalsIgnoreCase(Intent.ACTION_TIMEZONE_CHANGED))
			timeChanged(intent);
		
		stopSelf(); // stop the service...
		return startId;
	}

	@SuppressWarnings("unchecked")
	private int setAlarm(Intent intent) {
		byte[] data = intent.getByteArrayExtra("data");
		HashMap<String, String> parametersMap = null;
		if (null == data)
			return 0; // TODO Cannot proceed at all.
		parametersMap = (HashMap<String, String>) Utilities.deserializeObject(data);
		long[] lTimes = null;
		String sInterval = parametersMap.get(STR_INTERVAL);
		String sRepeat = parametersMap.get(STR_REPEAT);
		String sDateFormat = parametersMap.get(STR_FORMAT);
		String sDate = parametersMap.get(STR_DATE);		
		if (null == sRepeat)
			sRepeat = "none";
		lTimes = getTimes(sDate, sDateFormat, sRepeat, sInterval);
		if (null == lTimes)
			return 0; // a Non-Repeating Alarm may be in the past.
		// now set the alarm
		intent.setAction(ConfigManager.getInstance().getActionClass("PLAY_ALARM"));
		int id = getAlarmId(intent);
		if (0 == id) {
			LogManager.logWTF("Alarm Id should not be 0. Date: " + sDate + ". DateFormat: " + 
				sDateFormat + ". Repeat: " + sRepeat + ". Interval: " + sInterval);
			return 0;
		}
		PendingIntent pendingIntent = PendingIntent.getService(Cache.context, id, intent, 0);	
		AlarmManager alarmManager = (AlarmManager)Cache.context.getSystemService(Context.ALARM_SERVICE);
		if (sRepeat.equalsIgnoreCase("none") || (sInterval != null && sInterval.length() > 0))
			alarmManager.set(AlarmManager.RTC_WAKEUP, lTimes[0], pendingIntent);
		else
			alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, lTimes[0], lTimes[1], pendingIntent);
		LogManager.logDebug("Alarm set successfully. Id: " + id);
		return 1;
	}

	private int cancelAlarm(Intent intent) {
		intent.setAction(ConfigManager.getInstance().getActionClass("PLAY_ALARM"));
		int id = getAlarmId(intent);
	    if (PendingIntent.getBroadcast(this, id, intent, PendingIntent.FLAG_NO_CREATE) != null) {
			LogManager.logDebug("Unable to cancel alarm. The Alarm with Id: " + intent.getData() + " was not set.");
			return 0;
	    }
	    AlarmManager alarmManager = (AlarmManager)Cache.context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent pendingIntent = PendingIntent.getService(Cache.context, id, intent, 0);	
	    alarmManager.cancel(pendingIntent);
	    pendingIntent.cancel();
	    return 1;
	}

	// check if the alarm id is valid
	// start the activity in the appemble's library to start to the alarm
	@SuppressWarnings("unchecked")
	private int playAlarm(Intent intent) {
		if (null == intent || Cache.context == null)
			return 0;
		int id = getAlarmId(intent);
		byte[] data = intent.getByteArrayExtra("data");
		HashMap<String, String> parametersMap = null;
		if (null != data)
			parametersMap = (HashMap<String, String>) Utilities.deserializeObject(data);
		if (null == parametersMap)
			return 0; // TODO parametersMap should always be there.

		String sSystemDbName = parametersMap.get(Constants.STR_SYSTEM_DATABASE_NAME);
		String sContentDbName = parametersMap.get(Constants.STR_CONTENT_DATABASE_NAME);
		if (null == sContentDbName || id == 0)
			return 0;
		// Check if this alarm exists in the database.		
		if (null != sContentDbName && id > 0 && 
			0 == TableDao.getRowCount(sContentDbName, 
				"_alarm_manager", "_id = ? and content_dbname = ? and (is_completed is null or is_completed = 0)", 
				new String[] { String.valueOf(id), sContentDbName } )) {
			LogManager.logError("Unable to find Alarm in the content database: " + sContentDbName);
			return 0; // alarm does not exists..
		}
		// if it is a non-repeating alarm, mark it to complete
		String sRepeat = parametersMap.get(STR_REPEAT);
		if (null == sRepeat || "none".endsWith(sRepeat)) {
			String sDateFormat = parametersMap.get(STR_FORMAT);
			String sDate = parametersMap.get(STR_DATE);
			TableDao.updateColumn(sContentDbName, "_alarm_manager", "is_completed", "1", "_id = " + id);
			// if the alarm is in the past, do not play it
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(sDateFormat, Cache.context.getResources().getConfiguration().locale);
				Date time = sdf.parse(sDate);
				if (new Date().getTime() - time.getTime() > 300000) // if time now - alarm time > 5 min do not play the alarm.
					return 0; // alarm cannot be in the past.
			} catch (ParseException pe) {
				LogManager.logWTF(pe, "Unable to parse date: " + sDate + " to format: " + sDateFormat);
				return 0;
			}
		}
		String sAlarmType = parametersMap.get("type");
		if (sAlarmType == null)
			;
		else if (sAlarmType.equalsIgnoreCase("audio"))
			return playAudioAlarm(intent, parametersMap, data);
		else if (sAlarmType.equalsIgnoreCase("video"))
			return playAudioAlarm(intent, parametersMap, data);
		else if (sAlarmType.equalsIgnoreCase("email"))
			return playAudioAlarm(intent, parametersMap, data);
		else if (sAlarmType.equalsIgnoreCase("action")) {
			// Get all ON_PLAY_ALARM from the database. Execute those who sTarget = action_name. There can be multiple alarm set at the same time.
			ActionModel[] actions = ActionModel.getActions(sSystemDbName, 
			    sContentDbName, ON_PLAY_ALARM);
			Bundle parametersBundle = null;
	    	String sActionName = parametersMap.get("action_name"); // mandatory field when type = action
	    	String sTarget = parametersMap.get("target");			// mandatory field when type = action
	    	if (null == parametersBundle)
	    		parametersBundle = Utilities.convertHashMapToBundle(parametersMap);
		    for (int i = 0; null != sActionName && null != sTarget && null != actions && i < actions.length; i++) {
		    	if (sTarget.equals(actions[i].sTarget) && sActionName.equals(actions[i].getActionName()))
		    		Action.callAction(this, null, null, actions[i], parametersBundle);
		    }
		}
		return 1;
	}
	
	private int bootCompleted(Intent intent) {
		resetAlarms(intent, -1);
		return 0;
	}

	private int timeChanged(Intent intent) {
		// resetAllAlarms(intent, -1);
		return 0;
	}

	private int resetAlarms(Intent intent, int iAlarmId) {
		if (null == intent)
			return 0;
		if (null == Cache.context && false == Cache.initiate(this.getApplicationContext(), true))
			return 0;
		String sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);
		if (null == sContentDbName)
			return 0;
		
		String sQuery = "select _id, parent_id, type, date, format, repeat, system_dbname, content_dbname, data " +
			"from _alarm_manager where is_completed is null or is_completed = 0";
		if (iAlarmId > 0)
			sQuery += " and _id = " + iAlarmId;
		Cursor cursor = null;
		try {
			cursor = TableDao.createCursor(sContentDbName, sQuery, null);
			if (null == cursor) {
				LogManager.logError("Unable to create cursor for query: " + sQuery);
				return 0; // error in creating cursor or no rows returned.
			}
			if (cursor.getCount() == 0) {
				LogManager.logInfo("No alarms to set in the Alarm Manager");
				return 0;
			}
			int i = 0;
			int iRowCount = cursor.getCount();
			cursor.moveToFirst();
			while (i++ < iRowCount) {
				Intent newAlarmIntent = new Intent(Cache.context, AlarmService.class);
				iAlarmId = cursor.getInt(ID);
				if (iAlarmId <= 0)
					continue;
				sContentDbName = cursor.getString(CONTENT_DBNAME);
				String sSystemDbName = cursor.getString(SYSTEM_DBNAME);
				if (null == sContentDbName)
					continue;
				newAlarmIntent.setData(Uri.parse("custom://" + sContentDbName.hashCode() + "/" + iAlarmId));
				String sAction = intent.getAction();
				if (sAction.equalsIgnoreCase(Intent.ACTION_TIME_CHANGED) ||
					sAction.equalsIgnoreCase(Intent.ACTION_TIMEZONE_CHANGED)) {
					newAlarmIntent.setAction(ConfigManager.getInstance().getActionClass("PLAY_ALARM"));
					LogManager.logDebug("Cancelling alarm for id = " + iAlarmId);
					cancelAlarm(newAlarmIntent);
				}
				newAlarmIntent.setAction(ConfigManager.getInstance().getActionClass("SET_ALARM"));
				byte[] data = cursor.getBlob(DATA);
				if (null == data) {
					LogManager.logError("No Data. Not setting alarm with id = "  + iAlarmId);
					continue;
				}
				newAlarmIntent.putExtra("data", data);
				// do not need this. This is part of action. intent.putExtra("_id", lAlarmId);
				newAlarmIntent.putExtra(Constants.STR_CONTENT_DATABASE_NAME, sContentDbName);
				newAlarmIntent.putExtra(Constants.STR_SYSTEM_DATABASE_NAME, sSystemDbName);
				LogManager.logInfo("Setting alarm for id = " + iAlarmId);
				setAlarm(newAlarmIntent);
				cursor.moveToNext();
			}
		} catch (SQLException sqle) {
			LogManager.logWTF(sqle, "Unable to reset alarms using query: " + sQuery);
		} finally {
			if (null != cursor)
				cursor.close();
		}

		return 1; 
	}
	
    long getSeconds(int hours, int minutes, int seconds) {
    	Calendar dateCal = Calendar.getInstance();
    	// make it tomorrow
    	dateCal.add(Calendar.DAY_OF_YEAR, 1);
    	// Now set it to the time you want
    	dateCal.set(Calendar.HOUR_OF_DAY, hours);
    	dateCal.set(Calendar.MINUTE, minutes);
    	dateCal.set(Calendar.SECOND, seconds);
    	dateCal.set(Calendar.MILLISECOND, 0);
    	return dateCal.getTimeInMillis() - System.currentTimeMillis();
    }
    
    int getAlarmId(Intent intent) {
		Uri uri = intent.getData();
		if (null == uri || null == uri.getPath())
			return 0;
		return getAlarmId(uri.getPath().substring(1));
    }

    int getAlarmId(String sId) {
    	if (null == sId || sId.equalsIgnoreCase("all"))
    		return 0;
    	int id = 0;
		try {
			id = Integer.parseInt(sId);
		} catch (NumberFormatException nfe) {
			LogManager.logWTF(nfe, "Unable to parse alarmId to an integer: " + sId);
		}
		return id;
    }

    private long[] getTimes(String sDate, String sDateFormat, String sRepeat, String sInterval) {
    	long[] lTimes = new long[2]; 
		Calendar cFirst = Calendar.getInstance();
		// validate the date based on the format.
		Date time = null;
    	if (null != sDate && null != sDateFormat) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(sDateFormat, Cache.context.getResources().getConfiguration().locale);
				time = sdf.parse(sDate);
			} catch (ParseException pe) {
				LogManager.logWTF(pe, "Unable to parse date: " + sDate + " to format: " + sDateFormat);
				return null;
			}
    	}
    	// if it is interval, get the lTimes[0] and return
    	// interval and repeat cannot go together.
		if (sInterval != null && sInterval.length() > 0) {
//			if (null != time)
//				cFirst.setTime(time);				
			checkAndSetInterval(cFirst, sInterval);
			lTimes[0] = cFirst.getTimeInMillis();
			return lTimes;
		} 

		if (sRepeat.equalsIgnoreCase("none")) {
			if (time.getTime() - new Date().getTime() < 0) { // the time is in the past.
				LogManager.logError("An alarm set for the past that is non-repeating cannot be set");
				return null; // alarm cannot be in the past.
			}
			if (null != time)
				cFirst.setTime(time);
			lTimes[0] = cFirst.getTimeInMillis();
		} else {
			if (null != time) {
				// Now set it to the time you want
				cFirst.set(Calendar.HOUR_OF_DAY, time.getHours());
				cFirst.set(Calendar.MINUTE, time.getMinutes());
				cFirst.set(Calendar.SECOND, time.getSeconds());
				cFirst.set(Calendar.MILLISECOND, 0);
			}
			Calendar cPresent = Calendar.getInstance();			
			Calendar cRepeat = null;
			if (sRepeat.equalsIgnoreCase(STR_HOURLY)) {
				while (cPresent.getTimeInMillis() > cFirst.getTimeInMillis())
					cFirst.add(Calendar.HOUR, 1);
			} else if (sRepeat.equalsIgnoreCase(STR_DAILY)) {
				if(cPresent.getTimeInMillis() > cFirst.getTimeInMillis())
					cFirst.add(Calendar.DAY_OF_YEAR, 1);
			}
			else if (sRepeat.equalsIgnoreCase(STR_WEEKLY)) {
				Calendar cTimeToSetCalenDar = Calendar.getInstance();
				if (null != time)
					cTimeToSetCalenDar.setTime(time);
				if(cPresent.getTimeInMillis() > cFirst.getTimeInMillis()) {
					int weekDay = cTimeToSetCalenDar.get(Calendar.DAY_OF_WEEK);
					do {
						cFirst.add(Calendar.DAY_OF_WEEK, 1);
					} while(cFirst.get(Calendar.DAY_OF_WEEK) != weekDay);
				} else {
					int weekDay = cTimeToSetCalenDar.get(Calendar.DAY_OF_WEEK);
					while(cFirst.get(Calendar.DAY_OF_WEEK) != weekDay) {
						cFirst.add(Calendar.DAY_OF_WEEK, 1);
					}
				}				
			}
			else if (sRepeat.equalsIgnoreCase(STR_MONTHLY)) {
				Calendar cTimeToSetCalenDar =  Calendar.getInstance();
				if (null != time)
					cTimeToSetCalenDar.setTime(time);
				if (cPresent.getTimeInMillis() > cFirst.getTimeInMillis()) {
					int dayOfMonth = cTimeToSetCalenDar.get(Calendar.DAY_OF_MONTH);
					do {
						cFirst.add(Calendar.DAY_OF_MONTH, 1);
					} while(cFirst.get(Calendar.DAY_OF_MONTH) != dayOfMonth);
				} else {
					int dayOfMonth = cTimeToSetCalenDar.get(Calendar.DAY_OF_MONTH);
					while(cFirst.get(Calendar.DAY_OF_MONTH) != dayOfMonth) {
						cFirst.add(Calendar.DAY_OF_MONTH, 1);
					}
				}
			}
			else if (sRepeat.equalsIgnoreCase(STR_YEARLY)) {
				Calendar cTimeToSetCalenDar =  Calendar.getInstance();
				if (null != time)
					cTimeToSetCalenDar.setTime(time);
				if(cPresent.getTimeInMillis() > cFirst.getTimeInMillis()) {
					int dayOfMonth = cTimeToSetCalenDar.get(Calendar.DAY_OF_MONTH);
					int monthOfYear = cTimeToSetCalenDar.get(Calendar.MONTH);
					do {
						cFirst.add(Calendar.DAY_OF_MONTH, 1);
					} while(cFirst.get(Calendar.DAY_OF_MONTH) != dayOfMonth);
					while (cFirst.get(Calendar.MONTH) != monthOfYear) 
						cFirst.add(Calendar.MONTH, 1);
				} else {
					int dayOfMonth = cTimeToSetCalenDar.get(Calendar.DAY_OF_MONTH);
					int monthOfYear = cTimeToSetCalenDar.get(Calendar.MONTH);
					while(cFirst.get(Calendar.DAY_OF_MONTH) != dayOfMonth) {
						cFirst.add(Calendar.DAY_OF_MONTH, 1);
					}
					while (cFirst.get(Calendar.MONTH) != monthOfYear) 
						cFirst.add(Calendar.MONTH, 1);
				}
			}
			lTimes[0] = cFirst.getTimeInMillis();

			// Now calculate the repeat interval
			cRepeat = (Calendar)cFirst.clone();
			if (sRepeat.equalsIgnoreCase(STR_HOURLY))
				cRepeat.add(Calendar.HOUR, 1);
			else if (sRepeat.equalsIgnoreCase(STR_DAILY))
				cRepeat.add(Calendar.DAY_OF_YEAR, 1);
			else if (sRepeat.equalsIgnoreCase(STR_WEEKLY))
				cRepeat.add(Calendar.DAY_OF_YEAR, 7);
			else if (sRepeat.equalsIgnoreCase(STR_MONTHLY))
				cRepeat.add(Calendar.MONTH, 1);
			else if (sRepeat.equalsIgnoreCase(STR_YEARLY))
				cRepeat.add(Calendar.YEAR, 1);
			lTimes[1] = cRepeat.getTimeInMillis() - cFirst.getTimeInMillis();
			if (lTimes[1] < 0) // protective coding... just in case 
				lTimes[1] = 0;
		}
		return lTimes;
    }
    
    private int playAudioAlarm(Intent intent, HashMap<String, String> parametersMap, byte[] data) {		
		String sDisplayActivityClassName = parametersMap.get("display_activity_class");
		String sScreenName = parametersMap.get(Constants.STR_SCREEN_NAME);
		Class<?> className = null;
		// Display the alert box.
		try {
			if (null == sDisplayActivityClassName && null != sScreenName) {
				String sSystemDbName = parametersMap.get(Constants.STR_SYSTEM_DATABASE_NAME);
				String sContentDbName = parametersMap.get(Constants.STR_CONTENT_DATABASE_NAME);
				ScreenModel screenModel = ScreenDao.getScreenByName(sSystemDbName, sContentDbName, sScreenName);
				if (null != screenModel) {
					className = ScreenModel.getClass(screenModel, screenModel.sTabGroupName != null);
				}
			} else if (null == sDisplayActivityClassName || sDisplayActivityClassName.length() == 0 ||
				sDisplayActivityClassName.equalsIgnoreCase("alert") || 
				sDisplayActivityClassName.equalsIgnoreCase("dialog"))
				className = Class.forName("com.appemble.avm.dynamicapp.AlarmActivity");
			else if (null != sDisplayActivityClassName)
				className = Class.forName(sDisplayActivityClassName);
				
		} catch (ClassNotFoundException cnfe) {
			LogManager.logWTF(cnfe, "Unable to find class: " + sDisplayActivityClassName);
			return 0;
		}
		if (null == className) {
			LogManager.logError("Unable to find activity for displaying alarm.");
			return 0;
		}
			
		intent.setClass(Cache.context, className);
		intent.putExtra(Constants.STR_SCREEN_NAME, parametersMap.get(Constants.STR_SCREEN_NAME));
		Object o = Utilities.deserializeObject(data);
		intent.putExtra(Constants.STR_TARGET_PARAMETER_VALUE_LIST, data);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		int iAlarmId = getAlarmId(intent);
		LogManager.logDebug("About to play alarm for id = " + iAlarmId);
		startActivity(intent);
    	return 1;
    }

    public static String checkAndSetInterval(Calendar calendar, String sInterval) {
    	if (null == calendar || null == sInterval)
    		return null;
        // -- Match target against pattern.
        Matcher matcher = Utilities.DIMENSION_PATTERN.matcher(sInterval);
        if (null != matcher && matcher.matches()) {
            // -- Match found.
            // -- Extract value.
//        	String sSign = matcher.group(1);
            float value = Float.valueOf(matcher.group(2)).floatValue();
            // -- Extract dimension units.
            String unit = matcher.group(4);
            if (null != unit)
            	unit = unit.toLowerCase(Cache.context.getResources().getConfiguration().locale);
            if (null == unit || unit.equals("s") || unit.contains("sec"))
            	calendar.add(Calendar.SECOND, (int)value);
            else if (unit.equals("m") || unit.contains("min"))
            	calendar.add(Calendar.MINUTE, (int)value);
            else if (unit.equals("h") || unit.contains("hr") || unit.contains("hour"))
            	calendar.add(Calendar.HOUR_OF_DAY, (int)value);
            else if (unit.equals("d") || unit.contains("day"))
            	calendar.add(Calendar.DATE, (int)value);
            else if (unit.equals("w") || unit.contains("week"))
            	calendar.add(Calendar.WEEK_OF_YEAR, (int)value);
            else if (unit.equals("mon") || unit.contains("month"))
            	calendar.add(Calendar.MONTH, (int)value);
            else if (unit.equals("y") || unit.contains("yr") || unit.contains("year"))
            	calendar.add(Calendar.YEAR, (int)value);
            else
            	return "Interval must contain one of following s, sec, m, min, d, day, days, w, week, weeks, mon, month, months, y, year, years";
        }
        return null;
	}
}